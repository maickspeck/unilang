<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function index()
	{
//		$this->rodar($this->domQuixote());
//		$this->rodar($this->infinitaHighway());
//		$this->rodar($this->vidaReal());
//		$this->rodar($this->oPreco());
//		$this->rodar($this->RefraoBolero());
//		$this->rodar($this->euQueNaoAmoVoce());
//		$this->rodar($this->ateOFim());
//		$this->rodar($this->tresx4());
//		$this->rodar($this->naVeia());
//		$this->rodar($this->terraDeGigantes());
//		$this->rodar($this->depoisDeNos());
		$this->rodar($this->outrasFrequencias());

//		$this->load->template('login');
	}

	public function rodar($music)
	{
		foreach ($music as $linha => $frase)
		{
			echo '-------------------<br>';
			echo '<br>'.$frase.'<br>';
			echo '<br>=><br>';
			$frase = str_replace('!', ' !', $frase);
			echo '<br>'.$this->Words->traduzir($frase).'<br>';
		}
	}

	public function outrasFrequencias()
	{
		$music[] = 'Seria mais fácil fazer como todo mundo faz';
		$music[] = 'O caminho mais curto';
		$music[] = 'Produto que rende mais';
		$music[] = 'Seria mais fácil fazer como todo mundo faz';
		$music[] = 'Um tiro certeiro';
		$music[] = 'Modelo que vende mais';
		return $music;
	}

	public function depoisDeNos()
	{
		$music[] = 'Hoje os ventos do destino começaram a soprar';
		$music[] = 'Nosso tempo de menino foi ficando para trás';
		$music[] = 'Com a força de um moinho que trabalha devagar';
		$music[] = 'Vai buscar o teu caminho';
		$music[] = 'nunca olha para trás';

		$music[] = 'Hoje o tempo voa nas asas de um avião';
		$music[] = 'Sobrevoa os campos da destruição';
		$music[] = 'É um mensageiro das almas dos que virão ao mundo';
		$music[] = 'Depois de nós';

		$music[] = 'Hoje o céu está pesado';
		$music[] = 'Vem chegando temporal';
		$music[] = 'Nuvens negras do passado';
		$music[] = 'Delirante flor do mal';
		$music[] = 'Cometemos o pecado de não saber perdoar';
		$music[] = 'Sempre olhando para o mesmo lado';
		$music[] = 'Feito estátuas de sal';

		$music[] = 'Hoje o tempo escorre dos dedos da nossa mão';
		$music[] = 'Ele não devolve o tempo perdido em vão';
		$music[] = 'É um mensageiro das almas dos que virão ao mundo';
		$music[] = 'Depois de nós';

		$music[] = 'Meninos na beira da estrada';
		$music[] = 'Escrevem mensagens com lápis de luz';
		$music[] = 'Serão mensageiros divinos';
		$music[] = 'Com suas espadas douradas, azuis';
		$music[] = 'Na terra, no alto dos montes';
		$music[] = 'Florestas do Norte, cidades do Sul';
		$music[] = 'Meninos avistam ao longe';
		$music[] = 'A estrela do menino Jesus';

		return $music;
	}

	public function terraDeGigantes()
	{
		$music[] = 'Hey mãe!';
		$music[] = 'Eu tenho uma guitarra elétrica';
		$music[] = 'Durante muito tempo isso foi tudo que eu queria ter';

		$music[] = 'Mas, hey mãe';
		$music[] = 'Alguma coisa ficou pra trás';
		$music[] = 'Antigamente eu sabia exatamente o que fazer';

		$music[] = 'Hey mãe!';
		$music[] = 'Tem uns amigos tocando comigo';
		$music[] = 'Eles são legais, além do mais, não querem nem saber';
		$music[] = 'Mas agora, lá fora todo mundo é uma ilha';
		$music[] = 'Há milhas, e milhas, e milhas de qualquer lugar';

		$music[] = 'Nessa terra de gigantes';
		$music[] = 'Eu sei, já ouvimos tudo isso antes';
		$music[] = 'A juventude é uma banda numa propaganda de refrigerantes';

		$music[] = 'As revistas, as revoltas, as conquistas da juventude';
		$music[] = 'São heranças, são motivos pras mudanças de atitude';
		$music[] = 'Os discos, as danças, os riscos da juventude';
		$music[] = 'A cara limpa, a roupa suja, esperando que o tempo mude';

		$music[] = 'Nessa terra de gigantes';
		$music[] = 'Tudo isso já foi dito antes';
		$music[] = 'A juventude é uma banda numa propaganda de refrigerantes';

		$music[] = 'Hey mãe!';
		$music[] = 'Já não esquento a cabeça';
		$music[] = 'Durante muito tempo isso foi só o que eu podia fazer';
		$music[] = 'Mas, hey hey mãe por mais que a gente cresça';
		$music[] = 'Há sempre alguma coisa que a gente não pode entender';

		$music[] = 'Por isso, mãe';
		$music[] = 'Só me acorda quando o sol tiver se posto';
		$music[] = 'Eu não quero ver meu rosto antes de anoitecer';
		$music[] = 'Pois agora lá fora, O mundo todo é uma ilha';
		$music[] = 'Há milhas, e milhas, e milhas';

		return $music;
	}

	public function naVeia()
	{
		$music[] = 'Se você perguntar por mim';
		$music[] = 'Vão dizer que eu ando muito estranho';
		$music[] = 'Vão dizer que eu ando por aí';
		$music[] = 'Quando você perguntar por mim';

		$music[] = 'Se você perguntar por mim';
		$music[] = 'Vão dizer as coisas mais estranhas';
		$music[] = 'Nenhuma resposta vai satisfazer';
		$music[] = 'Quando você perguntar por mim';

		$music[] = 'Vem!';
		$music[] = 'Ver com os próprios olhos';
		$music[] = 'Vem!';
		$music[] = 'Ver a vida como ela é';
		$music[] = 'Vem!';
		$music[] = 'Ver com os próprios olhos';
		$music[] = 'Vem!';
		$music[] = 'Ver a vida como ela é';

		$music[] = 'Se você está mesmo afim de saber por onde eu ando';
		$music[] = 'De saber por quê eu ando assim';
		$music[] = 'É melhor nem perguntar por mim';

		$music[] = 'Sem filtro, na veia';

		return $music;
	}

	public function tresx4()
	{

		$music[] = 'Diga a verdade ao menos uma vez na vida';
		$music[] = 'Você se apaixonou pelos meus erros';
		$music[] = 'Não fique pela metade';
		$music[] = 'Vá em frente, minha amiga';
		$music[] = 'Destrua a razão desse beco sem saída';

		$music[] = 'Diga a verdade ao menos uma vez na vida';
		$music[] = 'Você se apaixonou pelos meus erros';
		$music[] = 'E eu perdi as chaves';
		$music[] = 'Mas que cabeça a minha';
		$music[] = 'Agora vai ter que ser para toda a vida';

		$music[] = 'Somos o que há de melhor';
		$music[] = 'Somos o que dá pra fazer';
		$music[] = 'O que não dá pra evitar';
		$music[] = 'E não se pode escolher';

		$music[] = 'Se eu tivesse a força que você pensa que eu tenho';
		$music[] = 'Eu gravaria no metal da minha pele o teu desenho';

		$music[] = 'Feitos um pro outro';
		$music[] = 'Feitos pra durar';
		$music[] = 'Uma luz que não produz sombra';

		$music[] = 'Somos o que há de melhor';
		$music[] = 'Somos o que dá pra fazer';
		$music[] = 'O que não dá pra evitar';
		$music[] = 'E não se pode esconder';

		return $music;
	}

	public function ateOFim()
	{
		$music[] = 'Não vim até aqui pra desistir agora';
		$music[] = 'Entendo você Se você quiser ir embora';
		$music[] = 'Não vai ser a primeira vez nas últimas 24 horas';
		$music[] = 'Mas eu não vim até aqui pra desistir agora';

		$music[] = 'Minhas raízes estão no ar';
		$music[] = 'Minha casa é qualquer lugar';
		$music[] = 'Se depender de mim eu vou até o fim';
		$music[] = 'Voando sem instrumentos';
		$music[] = 'Ao sabor do vento';
		$music[] = 'Se depender de mim eu vou até o fim';

		$music[] = 'A ilha não se curva noite adentro';
		$music[] = 'Vida afora';
		$music[] = 'Toda a vida o dia inteiro';
		$music[] = 'Não seria exagero';
		$music[] = 'Se depender de mim';
		$music[] = 'Eu vou até o fim';

		$music[] = 'Cada célula todo fio de cabelo';
		$music[] = 'Falando assim parece exagero';
		$music[] = 'Mas se depender de mim';
		$music[] = 'Eu vou até o fim';

		return $music;
	}

	public function euQueNaoAmoVoce()
	{
		$music[] = 'Eu que não fumo queria um cigarro';
		$music[] = 'Eu que não amo você';
		$music[] = 'Envelheci dez anos ou mais nesse último mês';
		$music[] = 'Senti saudade, vontade de voltar';
		$music[] = 'Fazer a coisa certa, aqui é o meu lugar';
		$music[] = 'Mas, sabe como é difícil encontrar';
		$music[] = 'A palavra certa, a hora certa de voltar';
		$music[] = 'A porta aberta, a hora certa de chegar';

		$music[] = 'Eu que não fumo queria um cigarro';
		$music[] = 'Eu que não amo você';
		$music[] = 'Envelheci dez anos ou mais nesse último mês';
		$music[] = 'Eu que não bebo, pedi um conhaque pra enfrentar o inverno que entra pela porta que você deixou aberta ao sair';
		$music[] = 'O certo é que eu dancei sem querer dançar';
		$music[] = 'Agora já nem sei qual é o meu lugar';
		$music[] = 'Dia e noite sem parar procurei sem encontrar';
		$music[] = 'A palavra certa, a hora certa de voltar';
		$music[] = 'A porta aberta, a hora certa de chegar';

		return $music;
	}

	public function RefraoBolero()
	{
		$music[] = 'Eu que falei "nem pensar"';
		$music[] = 'Agora me arrependo, roendo as unhas';
		$music[] = 'Frágeis testemunhas de um crime sem perdão';

		$music[] = 'Mas eu falei "nem pensar"';
		$music[] = 'Coração na mão, como refrão de bolero';
		$music[] = 'Eu fui sincero como não se pode ser';

		$music[] = 'Um erro assim tão vulgar';
		$music[] = 'Nos persegue a noite inteira';
		$music[] = 'E quando acaba a bebedeira ele consegue nos achar';

		$music[] = 'Num bar';
		$music[] = 'Com um vinho barato';
		$music[] = 'Um cigarro no cinzeiro,';
		$music[] = 'E uma cara embriagada no espelho do banheiro';

		$music[] = 'Teus lábios são labirintos que atraem os meus instintos mais sacanas';
		$music[] = 'Teu olhar sempre distante sempre me engana';
		$music[] = 'Eu entro sempre na tua dança de cigana';

		$music[] = 'Teus lábios são labirintos que atraem os meus instintos mais sacanas';
		$music[] = 'Teu olhar sempre distante sempre me engana';
		$music[] = 'Eu entro sempre na tua dança de cigana';
		$music[] = 'É o fim do mundo todo dia da semana';

		return $music;
	}

	public function oPreco()
	{
		$music[] = 'O preço que se paga às vezes é alto demais';
		$music[] = 'É alta madrugada, já é tarde demais';
		$music[] = 'Pra pedir perdão';
		$music[] = 'Pra fingir que não foi mal';

		$music[] = 'Uma luz se apaga no prédio em frente ao meu';
		$music[] = 'Sempre em frente foi o conselho que ela me deu';
		$music[] = 'Sem me avisar que iria ficar pra trás';

		$music[] = 'E agora eu pago os meus pecados por ter acreditado que só se vive uma vez';
		$music[] = 'Pensei que era liberdade, mas na verdade eram as grades da prisão';
		$music[] = 'Da Prisão';

		$music[] = 'O preço que se paga às vezes é alto demais';
		$music[] = 'É alta madrugada, já é tarde demais';
		$music[] = 'Mais uma luz se apaga no prédio em frente ao meu';
		$music[] = 'É a última janela iluminada';
		$music[] = 'Nada de anormal';
		$music[] = 'Amanhã ela vai voltar';

		$music[] = 'Enquanto isso eu pago os meus pecados por ter acreditado que só se vive uma vez';
		$music[] = 'Pensei que era liberdade, mas na verdade me enganei outra vez';
		$music[] = 'Outra vez';

		$music[] = 'Enquanto isso eu pago os meus pecados por ter acreditado que só se vive uma vez';
		$music[] = 'Pensei que era liberdade, mas na verdade era só solidão';
		$music[] = 'Solidão';


		return $music;
	}

	public function vidaReal()
	{
		$music[] = 'Cai a noite sobre a minha indecisão';
		$music[] = 'Sobrevoa o inferno minha timidez';
		$music[] = 'Um telefonema bastaria';
		$music[] = 'Passaria a limpo a vida inteira';

		$music[] = 'Cai a noite sem explicação';
		$music[] = 'Sem fazer a ligação';

		$music[] = 'Na hora da canção em que eles dizem baby';
		$music[] = 'Eu não soube o que dizer';
		$music[] = 'Ah vida real';

		$music[] = 'Esperei chegar a hora certa';
		$music[] = 'Por acreditar que ela viria';
		$music[] = 'Deixei no ar a porta aberta';
		$music[] = 'No final de cada dia';
		$music[] = 'Cai a noite doce escuridão';
		$music[] = 'De madura vai ao chão';

		$music[] = 'Ah vida real';
		$music[] =  'Como é que eu troco de canal?';
		return $music;
	}

	public function domQuixote()
	{
		$music[] = 'Muito prazer, meu nome é Otário';
		$music[] = 'Vindo de outros tempos, mas sempre no horário';
		$music[] = 'Peixe fora da água, borboletas no aquário';
		$music[] = 'Muito prazer, meu nome é otário';
		$music[] = 'Na ponta dos cascos e fora do páreo';
		$music[] = 'Puro sangue, puxando carroça';

		$music[] = 'Um prazer cada vez mais raro';
		$music[] = 'Aerodinâmica num tanque de guerra';
		$music[] = 'Vaidades que a terra um dia há de comer';
		$music[] = 'Ás de Espadas fora do baralho';
		$music[] = 'Grandes negócios, pequeno empresário';
		$music[] = 'Muito prazer, me chamam de otário';

		$music[] = 'Por amor às causas perdidas';
		$music[] = 'Tudo bem, até pode ser que os dragões sejam moinhos de vento';
		$music[] = 'Vaidades que a terra um dia há de comer';
		$music[] = 'Tudo bem, seja o que for';
		$music[] = 'Seja por amor às causas perdidas';
		$music[] = 'Por amor às causas perdidas';

		$music[] = 'Tudo bem, até pode ser que os dragões sejam moinhos de vento';
		$music[] = 'Muito prazer, ao seu dispor Se for por amor às causas perdidas';
		$music[] = 'Por amor às causas perdidas';

		
		return $music;
	}


	public function infinitaHighway()
	{
		$music[] = 'Você me faz correr demais os riscos desta highway';
		$music[] = 'Você me faz correr atrás do horizonte desta highway';
		$music[] = 'Ninguém por perto, silêncio no deserto';
		$music[] = 'Deserta Highway';
		$music[] = 'Estamos sós e nenhum de nós sabe exatamente onde vai parar';

		$music[] = 'Mas não precisamos saber pra onde vamos';
		$music[] = 'Nós só precisamos ir';
		$music[] = 'Não queremos ter o que não temos';
		$music[] = 'Nós só queremos viver';
		$music[] = 'Sem motivos, nem objetivos';
		$music[] = 'Estamos vivos e isto é tudo';
		$music[] = 'É sobretudo a lei dessa infinita highway';

		$music[] = 'Quando eu vivia e morria na cidade';
		$music[] = 'Eu não tinha nada, nada a temer';
		$music[] = 'Mas eu tinha medo, medo dessa estrada';
		$music[] = 'Olhe só, Veja você';

		$music[] = 'Quando eu vivia e morria na cidade';
		$music[] = 'Eu tinha de tudo, tudo ao meu redor';
		$music[] = 'Mas tudo que eu sentia era que algo me faltava';
		$music[] = 'E à noite eu acordava banhado em suor';

		$music[] = 'Não queremos lembrar o que esquecemos';
		$music[] = 'Nós só queremos viver';
		$music[] = 'Não queremos aprender o que sabemos';
		$music[] = 'Não queremos nem saber';
		$music[] = 'Sem motivos, nem objetivos';
		$music[] = 'Estamos vivos e é só';
		$music[] = 'Só obedecemos a lei dessa infinita highway';

		$music[] = "Escute garota, o vento canta uma canção";
		$music[] = "Dessas que a gente nunca canta sem razão";
		$music[] = "Me diga garota, será a estrada uma prisão?";
		$music[] = "Eu acho que sim, você finge que não";
		$music[] = "Mas nem por isso ficaremos parados com a cabeça nas nuvens e os pés no chão";
		$music[] = "Tudo bem, garota, não adianta mesmo ser livre";
		$music[] = "Se tanta gente vive sem ter como viver";

		$music[] = "Estamos sós e nenhum de nós sabe onde quer chegar";
		$music[] = "Estamos vivos sem motivos";
		$music[] = "Que motivos temos pra estar?";
		$music[] = "Atrás de palavras escondidas";
		$music[] = "Nas entrelinhas do horizonte dessa highway";
		$music[] = "Silenciosa highway";

		$music[] = "Eu vejo um horizonte trêmulo";
		$music[] = "Eu tenho os olhos úmidos";
		$music[] = "Eu posso estar completamente enganado";
		$music[] = "Eu posso estar correndo pro lado errado";
		$music[] = "Mas a dúvida é o preço da pureza";
		$music[] = "E é inútil ter certeza";
		$music[] = "Eu vejo as placas dizendo";
		$music[] = "Não corra, não morra, não fume";
		$music[] = "Eu vejo as placas cortando o horizonte";
		$music[] = "Elas parecem facas de dois gumes";

		$music[] = "Minha vida é tão confusa quanto a América Central";
		$music[] = "Por isso não me acuse de ser irracional";
		$music[] = "Escute garota, façamos um trato";
		$music[] = "Você desliga o telefone se eu ficar muito abstrato";
		$music[] = "Eu posso ser um Beatle, um beatnik";
		$music[] = "Ou um bitolado";
		$music[] = "Mas eu não sou ator";
		$music[] = "Eu não tô à toa do teu lado";
		$music[] = "Por isso, garota, façamos um pacto";
		$music[] = "De não usar a highway pra causar impacto";

		$music[] = "110, 120, 160";
		$music[] = "Só pra ver até quando o motor aguenta";
		$music[] = "Na boca em vez de um beijo um chiclet de menta";
		$music[] = "E a sombra do sorriso que eu deixei numa das curvas da highway";
		$music[] = "Highway";
		$music[] = "Infinita highway";
		return $music;
	}
}
