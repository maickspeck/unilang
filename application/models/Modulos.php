<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modulos extends CI_Model
{
	public $id;
	public $nome;
	public $nomeconfig;
	public $tabela;
	public $where;
	public $orderby;
	public $camada;

	public function getModulo($nomeconfig)
	{
		$this->nomeconfig = $nomeconfig;
		$mod = $this->getModuloByNameConfig();
		$this->nome = $mod['nome'];
		$this->tabela = $mod['tabela'];
		$this->where = $mod['where'];
		$this->orderby = $mod['orderby'];
		$this->camada = $mod['camada'];
		$this->id = $mod['id'];
		return $this;
	}

	public function getModuloByNameConfig()
	{
		$nome = $this->nomeconfig;
		$this->db->select('*');
		$this->db->where('nomeconfig', $nome);
		return $this->db->get('modulos')->row_array();
	}
}
