<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Model
{
	public function inserirLog($tabela, $id, $acao, $funcao='', $erro='N', $msgerro='', $dados=[])
	{
		$log['tabela'] = $tabela;
		$log['idregistro'] = $id;
		$log['datalog'] = date('Y-m-d h:i:s');
		$log['acao'] = $acao;
		$log['funcao'] = $funcao;
		$log['erro'] = $erro;
		$log['dados'] = json_encode($dados);
		if($msgerro)
			$log['msgerro'] = $msgerro;

		$this->Database->insert($log, 'loggatilhos');
	}

	public function iniciarServico($servico, &$id='', $idrequisicao=false)
	{
		$log['id'] = $id = uniqid();
		$log['servico'] = $servico;
		if($idrequisicao) $log['idrequisicaopai'] = $idrequisicao;
		$log['dataini'] = date('Y-m-d H:i:s');
		return $this->Database->insert($log, 'logservicos');
	}

	public function finalizarServico($id)
	{
		$this->Database->update(array('datafim' => date('Y-m-d H:i:s')), 'logservicos', $id);
	}

	public function gravarLogRobo($acao, $tabela, $idlogservico, $identificador, $erro=0, $msg='', $idaux='')
	{
		$log['acao'] = $acao;
		$log['tabela'] = $tabela;
		$log['idaux'] = $idaux;
		$log['idlogservico'] = $idlogservico;
		$log['identificador'] = $identificador;
		$log['erro'] = $erro;
		$log['mensagem'] = $msg;
		$log['datalog'] = date('Y-m-d H:i:s');
		return $this->Database->insert($log, 'logrobo');
	}
}
