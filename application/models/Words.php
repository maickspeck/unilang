<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Words extends CI_Model
{
	public $cont;
	public $index;
	public $indexSentence;
	public $fraseOriginal;
	public $contSentence;
	public $pergunta = false;

	public function traduzir($linha)
	{
		$this->fraseOriginal = $linha;
		$sentencas = explode(',', $linha);
		$this->cont = sizeof(array_filter($sentencas));
		$sentences = [];

		foreach ($sentencas as $i => $frase)
		{
			$this->index = $i;
			if(strstr($frase, '?')) {
				$frase = str_replace('?', '', $frase);
				$this->pergunta = true;
			}
			$sentences[] = $this->traduzirFrase($frase);
			$this->pergunta = false;
		}

		return $this->retornaFrase(implode(', ', $sentences));
	}

	public function traduzirFrase($frase)
	{
		$frase =  $this->getExpressoes($frase);
		$palavras = array_values(array_filter(explode(' ', $frase)));
		$this->contSentence = sizeof($palavras);
		$palavrasTraduzidas = [];

		foreach ($palavras as $key => $word)
		{
			$this->indexSentence = $key;
			if(preg_match("/\[+[\w\s]+\]+/i",$word) == 1)
			{
				$id = str_replace('[', '', $word);
				$id = str_replace(']', '', $id);
				$dbWord = $this->Database->getBy('palavras', $id);
				$dbWord['classe'] = $dbWord['idclasse'];
				$palavrasTraduzidas[] = $dbWord;
				continue;
			}

			$dbWord = $this->getDbWord($word, $frase);
			if(!$dbWord)
			{
				$arr['classe'] = '';
				$arr['word'] = $word;
				$palavrasTraduzidas[] = $arr;
			}
			else
				$palavrasTraduzidas[] = $this->retornaTraducaoPalavra($dbWord, $palavras, $key, $palavrasTraduzidas);
		}

		$palavrasTraduzidas = $this->ordenarFrase($palavrasTraduzidas);
		$palavrasTraduzidas = $this->reordenacao($palavrasTraduzidas);
		$palavrasTraduzidas = $this->aplicaULtimasRegras($palavrasTraduzidas);

		if($this->pergunta)
			$palavrasTraduzidas[] = array('word' => '?');

		$imploF = implode(' ', array_column($palavrasTraduzidas, 'word'));
		return $this->retornaFrase($imploF);
	}

	public function retornaFrase($frase)
	{
		$frase = ' '.$frase.' ';
		$frase = strtolower($frase);
		$frase = str_replace('  ', " ", $frase);
		$frase = str_replace(' i ', " I ", $frase);
		$frase = str_replace(' the what ', " what ", $frase);
		$frase = str_replace(' The what ', " What ", $frase);
		$frase = str_replace(' what the ', " that ", $frase);
		$frase = str_replace(' we are ', " we're ", $frase);
		$frase = str_replace(' not was ', " wasn't ", $frase);
		$frase = str_replace(' was not ', " wasn't ", $frase);
		$frase = str_replace(" don't will ", " won't ", $frase);
		$frase = str_replace(" don't can ", " can't ", $frase);
		$frase = str_replace(" doesn't will ", " won't ", $frase);
		$frase = str_replace(' it is ', " it's ", $frase);
		$frase = str_replace(' not am ', " am not ", $frase);
		$frase = str_replace(' you are ', " you're ", $frase);
		$frase = str_replace(' we will ', " we'll ", $frase);
		$frase = str_replace(' you will ', "you'll ", $frase);
		$frase = str_replace(' I am ', " I'm ", $frase);
		$frase = str_replace(" would don't ", " wouldn't ", $frase);
		$frase = str_replace(" don't am ", " am not ", $frase);
		$frase = str_replace(' I will ', " I'll ", $frase);
		$frase = trim($frase);
		$frase = ucfirst($frase);
		$frase = str_replace(" i'm ", " I'm ", $frase);
		return $frase;
	}

	public function matchPAV(&$traduzido, $i, $comp)
	{
		$isset1 = isset($traduzido[$i-1]['classe']);
		$isset2 = isset($traduzido[$i-2]['classe']);

		if($isset1 && $traduzido[$i-1]['classe'] == '13')
		{
			if(!$isset2 || ($isset2 && $traduzido[$i-2]['classe'] !== '3'))
			{
				if($isset2)
					$traduzido[$i-2]['word'] = $comp.$traduzido[$i-2]['word'];
				else {
					$arr['word'] = $comp;
					$arr['classe'] = 3;
					array_unshift($traduzido, $arr);
				}
			}
			return true;
		}
		return false;
	}

	public function matchThatNeg($traduzido, $i, $frase)
	{
		$issetback = isset($traduzido[$i-1]['classe']);
		$isset1 = isset($frase[$i+1]['classe']);

		if($issetback && $traduzido[$i-1]['word'] == 'that' && $isset1 && $frase[$i+1]['classe'] == '2')
			return false;

		return true;
	}

	public function matchPAVfreq(&$traduzido, $i)
	{
		$isset1 = isset($traduzido[$i-1]['classe']);
		$isset2 = isset($traduzido[$i-2]['classe']);

		if($isset1 && $traduzido[$i-1]['classe'] == '19')
		{
			if($isset2 && $traduzido[$i-2]['classe'] == '3')
				return true;
		}

		return false;
	}

	public function matchSVQuestion(&$traduzido, $i)
	{
		$isset1 = isset($traduzido[$i-1]['classe']);

		if($isset1 && in_array($traduzido[$i-1]['classe'], [15, 14, 17]))
			return true;

		if(!$this->pergunta && !$isset1)
			return true;

		if($isset1 && $traduzido[$i-1]['classe'] == '1' && $this->pergunta)
		{
			return true;
		}

		return false;
	}

	public function retornaTraducaoPalavra($palavra, $frase, $i, &$palavrasTraduzidas)
	{
		$idclasse = $palavra['idclasse'];
		$isset1 = isset($frase[$i + 1]);
		$issetback = isset($palavrasTraduzidas[$i - 1]['classe']) && isset($palavrasTraduzidas[$i - 1]['word']);
		switch ($palavra['idclasse'])
		{
			// Pronome
			case 3:
			case 5:
			case 21:
			case 28:
				if((isset($palavrasTraduzidas[$i - 1]['classe']) && $palavrasTraduzidas[$i - 1]['classe'] == '9') || !$isset1)
				{
					if($idclasse == 3)
						$palavra['idclasse'] = 12;
					else if($idclasse == 21 && ((!$issetback || (!in_array($palavrasTraduzidas[$i-1]['classe'], ['9'])) && !in_array($palavrasTraduzidas[$i-1]['word'], ['for'])) || !$isset1))
						$palavra['idclasse'] = 22;
				}

				$word = $this->getPronome($palavra);

				$word['idclasse'] = $idclasse;

				if($this->pergunta)
					$word['word'] = 'do '.$word['word'];
//			pre($this->db->last_query(), 0);
				break;
			// Verbo
			case 2:
				if($this->indexSentence == 0 && $this->pergunta && $palavra['word'] == 'será')
				{
					$palavra['pessoa'] = 3;
					$palavra['plural'] = 0;
					$palavra['idtempo'] = 1;
				}

				if($issetback && strtolower($palavrasTraduzidas[$i-1]['word']) == 'you' && in_array($palavra['idverbo'], [122, 57]))
				{
					$palavra['pessoa'] = 2;
					$palavra['plural'] = 1;
				}

				$word = $this->getVerbo($palavra);
				//pre($this->db->last_query(), 0);
				$w['idtempo'] = $palavra['idtempo'];
				$w['idverbo'] = $palavra['idverbo'];
				$aux = $word['word'];
				$idverbo = $word['idverbo'];
				$comp = '';
				$pavfreq = $this->matchPAVfreq($palavrasTraduzidas, $i);



				if(!isset($frase[$i - 1]) || (isset($palavrasTraduzidas[$i-1]['classe']) && !in_array($palavrasTraduzidas[$i -1]['classe'], [3, 2, 12, 10])))
				{

					if($this->matchSVQuestion($palavrasTraduzidas, $i) || ($word['idverbo'] == '55' && !$this->pergunta && (!isset($palavrasTraduzidas[$i-1]['classe']) || !in_array($palavrasTraduzidas[$i-1]['classe'], [1, 8]) )))
					{
						if(isset($palavrasTraduzidas[$i-1]['cod']) && $palavrasTraduzidas[$i-1]['cod'] == 2)
							$comp = '';
						else if(!$pavfreq && (!$issetback || $palavrasTraduzidas[$i-1]['word'] !== 'if') && !$this->matchSubThat($palavrasTraduzidas, $i))
							$comp = $this->retornaPessoaVerbo($palavra) . ' ';


						if($this->matchPAV($palavrasTraduzidas, $i, $comp))
							$comp = '';

//						if($word['idverbo'] == '55' && trim($comp))
//							$comp .= ' ashky ';
					}
				}

				if(!$comp)
					$word['pess'] = $this->retornaPessoaVerbo($palavra);
				else
					$word['pess'] = $comp;
//				pre($idverbo, 0);
				$comp2 = '';

				if($palavra['idtempo'] == 3 && $aux !== 'will') {
						$comp2 = 'will ';
				}


				$comp = $comp2;

				if($palavra['idtempo'] == 5 && $aux !== 'would' && !in_array($palavra['idverbo'], ['282']))
					$comp .= 'would ';

				$word['word'] = $comp.$aux;

				if($palavra['idtempo'] == '2' && $word['idtempo'] !== '2' && $issetback && !in_array($palavrasTraduzidas[$i-1]['classe'], ['15'])) {
					if (substr($word['word'], -1) == 'e')
						$word['word'] .= 'd';
					else
						$word['word'] .= 'ed';
				}
//				pre($palavra, 0);
				if(($palavra['idtempo'] == '6' || ($palavra['idtempo'] == '1' && $palavra['pessoa'] == '3')) && $palavra['plural'] == '0' && $palavra['idverbo'] !== '122')
				{
					if(!$pavfreq && !$word['expression'] && (!$issetback || ($palavrasTraduzidas[$i-1]['word'] !== 'if' && $palavrasTraduzidas[$i-1]['classe'] !== '15')) && substr($word['word'], -1) !== 's')
						$word['word'] .= 's';
				}

				if($palavra['idtempo'] !== '4' && isset($palavrasTraduzidas[$i-1]['classe']) && $palavrasTraduzidas[$i-1]['classe'] == 2 && $palavrasTraduzidas[$i-1]['word'] !== 'will')
				{
					if(!strstr($palavrasTraduzidas[$i-1]['word'], ' will') && !in_array($palavra['idverbo'], ['122', '57', '98', '282']))
						if(isset($palavrasTraduzidas[$i-2]['classe']) && $palavrasTraduzidas[$i-2]['classe'] !== '5')
							if(!$issetback || $palavrasTraduzidas[$i-1]['classe'] !== '2' || ($palavrasTraduzidas[$i-1]['classe'] == 2) && !in_array($palavrasTraduzidas[$i-1]['idverbo'], ['282', '895']))
							$word['word'] = 'to '.$word['word'];
				}

				if($palavra['idtempo'] == '4' && $word['idtempo'] !== '4')
					$word['word'] = $this->getVerboING($palavra['idverbo'], $word['word']);

//				pre($this->db->last_query(), 0);
				if(!$word)
					pre($this->db->last_query());
				break;
			case 7:
				$word = $this->getArtigo($palavra);
				break;
			case 14:
//				pre($frase, 0);
				if(isset($palavrasTraduzidas[$i-1]['classe']) && in_array($palavrasTraduzidas[$i-1]['classe'] , [8,2, 1]) && $palavra['word'] == 'que') {
					$word['word'] = 'that';
					$word['idclasse'] = 14;
				}
				else
					$word = $this->getTraducao($palavra, $frase);
				break;
			case 15:
				$word = $palavra;
				break;
			default:
				$word = $this->getTraducao($palavra, $frase);

//					pre($this->db->last_query(), 0);
				break;

		}

		$w['expressao'] = '';

		if(!$word || !isset($word['word'])) {
			$w['expressao'] = 1;
			$word['word'] = $palavra['word'];

		}

		$w['word'] = $word['word'];

		if(isset($word['cod']))
			$w['cod'] = $word['cod'];

		if(isset($word['pess']))
			$w['pess'] = $word['pess'];

//		pre($word, 0);
		$w['classe'] = $word['idclasse'];

		if(isset($word['plural']))
			$w['plural'] = $word['plural'];
		$w['palavra'] = $palavra['word'];
		return $w;
	}

	public function getPronome($word)
	{
		$where = array('pessoa' => $word['pessoa'], 'ididioma' => 1, 'idclasse' => $word['idclasse']);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->where("(plural = {$word['plural']} OR plural IS NULL)");
		$this->db->where("(genero = '{$word['genero']}' OR genero IS NULL)");
		return $this->db->get('palavras', 1)->row_array();
	}

	public function getVerbo($verbo)
	{
		$this->db->select('idpalavra1');
		$this->db->where('idpalavra2', $verbo['idverbo']);
		$verboIng = $this->db->get('traducoes')->result_array();

		if(sizeof($verboIng) > 1) {
			$verboIngAux = $this->getSinonimo($verbo['word'], '$frase', $verboIng);
			if($verboIngAux)
				$verboIng = $verboIngAux['idverbo'];
			else
				$verboIng = $verboIng[0]['idpalavra1'];
		}
		else if (isset($verboIng[0]))
			$verboIng = $verboIng[0]['idpalavra1'];
		$this->db->select('*');
		$this->db->where('palavras.ididioma', 1);
		$this->db->where('palavras.idverbo', $verboIng);
		$this->db->where("(plural = '{$verbo['plural']}' OR plural IS NULL)");
		$this->db->where("(pessoa = '{$verbo['pessoa']}' OR pessoa IS NULL)");
		$this->db->where("(idtempo = '{$verbo['idtempo']}' OR idtempo IS NULL)");
		$this->db->order_by('idtempo desc, plural desc, pessoa desc');

		return $this->db->get('palavras', 1)->row_array();
	}

	public function getTraducao($word, $frase='')
	{
//		pre($frase, 0);
		$this->db->select('*');
		$this->db->join('traducoes', 'traducoes.idpalavra1 = palavras.id AND traducoes.idpalavra2 = '.$word['id']);
		$this->db->where('ididioma', 1);
		$this->db->order_by('grau desc, palavras.id');
		$words = $this->db->get('palavras')->result_array();
//		pre($words, 0);
//		pre($words, 0);
		if(sizeof($words) > 1)
		{
			$sinonimo = $this->getSinonimo($word['word'], $frase, $words);
			if($sinonimo)
				return $sinonimo;
			else if (isset($words[0]))
				return $words[0];
		}
		else if (isset($words[0]))
			return $words[0];
	}

	public function getArtigo($word)
	{
		$this->db->select('*');
		$this->db->where('ididioma', 1);
		$this->db->where('idclasse', 7);
		$this->db->where("(plural = '{$word['plural']}' OR plural IS NULL)");
		$this->db->where("(genero = '{$word['genero']}' OR genero IS NULL)");
		$this->db->where("(pessoa = '{$word['pessoa']}' OR pessoa IS NULL)");
		return $this->db->get('palavras', 1)->row_array();
//		return $at;
	}

	public function matchSubThat($frase, $i)
	{
		$issetback = isset($frase[$i-1]);
		$isset1 = isset($frase[$i+1]);
		$issetback2 = isset($frase[$i-2]);

		if($issetback && $issetback2 && $frase[$i-1]['word'] == 'that' && $frase[$i-2]['classe'] == '1')
			return true;

		return false;
	}

	public function ordenarFrase($frase)
	{
		$fraseFinal = [];
		$second = false;
		$op = 0;
		foreach ($frase as $i => $palavra)
		{
			if(!isset($frase[$i]['classe']))
				pre($frase);
			if($frase[$i]['classe'] == 'pula')
				continue;

			$issetback = isset($fraseFinal[$i-1]);
			$isset1 = isset($frase[$i+1]);
			$isset2 = isset($frase[$i+2]);

			if($palavra['classe'] == 5)
			{
				$fraseFinal[$i] = $frase[$i + 1];
				$fraseFinal[$i+1] = $palavra;
				$frase[$i + 1]['classe'] = 'pula';
				continue;
			}

			if($palavra['classe'] == 11)
			{
				if(isset($frase[$i - 1]['palavra']) && $frase[$i - 1]['classe'] == '1')
				{
					$sub = $fraseFinal[$i-1];
					$fraseFinal[$i-1] = $palavra;
					$fraseFinal[$i] = $sub;
					continue;
				}
			}

			if($palavra['classe'] == 14)
			{
				if($issetback && $fraseFinal[$i-1]['word'] == 'all')
					continue;
				if(isset($fraseFinal[$i-1]['classe']) && $fraseFinal[$i-1]['classe'] == '27' && $palavra['word'] == 'what')
					$palavra['word'] = 'that';

			}

			if($palavra['classe'] == '15')
			{
				$fraseFinal[$i]['classe'] = 15;
				$next = array_values(array_filter(explode(' ',$frase[$i+1]['word'])));
				if(sizeof($next) > 1 && (isset($frase[$i-1]['classe']) && $frase[$i-1]['classe'] == 3))
					unset($fraseFinal[$i-1]);

				if(sizeof($next) > 1 && $next[1] == 'had')
				{
					$fraseFinal[$i]['word'] = '';
					continue;
				}

				if(!$this->matchThatNeg($fraseFinal, $i, $frase))
				{
					if(!isset($frase[$i+1]['pess']))
						$frase[$i+1]['pess'] = 'you';

					$fraseFinal[$i]['word'] = $this->buscaNegacao($frase[$i+1]['pess'], $frase[$i+1]['idtempo']);
					$fraseFinal[$i+1] = $frase[$i+1];
					$verb = $next[0];
					if(sizeof($next) > 1)
						$verb = $next[1];
					$fraseFinal[$i+1]['word'] = $verb;
					$frase[$i + 1]['classe'] = 'pula';
					continue;
				}

				if($isset1 && $frase[$i+1]['word'] == 'if' && $isset2 && $frase[$i+2]['classe'] == '2')
				{
					$frase[$i + 1]['classe'] = 'pula';

					if(!isset($frase[$i+2]['pess']))
						$frase[$i+2]['pess'] = 'you';

					$fraseFinal[$i]['word'] = $this->buscaNegacao($frase[$i+2]['pess'], $frase[$i+2]['idtempo']);
					continue;
				}

				if(sizeof($next) > 1)
				{
					$frase[$i + 1]['classe'] = 'pula';
					$fraseFinal[$i]['word'] = $next[0].' '.$this->buscaNegacao($next[0], $frase[$i+1]['idtempo']);
					$fraseFinal[$i + 1]['word'] = $next[1];
					$fraseFinal[$i+1]['classe'] = 2;
					continue;
				}
				else if(isset($frase[$i+1]['classe']) && (($frase[$i+1]['classe'] == 2) || $frase[$i+1]['word'] == 'me'))
				{
					$verb = $frase[$i+1];
					if($frase[$i+1]['word'] == 'me' && isset($frase[$i+2]) && $frase[$i+2]['classe'] == 2)
						$verb = $frase[$i+2];

					if(isset($verb['idverbo']) && $verb['idverbo'] == 122)
						$fraseFinal[$i]['word'] = 'not';
					else
						$fraseFinal[$i]['word'] = $this->buscaNegacao($verb['word'], $verb['idtempo']);
//					pre($this->db->last_query());
					continue;
				}

			}

			if($palavra['classe'] == '19' && isset($frase[$i+1]['classe']) && ($frase[$i+1]['classe'] == 2 && $frase[$i+1]['idverbo'] == '122'))
			{
//				pre($palavra);
				$verb = $frase[$i+1];
				$pesVerb = array_filter(explode(' ', $verb['word']));

				if(sizeof($pesVerb) > 1 && $i > 0)
					$verb['word'] = $pesVerb[1];

				$fraseFinal[$i] = $verb;
				$fraseFinal[$i + 1] = $palavra;
				$frase[$i + 1]['classe'] = 'pula';
//					pre($this->db->last_query());
				continue;
			}

			if($palavra['classe'] == 2 && isset($frase[$i - 1]['palavra']) && $frase[$i - 1]['palavra'] == 'a')
			{
				$fraseFinal[$i-1]['word'] = 'to';
				$fraseFinal[$i] = $palavra;
				continue;
			}

			$fraseFinal[$i] = $palavra;
		}

		if($op)
			pre($fraseFinal);

		return array_values($fraseFinal);
	}

	public function reordenacao($frase)
	{
		$fraseFinal = [];
		foreach ($frase as $i => $palavra)
		{
			$isset1 = isset($frase[$i + 1]);
			$isset2 = isset($frase[$i + 2]);
			$issetback = isset($frase[$i - 1]);
			$issetback2 = isset($frase[$i - 2]);


			if($palavra['classe'] == '2')
			{
				if($palavra['word'] == 'will' && $isset1 && $frase[$i+1]['classe'] !== '2')
				{
					$palavra['word'] = 'will be';
				}

			}

			if(!isset($palavra['palavra']) && isset($frase[$i]['expression']))
				$palavra['palavra'] = '';

			if (!isset($frase[$i]['classe']))
				pre($frase);
			if ($frase[$i]['classe'] == 'pula')
				continue;

			if($palavra['classe'] == '9' && $issetback)
			{
				$next = $frase[$i + 1]['word'];

				if($palavra['word'] == 'a' && in_array(substr($next, 0,1),['a','e','i','o','u','h']))
				{
					$palavra['word'] = 'an';
					$fraseFinal[$i] = $palavra;
					continue;
				}

				if($palavra['word'] == 'at' && $isset1 && $frase[$i+1]['classe'] == '22')
				{
					$palavra['word'] = 'of';
				}

				if($palavra['palavra'] == 'de' && ($i == 0 && $this->index == 0) || ($isset1 && $frase[$i+1]['classe'] == '4'))
				{
					$palavra['word'] = 'from';
				}

				if($isset1 && $issetback && in_array($palavra['palavra'], ['de', 'do', 'da']) && $frase[$i+1]['classe'] == 1 && isset($frase[$i+1]['cod'])&& $frase[$i+1]['cod'] == 1 && $fraseFinal[$i-1]['classe'] == 1)
				{
					$back = $fraseFinal[$i - 1];
//					$back['word'] .= 'testeando';
					$fraseFinal[$i - 1] = $frase[$i+1];
					$fraseFinal[$i] = $back;
					$frase[$i+1]['classe'] = 'pula';
					continue;
				}
			}

			if($palavra['classe'] == '9' && $issetback && $isset1 && $palavra['palavra'] == 'de')
			{
				if($frase[$i - 1]['word'] == 'change' && $frase[$i + 1]['plural'] == '0')
				{
					$fraseFinal[$i] = $frase[$i+1];
					$fraseFinal[$i]['word'] .= 's';
					$frase[$i+1]['classe'] = 'pula';
					continue;
				}
			}

			if($palavra['classe'] == '6' && $issetback && $palavra['palavra'] == 'demais')
			{
				if($fraseFinal[$i-1]['classe'] == '11')
				{
					$palavra['word'] = 'too';
					$adj = $fraseFinal[$i-1];
					$fraseFinal[$i] = $adj;
					$fraseFinal[$i-1] = $palavra;
					continue;
				}
			}

			if($palavra['classe'] == '10' && $issetback && $palavra['palavra'] == 'sem')
			{
				$next = $frase[$i + 1];

				if(($this->indexSentence > 0 && $next['classe'] == '1') || $next['classe'] == '2')
				{
					$palavra['word'] = 'without';
					$fraseFinal[$i] = $palavra;
					if($next['classe'] == 2)
					{
						$fraseFinal[$i+1]['word'] = $this->getVerboING($frase[$i + 1]['idverbo'], $frase[$i + 1]['word']);
						$fraseFinal[$i+1]['classe'] = 2;
						$fraseFinal[$i+1]['idtempo'] = 4;
						$frase[$i+1]['classe'] = 'pula';
					}
					continue;
				}
			}

			if($palavra['classe'] == '9' && $issetback && strtolower($palavra['palavra']) == 'por')
			{
				$next = $frase[$i + 1];

				if($next['classe'] == '2' && (!$next['idtempo'] || $next['idtempo'] == 1))
				{
					$palavra['word'] = 'for';
					$fraseFinal[$i] = $palavra;
					if($next['classe'] == 2)
					{
						$arr['idverbo'] = $frase[$i+1]['idverbo'];
						$arr['idtempo'] = 4;
						$arr['word'] = $frase[$i+1]['word'];
						$arr['pessoa'] = '';
						$arr['plural'] = '';
						$fraseFinal[$i+1]['word'] = $this->getVerbo($arr)['word'];
						$fraseFinal[$i+1]['classe'] = 2;
						$fraseFinal[$i+1]['idtempo'] = 4;
						$frase[$i+1]['classe'] = 'pula';
					}
					continue;
				}
			}

			if($palavra['classe'] == '14' && $issetback && strtolower($palavra['palavra']) == 'que')
			{
				if(isset($fraseFinal[$i-1]['idtempo']) && $fraseFinal[$i-1]['idtempo'] == 4)
					$palavra['word'] = 'that';
				else if($palavra['word'] !== 'that')
				{
					$back = false;
					if($issetback && $fraseFinal[$i-1]['classe'] == '2')
						$back = $fraseFinal[$i-1];
					else if($issetback && strtolower($fraseFinal[$i-1]['word']) == 'the' && $issetback2 && $fraseFinal[$i-2]['classe'] == '2')
						$back = $fraseFinal[$i-2];

					if(isset($back['idverbo']) && $back['idverbo'] == '122')
					{
						$palavra['word'] = $palavra['word'].' '.$back['pess'];
					}
				}
			}

			if($palavra['classe'] == '17' && $issetback && $isset1 && strtolower($palavra['palavra']) == 'mais')
			{
				if($fraseFinal[$i-1]['classe'] == 1 && $frase[$i+1]['classe'] == 11)
				{
					$back = $fraseFinal[$i-1];
					$fraseFinal[$i-1] = $palavra;
					$fraseFinal[$i-1]['word'] = 'most';
					$fraseFinal[$i] = $frase[$i+1];
					$fraseFinal[$i+1] = $back;
					$frase[$i+1]['classe'] = 'pula';
					continue;
				}
			}

			if($palavra['classe'] == '19' && $issetback && $issetback2 && $fraseFinal[$i-1]['classe'] == 2 && $fraseFinal[$i-2]['classe'] == 3)
			{
				$back = $fraseFinal[$i-1];
				$fraseFinal[$i] = $back;
				$fraseFinal[$i-1] = $palavra;
				continue;
			}

			if($palavra['classe'] == '18' && $isset1 && in_array($frase[$i+1]['classe'], ['10','2']) && (!$issetback || $fraseFinal[$i-1]['classe'] !== '3') && $palavra['word'] == 'already')
			{
				$indexNext = 1;
				if($frase[$i+1]['classe'] == '10' && $isset2 && $frase[$i+2]['classe'] == '2')
					$indexNext = 2;

				$next = array_values(array_filter(explode(' ',$frase[$i+$indexNext]['word'])));
				if(sizeof($next) > 1)
				{
					if($frase[$i+$indexNext]['idverbo'] == '122')
					{
						$palavra['word'] =  $frase[$i+$indexNext]['word']. ' already';
						$fraseFinal[$i] = $palavra;
						if($indexNext == 2)
						{
							$fraseFinal[$i+1] = $frase[$i+1];
							$frase[$i+1]['classe'] = 'pula';
						}
						$fraseFinal[$i+$indexNext] = $frase[$i+1];
						$fraseFinal[$i+$indexNext]['word'] = '';
						$frase[$i+$indexNext]['classe'] = 'pula';
						continue;
					}

					$palavra['word'] =  $next[0]. ' already';
					$fraseFinal[$i] = $palavra;
					if($indexNext == 2)
					{
						$fraseFinal[$i+1] = $frase[$i+1];
						$frase[$i+1]['classe'] = 'pula';
					}
					$fraseFinal[$i+$indexNext] = $frase[$i+1];
					$fraseFinal[$i+$indexNext]['word'] = $next[1];
					$frase[$i+$indexNext]['classe'] = 'pula';
					continue;
				}
				else
					$palavra['word'] = $frase[$i+$indexNext]['pess'].' already';
			}

			$fraseFinal[$i] = $palavra;
		}

		return array_values($fraseFinal);
	}

	public function aplicaUltimasRegras($frase)
	{
		$isset1 = isset($frase[1]);
		$isset2 = isset($frase[2]);

		if($isset1 && $isset2)
		{
			foreach ($frase as $i => $palavra)
			{
				$issetNext = isset($frase[$i+1]);
				$issetdbNext = isset($frase[$i+2]);
				$issetback = isset($frase[$i-1]);

				if($palavra['word'] == 'like that' && $issetNext && $frase[$i + 1]['word'] == 'so' && $issetdbNext && $frase[$i+2]['classe'] == 11)
				{
					if($issetback && $frase[$i-1]['classe'] == 1)
						return $this->regraSuch($frase, $i);
				}
			}
		}
		return $frase;
	}

	public function regraSuch($frase, $i)
	{
		$adj = $frase[$i+2];
		$sub = $frase[$i-1];

		$new[]['word'] = 'Such a';
		$new[] = $adj;
		$new[] = $sub;
		return $new;
	}

	public function getSinonimo($palavra, $frase, $words)
	{
		$this->db->select('*');
		$this->db->where('expressao', $palavra);
		$contexto = $this->db->get('sinonimos')->result_array();
		$stop = false;

		foreach ($contexto as $c => $val)
		{
			if($val['contexto'])
			{
				$context = explode('/', $val['contexto']);
				foreach ($context as $i => $txt)
				{
					if(strstr(strtolower($this->fraseOriginal), strtolower(trim($txt))))
					{
						return $this->Database->getBy('palavras', $val['idpalavra']);
						$stop = true;
					}
				}
			}
		}

		return false;
	}

	public function retornaPessoaVerbo($verbo)
	{
		if($this->indexSentence == 0 && $this->index > 0 && !$verbo['idtempo']) {
			return '';
		}

		$this->db->select('word');
		$this->db->where('idclasse', 3);
		$this->db->where('ididioma', 1);
		$this->db->where("(plural = '{$verbo['plural']}' OR plural IS NULL)");
		$this->db->where("(genero = '{$verbo['genero']}' OR genero IS NULL)");
		$this->db->where("(idtempo = '{$verbo['idtempo']}' OR idtempo IS NULL)");
		$this->db->where("(pessoa = '{$verbo['pessoa']}' OR pessoa IS NULL)");
		$this->db->order_by('idtempo desc, plural desc, pessoa desc, genero desc');
		$pessoa = $this->db->get('palavras', 1)->row_array()['word'];

		if($this->pergunta && $verbo['idverbo'] !== '122')
			$pessoa = 'do '.$pessoa;

		return $pessoa;
//		pre($this->db->last_query());
	}

	public function buscaNegacao($pessoa, $idtempo, $idverbo=false)
	{
		$pessoa = $this->Database->getBy('palavras', $pessoa, 'word');
		$this->db->select('word');
		$this->db->where('idclasse', 15);
		$this->db->where('ididioma', 1);
		$this->db->where("(plural = '{$pessoa['plural']}' OR plural IS NULL)");
		$this->db->where("(pessoa = '{$pessoa['pessoa']}' OR pessoa IS NULL)");
		$this->db->where("(idtempo = '{$idtempo}' OR idtempo IS NULL OR idtempo = 1)");
		$this->db->order_by('idtempo desc, plural desc, pessoa desc');
		return $this->db->get('palavras', 1)->row_array()['word'];
	}

	public function getDbWord($word, $frase)
	{
		$wordUp = strtoupper($word);
		$wordLow = strtolower($word);
		$this->db->select('*');
		$this->db->where('ididioma', 2);
		$this->db->where("(LOWER(word) = '{$wordLow}' collate utf8_bin OR UPPER(word) = '{$wordUp}' collate utf8_bin OR word = '{$word}' collate utf8_bin)");
		return $this->db->get('palavras')->row_array();
	}

	public function getExpressoes($frase)
	{
		$arr = array_filter(explode(' ', $frase));
		foreach ($arr as $key => $value)
		{
			$value = trim($value);
			$this->db->select('*');
			$this->db->where("expressao like '%{$value}%'");
			$this->db->order_by('CHAR_LENGTH(expressao) DESC');
			$id = $this->db->get('expressoes')->result_array();
			if($id)
				$this->trataExpressao($id, $frase);
		}

		return $frase;
//		pre($this->db->last_query(), 0);
	}

	public function trataExpressao($exp, &$frase)
	{
		$frase = strtolower($frase);
		foreach ($exp as $i => $val)
		{
			$val['expressao'] = strtolower($val['expressao']);
			if(strstr($frase, $val['expressao']))
			{

				$aux = $frase;
				$aux = str_replace($val['expressao'], ' ['.$val['idpalavra'].']', $aux);

				if(trim($frase) == trim($val['expressao']))
					$frase = str_replace($val['expressao'], ' ['.$val['idpalavra'].']', $frase);
				else
					$frase = str_replace(' '.$val['expressao'], ' ['.$val['idpalavra'].']', $frase);

				if(!strpos($frase, '[') && strpos($aux, '[') == 1)
					$frase = $aux;
				break;
			}
		}
	}

	public function getVerboING($idverbo, $word)
	{
		$arr['idverbo'] = $idverbo;
		$arr['idtempo'] = 4;
		$arr['word'] = $word;
		$arr['pessoa'] = '';
		$arr['plural'] = '';

		$verbo = $this->getVerbo($arr);
		if($verbo['idtempo'] == 4)
			return $verbo['word'];
		else
			return $verbo['word'].'ing';
	}
}
