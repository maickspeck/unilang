<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$frameworkUrl = $this->config->item('header_framework');

import('assets/js/pages/savedados.js', 'js', false);
import('assets/js/conversoes.js', 'js', false);

$action = 'db/save';
$action = $this->config->item('hostFixo').$action;

?>

<br>
	<?php if(isset($_SESSION['mensagem'])): ?>
		<div class="mensagem" style="width: 70%; margin: auto;">
			<div class="ui <?=$_SESSION['mensagem']['status']?> message">
				<i class="close icon"></i>
				<div class="header">
					<?=$_SESSION['mensagem']['texto']?>
				</div>
				<p></p>
			</div>
			<br>
		</div>
	<?php
		unset($_SESSION['mensagem']);
		endif;
	?>

	<div style="width: 70%; margin: auto; background: lightgray; padding: 30px; border-radius: 9px;">
		<h2 class="ui dividing header" style="height: 70px"> <span style="float: left; line-height: 70px"><?=$modulo->nome?>
				<?php if($idedicao): ?>
					<a href="<?=base_url().'save/'.$modulo->nomeconfig?>" class="new" >Novo+</a>
				<?php endif; ?>
				</span> <img style="float: right" src="<?=base_url()?>assets/img/icones/<?=$modulo->nomeconfig?>.png"> <a class="anchor" id="types"></a></h2>
		<form id="form" class="ui form" action="<?=$action?>" method="POST">
			<input type="hidden" name="tabela" value="<?=$modulo->tabela?>">
			<input type="hidden" id="modulo" name="modulo" value="<?=$modulo->nomeconfig?>">
			<input type="hidden" name="idedicao" id="idedicao" value="<?=$idedicao?>">
			<div style="overflow: auto; height: 69%">
				<?=$campos;?>
			</div>
			<br>
			<button class="ui button primary" type="submit">Salvar</button>
		</form>
	</div>
	<br>
</div>
