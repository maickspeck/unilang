   <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$frameworkUrl = $this->config->item('header_framework');
?>
<!-- <link rel="stylesheet" href="<?=base_url() ?>assets/css/estilo.css" media="screen" type="text/css"> -->
<script src="assets/js/pages/classificacao.js?<?=filemtime('assets/js/pages/savedados.js');?>"></script>


<br>


<div class="ui middle aligned center aligned grid" style="height: 100%">
    <div class="ui column centered grid ui segment mob-div " style="" id="mob-div">
        <div style="float:left; width:100%" class="div-pad">
            <div class="ui grid" >
                <div class="sixteen wide column">
                    <h3 style="text-align: left;  "><span class="title-j">Classificação</span></h3> 
                </div>
            </div>
            <hr style="margin-top: -10px">
            <div class="ui segment">
                <div class=""> 
                    <div class="ui grid prox">
                        <div class="two wide column coluna thead first">
                            #
                        </div>
                        <div class="eight wide column coluna thead middle2">
                            Nome
                        </div>
                        <div class="two wide column coluna thead middle2">
                            <span>Pont.</span>
                        </div>
                        <div class="two wide column coluna thead middle">
                            <span>Plac.</span>
                        </div>
                        <div class="two wide column coluna thead last">
                            <span>Ult.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui message msg" style="display:none"><span class="txtMessage"></span></div>
        </div>
    </div>
    <br>
</div>
