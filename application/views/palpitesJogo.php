   <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$frameworkUrl = $this->config->item('header_framework');
?>
<!-- <link rel="stylesheet" href="<?=base_url() ?>assets/css/estilo.css" media="screen" type="text/css"> -->
<script src="assets/js/pages/palpitesJogo.js?<?=filemtime('assets/js/pages/palpitesJogo.js');?>"></script>

<br>


<div class="ui middle aligned center aligned grid" style="height: 100%">
    <div class="ui column centered grid ui segment mob-div " style="" id="mob-div">
        <div style="float:left;" class="div-pad">
            <div class="ui grid">
                <div class="sixteen wide column carreg" style="display: none">
                    <div class="ui form" style="margin-top: -5px;">
                        <div class="fields">
                            <div class="four wide field" style="max-width: 40%">
                                <label style="text-align: left;">Rodada</label>
                                <select class="ui fluid dropdown select-rodada">
                                </select>
                            </div>
                            <div class="twelve wide field select-jg" style=" ">
                                <label style="text-align: left;">Jogo</label>
                                <select class="ui fluid dropdown select-jogo">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sixteen wide column" style="padding-top: 0;">
                    <h3 style="text-align: left; ">Palpites - <span class="title-j"> </span></h3>     
                </div>
            </div>
            <hr style="margin-top: -10px">
            <div class="jogos-prox"> 
                
            </div>
            <div class="ui message msg" style="display:none"><span class="txtMessage"></span></div>
            <button class="ui green button" type="button" id="btn-all" style="width: 100%; display: none">Enviar Todos os Palpites</button> 
            <span style="float: left">* Define vencedor nos pênaltis em caso de empate</span>
        </div>
    </div>
    <br>
</div>