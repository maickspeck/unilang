<div class="ui modal modal-novotipo" style="display: none">
	<div class="header">Novo Tipo</div>
	<div class="content">
		<form class="ui form form-novotipo">
			<input class="limpa" type="hidden" name="id" id='tipo-id'>
			<div class="field">
				<label>Nome</label>
				<input class="limpa" type="text" name="nome" id='tipo-nome' placeholder="Nome">
			</div>
			<div class="field">
				<label>Descrição</label>
				<textarea class="limpa" name="descricao" id="tipo-descricao"></textarea>
			</div>
			<div class="field">
				<label>Escala</label>
				<input type="text" id="tipo-escala" name="escala" placeholder="Escala">
			</div>
			<div class="field">
				<label>Tabela</label>
				<input type="text" id="tipo-tabela" name="tabela" placeholder="Tabela" value="<?=$modulo->tabela?>">
			</div>
		</form>
	</div>
	<div class="actions">
		<div class="ui cancel button">Cancelar</div>
		<div class="ui green approve button" id="btn-novotipo">Adicionar</div>
	</div>
</div>

<div class="ui modal modal-novosubtipo" style="display: none">
	<div class="header">Novo Subtipo</div>
	<div class="content">
		<form class="ui form form-novosubtipo">
			<input class="limpa" type="hidden" name="id" id='subtipo-id'>
			<div class="field">
				<label>Nome</label>
				<input class="limpa" type="text" name="nome" id='subtipo-nome' placeholder="Nome">
			</div>
			<div class="field">
				<label>Descrição</label>
				<textarea class="limpa" name="descricao" id="subtipo-descricao"></textarea>
			</div>
			<input type="hidden" id="subtipo-idexterno" name="idtipo">
		</form>
	</div>
	<div class="actions">
		<div class="ui cancel button">Cancelar</div>
		<div class="ui green approve button" id="btn-novosubtipo">Adicionar</div>
	</div>
</div>
