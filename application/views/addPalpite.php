   <?php
defined('BASEPATH') OR exit('No direct script access allowed');
$frameworkUrl = $this->config->item('header_framework');
/*error_reporting(0); 
$valor = 6500;
$valorInicio = $valor;
$parcelas =
$juros = 0.01;
for($i = 0; $i < 40; $i++)
{
    echo 'parcela -> '.$valor * $juros."</br>";
    $valor = ($valor) + $valor * $juros;
    $valorInicio = $valor * $juros;
    echo 'total ->'.$valor."<br><br>";
}
echo $valor;
die;*/
?>




<script src="assets/js/pages/addPalpite.js?<?=filemtime('assets/js/pages/addPalpite.js');?>"></script>


<style>
    body, td {
      font-family: Segoe UI;
      font-size: 10pt;
    }
  </style>

<div class="div-item">
<span class="titulo-item" name="663"/></span>
<h1>Wasp-17b</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/03/2019 13:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>11/03/2019 17:00</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Grupo Local, Laniakea, Planeta, Retrógrado, Sistema Wasp-17, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://exoplanet.eu/catalog/wasp-17_b/"><i>http://exoplanet.eu/catalog/wasp-17_b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 3,7 dias terrestres</div><div><span style="font-weight: bold;">Temperatura:</span> ? Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 47.407.000 Milhas - 0,51U</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 99,34 R⊕ | 8,87 RJ</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 247,88 M⊕ | 0,78 MJ'</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> Sódio, água, potássio, carbono e oxigênio</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,02</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 1.300 anos-luz</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÀFICA</span></div><div><span style="font-style: italic;"><span src="assets/img/espaco/Image.png" type="image/png" data-filename="Image.png"/></span></div></span>
</div>
</div>

<hr><div class="div-item">
<span class="titulo-item" name="665"/></span>
<h1>1RXS 1609</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/03/2019 16:35</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>11/03/2019 16:47</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Arrumar, Classificação Estelar K, Estrela, Estrela Única, Laranja, Milhões de Anos</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.openexoplanetcatalogue.com/planet/1RXS1609%20b/"><i>http://www.openexoplanetcatalogue.com/planet/1RXS1609%20b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> K7V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 1,35 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 0,85<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ~ 4 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.060 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 456 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático: </span> ? <span style="color: rgb(34, 34, 34);">anos-luz </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  13,6</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 5 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 10,6 km/s</span></div><div><span style="font-weight: bold;">Composição:</span><span style="font-weight: bold;"> </span>Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/1RXS1609.weblarger.png" type="image/png" data-filename="1RXS1609.weblarger.png"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span style="font-style: italic;"><span src="assets/img/espaco/Scorpius_constellation_map.png" type="image/png" data-filename="Scorpius_constellation_map.png"/></span></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="668"/></span>
<h1>Líridas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 12:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 19:56</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Cometa C/1861 G1 (Thatcher), Complexo de Superaglomerados Pisces-Cetus, Constelação da Lyra, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/lyrids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/lyrids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Cometa C/1861 G1 (Thatcher)</span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação da Lyra</div><div><span style="font-weight: bold;">Melhor visualização:</span> Hemisfério Norte</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (16 de Abril ~ 25 de Abril)</div><div><span style="font-weight: bold;">Pico:</span> ~ 21 de Abril</div><div><span style="font-weight: bold;">Velocidade:</span> 48 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 18/h</div><div><span style="font-weight: bold;">ZHR:</span> 18</div><div><br/></div><div><span src="assets/img/espaco/lyrid-meteor-shower-2012-bryan-emfinger.jpg" type="image/jpeg" data-filename="lyrid-meteor-shower-2012-bryan-emfinger.jpg" style="cursor: default;" width="900"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="670"/></span>
<h1>Perseidas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 12:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 19:56</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Cometa 109P/Swift-Tuttle, Complexo de Superaglomerados Pisces-Cetus, Constelação de Perseu, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/perseids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/perseids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Cometa 109P/Swift-Tuttle     </span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação de Perseu</div><div><span style="font-weight: bold;">Melhor visualização:</span> Hemisfério Norte</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (14 de Julho ~ 24 de Agosto)</div><div><span style="font-weight: bold;">Pico:</span> ~ 12 de Agosto</div><div><span style="font-weight: bold;">Velocidade:</span> 59 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 100/h</div><div><span style="font-weight: bold;">ZHR:</span> 100</div><div><br/></div><div><span src="assets/img/espaco/468_perseids_main.jpg" type="image/jpeg" data-filename="468_perseids_main.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="672"/></span>
<h1>Geminídas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 14:11</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 19:56</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Gêmeos, Grupo Local, Laniakea, Nuvem Interestelar Local, Phaethon, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/geminids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/geminids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Asteroide Phaethon</span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação de Gêmeos</div><div><span style="font-weight: bold;">Melhor visualização:</span> todo o mundo</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (04 de Dezembro ~ 17 de Dezembro)</div><div><span style="font-weight: bold;">Pico:</span> ~ 13 de Dezembro</div><div><span style="font-weight: bold;">Velocidade:</span> 35 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 120/h</div><div><span style="font-weight: bold;">ZHR:</span> 120</div><div><br/></div><div><span src="assets/img/espaco/460_geminids_main.jpg" type="image/jpeg" data-filename="460_geminids_main.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="674"/></span>
<h1>Ultima Thule</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/03/2019 18:03</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 19:48</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão de Kuiper, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, QB1-o, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Transnetuniano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=486958"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=486958</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Número:</span> 486958</div><div><span style="font-weight: bold;">Período de rotação:</span> 15 horas</div><div><span style="font-weight: bold;">Período orbital:</span> 298 anos e 3 meses</div><div><span style="font-weight: bold;">Temperatura:</span>  - 238,15 ºC</div><div><span style="font-weight: bold;">Distância do Sol:</span> 4.145.000.000 Milhas - 44,6AU</div><div><span style="font-weight: bold;">Diâmetro</span><span style="font-weight: bold;">:</span>  ~ 32 km</div><div><span style="font-weight: bold;">Gravidade:</span> ~ 0.0023 m/s²</div><div><span style="font-weight: bold;">Composição:</span> Rocha, basalto e lava congelada</div><div><span style="font-weight: bold;">Velocidade orbital:</span> 4,4 km/s</div><div> </div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; É um corpo binário de contato, ou seja, dois corpos colidiram e se fundiram orbitando como um só</div><div><br/></div><div><span src="assets/img/espaco/Ultima-Thule-Jan23.jpg" type="image/jpeg" data-filename="Ultima-Thule-Jan23.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="676"/></span>
<h1>Apollo</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 13:21</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 18:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo Q, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=1862"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=1862</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Número:</span> 1862</span></div><div><span style="font-weight: bold;">Período de rotação:</span> 6 min</div><div><span style="font-weight: bold;">Período orbital:</span> 1 ano e 9 meses</div><div><span style="font-weight: bold;">Temperatura:</span>  - 51 ºC</div><div><span style="font-weight: bold;">Distância do Sol:</span> 130.000.000 Milhas - 1.4AU</div><div><span style="font-weight: bold;">Diâmetro</span><span style="font-weight: bold;">:</span>  1.5 km</div><div><span style="font-weight: bold;">Gravidade:</span>  0.0005 m/s²</div><div><span style="font-weight: bold;">Composição:</span> metais</div><div><span style="font-weight: bold;">Velocidade orbital:</span>  22.5 km/s</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Possui um satélite(S/2005 (1862) 1) </div><div><br/></div><div>//<span style="font-style: italic;">ARTE CONCEITUAL</span></div><div><span src="assets/img/espaco/1862Apollo_(Lightcurve_Inversion).png" type="image/png" data-filename="1862Apollo_(Lightcurve_Inversion).png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="678"/></span>
<h1>EH1</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 11:28</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 18:17</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Armor, Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Destruído, EH1, Grupo Local, Laniakea, NEO, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=196256"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=196256</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Número:</span> 196256</span></div><div><span style="font-weight: bold;">Período de rotação:</span> ?</div><div><span style="font-weight: bold;">Período orbital:</span> 5 anos e 6 meses</div><div><span style="font-weight: bold;">Temperatura:</span>  - ? ºC (-50)</div><div><span style="font-weight: bold;">Distância do Sol:</span> 290.000.000 Milhas - 3.12AU</div><div><span style="font-weight: bold;">Diâmetro</span><span style="font-weight: bold;">:</span>  3.3 km</div><div><span style="font-weight: bold;">Gravidade:</span> ? m/s² (extremamente fraca)</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><span style="font-weight: bold;">Velocidade orbital:</span>  ? km/s</div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÁFICA DE SUA ÓRBITA</span></div><div><span src="assets/img/espaco/320px-Орбита_астероида_196256_2003_EH1.gif" type="image/gif" data-filename="320px-Орбита_астероида_196256_2003_EH1.gif" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="680"/></span>
<h1>Cinturão Principal de Asteróides</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>22/05/2018 15:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 18:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Disco Circunstelar, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://history.nasa.gov/SP-345/ch4.htm"><i>https://history.nasa.gov/SP-345/ch4.htm</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Distância do sol:</span> 1.3 ~ 3.8 AU</span></div><div><span style="font-weight: bold;">Largura:</span> 1.5 AU</div><div><span style="font-weight: bold;">Massa:</span> 2,93 x 1<span style="font-size: 12.32px; text-align: left; background-color: rgb(249, 249, 249); font-family: sans-serif;">0</span><span style="vertical-align: super; background-color: rgb(249, 249, 249); font-family: sans-serif; line-height: 1; text-align: left;">21 </span></div><div><span style="font-weight: bold;">     (eq. Terras):</span> 0,0004</div><div><span style="font-weight: bold;">Temperatura:</span> 200-165 K</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Quantidade aprox. de asteroides(enumerados): 3.500</div><div><br/></div><div><span src="assets/img/espaco/asteroid-belt.jpg" type="image/jpeg" data-filename="asteroid-belt.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="682"/></span>
<h1>Eta Aquáridas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 12:12</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 17:58</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Cometa 1P/Halley, Complexo de Superaglomerados Pisces-Cetus, Constelação de Aquário, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/eta-aquarids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/eta-aquarids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Cometa 1P/Halley</span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação de Aquarius</div><div><span style="font-weight: bold;">Melhor visualização:</span> Hemisfério Norte e Hemisfério Sul(um pouco melhor)</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (19 de Abril ~ 28 de Maio)</div><div><span style="font-weight: bold;">Pico:</span> ~ 06 de Maio</div><div><span style="font-weight: bold;">Velocidade:</span> 66 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 15/h</div><div><span style="font-weight: bold;">ZHR:</span> 55</div><div><br/></div><div><span src="assets/img/espaco/472_eta_aquarids_main.jpg" type="image/jpeg" data-filename="472_eta_aquarids_main.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="684"/></span>
<h1>Quadrântidas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 11:21</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 17:57</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação do Boieiro, EH1, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/quadrantids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/quadrantids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Asteroide EH1</span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação do Boieiro</div><div><span style="font-weight: bold;">Melhor visualização:</span> Hemisfério Norte</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (28 de Dezembro ~ 12 de Janeiro)</div><div><span style="font-weight: bold;">Pico:</span> ~ 3 de Janeiro</div><div><span style="font-weight: bold;">Velocidade:</span> 41 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 80/h</div><div><span style="font-weight: bold;">ZHR:</span> 120</div><div><br/></div><div><span src="assets/img/espaco/180103-quadrantids-630x394.jpg" type="image/jpeg" data-filename="180103-quadrantids-630x394.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="686"/></span>
<h1>Delta do Sul Aquariids</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 12:38</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 17:57</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Aquário, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/delta-aquariids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/delta-aquariids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Cometa 96P/Machholz (Suspeito)</span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação de Aquarius</div><div><span style="font-weight: bold;">Melhor visualização:</span> Hemisfério Sul e lateralidades do Hemisfério Norte</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (12 de Julho ~ 23 de Agosto)</div><div><span style="font-weight: bold;">Pico:</span> ~ 30 de Julho</div><div><span style="font-weight: bold;">Velocidade:</span> 41 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 20/h</div><div><span style="font-weight: bold;">ZHR:</span> 16</div><div><br/></div><div><span src="assets/img/espaco/15-191.jpg" type="image/jpeg" data-filename="15-191.jpg" style="cursor: default;cursor: default;" width="2559"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="688"/></span>
<h1>Oriónidas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 13:11</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 17:55</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Cometa 1P/Halley, Complexo de Superaglomerados Pisces-Cetus, Constelação de Órion, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/orionids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/orionids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Cometa 1P/Halley</span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação de Órion</div><div><span style="font-weight: bold;">Melhor visualização:</span> Hemisfério Norte e Hemisfério Sul</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (02 de Outubro ~ 07 de Novembro)</div><div><span style="font-weight: bold;">Pico:</span> ~ 21 de Outubro</div><div><span style="font-weight: bold;">Velocidade:</span> 66 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 15/h</div><div><span style="font-weight: bold;">ZHR:</span> 20</div><div><br/></div><div><span src="assets/img/espaco/466_orionids_main.jpg" type="image/jpeg" data-filename="466_orionids_main.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="690"/></span>
<h1>Leónidas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 13:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 17:54</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Cometa 55P/Tempel-Tuttle, Complexo de Superaglomerados Pisces-Cetus, Constelação de Leão, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/leonids/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/meteors-and-meteorites/leonids/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Astro relacionado:</span> Cometa<span style="font-weight: bold;"> </span>55P/Tempel-Tuttle</span></div><div><span style="font-weight: bold;">Localizada:</span> Constelação de Leão</div><div><span style="font-weight: bold;">Melhor visualização:</span> ?</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (06 de Novembro ~ 30 de Novembro)</div><div><span style="font-weight: bold;">Pico:</span> ~ 17 de Novembro</div><div><span style="font-weight: bold;">Velocidade:</span> 71 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 15/h</div><div><span style="font-weight: bold;">ZHR:</span> 15</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; A cada 33 anos ocorre uma tempestade de leónidas em que milhares de meteoros podem ser visto no céu</div><div><br/></div><div><span src="assets/img/espaco/Leonid_meteor_shower_as_seen_from_space_(1997).jpg" type="image/jpeg" data-filename="Leonid_meteor_shower_as_seen_from_space_(1997).jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="692"/></span>
<h1>Sistema Wasp-17</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/03/2019 13:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 13:27</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema Planetário, Sistema Wasp-17, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://wasp-planets.net/tag/wasp-17b/"><i>https://wasp-planets.net/tag/wasp-17b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Estrela:</span> <span style="word-wrap: break-word; -webkit-line-break: after-white-space;">Wasp-17</span></div><div><span style="font-weight: bold;">Planetas Conhecidos:</span> 1</div><div><span style="font-weight: bold;">Distância do disco:</span> &gt; 0,51 UA</div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÀFICA DA ÒRBITA</span></div><div><span src="assets/img/espaco/Image [1].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="694"/></span>
<h1>HD 147873 c [Wazyue]</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/03/2019 20:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 13:03</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema HD 147873, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/3413/hd-147873-c/"><i>https://exoplanets.nasa.gov/newworldsatlas/3413/hd-147873-c/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 491 dias e 12 horas terrestres</div><div><span style="font-weight: bold;">Temperatura:</span> ? Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 126.419.000 Milhas - 1.36U</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> ? R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 731 M⊕ | 2,3 MJ</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,23</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 342 anos-luz</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÀFICA</span></div><div><span style="font-style: italic;"><span src="assets/img/espaco/hd_147873_c.png" type="image/png" data-filename="hd_147873_c.png"/></span></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="696"/></span>
<h1>HD 147873 b [Phonustungr-pal]</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/03/2019 20:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/03/2019 13:03</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema HD 147873, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/3412/hd-147873-b/"><i>https://exoplanets.nasa.gov/newworldsatlas/3412/hd-147873-b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 116 dias e 14 horas terrestres</div><div><span style="font-weight: bold;">Temperatura:</span> ? Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 48.522.000 Milhas - 0.522AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> ? R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 1.634 M⊕ | 5,1 MJ</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,2</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 342 anos-luz</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÀFICA</span></div><div><span src="assets/img/espaco/hd_147873_b.png" type="image/png" data-filename="hd_147873_b.png"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="698"/></span>
<h1>γ2 Normae</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/03/2019 22:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/03/2019 22:50</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca-Amarela, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação da Norma, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/gamma2nor.html"><i>http://stars.astro.illinois.edu/sow/gamma2nor.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="font-weight: bold;">Classificação:</span> K0lll</div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Norma</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 2,16 M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,8 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.699 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:</span> 24.021<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 129 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  4,02</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 1,05</span></div><div><span style="font-weight: bold;">Composição:</span> Hélio e hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [2].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="700"/></span>
<h1>γ Velorum E</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 10:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/03/2019 22:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Vel OB 2, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Sistema Gamma Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astronomytrek.com/star-constellation-facts-vela/"><i>http://www.astronomytrek.com/star-constellation-facts-vela/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <span style="font-family: 'Times New Roman'; text-align: center;">?</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Vela</span></div><b>Raio equatorial:</b> ? R<sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b>Distância do Centro Galático:</b> 24.322<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 840 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  13</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição: </b>?</div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/gammavel_airy.png" type="image/png" data-filename="gammavel_airy.png"/></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><span src="assets/img/espaco/544c7da1e8c724205f95b9a1547ddc2e.jpg" type="image/jpeg" data-filename="544c7da1e8c724205f95b9a1547ddc2e.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Vela_constellation_map.png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="705"/></span>
<h1>Mercúrio</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>20/02/2018 14:15</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Mercúrio, Nuvem Interestelar Local, Planeta, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/mercury/by-the-numbers/"><i>https://solarsystem.nasa.gov/planets/mercury/by-the-numbers/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 59 dias terrestres</span></div><div><span style="font-weight: bold;">Ano:</span> 88 dias terrestres</div><div><span style="font-weight: bold;">Temperatura:</span>166</div><div><span style="font-weight: bold;">Luas:</span> 0</div><div><span style="font-weight: bold;">Distância do Sol:</span> 36.000.000 Milhas - 0.4AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span>  0,3829 R⊕</div><div><span style="font-weight: bold;">Peso:</span>  0.37 da terra</div><div><span style="font-weight: bold;">Massa:</span> 0,055 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span> 3.7 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> principalmente potássio e sódio, contendo também argônio, hélio, nitrogênio, água, dióxido de carbono e hidrogênio</div><div><span style="font-weight: bold;">     [Oxigênio](atômico):</span> 9.5%</div><div>     [<span style="font-weight: bold;">Oxigênio](molecular):</span> 5.6%</div><div><span style="font-weight: bold;">Vel. p/ orb. sol:</span>   170,503 km/h</div><div><b>Excentricidade:</b> 0,205603</div><div><span style="font-weight: bold;">Área de superfície:</span> 74.797.000 km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 0,61 UA</div><div><span style="font-weight: bold;">Oposição:</span> </div><div><span style="font-weight: bold;">Melhor momento de observação:</span> <span style="text-decoration: underline;">Amanhecer</span>{<span style="color: rgb(250, 122, 0);">09-14(Agosto) | 21(Novembro)-07(Dezembro)</span>}, <span style="text-decoration: underline;">Anoitecer</span>{<span style="color: rgb(26, 173, 224);">15(Fevereiro)-03(Março) | 08-26(Junho)</span>}</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Planeta extremamente quente quando está com um lado do sol</div><div>&gt; Planeta extremamente frio no lado sem o sol, pois sua atmosfera não guarda calor</div><div>&gt; Alta radiação</div><div>&gt; Pouco oxigênio</div><div>&gt; Bombardeado por asteroides pois não tem atmosfera</div><div><br/></div><div><span style="font-weight: bold;">----- Fenômenos -----</span></div><div>&gt; Terremotos pela gravidade do sol</div><div>&gt; Na noite teria show de luzes dos raios do sol</div><div>&gt; Frequente chuva de meteoros</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Terrestre</div><div>&gt; Planeta mais perto do sol</div><div>&gt; Menor planeta do sistema solar</div><div>&gt; Sem atmosfera praticamente</div><div>&gt; Lado do sol muito quente, lado sem sol muito frio</div><div>&gt; Magnetismo forte parecido com a terra</div><div>&gt; Planeta com mais crateras do sistema solar</div><div><br/></div><div><span src="assets/img/espaco/439_MercurySubtleColors1200w.jpg" type="image/jpeg" data-filename="439_MercurySubtleColors1200w.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/440_UnmaskingMercury1000w (1).jpg" type="image/jpeg" data-filename="440_UnmaskingMercury1000w (1).jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="750"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="708"/></span>
<h1>Júpiter</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>15/02/2018 13:12</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:21</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Gigante Gasoso, Grupo Local, Júpiter, Laniakea, Nuvem Interestelar Local, Planeta, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/jupiter/by-the-numbers/"><i>https://solarsystem.nasa.gov/planets/jupiter/by-the-numbers/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 9h 50min</span></div><div><span style="font-weight: bold;">Ano:</span> 12 anos terrestre</div><div><span style="font-weight: bold;">Temperatura:</span> [-108 ºC]</div><div><span style="font-weight: bold;">Luas:</span> 53(talvez 69)</div><div><span style="font-weight: bold;">Distância do Sol:</span> 778.000.000 Milhas - 5.2AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 11,2 R⊕</div><div><span style="font-weight: bold;">Peso:</span> 2.53 da terra</div><div><span style="font-weight: bold;">Massa:</span> 317,8 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span> 24.79 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> principalmente hidrogênio e hélio, um pouco de vapor de água, fósforo, amônia e metano</div><div><span style="font-weight: bold;">Vel. p/ orb. sol:</span> 107.118 km/h</div><div><b>Excentricidade:</b> 0,0489</div><div><span style="font-weight: bold;">Área de superfície:</span> 61.418.738.571 km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 4,2 UA    </div><div><span style="font-weight: bold;">Oposição:</span> 10 de Junho de 2019</div><div><br/></div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Maior parte sendo atmosfera feita apenas de gás com núcleo sólido extremamente quente</div><div>&gt; Sem oxigênio</div><div>&gt; Sem solo</div><div><br/></div><div><span style="font-weight: bold;">----- Fenômenos -----</span></div><div>&gt; Super tempestades do tamanho da terra</div><div>&gt; Furações gigantes que estão em atividade por mais de 100 anos</div><div>&gt; Tempestades coloridas como auroras boreais </div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Gigante gasoso</div><div>&gt; Primeiro planeta do sistema solar</div><div>&gt; Maior planeta do sistema solar</div><div>&gt; Júpiter também possui anéis porém são muito inferiores aos de saturno</div><div><br/></div><div><span src="assets/img/espaco/113_PIA02873.jpg" type="image/jpeg" data-filename="113_PIA02873.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1920"/></div><div><br/></div><div><span src="assets/img/espaco/tumblr_n3ebihzocs1qakzwwo1_500.gif" type="image/gif" data-filename="tumblr_n3ebihzocs1qakzwwo1_500.gif" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/627_PIA21382.jpg" type="image/jpeg" data-filename="627_PIA21382.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1510"/></div><div><br/></div><div><span src="assets/img/espaco/613_PIA21970_1200w.jpg" type="image/jpeg" data-filename="613_PIA21970_1200w.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="713"/></span>
<h1>Gliese 667 C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 20:33</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 20:40</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Gliese 667, Sistema Gliese 667 C, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402361576&Name=HD%20156384C&submit=submit"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402361576&amp;Name=HD%20156384C&amp;submit=submit</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> M1.5V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio:</span> 0,42 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 0,31<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 3.700 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 23,2 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  10,2</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 12,03</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 2-10 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Metais neutros e dióxidos de Titânio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Gliese667-062913-0324ut-L11mRGB7m-B-EMr.jpg" type="image/jpeg" data-filename="Gliese667-062913-0324ut-L11mRGB7m-B-EMr.jpg"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/Scorpius_constellation_map [1].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="716"/></span>
<h1>Sistema Alpha Virginis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 18:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 19:14</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Virgem, Grupo Local, Laniakea, Sistema Alpha Virginis, Sistema Binário de Estrelas, Sistema Estelar, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/spica.html"><i>http://stars.astro.illinois.edu/sow/spica.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Espiga A e Espiga B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Espiga A</div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.025 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância da Terra:</b> 262 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b><br/></b></span></div><div><span style="color: rgb(34, 34, 34);"><b>Órbita de Espiga B:</b> 4.014 dias<b> </b>[0.12 UA]</span></div><div><br/></div><div><span src="assets/img/espaco/download.jpg" type="image/jpeg" data-filename="download.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span style="font-style: italic;"><span src="assets/img/espaco/VIR.gif" type="image/gif" data-filename="VIR.gif"/></span></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="719"/></span>
<h1>Espiga B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 19:04</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 19:06</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Azul-Branca, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Virgem, Estrela, Grupo Local, Laniakea, Sistema Alpha Virginis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.constellation-guide.com/spica/"><i>https://www.constellation-guide.com/spica/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> B2 V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Virgem</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 3,74 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 7,21<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,15 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 20.900 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 250 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -1,5</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 58 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/spica.jpg" type="image/jpeg" data-filename="spica.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/VIR [1].gif" type="image/gif" data-filename="VIR.gif"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="722"/></span>
<h1>Espiga A</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 17:46</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 19:04</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Virgem, Estrela, Gigante, Grupo Local, Laniakea, Milhões de Anos, Sistema Alpha Virginis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/spica.html"><i>http://stars.astro.illinois.edu/sow/spica.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B1 llll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Virgem</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 7,47 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 11,43<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,71 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.300 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 250 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,97</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,55</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 12,5 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 165 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/spica [1].jpg" type="image/jpeg" data-filename="spica.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/VIR [2].gif" type="image/gif" data-filename="VIR.gif"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="725"/></span>
<h1>β Scorpii Eb</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 18:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:53</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Beta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://adsabs.harvard.edu/abs/2010A&A...509A..21C"><i>http://adsabs.harvard.edu/abs/2010A&amp;A...509A..21C</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span> ?</div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Massa:</span> ?<span style="vertical-align: super; font-size: smaller; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.774 anos-luz    </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 400 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Graffias.jpg" type="image/jpeg" data-filename="Graffias.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia.jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="728"/></span>
<h1>β Scorpii Ea</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 18:48</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:51</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass US, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Beta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://adsabs.harvard.edu/abs/2010A&A...509A..21C"><i>http://adsabs.harvard.edu/abs/2010A&amp;A...509A..21C</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span> B</div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,4 R☉</div><div><span style="font-weight: bold;">Massa:</span> 3,5<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,2 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 24.000 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.774 anos-luz    </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 400 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 5 km/s</div><div><span style="font-weight: bold;">Composição:</span> Manganês, Mercúrio, Fósforo, Sílicio, hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Graffias [1].jpg" type="image/jpeg" data-filename="Graffias.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [1].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="731"/></span>
<h1>β Scorpii C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 18:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:46</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass US, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Beta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/graffias.html"><i>http://stars.astro.illinois.edu/sow/graffias.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span> B2V</div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,9 R☉</div><div><span style="font-weight: bold;">Massa:</span> 8<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,8 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 24.000 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.774 anos-luz    </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 400 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 55 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Graffias [2].jpg" type="image/jpeg" data-filename="Graffias.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [2].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="734"/></span>
<h1>β Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 18:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:44</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Beta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://adsabs.harvard.edu/abs/1975ApJ...200...61V"><i>http://adsabs.harvard.edu/abs/1975ApJ...200...61V</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span> <span style="word-wrap: break-word; -webkit-line-break: after-white-space;">?</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R☉</div><div><span style="font-weight: bold;">Massa:</span> 8<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.774 anos-luz    </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 400 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Graffias [3].jpg" type="image/jpeg" data-filename="Graffias.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [3].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="737"/></span>
<h1>β Scorpii Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 18:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:43</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass US, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Beta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://adsabs.harvard.edu/abs/2010A&A...509A..21C"><i>http://adsabs.harvard.edu/abs/2010A&amp;A...509A..21C</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação: </span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">B1.5</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 4 R☉</div><div><span style="font-weight: bold;">Massa:</span> 10,4<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 26.400 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.774 anos-luz    </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 400 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 6 milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Graffias [4].jpg" type="image/jpeg" data-filename="Graffias.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [4].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="740"/></span>
<h1>Acrab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 15:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:34</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Beta Scorpii, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=beta+Sco"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=beta+Sco</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B0.5</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6,3 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Massa:</span> 15<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,8 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 28.000 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.774 anos-luz    </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 400 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  4,92</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -1,60</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 6 milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/zaaz5_iw_400x400.jpg" type="image/jpeg" data-filename="zaaz5_iw_400x400.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [5].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="743"/></span>
<h1>Sistema Beta Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 13:55</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:31</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Beta Scorpii, Sistema Estelar, Sistema Múltiplo, Sistema Sêxtuplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/Beta_Scorpii.html"><i>https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/Beta_Scorpii.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Acrab, β Scorpii Ab, β Scorpii B, β Scorpii C, β Scorpii Ea, β Scorpii Eb</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Acrab</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.774 anos-luz</span></div><div><span style="font-weight: bold;">Distância da Terra: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">400 </span><span style="color: rgb(34, 34, 34);">anos-luz</span><span style="color: rgb(34, 34, 34);">    </span><span style="color: rgb(34, 34, 34);"> </span></div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/beta-scorpio-sketch-s.jpg" type="image/jpeg" data-filename="beta-scorpio-sketch-s.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">REPRESENTAÇÃO DA ESTRUTURA DO SISTEMA</span></div><div><span src="assets/img/espaco/Image [3].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [6].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="747"/></span>
<h1>Sistema Alpha Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 18:00</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 18:03</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Bolha Loop I, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Alpha Scorpii, Sistema Binário de Estrelas, Sistema Estelar, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/antares.html"><i>http://stars.astro.illinois.edu/sow/antares.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Estrelas:</span> <span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">Antares e </span>α <span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">Scorpii B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Antares</div><div><span style="font-weight: bold;">Distância do disco:</span> 530 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.624 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Sol:</span> <span style="color: rgb(34, 34, 34);">600 anos-luz</span></div><div><br/></div><div><span style="font-weight: bold;">Órbita </span><span style="font-weight: bold;">α Scorpii</span><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;"> B:</span> 1.200-2.562 anos <span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">[530 UA]</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Giuseppe-Donatiello-Antares-20140702-DEF-dida_1404301254_lg.png" type="image/png" data-filename="Giuseppe-Donatiello-Antares-20140702-DEF-dida_1404301254_lg.png"/></div><div><br/></div><div>// LOCALIZAÇÃO DO SISTEMA</div><div><span src="assets/img/espaco/Scorpius_constellation_map [2].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="750"/></span>
<h1>α Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 17:47</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:58</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass US, Azul-Branca, Bolha Loop I, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Alpha Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://spaceweathergallery.com/indiv_upload.php?upload_id=98905"><i>http://spaceweathergallery.com/indiv_upload.php?upload_id=98905</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> B2.5</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> <span style="background-color: rgb(249, 249, 249);">Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 5,2 R☉</div><div><span style="font-weight: bold;">Massa:</span> 7,2<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,9 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 18.500 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.624</span><span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span> <span style="color: rgb(34, 34, 34);">600 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ?  anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 250 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Giuseppe-Donatiello-Antares-20140702-DEF-dida_1404301254_lg [1].png" type="image/png" data-filename="Giuseppe-Donatiello-Antares-20140702-DEF-dida_1404301254_lg.png"/></div><div><br/></div><div>// LOCALIZAÇÃO DA ESTRELA</div><div><span src="assets/img/espaco/Scorpius_constellation_map [3].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="753"/></span>
<h1>Antares</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 10:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:51</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Bolha Loop I, Braço de Órion, Cinturão de Gould, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Alpha Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Vermelha, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Antares"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Antares</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(249, 249, 249); text-align: left;">M1,5Iab-b</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 883 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; background-color: rgb(249, 249, 249); text-align: left; vertical-align: sub; color: rgb(11, 0, 128); line-height: 1; text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 15,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,9 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 3.500 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.624 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span> <span style="color: rgb(34, 34, 34);">600 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,09</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -5,28</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 11 Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 20 km/s</div><div><span style="font-weight: bold;">Composição:</span> Metais neutros e dióxidos de Titânio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/eso1726a-Antares-VTLI-ESO-1080x1080.jpg" type="image/jpeg" data-filename="eso1726a-Antares-VTLI-ESO-1080x1080.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Antares.jpg" type="image/jpeg" data-filename="Antares.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/1_pl535_volta-ao-mundo13.jpg" type="image/jpeg" data-filename="1_pl535_volta-ao-mundo13.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/></div><div><br/></div><div>// LOCALIZAÇÃO DA ESTRELA</div><div><span src="assets/img/espaco/Scorpius_constellation_map [4].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="758"/></span>
<h1>Cinturão de Kuiper</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>22/05/2018 20:39</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão de Kuiper, Complexo de Superaglomerados Pisces-Cetus, Disco Circunstelar, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Transnetuniano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/solar-system/kuiper-belt/overview/"><i>https://solarsystem.nasa.gov/solar-system/kuiper-belt/overview/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><br/></div><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Distância do sol:</span> 30 ~ 55 AU</span></div><div><span style="font-weight: bold;">Largura:</span> 25 AU</div><div><span style="font-weight: bold;">Massa:</span> 5,9736 x 1<span style="font-size: 12.32px; text-align: left; background-color: rgb(249, 249, 249); font-family: sans-serif;">0</span><span style="vertical-align: super; background-color: rgb(249, 249, 249); font-family: sans-serif; line-height: 1; text-align: left;">23 </span></div><div><span style="font-weight: bold;">     (eq. Terras):</span> 0,1</div><div><span style="font-weight: bold;">Temperatura média:</span> 50 K</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Estima-se mais de 100 mil corpos e trilhões de cometas no cinturão </div><div>&gt; Muitos corpos congelados</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/399_KuiperBeltinDepth1200w.jpg" type="image/jpeg" data-filename="399_KuiperBeltinDepth1200w.jpg" style="cursor: default;cursor: default;cursor: default;" width="1200"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="761"/></span>
<h1>Gamma Normídeas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/03/2019 21:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/03/2019 22:19</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Chuva de Meteoros, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Norma, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://books.google.com.au/books?id=t026BAAAQBAJ&pg=PA61&lpg=PA61&dq=gamma+normids&source=bl&ots=07PVtZdxUu&sig=0ys3YDjyyqrtBXdpKT5cl_c9-yY&hl=en&sa=X&redir_esc=y#v=onepage&q=gamma%20normids&f=false"><i>https://books.google.com.au/books?id=t026BAAAQBAJ&amp;pg=PA61&amp;lpg=PA61&amp;dq=gamma+normids&amp;source=bl&amp;ots=07PVtZdxUu&amp;sig=0ys3YDjyyqrtBXdpKT5cl_c9-yY&amp;hl=en&amp;sa=X&amp;redir_esc=y#v=onepage&amp;q=gamma%20normids&amp;f=false</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Astro relacionado:</span>?</div><div><span style="font-weight: bold;">Localizada:</span> Constelação de Norma</div><div><span style="font-weight: bold;">Melhor visualização:</span> Hemisfério Sul</div><div><span style="font-weight: bold;">Ocorre:</span> ~ (07 de Março ~ 23 de Março)</div><div><span style="font-weight: bold;">Pico:</span> ~ 15 de Março</div><div><span style="font-weight: bold;">Velocidade:</span> 68 km/s</div><div><span style="font-weight: bold;">Frequência da chuva no pico:</span> 6/h</div><div><span style="font-weight: bold;">ZHR:</span> 1-2</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/8561661837_38d000ab7f_b.jpg" type="image/jpeg" data-filename="8561661837_38d000ab7f_b.jpg"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="763"/></span>
<h1>Constelação de Norma</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/03/2019 21:52</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/03/2019 22:00</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação da Norma, Hemisfério Sul, NGQ4</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Nome:</span> Norma, o Esquadro</div><div><span style="font-weight: bold;">Genitivo:</span><span style="color: rgb(34, 34, 34); font-family: sans-serif;"> Normae</span></div><div><span style="font-weight: bold;">Abreviação:</span> Nor</div><div><span style="font-weight: bold;">Área Total:</span> 165º quadrados </div><div><span style="font-weight: bold;">Quadrante:</span> NGQ4</div><div style="text-align: left;"><span style="font-weight: bold;">Estrelas Principais:</span> 4</div><div style="text-align: left;"><span style="font-weight: bold;">Estrelas Secundárias:</span> 13</div><div style="text-align: left;"><span style="font-weight: bold;">Meridiano:</span> Julho</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Constellation_Norma.jpg" type="image/jpeg" data-filename="Constellation_Norma.jpg"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Image [4].png" type="image/png" data-filename="Image.png"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/610px-Norma_IAU.svg.png" type="image/png" data-filename="610px-Norma_IAU.svg.png"/><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="767"/></span>
<h1>Wasp-17</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/03/2019 20:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>06/03/2019 21:00</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Braço de Órion, Branca-Amarela, Classificação Estelar F, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Wasp-17, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=WASP-17"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=WASP-17</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> F6V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 1,38 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,2<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,14 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 6.509 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 1.300 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.813<span style="color: rgb(34, 34, 34);"> anos-luz </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  11,5</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 3 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 10,6 km/s</span></div><div><span style="font-weight: bold;">Composição:</span><span style="font-weight: bold;"> </span>Hidrogênio, ferro e metais ionizados</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [5].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><i>// LOCALIZAÇÃO DA ESTRELA<span> </span></i></div><div><i><span src="assets/img/espaco/Scorpius_constellation_map [5].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></i></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="770"/></span>
<h1>Sistema HD 147873</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/03/2019 20:38</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>06/03/2019 20:40</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema HD 147873, Sistema Planetário, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.openexoplanetcatalogue.com/planet/HD%20147873%20c/"><i>http://www.openexoplanetcatalogue.com/planet/HD%20147873%20c/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Estrela:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> HD 147873</span></div><div><span style="font-weight: bold;">Planetas Conhecidos:</span> 2</div><div><span style="font-weight: bold;">Distância do disco:</span> &gt; 1,36 UA</div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÁFICA DA ÒRBITA</span></div><div><span src="assets/img/espaco/Image [6].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="772"/></span>
<h1>HD 147873</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/03/2019 11:52</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/03/2019 12:08</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Amarela, Anã, Bilhões de Anos, Braço de Órion, Classificação Estelar G, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema HD 147873, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> G1V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,17 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,38<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.972 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 342 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.813<span style="color: rgb(34, 34, 34);"> anos-luz </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  7,96</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 3</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 2,4 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 0,96 km/s</span></div><div><span style="font-weight: bold;">Composição:</span><span style="font-weight: bold;"> </span>Hidrogênio, metais ionizados e metais neutros</div><div><br/></div><div><br/></div><div><i><span src="assets/img/espaco/Image [7].png" type="image/png" data-filename="Image.png"/></i></div><div><span style="font-style: italic;"><br/></span></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA   </span></div><div>?</div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="774"/></span>
<h1>HD 159868 c [Shino]</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>03/03/2019 23:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/03/2019 00:26</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema HD 159868, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/1183/hd-159868-c/"><i>https://exoplanets.nasa.gov/newworldsatlas/1183/hd-159868-c/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 351 dias</div><div><span style="font-weight: bold;">Temperatura:</span> ? Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 9.593.000 Milhas - 1.032AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> ? R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 244 M⊕ | 0,768 MJ</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,184</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 182 anos-luz</div><div><br/></div><div><br/></div><div>// REPRESENTAÇÃO GRÁFICA</div><div><span src="assets/img/espaco/hd_159868_c.png" type="image/png" data-filename="hd_159868_c.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="776"/></span>
<h1>Sistema HD 159868</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/03/2019 00:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/03/2019 00:11</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema HD 159868, Sistema Planetário, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=LTT++7019&NbIdent=query_hlinks&Coord=17+38+59.5264302446-43+08+43.844349464&children=2&submit=children&hlinksdisplay=h_all"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=LTT++7019&amp;NbIdent=query_hlinks&amp;Coord=17+38+59.5264302446-43+08+43.844349464&amp;children=2&amp;submit=children&amp;hlinksdisplay=h_all</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Estrela:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> HD 159868</span></div><div><span style="font-weight: bold;">Planetas Conhecidos:</span> 2</div><div><span style="font-weight: bold;">Distância do disco:</span> &gt; 2,32 UA</div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÀFICA DA ÒRBITA</span></div><div><span style="font-style: italic;"><span src="assets/img/espaco/Image [8].png" type="image/png" data-filename="Image.png"/></span></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="778"/></span>
<h1>HD 159868 b [Mirsi Phaë]</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>03/03/2019 23:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>03/03/2019 23:41</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema HD 159868, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/293/hd-159868-b/"><i>https://exoplanets.nasa.gov/newworldsatlas/293/hd-159868-b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 3 anos e 2 meses</div><div><span style="font-weight: bold;">Temperatura:</span> ? Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 215.657.000 Milhas - 3.32AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> ? R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 704 M⊕ | 2,218 M<a href="https://en.wikipedia.org/wiki/Jupiter_mass" style="background: none rgb(248, 249, 250); letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; vertical-align: sub; font-size: smaller; font-size: smaller; color: rgb(11, 0, 128); font-family: sans-serif; font-variant-caps: normal; font-variant-ligatures: normal; line-height: 1;" title="Jupiter mass">J</a></div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,024</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 182 anos-luz</div><div><br/></div><div><br/></div><div>// REPRESENTAÇÃO GRÁFICA</div><div><span src="assets/img/espaco/hd_159868_b.png" type="image/png" data-filename="hd_159868_b.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="780"/></span>
<h1>HD 159868</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>03/03/2019 22:42</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>03/03/2019 23:22</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Amarela, Anã, Bilhões de Anos, Braço de Órion, Classificação Estelar G, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Grupo Local, Laniakea, Sistema HD 159868, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+159868"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+159868</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> G5V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,1 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,13<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,85 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.583 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 182 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.950<span style="color: rgb(34, 34, 34);"> anos-luz </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  7,24</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 3,6</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 6 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 0,96 km/s</span></div><div><span style="font-weight: bold;">Composição:</span><span style="font-weight: bold;"> </span>Hidrogênio, metais ionizados e metais neutros</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [9].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA   </span></div><div><span src="assets/img/espaco/Scorpius_constellation_map [6].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="783"/></span>
<h1>Gliese 667 B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 20:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>03/03/2019 23:13</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Laranja, Sistema Gliese 667, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402361579&Name=HD%20156384B&submit=submit"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402361579&amp;Name=HD%20156384B&amp;submit=submit</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> K4V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 0,7 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 0,69<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 23,2 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  7,38</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 8,02</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 6 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s </span></div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Gliese667-062913-0324ut-L11mRGB7m-B-EMr [1].jpg" type="image/jpeg" data-filename="Gliese667-062913-0324ut-L11mRGB7m-B-EMr.jpg"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/Scorpius_constellation_map [7].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="786"/></span>
<h1>Sistema Póllux</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 12:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 14:38</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Gêmeos, Estrela Única, Grupo Local, Laniakea, Sistema Estelar, Sistema Planetário, Sistema Póllux, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://exoplanetes.esep.pro/index.php/br/quelques-questions-br/quel-est-le-nom-des-exoplanetes-br"><i>http://exoplanetes.esep.pro/index.php/br/quelques-questions-br/quel-est-le-nom-des-exoplanetes-br</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrela:</span> Póllux</span></div><div><span style="font-weight: bold;">Planetas Conhecidos:</span> 1</div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><br/></div><div><i>// REPRESENTAÇÂO GRÀFICA DA ÒRBITA</i></div><div><span src="assets/img/espaco/Image [10].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="788"/></span>
<h1>Sistema Hamal</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 11:35</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 14:33</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Áries, Estrela Única, Grupo Local, Laniakea, Sistema Estelar, Sistema Hamal, Sistema Planetário, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/hamal"><i>https://www.universeguide.com/star/hamal</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrela:</span> Hamal</span></div><div><span style="font-weight: bold;">Planetas Conhecidos:</span> 1</div><div><span style="font-weight: bold;">Distância do disco:</span> ? </div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO DA ÓRBITA DO PLANETA</span></div><div><span src="assets/img/espaco/Image [11].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="790"/></span>
<h1>Sistema Tarf</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 19:10</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 14:29</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Câncer, Estrela Única, Grupo Local, Laniakea, Sistema Estelar, Sistema Planetário, Sistema Tarf, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/altarf"><i>https://www.universeguide.com/star/altarf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrela:</span> Tarf</span></div><div><span style="font-weight: bold;">Planetas Conhecidos:</span> 1</div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO DA ÓRBITA DO PLANETA</span></div><div><span src="assets/img/espaco/Image [12].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="792"/></span>
<h1>GJ 667 Cd</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/03/2019 13:48</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 14:10</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Grupo Local, Laniakea, Planeta, Sistema Gliese 667, Sistema Gliese 667 C, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="about:blank#blocked"><i>about:blank#blocked</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 91 dias e 14 horas</div><div><span style="font-weight: bold;">Temperatura:</span> - 67 Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 25.655.000 Milhas - 0.276AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 2 R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 5,1 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,03</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 23,2 anos-luz</div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÀFICA</span></div><div><span src="assets/img/espaco/Image [13].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><span src="assets/img/espaco/gl667c2s.jpg" type="image/jpeg" data-filename="gl667c2s.jpg"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="795"/></span>
<h1>GJ 667 Cf</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/03/2019 12:21</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 13:50</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Grupo Local, Laniakea, Planeta, Sistema Gliese 667, Sistema Gliese 667 C, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/1126/gj-667-c-f/"><i>https://exoplanets.nasa.gov/newworldsatlas/1126/gj-667-c-f/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 39 dias</div><div><span style="font-weight: bold;">Temperatura:</span> - 14 Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 14.501.000 Milhas - 0.156AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 1,4 R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 2,7 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,03</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 23,2 anos-luz</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÂO GRÀFICA</span></div><div><span src="assets/img/espaco/a5e1ddac00f8244abfeac19ffe9b15c3--space-facts-planets.jpg" type="image/jpeg" data-filename="a5e1ddac00f8244abfeac19ffe9b15c3--space-facts-planets.jpg"/></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/sunsets.jpg" type="image/jpeg" data-filename="sunsets.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="798"/></span>
<h1>Sistema Gliese 667 C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/03/2019 12:47</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 13:36</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema Gliese 667, Sistema Gliese 667 C, Sistema Planetário, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.solstation.com/stars/mlo4abc.htm#mlo4c"><i>http://www.solstation.com/stars/mlo4abc.htm#mlo4c</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Estrela:</span> Gliese 667 C</div><div><span style="font-weight: bold;">Planetas Conhecidos:</span> 5</div><div><span style="font-weight: bold;">Distância do disco:</span> &gt; 0,549 UA</div><div><br/></div><div><i>// REPRESENTAÇÂO GRÀFICA DO SISTEMA</i></div><div><span src="assets/img/espaco/Gliese_667_system.jpg" type="image/jpeg" data-filename="Gliese_667_system.jpg"/></div><div><br/></div><div><span src="assets/img/espaco/gl667c1o.jpg" type="image/jpeg" data-filename="gl667c1o.jpg"/></div><div><span src="assets/img/espaco/gl667co1.jpg" type="image/jpeg" data-filename="gl667co1.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="802"/></span>
<h1>Sistema Gliese 667</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 20:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 12:49</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema Estelar, Sistema Gliese 667, Sistema Múltiplo, Sistema Ternário de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=CCDM+J17190-3459ABC&NbIdent=query_hlinks&Coord=17+18+56.4-34+59+22&children=3&submit=children&hlinksdisplay=h_all"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=CCDM+J17190-3459ABC&amp;NbIdent=query_hlinks&amp;Coord=17+18+56.4-34+59+22&amp;children=3&amp;submit=children&amp;hlinksdisplay=h_all</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; word-wrap: break-word; -webkit-line-break: after-white-space; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Estrelas:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; word-wrap: break-word; -webkit-line-break: after-white-space; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> </span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; word-wrap: break-word; -webkit-line-break: after-white-space; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">Gliese 667 A, Gliese 667 B e Gliese 667 C</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Estrela Principal:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> Gliese 667 A</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Distância do disco:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> 230 UA</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Período Orbital Galático:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> ?</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Velocidade Orbital Galática:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> ?</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Distância do Centro Galático: </span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(0, 0, 0); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">~</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;"> </span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">?</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> anos-luz</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Distância da Terra:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> </span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">23,2 anos-luz</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><br style="box-sizing: border-box;"/></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Órbita de Gliese 667 B:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> </span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">42,15 anos</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;"> </span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">[12 UA]  </span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal; font-weight: bold;">Órbita de Gliese 667 C:</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> ? </span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">[230 UA]</span><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;">   </span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><br/></span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><br/></span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="font-size: medium;"><span src="assets/img/espaco/Image [14].png" type="image/png" data-filename="Image.png"/></span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; color: rgb(34, 34, 34); font-family: Tahoma; font-variant-caps: normal; font-variant-ligatures: normal;"> </span></div><div style="text-align: start;"><span src="assets/img/espaco/Gliese667-062913-0324ut-L11mRGB7m-B-EMr [2].jpg" type="image/jpeg" data-filename="Gliese667-062913-0324ut-L11mRGB7m-B-EMr.jpg"/></div><div style="text-align: start;"><br/></div><div style="text-align: start;"><span style="font-style: italic;">// LOCALIZAÇÃO DO SISTEMA</span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="font-size: medium;"><span src="assets/img/espaco/Scorpius_constellation_map [8].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></span></div><div style="box-sizing: border-box; margin: 0px; padding: 0px; font-size: medium; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><span style="box-sizing: border-box; font-size: medium; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><br/></span></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="806"/></span>
<h1>GJ 667 Cg</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/03/2019 12:39</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 12:43</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema Gliese 667, Sistema Gliese 667 C, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 256 dias e 4 horas</div><div><span style="font-weight: bold;">Temperatura:</span> - 116 Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 51.032.000 Milhas - 0.549AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 1,9 R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 4,6 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,08</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 23,2 anos-luz</div><div><br/></div><div><i>// REPRESENTAÇÂO GRÀFICA</i></div><div><span src="assets/img/espaco/Image [15].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="808"/></span>
<h1>Gliese 667 A</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 19:47</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 12:32</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Laranja, Sistema Gliese 667, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402361578&Name=HD%20156384A&submit=submit"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402361578&amp;Name=HD%20156384A&amp;submit=submit</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> K3V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 0,76 R☉</div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 0,73<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 23,2 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  6,37</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 7,07</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 6 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição:</span> pouca concentração de hélio e hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/800px-The_sky_around_the_star_Gliese_667.jpg" type="image/jpeg" data-filename="800px-The_sky_around_the_star_Gliese_667.jpg"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span style="font-style: italic;"><span src="assets/img/espaco/Scorpius_constellation_map [9].png" type="image/png" data-filename="Scorpius_constellation_map.png"/></span></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="811"/></span>
<h1>GJ 667 Ce</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/03/2019 11:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 12:23</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Grupo Local, Laniakea, Planeta, Sistema Gliese 667, Sistema Gliese 667 C, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/4945/gj-667-c-e/"><i>https://exoplanets.nasa.gov/newworldsatlas/4945/gj-667-c-e/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> ?</div><div><span style="font-weight: bold;">Ano:</span> 62 dias e 4 horas</div><div><span style="font-weight: bold;">Temperatura:</span> - 43 Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 19.799.000 Milhas - 0.156AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 1,5 R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 2,7 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,02</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 23,2 anos-luz</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/capa-1-696x335.png" type="image/png" data-filename="capa-1-696x335.png"/></div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO DO POR DO SOL COMPARADO COM O SOL</span></div><div><span src="assets/img/espaco/80513059826115135.jpg" type="image/jpeg" data-filename="80513059826115135.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="814"/></span>
<h1>GJ 667 Cc</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/03/2019 11:04</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 11:39</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Grupo Local, Laniakea, Planeta, Sistema Gliese 667, Sistema Gliese 667 C, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/1125/gj-667-c-c/"><i>https://exoplanets.nasa.gov/newworldsatlas/1125/gj-667-c-c/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> 28 dias e 2 horas</div><div><span style="font-weight: bold;">Ano:</span> 28 dias e 2 horas</div><div><span style="font-weight: bold;">Temperatura:</span> 4,3 Cº</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 11.619.000 Milhas - 0.125AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 1,52 R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 3,8 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,02</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 23,2 anos-luz</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; O planeta se comporta como a lua com a mesma face sempre para a estrela, sempre dia e sempre noite nos lados</div><div><br/></div><div><span src="assets/img/espaco/SER_Gliese667Cc_With_Stars2.jpg" type="image/jpeg" data-filename="SER_Gliese667Cc_With_Stars2.jpg"/></div><div><br/></div><div><span src="assets/img/espaco/667cc2s.jpg" type="image/jpeg" data-filename="667cc2s.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="817"/></span>
<h1>GJ 667 Cb</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 22:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 11:33</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema Gliese 667, Sistema Gliese 667 C, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.solstation.com/stars/mlo4abc.htm#mlo4c-b"><i>http://www.solstation.com/stars/mlo4abc.htm#mlo4c-b</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Dia:</span> 7 dias terrestres e 4h</div><div><span style="font-weight: bold;">Ano:</span> 7 dias terrestres  e 4h</div><div><span style="font-weight: bold;">Temperatura:</span> (200 Cº)</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 4.647.000 Milhas - 0.05AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 1-4 R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 5,6 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,112</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 23,2 anos-luz</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; O planeta se comporta como a lua com a mesma face sempre para a estrela, sempre dia e sempre noite nos lados</div><div><br/></div><div><span src="assets/img/espaco/Gliese_667.jpg" type="image/jpeg" data-filename="Gliese_667.jpg"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="820"/></span>
<h1>Warm Dust(Próxima Centauri)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>10/07/2018 11:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Disco Circunstelar, Grupo Local, Laniakea, Nuvem G, Sistema Alpha Centauri, Sistema Próxima Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Distância da estrela:</span> ~ 0.4 AU</span></div><div><span style="font-weight: bold;">Largura:</span> ~ 0.1 AU</div><div><span style="font-weight: bold;">Massa:</span> ?</div><div><span style="font-weight: bold;">Temperatura:</span> ?</div><div><br/></div><div><span style="font-style: italic;">//CONCEPÇÃO ARTÍSTICA</span></div><div><span src="assets/img/espaco/download [1].jpg" type="image/jpeg" data-filename="download.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="822"/></span>
<h1>Outer Belt(Próxima Centauri)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>10/07/2018 12:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Disco Circunstelar, Grupo Local, Laniakea, Nuvem G, Sistema Alpha Centauri, Sistema Próxima Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Distância da estrela:</span> ~30 AU</span></div><div><span style="font-weight: bold;">Largura:</span> ~ 1 AU</div><div><span style="font-weight: bold;">Massa:</span> 0.00001 da terra</div><div><span style="font-weight: bold;">Temperatura:</span> ?</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//CONCEPÇÃO ARTÍSTICA</span></div><div><span src="assets/img/espaco/download [2].jpg" type="image/jpeg" data-filename="download.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="824"/></span>
<h1>Cold Belt(Próxima Centauri)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>10/07/2018 12:12</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Disco Circunstelar, Grupo Local, Laniakea, Nuvem G, Sistema Alpha Centauri, Sistema Próxima Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Distância da estrela:</span> 1~4 AU</span></div><div><span style="font-weight: bold;">Largura:</span> ~ 3 AU</div><div><span style="font-weight: bold;">Massa:</span> ?</div><div><span style="font-weight: bold;">Temperatura:</span> ?</div><div><br/></div><div><span style="font-style: italic;">//CONCEPÇÃO ARTÍSTICA</span></div><div><span src="assets/img/espaco/download [3].jpg" type="image/jpeg" data-filename="download.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="826"/></span>
<h1>Debris Disk(Epsilon Sagittarii)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 17:11</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:15</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Sagitário, Disco Circunstelar, Grupo Local, Laniakea, Sistema Epsilon Sagittarii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://arxiv.org/pdf/1111.5618.pdf"><i>https://arxiv.org/pdf/1111.5618.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Distância da estrela:</span> 106 AU</div><div><span style="font-weight: bold;">Largura:</span> ~ 49 AU</div><div><span style="font-weight: bold;">Massa:</span> ?</div><div><span style="font-weight: bold;">Temperatura:</span> 100 K</div><div><br/></div><div><span src="assets/img/espaco/download [4].jpg" type="image/jpeg" data-filename="download.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="828"/></span>
<h1>Sistema Epsilon Sagittarii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 18:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:06</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Sagitário, Grupo Local, Laniakea, Sistema Binário de Estrelas, Sistema Epsilon Sagittarii, Sistema Estelar, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/kausaus.html"><i>http://stars.astro.illinois.edu/sow/kausaus.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Kaus Australis e ε Sagittarii B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Kaus Australis</div><div><span style="font-weight: bold;">Distância do disco:</span> 155 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.995 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Sol:</span> 143 anos-luz</span></div><div><br/></div><div><b>Órbita </b><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>ε Sagittarii B:</b> [106 UA]</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/download [5].jpg" type="image/jpeg" data-filename="download.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/1000px-Sagittarius_constellation_map.svg.png" type="image/png" data-filename="1000px-Sagittarius_constellation_map.svg.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="831"/></span>
<h1>Kaus Australis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 17:22</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 17:00</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Braço de Órion, Centena de Milhões de Anos, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Sagitário, Estrela, Gigante, Grupo Local, Laniakea, Sistema Epsilon Sagittarii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ε</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Epsilon+Sgr"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Epsilon+Sgr</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B95lll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Sagitário</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6,8 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 3,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,5 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 9.960 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 143 anos luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância do Centro Galático:</b> 23.995 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,85</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -1,41</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 232 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 236 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/Epsilon_Sagittarii.jpg" type="image/jpeg" data-filename="Epsilon_Sagittarii.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="833"/></span>
<h1>ε Sagittarii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 11:33</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:59</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Sagitário, Estrela, Grupo Local, Laniakea, Sistema Epsilon Sagittarii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ε</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/kausaustralis"><i>https://www.universeguide.com/star/kausaustralis</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> ?</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Sagitário</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 0,95<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.807 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 143 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  14,3</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -1,41</span></div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/download [6].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;"/></div><div><br/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/1000px-Sagittarius_constellation_map.svg [1].png" type="image/png" data-filename="1000px-Sagittarius_constellation_map.svg.png"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="836"/></span>
<h1>π Scorpii C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 16:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:37</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Pi Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, π</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span> ?</div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R☉</div><div><span style="font-weight: bold;">Massa:</span> ?<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície: </span>? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 590 anos luz</span></div><div><span style="font-weight: bold;">Distância do centro galático:</span> 23.610 anos-luz</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750.jpg" type="image/jpeg" data-filename="pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750.jpg" style="cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [7].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="839"/></span>
<h1>Sistema Pi Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 14:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Estelar, Sistema Múltiplo, Sistema Pi Scorpii, Sistema Quádruplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/pisco.html"><i>http://stars.astro.illinois.edu/sow/pisco.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Fang</span> <span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">π Scorpii Ab,</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> π Scorpii B e </span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">π Scorpii C</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Fang</div><div><span style="font-weight: bold;">Distância do disco:</span> 8.000 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância da Terra:</span> 590 anoz-luz</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.610 anos-luz</span></div><div><br/></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">π Scorpii Ab</span></span><span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif; font-weight: bold;">:</span> 1.571 dias</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>π Scorpii B</b></span><span style="font-weight: bold;">:</span> 1.600 anos [7.000 UA] </div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space; font-weight: bold;">π Scorpii C</span><span style="font-weight: bold;">:</span> [7.090 UA] </div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750 [1].jpg" type="image/jpeg" data-filename="pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [8].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="842"/></span>
<h1>π Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 16:24</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:31</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Cinturão de Gould, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Laranja, Sistema Pi Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, π</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/pisco.html"><i>http://stars.astro.illinois.edu/sow/pisco.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span> <span style="word-wrap: break-word; -webkit-line-break: after-white-space;">K</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R☉</div><div><span style="font-weight: bold;">Massa:</span> ?<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície: </span>? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 21.000 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 590 anos luz</span></div><div><span style="font-weight: bold;">Distância do centro galático:</span> 23.610 anos-luz</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750 [2].jpg" type="image/jpeg" data-filename="pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750.jpg" style="cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [9].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="845"/></span>
<h1>α Leonis Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 15:30</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:29</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Braço de Órion, Branca, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Leão, Estrela, Grupo Local, Laniakea, Sistema Alpha Leonis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astronomytrek.com/star-facts-regulus/"><i>http://www.astronomytrek.com/star-facts-regulus/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> B</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Leão</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R☉</div><div><span style="font-weight: bold;">Massa:</span> 0,3<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.171 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 79 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 1 Bilhão de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div>    </div><div><span src="assets/img/espaco/download [7].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/1024px-Leo_constellation_map.png" type="image/png" data-filename="1024px-Leo_constellation_map.png"/>  </div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="848"/></span>
<h1>α Leonis B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 15:30</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:29</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Leão, Estrela, Grupo Local, Laniakea, Laranja, Sistema Alpha Leonis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/regulus"><i>https://www.universeguide.com/star/regulus</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> K2V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Leão</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R☉</div><div><span style="font-weight: bold;">Massa:</span> 0,3<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,4 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.885 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.171 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 79 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  7,6</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 6,3</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div>    </div><div><span src="assets/img/espaco/download [8].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/1024px-Leo_constellation_map [1].png" type="image/png" data-filename="1024px-Leo_constellation_map.png"/>  </div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="851"/></span>
<h1>α Leonis C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/03/2019 15:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:29</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação de Leão, Estrela, Grupo Local, Laniakea, Sistema Alpha Leonis, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea, α</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> M5V</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Leão</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R☉</div><div><span style="font-weight: bold;">Massa:</span> 0,3<span style="vertical-align: super; font-size: 11px; box-sizing: border-box; position: relative; top: -0.5em; color: rgb(58, 58, 58); line-height: 0;"> </span>M☉</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.171 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 79 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);"> 13</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 11,6</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Metais neutros e dióxidos de titânio</div><div><br/></div><div>    </div><div><span src="assets/img/espaco/download [9].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/1024px-Leo_constellation_map [2].png" type="image/png" data-filename="1024px-Leo_constellation_map.png"/>  </div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="854"/></span>
<h1>π Scorpii Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 11:04</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:24</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass US, Braço de Órion, Branca, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Pi Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, π</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/piscorpii"><i>https://www.universeguide.com/star/piscorpii</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B2 V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 4 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Massa:</span> ?<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície: </span>? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 590 anos luz</span></div><div><span style="font-weight: bold;">Distância do centro galático:</span> 23.610 anos-luz</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  12,2</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,85</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 87 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750 [3].jpg" type="image/jpeg" data-filename="pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750.jpg" style="cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [10].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="857"/></span>
<h1>Fang</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 17:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:23</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass US, Braço de Órion, Branca, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Pi Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, π</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+143018"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+143018</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B1 V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 5 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Massa:</span> 12<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície: </span>? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.230 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 590 anos luz</span></div><div><span style="font-weight: bold;">Distância do centro galático:</span> 23.610 anos-luz</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,89</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,85</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 13 Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 108 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750 [4].jpg" type="image/jpeg" data-filename="pi-scorpii-76da7a7a-2598-4feb-9e9b-ad77b051aee-resize-750.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span><br/></div><div><span src="assets/img/espaco/sco_map - Copia [11].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="860"/></span>
<h1>Sistema Alpha Leonis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 16:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 16:00</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Leão, Grupo Local, Laniakea, Sistema Alpha Leonis, Sistema Estelar, Sistema Múltiplo, Sistema Quádruplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/REGULUS.html"><i>http://stars.astro.illinois.edu/sow/REGULUS.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Regulus, α Leonis Ab , α Leonis B, α Leonis C</span></div><div><span style="font-weight: bold;">Estrela Principal: </span>Regulus</div><div><span style="font-weight: bold;">Distância do disco:</span> 5.000 AU</div><div><span style="font-weight: bold;">Distância da Terra:</span> 79 anos-luz</div><div><span style="font-weight: bold;">Distância do centro galático:</span> 24.171 anos-luz</div><div><br/></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">α Leonis A</span></span><span style="font-weight: bold;">b</span><span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif; font-weight: bold;">:</span> 40 dias [0,35 UA]</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">α Leonis B</span><span style="font-weight: bold;">/</span><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">α Leonis C</span><span style="font-weight: bold;"> -&gt; Regulus</span><span style="color: rgb(34, 34, 34); font-weight: bold;">:</span><span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">2.000</span><span style="color: rgb(34, 34, 34);"> anos [5.000 UA]</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">α Leonis C -&gt; </span><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">α Leonis B</span><span style="font-weight: bold;">:</span> 600 anos [100 UA]</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/download [10].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/1024px-Leo_constellation_map [3].png" type="image/png" data-filename="1024px-Leo_constellation_map.png"/>    </div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="863"/></span>
<h1>Regulus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 16:00</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 15:39</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Bilhões de Anos, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Leão, Estrela, Grupo Local, Laniakea, Sistema Alpha Leonis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/regulus.html"><i>http://stars.astro.illinois.edu/sow/regulus.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><br/></div><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B8lVn</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Leão</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 3.15 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Massa:</span> 1.8<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,54 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.885 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.171 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 79 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,4</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -0,5</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 1 Bilhão de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 347 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/20150715_062759_n7eua_171.jpg" type="image/jpeg" data-filename="20150715_062759_n7eua_171.jpg" style="cursor: default;cursor: default;" width="2048"/></div><div><br/></div><div><span src="assets/img/espaco/Regulus.jpg" type="image/jpeg" data-filename="Regulus.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/1024px-Leo_constellation_map [4].png" type="image/png" data-filename="1024px-Leo_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="867"/></span>
<h1>Nuvem Interestelar Local</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/01/2019 18:22</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 13:41</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar, Nuvem Interestelar Local, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://science.nasa.gov/science-news/science-at-nasa/2004/17dec_heliumstream"><i>https://science.nasa.gov/science-news/science-at-nasa/2004/17dec_heliumstream</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Diametro:</span> 30 anos-luz</span></div><div><span style="font-weight: bold;">Massa Total:</span> ? M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Massa da Nuvem:</span> ? M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Idade:</span> ? milhões de anos<span>    </span></div><div><span style="font-weight: bold;">Temperatura Média:</span> <span style="color: rgb(34, 34, 34);">6,730 °C</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Sigla:</span> <span style="color: rgb(34, 34, 34);">LIC(Local Interstellar Cloud)</span></div><div><span style="background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="font-size: 14px;"><span style="font-size: 14px; color: rgb(34, 34, 34); font-style: italic;">//REPRESENTAÇÃO COM A LOCALIZAÇÃO DA LIC</span></span></div><div><span src="assets/img/espaco/localcloud_ibex_960.jpg" type="image/jpeg" data-filename="localcloud_ibex_960.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;" width="960"/></div><div><span style="background-color: rgb(255, 255, 255); font-size: 14px;"><br/></span></div><div><span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34);">// REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/localcloud_frisch.jpg" type="image/jpeg" data-filename="localcloud_frisch.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="870"/></span>
<h1>Nuvem G</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>26/01/2019 01:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 13:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem G, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.cambridge.org/core/services/aop-cambridge-core/content/view/EADF3783204F28CE057EBE151B6CA6DE/S0252921100070652a.pdf/observations_of_the_local_interstellar_cloud.pdf"><i>https://www.cambridge.org/core/services/aop-cambridge-core/content/view/EADF3783204F28CE057EBE151B6CA6DE/S0252921100070652a.pdf/observations_of_the_local_interstellar_cloud.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Massa Total:</span> ? M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Massa da Nuvem:</span> ? M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Diâmetro:</span> ? anos-luz</div><div><span style="font-weight: bold;">Idade:</span> ? milhões de anos</div><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Temperatura Média:</span> 5.900<span style="color: rgb(34, 34, 34);"> °C</span></span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Sigla:</span><span style="color: rgb(34, 34, 34);"> GC</span></div><div><br/></div><div><span style="color: rgb(34, 34, 34); font-style: italic;">//REPRENTAÇÃO EM DESENHO</span></div><div><span style="color: rgb(34, 34, 34);"><span src="assets/img/espaco/220px-The_Local_Interstellar_Cloud_and_neighboring_G-cloud_complex.gif" type="image/gif" data-filename="220px-The_Local_Interstellar_Cloud_and_neighboring_G-cloud_complex.gif" style="cursor: default;cursor: default;"/></span></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="872"/></span>
<h1>Cinturão de Gould</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 22:48</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/03/2019 13:14</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Cinturão Estelar, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://nature1st.net/mag/?page_id=980"><i>http://nature1st.net/mag/?page_id=980</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Raio:</span> 1.956 anos-luz</div><div><span style="font-weight: bold;">Idade:</span> 30-60 milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/GB-1-Copy-2.jpg" type="image/jpeg" data-filename="GB-1-Copy-2.jpg"/></div><div><br/></div><div><span src="assets/img/espaco/GB-141.jpg" type="image/jpeg" data-filename="GB-141.jpg"/></div><div><br/></div><div><span src="assets/img/espaco/Image [16].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><span src="assets/img/espaco/Image [17].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><span src="assets/img/espaco/GB-121.jpg" type="image/jpeg" data-filename="GB-121.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="879"/></span>
<h1>Nebulosa do Lápis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 21:33</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 21:41</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Grupo Local, Laniakea, Nebulosa, Região HII, SNR Vela, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://hubblesite.org/image/1350/gallery"><i>http://hubblesite.org/image/1350/gallery</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">ID:</span> NGC 2736 </div><div><span style="font-weight: bold;">Tipo:</span> ?</div><div><span style="font-weight: bold;">Constelação:</span> Vela</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> 12</div><div><span style="font-weight: bold;">Dimensão Aparente:</span> 30x7arcmins</div><div><span style="font-weight: bold;">Raio:</span> 5 anos-luz</div><div><span style="font-weight: bold;">Distância:</span> 815 anos-luz     </div><div><span style="font-weight: bold;">Densidade:</span> ?</div><div><span style="font-weight: bold;">Temperatura Média</span><span style="font-weight: bold;">:</span> ? K (alta )</div><div><span style="font-weight: bold;">Composição:</span> Gás, poeira estelar e hidrogênio ionizado</div><div><span style="font-weight: bold;">Massa:</span> ? M☉</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Pencil_hst_big.jpg" type="image/jpeg" data-filename="Pencil_hst_big.jpg"/></div><div><br/></div><div>// LOCALIZAÇÃO DA NEBULOSA</div><div><span src="assets/img/espaco/260px-Vela_IAU.svg.png" type="image/png" data-filename="260px-Vela_IAU.svg.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="882"/></span>
<h1>Nebulosa de Gum</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 20:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 21:33</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Puppis, Constelação da Vela, Emissão, Grupo Local, Laniakea, Nebulosa, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://apod.nasa.gov/apod/ap090822.html"><i>https://apod.nasa.gov/apod/ap090822.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">ID:</span> NGC 2024 </div><div><span style="font-weight: bold;">Tipo:</span> Emissão(Resto de Supernova)</div><div><span style="font-weight: bold;">Constelação:</span> Vela e Popa</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> 12</div><div><span style="font-weight: bold;">Dimensão Aparente:</span> 35x35 arcmins</div><div><span style="font-weight: bold;">Raio:</span> 950 anos-luz</div><div><span style="font-weight: bold;">Distância:</span> 1.470 anos-luz     </div><div><span style="font-weight: bold;">Densidade:</span> 2 cm-³</div><div><span style="font-weight: bold;">Temperatura Média</span><span style="font-weight: bold;">:</span> ? K</div><div><span style="font-weight: bold;">Composição:</span> Gás, poeira estelar e hidrogênio</div><div><span style="font-weight: bold;">Massa:</span> ? M☉</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/749px-GumNebula.png" type="image/png" data-filename="749px-GumNebula.png"/></div><div><br/></div><div><i>// LOCALIZAÇÃO DA NEBULOSA DE GUM</i></div><div><i><span src="assets/img/espaco/260px-Puppis_IAU.svg.png" type="image/png" data-filename="260px-Puppis_IAU.svg.png"/></i></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="885"/></span>
<h1>SNR Vela</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 21:13</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 21:19</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Grupo Local, Laniakea, Remanescente de Supernova, SNR Vela, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://adsabs.harvard.edu/abs/1999ApJ...515L..25C"><i>http://adsabs.harvard.edu/abs/1999ApJ...515L..25C</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Constelação:</span> Vela</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> 12</div><div><span style="font-weight: bold;">Distância:</span> 815 anos-luz     <span>    </span></div><div><span style="font-weight: bold;">Temperatura Média</span><span style="font-weight: bold;">:</span> ? K<span>    </span></div><div><span style="font-weight: bold;">Massa:</span> ? M☉</div><div><b>Expoliu há:</b> 11-12.300 anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/image.jpg" type="image/jpeg" data-filename="image.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="887"/></span>
<h1>Lambda Orionis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 20:00</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 20:09</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado Aberto, Aglomerado de Estrelas, Anel Lambda Orionis, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://arxiv.org/pdf/1008.3916.pdf"><i>https://arxiv.org/pdf/1008.3916.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Outros ID:</span> Collinder 69</div><div><span style="font-weight: bold;">Constelações:</span> Órion</div><div><span style="font-weight: bold;">Distância:</span> 1.100 anos-luz</div><div><span style="font-weight: bold;">Magnitude Aparente:</span>  2,8</div><div><span style="font-weight: bold;">Idade:</span> 5 milhões de anos </div><div><span style="font-weight: bold;">Número estimado de estrelas: </span>~ 436 estrelas</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/220px-Orion's_Big_Head_Revealed_in_Infrared.jpg" type="image/jpeg" data-filename="220px-Orion's_Big_Head_Revealed_in_Infrared.jpg"/></div><div><br/></div><div>// LOCALIZAÇÃO DO AGLOMERADO</div><div><span src="assets/img/espaco/260px-Orion_constellation_map.svg.png" type="image/png" data-filename="260px-Orion_constellation_map.svg.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="890"/></span>
<h1>Anel Lambda Orionis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 19:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 20:02</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel Lambda Orionis, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="about:blank#blocked"><i>about:blank#blocked</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Distancia:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> 1.430 anoz-luz</span></div><div><span style="font-weight: bold;">Massa Total:</span> ? M☉</div><div><span style="font-weight: bold;">Massa da Nuvem:</span> ? M☉</div><div><span style="font-weight: bold;">Diâmetro:</span> 150 anos-luz</div><div><span style="font-weight: bold;">Idade:</span> 10 milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/280px-LambdaOrionisRegion.jpg" type="image/jpeg" data-filename="280px-LambdaOrionisRegion.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="892"/></span>
<h1>Associação Ori OB 1a</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 19:19</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 19:30</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1a, Ass tipo OB, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://arxiv.org/pdf/0812.0046.pdf"><i>https://arxiv.org/pdf/0812.0046.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Constelações:</span> Órion</div><div><span style="font-weight: bold;">Classificação Estelar:</span> O e B</div><div><span style="font-weight: bold;">Distância do Sol:</span> ? anos-luz(1.000)</div><div><span style="font-weight: bold;">Idade:</span> 12 milhões de anos</div><div><span style="font-weight: bold;">Estrelas:</span> &gt; 200</div><div><br/></div><div><span src="assets/img/espaco/220px-Orion_OB1_&_25_Ori_Group.png" type="image/png" data-filename="220px-Orion_OB1_&_25_Ori_Group.png"/></div><div><br/></div><div><span src="assets/img/espaco/Image [18].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="895"/></span>
<h1>Aglomerado 25 Oirionis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 19:03</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 19:18</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado Aberto, Aglomerado de Estrelas, Ass Ori OB 1a, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.astro.uni-jena.de/yeti/workshop/yeti-talks/briceno_25ori.pdf"><i>https://www.astro.uni-jena.de/yeti/workshop/yeti-talks/briceno_25ori.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Constelações:</span> Órion</div><div><span style="font-weight: bold;">Distância:</span> 1.000 anos-luz</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> ~ 4</div><div><span style="font-weight: bold;">Idade:</span> 7-10 milhões de anos </div><div><span style="font-weight: bold;">Número estimado de estrelas: </span>~ 200 estrelas</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [19].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><span src="assets/img/espaco/220px-Orion_OB1_&_25_Ori_Group [1].png" type="image/png" data-filename="220px-Orion_OB1_&_25_Ori_Group.png"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DO AGLOMERADO</span></div><div><span src="assets/img/espaco/260px-Orion_IAU.svg.png" type="image/png" data-filename="260px-Orion_IAU.svg.png"/></div><div><br/></div><div><br/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="899"/></span>
<h1>M78</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 16:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 16:41</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa, NGM Ori B, Reflexão, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.apolo11.com/spacenews.php?posic=dat_20120502-103113.inc"><i>https://www.apolo11.com/spacenews.php?posic=dat_20120502-103113.inc</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">ID:</span> M78 | NGC 2068</div><div><span style="font-weight: bold;">Tipo:</span> Reflexão</div><div><span style="font-weight: bold;">Constelação:</span> Órion</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> 8,3</div><div><span style="font-weight: bold;">Dimensão Aparente:</span> 8x6 arcmins</div><div><span style="font-weight: bold;">Raio:</span> 5 anos-luz</div><div><span style="font-weight: bold;">Distância:</span> 1.350 anos-luz     </div><div><span style="font-weight: bold;">Densidade:</span> ? (Alta)</div><div><span style="font-weight: bold;">Temperatura Média</span><span style="font-weight: bold;">:</span> ? K (Baixa)</div><div><span style="font-weight: bold;">Composição:</span> Gás, poeira estelar e hidrogênio</div><div><span style="font-weight: bold;">Massa:</span>  &gt; ? M☉</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/619px-Messier_78.jpg" type="image/jpeg" data-filename="619px-Messier_78.jpg"/></div><div><br/></div><div><i>// LOCALIZAÇÃO DA NEBULOSA</i></div><div><span style="color: rgb(11, 0, 128);"><sub><span src="assets/img/espaco/Orion_constellation_map.png" type="image/png" data-filename="Orion_constellation_map.png"/></sub></span></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="903"/></span>
<h1>Nebulosa da Chama</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 16:08</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 16:33</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Emissão, Grupo Local, Laniakea, Nebulosa, NGM Ori B, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.ipac.caltech.edu/2mass/gallery/showcase/flameneb/index.html"><i>http://www.ipac.caltech.edu/2mass/gallery/showcase/flameneb/index.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">ID:</span> NGC 2024 </div><div><span style="font-weight: bold;">Tipo:</span> Emissão </div><div><span style="font-weight: bold;">Constelação:</span> Órion</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> 10</div><div><span style="font-weight: bold;">Dimensão Aparente:</span> 30x30 arcmins</div><div><span style="font-weight: bold;">Raio:</span> 6 anos-luz</div><div><span style="font-weight: bold;">Distância:</span> 1.350 anos-luz     </div><div><span style="font-weight: bold;">Densidade:</span> ?</div><div><span style="font-weight: bold;">Temperatura Média</span><span style="font-weight: bold;">:</span> ? K</div><div><span style="font-weight: bold;">Composição:</span> Gás, poeira estelar e hidrogênio</div><div><span style="font-weight: bold;">Massa:</span>  &gt; 20 M☉</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Ngc2024_2mass.jpg" type="image/jpeg" data-filename="Ngc2024_2mass.jpg"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÂO DA NEBULOSA    </span></div><div><span src="assets/img/espaco/260px-Orion_IAU.svg [1].png" type="image/png" data-filename="260px-Orion_IAU.svg.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="906"/></span>
<h1>Nuvem Molecular Órion 4(OMC-4)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 13:36</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 14:42</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa de Órion, NGM Ori A, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://ism.strw.leidenuniv.nl/wiki/lib/exe/fetch.php?media=odell_orionreview_2008.pdf"><i>http://ism.strw.leidenuniv.nl/wiki/lib/exe/fetch.php?media=odell_orionreview_2008.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Distancia:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> ? anoz-luz</span></div><div><span style="font-weight: bold;">Massa Total:</span> ? M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Massa da Nuvem:</span> ? M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Diâmetro:</span> ? anos-luz</div><div><span style="font-weight: bold;">Idade:</span> ? milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [20].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="908"/></span>
<h1>Nuvem Molecular Órion 3(OMC-3)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 12:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 13:31</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa de Órion, NGM Ori A, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://herschel.esac.esa.int/2013MapmakingWorkshop/presentations/HOPS_WFischer_MapMaking2013_DPWS.pdf"><i>http://herschel.esac.esa.int/2013MapmakingWorkshop/presentations/HOPS_WFischer_MapMaking2013_DPWS.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Distancia:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> 1.467 anoz-luz</span></div><div><span style="font-weight: bold;">Massa Total:</span> &gt; 80 M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Massa da Nuvem:</span> ? M<a href="https://pt.wikipedia.org/wiki/Massa_solar" style="vertical-align: sub; font-size: smaller; font-size: smaller; background-image: none; background-color: rgb(249, 249, 249); color: rgb(11, 0, 128); line-height: 1;" title="Massa solar">☉</a></div><div><span style="font-weight: bold;">Diâmetro:</span> ? anos-luz</div><div><span style="font-weight: bold;">Idade:</span> ? milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/images.jpg" type="image/jpeg" data-filename="images.jpg"/></div><div><br/></div><div><span src="assets/img/espaco/Image [21].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="911"/></span>
<h1>Nuvem Molecular Órion 2(OMC-2)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 12:18</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 12:52</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa de Órion, NGM Ori A, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?letter=.&classic=YES&bibcode=1990AJ....100..518J&page=&type=SCREEN_VIEW&data_type=PDF_LOW&send=GET&filetype=.pdf"><i>http://articles.adsabs.harvard.edu/cgi-bin/nph-iarticle_query?letter=.&amp;classic=YES&amp;bibcode=1990AJ....100..518J&amp;page=&amp;type=SCREEN_VIEW&amp;data_type=PDF_LOW&amp;send=GET&amp;filetype=.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Distancia:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> 1.467 anoz-luz</span></div><div><span style="font-weight: bold;">Massa Total:</span> &gt; 22 M☉</div><div><span style="font-weight: bold;">Massa da Nuvem:</span> ? M☉</div><div><span style="font-weight: bold;">Diâmetro:</span> 0,65 anos-luz</div><div><span style="font-weight: bold;">Idade:</span> ? milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/DENIS-image-of-the-molecular-cloud-OMC2-as-seen-in-J-and-K-s-false-colours-blue-J.png" type="image/png" data-filename="DENIS-image-of-the-molecular-cloud-OMC2-as-seen-in-J-and-K-s-false-colours-blue-J.png"/></div><div><br/></div><div><span src="assets/img/espaco/Image [22].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="914"/></span>
<h1>Nebulosa Órion-KL(Kleinmann–Low)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 11:44</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 12:19</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa, Nebulosa de Órion, Nebulosa Órion-KL, NGM Ori A, OMC-1, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://books.google.com.br/books?id=iysfoeFGXjwC&pg=PA239&lpg=PA239&dq=Kleinmann-Low+Nebular+dimension&source=bl&ots=PEKYAayjJH&sig=ACfU3U2i9b36TQ3pkvasAF69eVrZ9tBvaw&hl=pt-PT&sa=X&ved=2ahUKEwjM69bj1t7gAhUiC9QKHTMpDWMQ6AEwAnoECAcQAQ#v=onepage&q=Kleinmann-Low%20Nebular%20dimension&f=false"><i>https://books.google.com.br/books?id=iysfoeFGXjwC&amp;pg=PA239&amp;lpg=PA239&amp;dq=Kleinmann-Low+Nebular+dimension&amp;source=bl&amp;ots=PEKYAayjJH&amp;sig=ACfU3U2i9b36TQ3pkvasAF69eVrZ9tBvaw&amp;hl=pt-PT&amp;sa=X&amp;ved=2ahUKEwjM69bj1t7gAhUiC9QKHTMpDWMQ6AEwAnoECAcQAQ#v=onepage&amp;q=Kleinmann-Low%20Nebular%20dimension&amp;f=false</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">ID:</span> Orion KL</div><div><span style="font-weight: bold;">Tipo:</span> ?</div><div><span style="font-weight: bold;">Constelação:</span> Órion</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> ?</div><div><span style="font-weight: bold;">Dimensão Aparente:</span> ? arcmins</div><div><span style="font-weight: bold;">Raio:</span>  0,16 anos-luz</div><div><span style="font-weight: bold;">Distância:</span> 1.500 anos-luz     </div><div><span style="font-weight: bold;">Densidade:</span> ?</div><div><span style="font-weight: bold;">Temperatura Média</span><span style="font-weight: bold;">:</span> &lt; 600 K</div><div><span style="font-weight: bold;">Composição:</span> Gás, poeira estelar, hidrogênio, oxigênio, gás carbônico e metais pesados</div><div><span style="font-weight: bold;">Massa:</span>  &gt; 50 M☉</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/heic1705a.jpg" type="image/jpeg" data-filename="heic1705a.jpg"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA NEBULOSA</span></div><div><span src="assets/img/espaco/Orion_constellation_map [1].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="918"/></span>
<h1>Alf Ari b</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 11:33</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 11:00</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Áries, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema Hamal, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/4217/alf-ari-b/"><i>https://exoplanets.nasa.gov/newworldsatlas/4217/alf-ari-b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> ?</span></div><div><span style="font-weight: bold;">Ano:</span> 380 dias terrestres</div><div><span style="font-weight: bold;">Temperatura:</span> ?</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 111.000.000 Milhas - 1.2AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> ? R⊕</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 572,04 M⊕ | 1,8 MJ</div><div><span style="font-weight: bold;">Gravidade:</span>  ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,25</div><div><span style="font-weight: bold;">Distância da Terra:</span> 65,8 anos-luz</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Planeta possui a massa de 1.8 de Júpiter</div><div><br/></div><div><span src="assets/img/espaco/Image [23].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="920"/></span>
<h1>Thestias</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 12:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:38</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Gêmeos, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema Póllux, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/1675/hd-62509-b/"><i>https://exoplanets.nasa.gov/newworldsatlas/1675/hd-62509-b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> ?</span></div><div><span style="font-weight: bold;">Ano:</span> 1 ano e 7 meses terrestres</div><div><span style="font-weight: bold;">Temperatura:</span> (235º)</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 152.000.000 Milhas - 1.64AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> ?  milhas</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Massa:</span> 730,94 M⊕  | 2,3 MJ</div><div><span style="font-weight: bold;">Gravidade:</span>  ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Considerado um &quot;Júpiter Quente&quot; com 2.3 de sua massa</div><div><br/></div><div>//<span style="font-style: italic;">CONCEPÇÃO ARTÍSTICA</span></div><div><span src="assets/img/espaco/Artist_impression_of_Pollux_B.jpg" type="image/jpeg" data-filename="Artist_impression_of_Pollux_B.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="922"/></span>
<h1>Bet Cnc b</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 19:29</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:38</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Câncer, Exoplaneta, Gigante Gasoso, Grupo Local, Laniakea, Planeta, Sistema Tarf, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanets.nasa.gov/newworldsatlas/4218/bet-cnc-b/"><i>https://exoplanets.nasa.gov/newworldsatlas/4218/bet-cnc-b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> ?</span></div><div><span style="font-weight: bold;">Ano:</span> 1 ano e 8 meses terrestres</div><div><span style="font-weight: bold;">Temperatura:</span> ?</div><div><span style="font-weight: bold;">Luas:</span> ?</div><div><span style="font-weight: bold;">Distância da estrela:</span> 158.000.000 Milhas - 1.7AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span></div><div><span style="font-weight: bold;">Massa:</span> 2.478 M⊕ | 7,8 MJ</div><div><span style="font-weight: bold;">Peso:</span> ? da terra</div><div><span style="font-weight: bold;">Gravidade:</span>  ? m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,08</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 290 anos-luz</div><div><br/></div><div><span src="assets/img/espaco/gasgiant_sq.jpg" type="image/jpeg" data-filename="gasgiant_sq.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="924"/></span>
<h1>Próxima b</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>10/07/2018 12:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:29</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Exoplaneta, Grupo Local, Laniakea, Nuvem G, Planeta, Sistema Alpha Centauri, Sistema Próxima Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://exoplanetarchive.ipac.caltech.edu/cgi-bin/DisplayOverview/nph-DisplayOverview?objname=Proxima+Cen+b"><i>https://exoplanetarchive.ipac.caltech.edu/cgi-bin/DisplayOverview/nph-DisplayOverview?objname=Proxima+Cen+b</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 11 dias terrestres e 4h</span></div><div><span style="font-weight: bold;">Ano:</span> 11 dias terrestres  e 4h</div><div><span style="font-weight: bold;">Temperatura:</span> - 39 ºC</div><div><span style="font-weight: bold;">Luas:</span> 0</div><div><span style="font-weight: bold;">Distância da estrela:</span> 4.647.000 Milhas - 0.05AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 1,1 R⊕</div><div><span style="font-weight: bold;">Peso:</span> 1,09 da terra</div><div><span style="font-weight: bold;">Massa:</span> 1,27 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span>  ~ 10,68 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> ?</div><div><span style="font-weight: bold;">Vel. p/ orb. estrela:</span> ? km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,35</div><div><span style="font-weight: bold;">Área de superfície:</span> ? km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 4,2 anos-luz</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Muito próximo a estrela</div><div>&gt; Forte radiação estelar atacando atmosfera</div><div> </div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Exoplaneta mais próximo do sistema solar</div><div>&gt; Sempre a mesma face para a estrela</div><div><br/></div><div><span src="assets/img/espaco/157097.282029-Proxima-B.png" type="image/png" data-filename="157097.282029-Proxima-B.png" style="cursor: default;cursor: default;cursor: default;" width="1024"/></div><div><br/></div><div><span src="assets/img/espaco/proxima_b.jpg" type="image/jpeg" data-filename="proxima_b.jpg" style="cursor: default;cursor: default;" width="912"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="927"/></span>
<h1>Vênus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/05/2018 23:07</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Planeta, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Vênus, Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/venus/"><i>https://solarsystem.nasa.gov/planets/venus/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 243 dias terrestres</span></div><div><span style="font-weight: bold;">Ano:</span> 225 dias terrestres</div><div><span style="font-weight: bold;">Temperatura:</span> -220 ~ 1000 [461] ºC (470 na superfície)</div><div><span style="font-weight: bold;">Luas:</span> 0</div><div><span style="font-weight: bold;">Distância do Sol:</span> 67.238.251 Milhas - 0.7AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 0,94 R⊕</div><div><span style="font-weight: bold;">Peso:</span>  0.9 da terra</div><div><span style="font-weight: bold;">Massa:</span> 0,815 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span> 8.87 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> principalmente dióxido de carbono e nitrogênio contendo também argônio, hélio, monóxido de carbono, neônio, dióxido de enxofre</div><div><span style="font-weight: bold;">Vel. p/ orb. sol:</span>   126,074 km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,006772<span>   </span></div><div><span style="font-weight: bold;">Área de superfície:</span> 460.234.317 km²</div><div><span style="font-weight: bold;">Oposição:</span> </div><div><span style="font-weight: bold;">Distância da Terra:</span> 0,2 UA</div><div><span style="font-weight: bold;">Melhor momento de observação:</span> <span style="text-decoration: underline;">Anoitecer</span>{Visível até dia 15 de outubro, <span style="color: rgb(26, 173, 224);">14-1</span><span style="color: rgb(26, 173, 224);">7 de agosto e 14-25 de setembro(melhores dias)</span>}</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Planeta extremamente quente </div><div>&gt; Sem oxigênio</div><div>&gt; Atmosfera quente, tóxica e com tempestades fortes e chuva de ácido sulfúrico</div><div><br/></div><div><span style="font-weight: bold;">----- Fenômenos -----</span></div><div>&gt; Efeito estufa extremo pela atmosfera</div><div>&gt; Tempestades muito fortes na atmosfera</div><div>&gt; Chuva de ácido sulfúrico na atmosfera</div><div>&gt; Vulcões ativos</div><div>&gt; Explosão a casa milhões de anos pelos vulcões</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Terrestre</div><div>&gt; Tempestades e chuvas não chegam no solo</div><div>&gt; Planeta mais quente do sistema solar</div><div>&gt; Tamanho e composição próximo ao da terra </div><div>&gt; Dia mais longo que o ano</div><div>&gt; Céu laranja</div><div>&gt; Céu sem estrela e sol sem muita distinção</div><div>&gt; Ar espesso,sem movimentos rápidos</div><div>&gt; Campo magnético muito mais fraco que o da terra</div><div>&gt; Planeta seco sem núcleo líquido</div><div><br/></div><div><span src="assets/img/espaco/688_Venus.jpg" type="image/jpeg" data-filename="688_Venus.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="4096"/></div><div><br/></div><div><span src="assets/img/espaco/9ab14692101dd4e6aac4f469dda73ca2.jpg" type="image/jpeg" data-filename="9ab14692101dd4e6aac4f469dda73ca2.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="930"/></span>
<h1>Marte</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/02/2018 10:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:25</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Marte, Nuvem Interestelar Local, Planeta, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://mars.nasa.gov/"><i>https://mars.nasa.gov/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 24h 37 min </span></div><div><span style="font-weight: bold;">Ano:</span> 687 dias</div><div><span style="font-weight: bold;">Temperatura:</span> -284 ~ 86 [ -81 ] ºC</div><div><span style="font-weight: bold;">Luas:</span> 2(Deimos &amp; Fobos)</div><div><span style="font-weight: bold;">Distância do Sol:</span> 142.000.000 Milhas - 1.5AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 0,53 R⊕</div><div><span style="font-weight: bold;">Peso:</span> 0.38 da Terra</div><div><span style="font-weight: bold;">Massa:</span> 0,107 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span> 3.71 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> principalmente dióxido de carbono, um pouco de vapor de água, nitrogênio, argônio e metano</div><div>     <span style="font-weight: bold;">[Oxigênio]:</span> 0.2%</div><div><span style="font-weight: bold;">Vel. p/ orb. sol:</span> 86.677 km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,0934</div><div><span style="font-weight: bold;">Área de superfície:</span> 144.371.391 km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 0,52 UA</div><div><span style="font-weight: bold;">Oposição:</span> 27 de julho de 2018</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Muito Frio</div><div>&gt; Sem proteção contra radiação</div><div>&gt; Quase nada de oxigênio</div><div>&gt; Solo difícil de cultivar</div><div>&gt; Pouca água</div><div><br/></div><div><span style="font-weight: bold;">----- Fenômenos -----</span></div><div>&gt; Tempestades de areia</div><div>&gt; Redemoinhos</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Estima-se que havia rios em marte antigamente</div><div>&gt; Possui o maior vulcão do sistema solar</div><div><br/></div><div><span src="assets/img/espaco/683_6453_mars-globe-valles-marineris-enhanced-full2.jpg" type="image/jpeg" data-filename="683_6453_mars-globe-valles-marineris-enhanced-full2.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="2970"/></div><div><br/></div><div><span src="assets/img/espaco/21448_PIA03170_fig1.jpg" type="image/jpeg" data-filename="21448_PIA03170_fig1.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1600"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="934"/></span>
<h1>Saturno</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/05/2018 11:29</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:24</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Gigante Gasoso, Grupo Local, Laniakea, Nuvem Interestelar Local, Planeta, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/saturn/"><i>https://solarsystem.nasa.gov/planets/saturn/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 10h 42min</span></div><div><span style="font-weight: bold;">Ano:</span> 29 anos terrestre</div><div><span style="font-weight: bold;">Temperatura:</span> -250 ~ 700 [-170 ºC]</div><div><span style="font-weight: bold;">Luas:</span> 53(talvez 62)</div><div><span style="font-weight: bold;">Distância do Sol:</span> 886.000.000 Milhas - 9.5AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 9,449 R⊕</div><div><span style="font-weight: bold;">Peso:</span> 1.06 da terra</div><div><span style="font-weight: bold;">Massa:</span> 95,159 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span> 10.44 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> principalmente hidrogênio e hélio, um pouco de metano, amoníaco, etano e deuterídio de hidrogênio</div><div><span style="font-weight: bold;">Vel. p/ orb. sol:</span> 34.701 km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,0565</div><div><span style="font-weight: bold;">Área de superfície:</span> 42.612.133.285 km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 8,5 UA</div><div><span style="font-weight: bold;">Oposição:</span>  09 de julho de 2019 (sendo visível até dia ? de novembro)</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Maior parte sendo atmosfera feita apenas de gás com núcleo sólido extremamente quente</div><div>&gt; Sem oxigênio</div><div>&gt; Sem solo</div><div><br/></div><div><span style="font-weight: bold;">----- Fenômenos -----</span></div><div>&gt; Super tempestades do tamanho da terra</div><div>&gt; Tempestades de raios a cada 30 anos 20 vezes mais fortes que os raios terrestres</div><div>&gt; Furacão hexagonal gigante de 360 km/h no seu polo (32.000 km de largura, 100 km de profundidade)</div><div>&gt; Auroras nos polos</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Gigante gasoso     </div><div>&gt; Saturno é menos denso que a água, ou seja, boiaria na água</div><div>&gt; Anéis de saturno são milhares de asteroides que se colidiram e ficaram em sua órbita</div><div>&gt; Anéis são distorcidos pela gravidade e formam até novas luas</div><div>&gt; Planeta mais achatado do sistema solar</div><div>&gt; Campo magnético muito forte</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/1200px-Saturn_during_Equinox.jpg" type="image/jpeg" data-filename="1200px-Saturn_during_Equinox.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1199"/></div><div><br/></div><div><span src="assets/img/espaco/1477933231_080673_1478005720_noticia_fotograma.jpg" type="image/jpeg" data-filename="1477933231_080673_1478005720_noticia_fotograma.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="980"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="937"/></span>
<h1>Urano</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/05/2018 21:36</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:24</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Gigante Gasoso, Grupo Local, Laniakea, Nuvem Interestelar Local, Planeta, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/uranus/saturn/"><i>https://solarsystem.nasa.gov/uranus/saturn/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 17h 14min</span></div><div><span style="font-weight: bold;">Ano:</span> 84 anos terrestre</div><div><span style="font-weight: bold;">Temperatura:</span> -224 ~ -216 [-220 ºC]</div><div><span style="font-weight: bold;">Luas:</span> 27</div><div><span style="font-weight: bold;">Distância do Sol:</span> 2.876.000.000 Milhas - 19.8AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span> 4,007 R⊕</div><div><span style="font-weight: bold;">Peso:</span> 0.9 da terra</div><div><span style="font-weight: bold;">Massa:</span> 14,536 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span> 8.87 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> principalmente hidrogênio, metano e hélio, um pouco de amônia, água(gelo) e hidrosulfeto de amônio</div><div><span style="font-weight: bold;">     [oxigênio]:</span> cristalizado</div><div><span style="font-weight: bold;">Vel. p/ orb. sol:</span> 24.477 km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,046</div><div><span style="font-weight: bold;">Área de superfície:</span> 8.083.079.690 km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 18,2 UA</div><div><span style="font-weight: bold;">Oposição:</span>  23 de outubro 2018(vistos em muitos meses do ano)</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Sem oxigênio em forma de ar respirável</div><div>&gt; Sem solo</div><div>&gt; Planeta extremamente frio</div><div>&gt; Radiação pelas brechas do campo magnético</div><div><br/></div><div><span style="font-weight: bold;">----- Fenômenos -----</span></div><div>&gt; Auroras em urano</div><div>&gt; Há condições para chover diamantes</div><div>&gt; Tempestades muito fortes</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Gigante gasoso     </div><div>&gt; Chamado de gigante de gelo</div><div>&gt; Possui anéis como saturno mas menos expressivos</div><div>&gt; Metade do ano um polo de urano fica exposto ao sol e a outra metade não</div><div>&gt; Campo magnético muito incomum com rotação incomum também abrindo e fechando em dias</div><div>&gt; Há nuvens brilhantes em urano que representam manchas escuras de longe</div><div>&gt; Único que gira apontando o eixo para o sol</div><div><br/></div><div><span src="assets/img/espaco/599_PIA18182.jpg" type="image/jpeg" data-filename="599_PIA18182.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1720"/></div><div><br/></div><div><span src="assets/img/espaco/605_PIA17306.jpg" type="image/jpeg" data-filename="605_PIA17306.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;" width="1024"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="940"/></span>
<h1>Netuno</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>03/05/2018 13:35</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/03/2019 10:23</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Gigante Gasoso, Grupo Local, Laniakea, Netuno, Nuvem Interestelar Local, Planeta, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/neptune/by-the-numbers/"><i>https://solarsystem.nasa.gov/planets/neptune/by-the-numbers/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Dia:</span> 16h 6 min</span></div><div><span style="font-weight: bold;">Ano:</span> 165 anos terrestre</div><div><span style="font-weight: bold;">Temperatura:</span>- 163</div><div><span style="font-weight: bold;">Luas:</span> 13(talvez 14)</div><div><span style="font-weight: bold;">Distância do Sol:</span> 4.503.000.000 Milhas - 30AU</div><div><span style="font-weight: bold;">Circunferência equatorial</span><span style="font-weight: bold;">:</span>  3,883 R⊕</div><div><span style="font-weight: bold;">Peso:</span>  1.13 da terra</div><div><span style="font-weight: bold;">Massa:</span> 17,147 M⊕</div><div><span style="font-weight: bold;">Gravidade:</span> 11.2 m/s²</div><div><span style="font-weight: bold;">Atmosfera:</span> principalmente hidrogênio, metano e hélio, um pouco de etano, água(gelo) e outras formas de gelo</div><div><span style="font-weight: bold;">     [oxigênio]:</span> em camadas líquidas</div><div><span style="font-weight: bold;">Vel. p/ orb. sol:</span>  19.566 km/h</div><div><span style="font-weight: bold;">Excentricidade:</span> 0,009</div><div><span style="font-weight: bold;">Área de superfície:</span> 7.618.272.763 km²</div><div><span style="font-weight: bold;">Distância da Terra:</span> 29 UA</div><div><span style="font-weight: bold;">Oposição:</span> 2 de Setembro de 2018(vistos em muitos meses do ano) (Veja 6,7 e 8 de dezembro perto de Marte)</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Sem oxigênio em forma de ar respirável</div><div>&gt; Sem solo</div><div>&gt; Planeta extremamente frio</div><div>&gt; Radiação pelas brechas do campo magnético</div><div><br/></div><div><span style="font-weight: bold;">----- Fenômenos -----</span></div><div>&gt; Auroras em netuno</div><div>&gt; Há condições para chover diamantes</div><div>&gt; Tempestades muito fortes com super rajadas de ventos</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Gigante gasoso     </div><div>&gt; Chamado como gigante de gelo</div><div>&gt; Possui anéis como saturno mas menos expressivos</div><div>&gt; Há nuvens brilhantes em netuno que representam manchas escuras de longe</div><div>&gt; Luz do sol em netuno seria como um crepúsculo sombrio</div><div>&gt; Possui a única lua que gira o contrário da rotação do planeta</div><div>&gt; Planeta mais distante do sol</div><div>&gt; Planeta mais ventoso do sistema solar</div><div><br/></div><div><span src="assets/img/espaco/611_PIA01492.jpg" type="image/jpeg" data-filename="611_PIA01492.jpg" style="cursor: default;cursor: default;cursor: default;" width="2188"/></div><div><br/></div><div><span src="assets/img/espaco/614_PIA01997.jpg" type="image/jpeg" data-filename="614_PIA01997.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="944"/></span>
<h1>Objeto Becklin–Neugebauer</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/02/2019 10:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/02/2019 11:51</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa de Órion, Nebulosa Órion-KL, NGM Ori A, OMC-1, Protoestrela, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.adaptiveoptics.org/News_0805_3.html"><i>http://www.adaptiveoptics.org/News_0805_3.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-line-break: after-white-space; font-weight: bold;">Classificação:</span><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"> B</span></div><div><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Òrion</span></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 7 M☉</div><div><span style="font-weight: bold;">Temperatura:</span> 4.498 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 1.500 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? Milhões de Anos</span></div><div><span style="text-align: center; font-weight: bold;">Composição:</span><span style="text-align: center;"> Metais neutros, hidrogênio e oxigênio</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/subarubn.jpg" type="image/jpeg" data-filename="subarubn.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="946"/></span>
<h1>21P/Giacobini-Zinner</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/05/2018 20:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>21/02/2019 16:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Família Júpiter, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/21p-giacobini-zinner/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/21p-giacobini-zinner/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Periélio:</span> 1AU</span></div><div><span style="font-weight: bold;">Afélio:</span> 6AU</div><div><span style="font-weight: bold;">Semi-eixo:</span> 526.000.000 km - 3.52AU</div><div><span style="font-weight: bold;">Período de orbital:</span> 6,6 anos</div><div><span style="font-weight: bold;">Diâmetro:</span> 2 km</div><div><span style="font-weight: bold;">Velocidade média:</span> ? km/h</div><div><span style="font-weight: bold;">Último periélio:</span> 11/02/2012</div><div><span style="font-weight: bold;">Próximo periélio:</span> 2025</div><div><br/></div><div><span src="assets/img/espaco/629_CometGiacobini-Zinner_732.jpg" type="image/jpeg" data-filename="629_CometGiacobini-Zinner_732.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="948"/></span>
<h1>Sol</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 17:46</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:53</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Amarela, Anã, Bilhões de Anos, Bolha Local, Braço de Órion, Cinturão de Gould, Classificação Estelar G, Complexo de Superaglomerados Pisces-Cetus, Estrela, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Sol, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/solar-system/sun/in-depth/"><i>https://solarsystem.nasa.gov/solar-system/sun/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> G2V</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 695.508 km (1 Raio Solar - R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub>)</div><div><span style="font-weight: bold;">Circunferência Equatorial:</span> 4.370.005 km</div><div><span style="font-weight: bold;">Massa:</span> 1.989 x 10<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;">30 </span></sup>(1 Massa Solar - M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub>)</div><div><span style="font-weight: bold;">Área de Superfície:</span> 6.000.000.000.000 km²</div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 274.0 m/s²</div><div><span style="font-weight: bold;">Temperatura:</span> 5.777 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> 225~250 milhões de anos</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> 220 km/s</div><div><span style="font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">~26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span> <span style="color: rgb(34, 34, 34);"> -26,74</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span> <span style="color: rgb(34, 34, 34);">4,85</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 4,6 Bilhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 1,9 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Principalmente hidrogênio e hélio, também contendo oxigênio, carbono, ferro, enxofre, neón, nitrogênio, sílicio e magnésio.</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; O sol possui manchas solares, que são soltadas por ele e são espalhadas ao seu redor através do vento solar</div><div>&gt; A temperatura da coroa do sol é muito maior do que a da superfície por causa do acúmulo energia ocorrido na coroa</div><div><br/></div><div><span src="assets/img/espaco/476_VenusTransitApproach1200w.jpg" type="image/jpeg" data-filename="476_VenusTransitApproach1200w.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/></div><div><br/></div><div><span src="assets/img/espaco/381_SunHighEnergyRays1200w.jpg" type="image/jpeg" data-filename="381_SunHighEnergyRays1200w.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/></div><div><br/></div><div><span src="assets/img/espaco/823_cover-1920_detail.jpg" type="image/jpeg" data-filename="823_cover-1920_detail.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="952"/></span>
<h1>Unukalhai</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 12:55</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:49</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação do Serpentário(Ophiuchus), Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=alf+Ser"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=alf+Ser</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K2 III</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Serpente</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 12 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> ? M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,5 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.498 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 73 anos-luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.083<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,62</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span> <span style="color: rgb(34, 34, 34);">0,88</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 4,3 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Metais neutros, hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [24].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Serpens_constellation_map.png" type="image/png" data-filename="250px-Serpens_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="955"/></span>
<h1>Capella Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 21:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Amarela, Braço de Órion, Centena de Milhões de Anos, Classificação Estelar G, Complexo de Superaglomerados Pisces-Cetus, Constelação do Colcheiro(Auriga), Estrela, Gigante, Grupo Local, Laniakea, Sistema Capella, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/capella.html"><i>http://stars.astro.illinois.edu/sow/capella.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> G1III</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Colcheiro(Auriga)</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,5 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 8,83<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,94 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.730 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 42 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.269<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,08</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 0,16</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 620 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 8,5 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio, hélio, carbono e oxigênio</div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/Capella1238233423.jpg" type="image/jpeg" data-filename="Capella1238233423.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/capella.jpg" type="image/jpeg" data-filename="capella.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/300px-Auriga_constellation_map.svg.png" type="image/png" data-filename="300px-Auriga_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="959"/></span>
<h1>Capella Aa</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 18:48</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca-Amarela, Centena de Milhões de Anos, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação do Colcheiro(Auriga), Estrela, Gigante, Grupo Local, Laniakea, Sistema Capella, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.constellation-guide.com/capella/"><i>https://www.constellation-guide.com/capella/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K0III</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Colcheiro(Auriga)</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 12 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 2,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,69 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.970 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 42 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.269<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,08</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 0,29</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 620 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 4,1 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Helio, carbono e oxigênio</div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/capella.gif" type="image/gif" data-filename="capella.gif" style="cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/capella [1].jpg" type="image/jpeg" data-filename="capella.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/300px-Auriga_constellation_map.svg [1].png" type="image/png" data-filename="300px-Auriga_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="963"/></span>
<h1>Deneb</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 15:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:44</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação do Cisne(Cygnus), Estrela, Estrela Única, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/deneb.html"><i>http://stars.astro.illinois.edu/sow/deneb.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> A2 la</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Cisne(Cygnus)</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 203 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 19 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,1 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 8.525 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 2.615 anos-luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.038<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,25</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span> <span style="color: rgb(34, 34, 34);">-8,38</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 20 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">// REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/03_HIP101825_A_20161220221211_1.jpg" type="image/jpeg" data-filename="03_HIP101825_A_20161220221211_1.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Deneb_2.jpg" type="image/jpeg" data-filename="Deneb_2.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Cygnus_constellation_map.png" type="image/png" data-filename="250px-Cygnus_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="967"/></span>
<h1>β Centauri B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 11:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:43</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Azul-Branca, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Estrela, Estrela Única, Grupo Local, Laniakea, Milhões de Anos, Sistema Beta Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/hadar.html"><i>http://stars.astro.illinois.edu/sow/hadar.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B1</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 4,6<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 390 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.878<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  4</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~14 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">/REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/BETACENTAURISISTEMI12.jpg" type="image/jpeg" data-filename="BETACENTAURISISTEMI12.jpg"/></div><div><br/></div><div><span src="assets/img/espaco/e7692faa6375e3812491b481e488af9a.jpg" type="image/jpeg" data-filename="e7692faa6375e3812491b481e488af9a.jpg"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Centaurus_constellation_map.png" type="image/png" data-filename="250px-Centaurus_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="971"/></span>
<h1>β Centauri Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 11:20</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:43</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Estrela, Gigante, Grupo Local, Laniakea, Milhões de Anos, Sistema Beta Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://arxiv.org/abs/astro-ph/0605220"><i>https://arxiv.org/abs/astro-ph/0605220</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B1III</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 10,3<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,5 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.000 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 390 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.878<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,88</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~14 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/BETACENTAURISISTEMI12 [1].jpg" type="image/jpeg" data-filename="BETACENTAURISISTEMI12.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/e7692faa6375e3812491b481e488af9a [1].jpg" type="image/jpeg" data-filename="e7692faa6375e3812491b481e488af9a.jpg" style="cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Centaurus_constellation_map [1].png" type="image/png" data-filename="250px-Centaurus_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="975"/></span>
<h1>Hadar</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 11:05</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:42</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Estrela, Gigante, Grupo Local, Laniakea, Milhões de Anos, Sistema Beta Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/hadar"><i>https://www.universeguide.com/star/hadar</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B1III</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 10,7<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,5 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.000 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 390 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.878<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,61</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -4,03</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~14 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/BETACENTAURISISTEMI12 [2].jpg" type="image/jpeg" data-filename="BETACENTAURISISTEMI12.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/e7692faa6375e3812491b481e488af9a [2].jpg" type="image/jpeg" data-filename="e7692faa6375e3812491b481e488af9a.jpg"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Centaurus_constellation_map [2].png" type="image/png" data-filename="250px-Centaurus_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="979"/></span>
<h1>Proxima Centauri</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 20:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:41</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Bolha Local, Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Estrela, Grupo Local, Laniakea, Nuvem G, Sistema Alpha Centauri, Sistema Próxima Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=V645+Cen"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=V645+Cen</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> M5.5Ve</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 0,15 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 0,12<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 5,2 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 3.042 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 4,26 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  11,3</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 15,6</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? 4,8 Bilhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> &gt;0,1 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Metais neutros e dióxidos de titânio</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Estrela mais próxima do sol</div><div><br/></div><div><span src="assets/img/espaco/gliese-1002-naine-rouge.jpg" type="image/jpeg" data-filename="gliese-1002-naine-rouge.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/New_shot_of_Proxima_Centauri,_our_nearest_neighbour.jpg" type="image/jpeg" data-filename="New_shot_of_Proxima_Centauri,_our_nearest_neighbour.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/eso1629b.jpg" type="image/jpeg" data-filename="eso1629b.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="983"/></span>
<h1>Rigil Kentaurus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 15:28</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:40</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Amarela, Anã, Bilhões de Anos, Bolha Local, Braço de Órion, Classificação Estelar G, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Estrela, Grupo Local, Laniakea, Nuvem G, Sistema Alpha Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/rigil-kent.html"><i>http://stars.astro.illinois.edu/sow/rigil-kent.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> G2V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 1,2 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,1<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,3 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.790 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 4,37 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,01</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 4,38</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 5 Bilhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 2,7 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados e metais neutros</div><div><br/></div><div><span src="assets/img/espaco/rigilkentaurus.jpg" type="image/jpeg" data-filename="rigilkentaurus.jpg" style="cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//CONCEPÇÃO ARTÍSTICA</span></div><div><span src="assets/img/espaco/class_g_star_small.jpg" type="image/jpeg" data-filename="class_g_star_small.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="986"/></span>
<h1>Toliman</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 15:44</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:40</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Bolha Local, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Estrela, Grupo Local, Laniakea, Laranja, Nuvem G, Sistema Alpha Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K1V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 0,83 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 0,9<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,37 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.260 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 4,37 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,33</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 5,71</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ~ 5 Bilhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 1,1 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados e metais neutros</div><div><br/></div><div><span src="assets/img/espaco/class_k_star_large.jpg" type="image/jpeg" data-filename="class_k_star_large.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Image [25].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="989"/></span>
<h1>Mirzam</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>26/01/2019 23:12</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:36</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Bolha Local, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação do Cão Maior, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Milhões de Anos, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Beta+CMa"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Beta+CMa</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B1 II-III</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Cão Maior</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 9,7 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 13,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,79 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 23.150 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 490 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,98</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,95</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 12,4 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 31 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [26].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="992"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Canis_Major_constellation_map.svg.png" type="image/png" data-filename="250px-Canis_Major_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="992"/></span>
<h1>Sirius B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 16:02</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Centena de Milhões de Anos, Classificação Estelar DA, Complexo de Superaglomerados Pisces-Cetus, Constelação do Cão Maior, Estrela, Grupo Local, Laniakea, Sistema Sirius, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/siriusb"><i>https://www.universeguide.com/star/siriusb</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> DA2</span></div><div style="text-align: left;"><span style="font-weight: bold;">Constelação:</span><span style="background-color: rgb(249, 249, 249);"> Cão Maior</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 0,0084 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,01<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 8,57 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.000 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ? anos</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> </span>24.142<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 8,6 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  8,44</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 11,18</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 228 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio, Hélio e metais</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/600-41169362-massive-star.jpg" type="image/jpeg" data-filename="600-41169362-massive-star.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA COMPARANDO COM A TERRA</span></div><div><span src="assets/img/espaco/tumblr_lni23tyBU21qzmwk7o1_500.jpg" type="image/jpeg" data-filename="tumblr_lni23tyBU21qzmwk7o1_500.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Siriuscrop-1.jpg" type="image/jpeg" data-filename="Siriuscrop-1.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/249px-Canis_Major_constellation_map.svg.png" type="image/png" data-filename="249px-Canis_Major_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/>  </div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="998"/></span>
<h1>Associação Upper Sco OB 2(US)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 13:07</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass tipo OB, Ass US, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.pas.rochester.edu/~emamajek/scocen.pdf"><i>http://www.pas.rochester.edu/~emamajek/scocen.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Constelações:</span> Escorpião</span></div><div><span style="font-weight: bold;">Classificação Estelar:</span> O e B</div><div><span style="font-weight: bold;">Distância do Sol:</span> 472 anos-luz</div><div><span style="font-weight: bold;">Idade:</span> 5-11 milhões de anos</div><div><span style="font-weight: bold;">Estrelas:</span> 120 pelo menos(massivas), mais de mil estrelas jovens</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [27].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1000"/></span>
<h1>Sistema Theta Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 13:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema Binário de Estrelas, Sistema Estelar, Sistema Múltiplo, Sistema Theta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/girtab.html"><i>http://stars.astro.illinois.edu/sow/girtab.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Sargas e θ Scorpii B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Sargas</div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.000 anos-luz</span></div><div><b>Distância da Terra:</b> 270 anos-luz</div><div><br/></div><div><span src="assets/img/espaco/135178329.CCsMQ1xD.jpg" type="image/jpeg" data-filename="135178329.CCsMQ1xD.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [12].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1003"/></span>
<h1>Sigma Orionis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 17:26</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 17:36</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado Aberto, Aglomerado de Estrelas, Ass Ori OB 1b, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Sigma Orionis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astro.sunysb.edu/fwalter/PUBS/sigori.pdf"><i>http://www.astro.sunysb.edu/fwalter/PUBS/sigori.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Constelações:</b> Órion
<div><b>Distância:</b> 1.369 anos-luz</div><div><b>Magnitude Aparente:</b> 18</div><div><b>Idade:</b> 2-3 milhões de anos </div><div><b>Número estimado de estrelas: </b>~ 700 estrelas e subestrelas</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Sigma_orionis_cluster.jpg" type="image/jpeg" data-filename="Sigma_orionis_cluster.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><i>// LOCALIZAÇÃO DO AGLOMERADO</i></div><div><span src="assets/img/espaco/Orion_constellation_map.svg.png" type="image/png" data-filename="Orion_constellation_map.svg.png"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1006"/></span>
<h1>Loop de Barnard</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 17:07</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 17:19</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Emissão, Grupo Local, Laniakea, Nebulosa, Região HII, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://uknowledge.uky.edu/cgi/viewcontent.cgi?referer=https://www.google.de/&httpsredir=1&article=1073&context=physastron_facpub"><i>https://uknowledge.uky.edu/cgi/viewcontent.cgi?referer=https://www.google.de/&amp;httpsredir=1&amp;article=1073&amp;context=physastron_facpub</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>ID:</b> Sh 2-276</div><b>Tipo:</b> Emissão/Reflexão
<div><b>Constelação:</b> Órion</div><div><b>Magnitude Aparente:</b> 5</div><div><b>Dimensão Aparente:</b> 41x27 arcmins</div><div><b>Raio:</b> 150 anos-luz</div><div><b>Distância:</b> 1.600 anos-luz     </div><div><b>Densidade:</b> ?</div><div><b>Temperatura Média</b><b>:</b> 5.960 K</div><div><b>Composição:</b> Gás, poeira estelar, hidrogênio e metais pesados</div><div><b>Massa:</b> ? <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/399px-BarnardLoopHunterWilson.jpg" type="image/jpeg" data-filename="399px-BarnardLoopHunterWilson.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1008"/></span>
<h1>Nuvem Gigante Molecular Órion A</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 16:47</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 16:51</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, NGM Ori A, Nuvem Gigante Molecular, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://astronomy.com/news/2018/05/the-most-detailed-map-of-the-orion-a-molecular-cloud-to-date"><i>http://astronomy.com/news/2018/05/the-most-detailed-map-of-the-orion-a-molecular-cloud-to-date</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Constelações:</b> Órion
<div><b>Distância:</b> ~ 1.200 anos-luz</div><div><b>Massa:</b> &gt; ?<b> </b><span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub> <b><br/>
</div>
Raio:</b> ?</div><div><b>Composição:</b> Hidrogênio, hélio, carbono e oxigênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/eso1701-compa.jpg" type="image/jpeg" data-filename="eso1701-compa.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1010"/></span>
<h1>Nuvem Gigante Molecular Órion B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 16:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 16:40</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, NGM Ori B, Nuvem Gigante Molecular, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.mpia.de/homes/rlau/Papers/oribook.pdf"><i>http://www.mpia.de/homes/rlau/Papers/oribook.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Constelações:</b> Órion
<div><b>Distância:</b> ~ 1.300 anos-luz</div><div><b>Massa:</b> &gt; 10.000<b> </b><span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub> <b><br/>
</div>
Raio:</b> ?</div><div><b>Composição:</b> Hidrogênio, hélio, carbono e oxigênio</div><div><br/></div><div><b><br/></b></div><div><span src="assets/img/espaco/Image [28].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div></span>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1012"/></span>
<h1>Nebulosa da Cabeça de Cavalo</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 13:49</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 15:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Difusa, Grupo Local, Laniakea, Nebulosa, Nebulosa Escura, NGM Ori B, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NGC+2023"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NGC+2023</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>ID:</b> Barnard 33 | LDN 1630</div><b>Tipo:</b> Escura
<div><b>Constelação:</b> Órion</div><div><b>Magnitude Aparente:</b> ?</div><div><b>Dimensão Aparente:</b> 8x6 arcmins</div><div><b>Raio:</b> 3.5 anos-luz</div><div><b>Distância:</b> 1.500 anos-luz     </div><div><b>Densidade:</b> ?</div><div><b>Temperatura Média</b><b>:</b> 7.600 - 8.000 K</div><div><b>Composição:</b> Gás e poeira estelar, hélio e muito hidrogênio ionizado</div><div><b>Massa:</b> 27 <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/583px-A_reproduction_of_a_composite_colour_image_of_the_Horsehead_Nebula_and_its_immediate_surroundings_-_Eso0202a.jpg" type="image/jpeg" data-filename="583px-A_reproduction_of_a_composite_colour_image_of_the_Horsehead_Nebula_and_its_immediate_surroundings_-_Eso0202a.jpg" style="cursor: default;cursor: default;"/><br/></div><div><br/></div><div><i>/// LOCALIZAÇÃO DA NEBULOSA</i></div><div><span src="assets/img/espaco/Orion_constellation_map [2].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1015"/></span>
<h1>NGC 2023</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 14:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 15:21</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Emissão/Reflecção, Grupo Local, Laniakea, Nebulosa, NGM Ori B, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>ID:</b> NGC 2023</div><b>Tipo:</b> Emissão/Reflexão
<div><b>Constelação:</b> Órion</div><div><b>Magnitude Aparente:</b> ?</div><div><b>Dimensão Aparente:</b> 10x8 arcmins</div><div><b>Raio:</b> 4.3 anos-luz</div><div><b>Distância:</b> 1.467 anos-luz     </div><div><b>Densidade:</b> ?</div><div><b>Temperatura Média</b><b>:</b> ? K</div><div><b>Composição:</b> Gás e poeira estelar e muito hidrogênio</div><div><b>Massa:</b> ? <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/610px-Sunset_Glow_in_Orion.jpg" type="image/jpeg" data-filename="610px-Sunset_Glow_in_Orion.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/Barnard_33.jpg" type="image/jpeg" data-filename="Barnard_33.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1018"/></span>
<h1>IC 434</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 13:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 14:15</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Emissão, Grupo Local, Laniakea, Nebulosa, NGM Ori B, Região HII, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://books.google.com.br/books?id=3upwkyksAJ8C&pg=PA194&lpg=PA194&dq=ic+434+density&source=bl&ots=ZZ3Gef2GjB&sig=ACfU3U0qC1jxDTej878WeOEY6T0w7qAI-A&hl=pt-PT&sa=X&ved=2ahUKEwiB0t7n-KTgAhWJILkGHd-VBisQ6AEwCHoECAIQAQ"><i>https://books.google.com.br/books?id=3upwkyksAJ8C&amp;pg=PA194&amp;lpg=PA194&amp;dq=ic+434+density&amp;source=bl&amp;ots=ZZ3Gef2GjB&amp;sig=ACfU3U0qC1jxDTej878WeOEY6T0w7qAI-A&amp;hl=pt-PT&amp;sa=X&amp;ved=2ahUKEwiB0t7n-KTgAhWJILkGHd-VBisQ6AEwCHoECAIQAQ</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>ID:</b> IC 434</div><b>Tipo:</b> Emissão
<div><b>Constelação:</b> Órion</div><div><b>Magnitude Aparente:</b> 7,3</div><div><b>Dimensão Aparente:</b> 65X60 arcmins</div><div><b>Raio:</b> 12.5 anos-luz</div><div><b>Distância:</b> 1.500 anos-luz</div><div><b>Densidade:</b> 10[ev6] cm-³</div><div><b>Temperatura Média</b><b>:</b> 7.600 - 8.000 K</div><div><b>Composição:</b> Gás e poeira estelar e muito hidrogênio ionizado</div><div><b>Massa:</b> ?</div><div><br/></div><div><i>// IMAGEM DA NEBULOSA QUE RODEIA A NGC 2023 E FICA EMBAIXO DA CABEÇA DO CAVALO</i><span src="assets/img/espaco/Barnard_33 [1].jpg" type="image/jpeg" data-filename="Barnard_33.jpg" style="cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Barnard33.jpg" type="image/jpeg" data-filename="Barnard33.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><i>/// LOCALIZAÇÃO DA NEBULOSA</i></div><div><span src="assets/img/espaco/Orion_constellation_map [3].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1022"/></span>
<h1>Nebulosa De Mairan</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 11:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 14:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Emissão, Grupo Local, Laniakea, Nebulosa, Nebulosa de Órion, NGM Ori A, Região HII, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.messier-objects.com/messier-43-de-mairans-nebula/"><i>https://www.messier-objects.com/messier-43-de-mairans-nebula/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>ID:</b> M43</div><b>Tipo:</b> Emissão
<div><b>Constelação:</b> Órion</div><div><b>Magnitude Aparente:</b> 9,0</div><div><b>Dimensão Aparente:</b> 20x15 arcmins</div><div><b>Raio:</b> 4.5 anos-luz</div><div><b>Distância:</b> 1.600 anos-luz</div><div><b>Densidade:</b> ?</div><div><b>Temperatura Média</b><b>:</b> ? K</div><div><b>Composição:</b> Gás e poeira estelar e muito hidrogênio ionizado</div><div><b>Massa:</b> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/600px-M43_HST.jpg" type="image/jpeg" data-filename="600px-M43_HST.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA NEBULOSA</i></div><div><span src="assets/img/espaco/Orion_constellation_map [4].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1025"/></span>
<h1>Nebulosa de Órion</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 10:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 14:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Emissão/Reflecção, Grupo Local, Laniakea, Nebulosa, Nebulosa de Órion, NGM Ori A, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/feature/goddard/2017/messier-42-the-orion-nebula"><i>https://www.nasa.gov/feature/goddard/2017/messier-42-the-orion-nebula</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>ID:</b> M42</div><b>Tipo:</b> Emissão/Reflecção
<div><b>Constelação:</b> Órion</div><div><b>Magnitude Aparente:</b> 4,0</div><div><b>Dimensão Aparente:</b> 65x60 arcmins</div><div><b>Raio:</b> 12-30 anos-luz</div><div><b>Distância:</b> 1.500 anos-luz</div><div><b>Densidade: </b><span style="background-color: rgb(255, 255, 255);">600 átomos</span><span style="background-color: rgb(255, 255, 255);">/cm³</span></div><div><b>Temperatura Média</b><b>:</b> 70 K| &lt; 10.000 K</div><div><b>Composição:</b> Gás e poeira estelar, hidrogênio, oxigênio e possivelmente água</div><div><b>Massa:</b> 2.000 <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><b><br/></b></span></font></div><div><b><br/></b></div><div><span src="assets/img/espaco/orion-nebula-xlarge_web.jpg" type="image/jpeg" data-filename="orion-nebula-xlarge_web.jpg" height="985" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="985"/><br/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA NEBULOSA</i></div><div><span src="assets/img/espaco/Orion_constellation_map [5].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1028"/></span>
<h1>Complexo de Nuvens Moleculares Órion</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 13:29</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 13:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Nuvens Moleculares, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.daviddarling.info/encyclopedia/O/Orion_Complex.html"><i>http://www.daviddarling.info/encyclopedia/O/Orion_Complex.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Distancia:</b> 1.000 - 1.400 anoz-luz</div><div><b>Idade:</b> &gt; 12 milhões de anos</div><div><b>Massa Total:</b> ? <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Diâmetro:</b> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Orion_Head_to_Toe.jpg" type="image/jpeg" data-filename="Orion_Head_to_Toe.jpg" style="cursor: default;"/><br/></div><div><br/></div><div><br/></div><div><br/></div><br/></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1030"/></span>
<h1>Trapézio(θ¹ Orionis)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/02/2019 10:48</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 11:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado de Estrelas, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa de Órion, NGM Ori A, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.messier-objects.com/trapezium-cluster/"><i>https://www.messier-objects.com/trapezium-cluster/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Constelações:</b> Órion
<div><b>Distância:</b> 1.600 anos-luz</div><div><b>Magnitude Aparente:</b> 4.0</div><div><b>Idade:</b> &gt; 300 mil anos </div><div><b>Número estimado de estrelas:</b> 5 brilhantes e mais</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Trapezium-Cluster.jpg" type="image/jpeg" data-filename="Trapezium-Cluster.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><i>// LOCALIZAÇÃO DO AGLOMERADO</i></div><div><span src="assets/img/espaco/483px-Orion_IAU.svg.png" type="image/png" data-filename="483px-Orion_IAU.svg.png"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1033"/></span>
<h1>Grupo de Associações Orion OB 1</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 22:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 10:32</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo de Associações, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://arxiv.org/pdf/0812.0046.pdf"><i>https://arxiv.org/pdf/0812.0046.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> 1.304 anos-luz</div><div><b>Idade:</b> 3-20 milhões de anos</div><div>     </div><div><br/></div><div><span src="assets/img/espaco/400px-Orion_OB1_&_25_Ori_Group.png" type="image/png" data-filename="400px-Orion_OB1_&_25_Ori_Group.png" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1035"/></span>
<h1>Associação Ori OB 1c</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 10:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 10:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1c, Ass tipo OB, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://arxiv.org/pdf/0812.0046.pdf"><i>https://arxiv.org/pdf/0812.0046.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Órion</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> ? anos-luz(1.300)</div><div><b>Idade:</b> 3-6 milhões de anos</div><div><b>Estrelas:</b> ? [B3 or earlier -&gt; 13]</div><div><br/>
----- <b>Curiosidades -----</b></div><div>&gt; Fica na região da espada de Órion</div><div><br/></div><div><span src="assets/img/espaco/Image [29].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/220px-Orion_OB1_&_25_Ori_Group [2].png" type="image/png" data-filename="220px-Orion_OB1_&_25_Ori_Group.png" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Image [30].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1039"/></span>
<h1>Associação Ori OB 1b</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 10:57</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/02/2019 10:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Ass tipo OB, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://arxiv.org/pdf/0812.0046.pdf"><i>https://arxiv.org/pdf/0812.0046.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Órion</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> 1.300 anos-luz</div><div><b>Idade:</b> 1,7-8 milhões de anos</div><div><b>Estrelas:</b> ?(3 brilhantes, B3&gt;=[13])</div><div><br/>
----- <b>Curiosidades -----</b></div><div>&gt; As estrelas do Cinturão de Órion</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/orion_mutti.jpg" type="image/jpeg" data-filename="orion_mutti.jpg" height="600" style="cursor: default;cursor: default;cursor: default;" width="900"/><br/></div><div><br/></div><div><span src="assets/img/espaco/220px-Orion_OB1_&_25_Ori_Group [3].png" type="image/png" data-filename="220px-Orion_OB1_&_25_Ori_Group.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [31].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1043"/></span>
<h1>Constelação de Cassiopeia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 21:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 22:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Cassiopeia, Hemisfério Norte, SGQ2</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Cassiopeia, a Rainha Sentada
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"> Cassiopeiae</font></div><div><b>Abreviação:</b> Cas</div><div><b>Área Total:</b> 804º quadrados </div><div><b>Quadrante:</b> SGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 5</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 53 </div><div style="text-align: left;"><b>Meridiano:</b> Novembro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CassiopeiaCC.jpg" type="image/jpeg" data-filename="CassiopeiaCC.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Cassiopeia_(image_right_side_up).jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Cassiopeia_(image_right_side_up).jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/603px-Cassiopeia_IAU.svg.png" type="image/png" data-filename="603px-Cassiopeia_IAU.svg.png"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1047"/></span>
<h1>Associação Cas-Tau OB</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 21:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 21:44</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Cas-Tau OB, Ass tipo OB, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Cassiopeia, Constelação de Touro, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://books.google.com.br/books?id=X91UBLr64FMC&pg=PA110&lpg=PA110&dq=Cassiopeia-taurus+ob&source=bl&ots=anhDmfsXN0&sig=ACfU3U0vmQEblsF-qZ3208N1MXiJ-qTGVA&hl=pt-PT&sa=X&ved=2ahUKEwik6Jf5naPgAhXDE7kGHRI-A8IQ6AEwD3oECAcQAQ#v=onepage&q=Cassiopeia-taurus%20ob&f=false"><i>https://books.google.com.br/books?id=X91UBLr64FMC&amp;pg=PA110&amp;lpg=PA110&amp;dq=Cassiopeia-taurus+ob&amp;source=bl&amp;ots=anhDmfsXN0&amp;sig=ACfU3U0vmQEblsF-qZ3208N1MXiJ-qTGVA&amp;hl=pt-PT&amp;sa=X&amp;ved=2ahUKEwik6Jf5naPgAhXDE7kGHRI-A8IQ6AEwD3oECAcQAQ#v=onepage&amp;q=Cassiopeia-taurus%20ob&amp;f=false</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Cassiopeia e Touro</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> 684 anos-luz</div><div><b>Idade:</b> ? milhões de anos</div><div><b>Estrelas:</b> 83(B)</div><div><br/></div><div><br/></div><div><i>// LOCALIZAÇÃO DA ASSOCIAÇÃO REPRESENTADA</i></div><div><span src="assets/img/espaco/Image [32].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1049"/></span>
<h1>Associação Cep OB 6</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 16:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 21:36</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Cep OB 6, Ass tipo OB, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Cepheus, Grupo Local, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%40122760&Name=Ass%20Cep%20OB%206&submit=submit"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%40122760&amp;Name=Ass%20Cep%20OB%206&amp;submit=submit</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Cepheus</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância:</b> 880 anos-luz</div><div><b>Idade:</b> ?</div><div><b>Estrelas:</b> 83(Tipo B)</div><div><br/></div><div><br/></div><span src="assets/img/espaco/Image [33].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1051"/></span>
<h1>Associação Lower Cen-Cru OB 2(LCC)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 13:19</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 21:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass LCC, Ass tipo OB, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Constelação do Centauro, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.pas.rochester.edu/~emamajek/scocen.pdf"><i>http://www.pas.rochester.edu/~emamajek/scocen.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Centauro e Crux</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> 384 anos-luz</div><div><b>Idade:</b> 15 milhões de anos</div><div><b>Estrelas:</b> aglomerado de estrelas jovens, 180 (massivas)</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [34].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Image [35].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1054"/></span>
<h1>Perseus OB3</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 21:04</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 21:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado Aberto, Aglomerado de Estrelas, Ass Per OB 3, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Perseu, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alphper-p.html"><i>http://stars.astro.illinois.edu/sow/alphper-p.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Constelações:</b> Perseus
<div><b>Distância:</b> 600 anos-luz</div><div><b>Magnitude Aparente:</b> 1.2</div><div><b>Idade:</b> 50-70 milhões de anos</div><div><b>Número estimado de estrelas:</b> &gt; 30 (Predomina tipo B)</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/perseus.jpg" type="image/jpeg" data-filename="perseus.jpg" height="584" style="cursor: default;cursor: default;cursor: default;cursor: default;" width="880"/><br/></div><div><br/></div><div><i>// LOCALIZAÇÃO DO AGLOMERADO</i></div><div><span src="assets/img/espaco/Perseus_constellation_map.svg.png" type="image/png" data-filename="Perseus_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1057"/></span>
<h1>Trumpler 10</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 15:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 21:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado Aberto, Aglomerado de Estrelas, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.univie.ac.at/webda/cgi-bin/ocl_page.cgi?dirname=tr10"><i>https://www.univie.ac.at/webda/cgi-bin/ocl_page.cgi?dirname=tr10</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Constelações:</b> Vela
<div><b>Distância:</b> 1.100 anos-luz</div><div><b>Magnitude Aparente:</b> 4.6</div><div><b>Idade:</b> 38-47 milhões de anos</div><div><b>Número estimado de estrelas:</b> 29(talvez 48)</div><div><b><br/></b></div><div><b><br/></b><div><span src="assets/img/espaco/tr10.png" type="image/png" data-filename="tr10.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><b><br/></b></div></div><div><br/></div><div><i>// GRÁFICO DE TIPO ESPECTRAL DAS ESTRELAS NO AGLOMERADO</i></div><div><span src="assets/img/espaco/Image [36].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1060"/></span>
<h1>δ Velorum D</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 17:49</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 17:55</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Sistema Delta Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/deltavel.html"><i>http://stars.astro.illinois.edu/sow/deltavel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> (M)</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Vela</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 80 anos-luz</span></div><div><b>Distância do Centro Galático:</b><b>  </b>24.132<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  13</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b> <font color="#222222">?</font></div><div><b>Composição: </b>Metais neutros e dióxidos de Titânio</div><div><br/></div><div><span src="assets/img/espaco/artworks-000302059005-kzwe3x-t500x500.jpg" type="image/jpeg" data-filename="artworks-000302059005-kzwe3x-t500x500.jpg" style="cursor: default;"/></div><div><br/></div><div><i>// LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [1].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1064"/></span>
<h1>Sirius A</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 19:22</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Centena de Milhões de Anos, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação do Cão Maior, Estrela, Grupo Local, Laniakea, Sistema Sirius, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/sirius.html"><i>http://stars.astro.illinois.edu/sow/sirius.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> A1V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Cão Maior</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 1,71 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 2,06<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,33 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 9.940 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ? anos</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.142<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 8,6 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  -1,47</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 1,42</span></div><div><span style="color: rgb(34, 34, 34);"><b>Idade:</b> 242 Milhões de Anos</span></div><div><span style="color: rgb(34, 34, 34);"><b>Velocidade de Rotação:</b> 16 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e rica em metais ionizados</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/foto.jpg" type="image/jpeg" data-filename="foto.jpg"/></div><div><br/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/249px-Canis_Major_constellation_map.svg [1].png" type="image/png" data-filename="249px-Canis_Major_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1067"/></span>
<h1>Aldebarã</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 17:33</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:31</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bilhões de Anos, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Touro, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/aldebaran.html"><i>http://stars.astro.illinois.edu/sow/aldebaran.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K5 lll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Touro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 44 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,16<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,59 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 3.910 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 65 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,86</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -0,61</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 6,4 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 2,7 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Metais neutros, hidrogênio e hélio</div><div><br/></div><div><span style="font-style: italic;">//CONCEPÇÃO ARTÍSTICA</span></div><div><span src="assets/img/espaco/estrelas aldebaran.png" type="image/png" data-filename="estrelas aldebaran.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/download [11].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/TAU.gif" type="image/gif" data-filename="TAU.gif" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1071"/></span>
<h1>Elnath</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 18:17</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Centena de Milhões de Anos, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Touro, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://wiki.travellerrpg.com/Beta_Tauri_(star)"><i>http://wiki.travellerrpg.com/Beta_Tauri_(star)</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B7III</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Touro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 4,2 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,65 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 13.824 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 134 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.269<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,65</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -1,42</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 100 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 59 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Manganês, Mercúrio, Fósforo, Sílicio, hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/B_class_star.jpg" type="image/jpeg" data-filename="B_class_star.jpg" style="cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/elnath-star-beta-tauri.jpg" type="image/jpeg" data-filename="elnath-star-beta-tauri.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/240px-Taurus_constellation_map.svg.png" type="image/png" data-filename="240px-Taurus_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1075"/></span>
<h1>Mirfak</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 10:10</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Per OB 3, Braço de Órion, Branca-Amarela, Cinturão de Gould, Classificação Estelar F, Complexo de Superaglomerados Pisces-Cetus, Constelação de Perseu, Estrela, Estrela Única, Grupo Local, Laniakea, Milhões de Anos, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/mirfak"><i>https://www.universeguide.com/star/mirfak</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">F5 Ib</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Perseu</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 68 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 8,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,9 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 6.350 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 510 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span> <span style="color: rgb(34, 34, 34);">24.622 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,8</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -5,1</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 41 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 20 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/alpha-persei-star.jpg" type="image/jpeg" data-filename="alpha-persei-star.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Mirfak.jpg" type="image/jpeg" data-filename="Mirfak.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/Perseus_constellation_map.svg [1].png" type="image/png" data-filename="Perseus_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1079"/></span>
<h1>α Piscium B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 11:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca, Complexo de Superaglomerados Pisces-Cetus, Constelação de Peixes, Estrela, Grupo Local, Laniakea, Sistema Alpha Piscium, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://wiki.travellerrpg.com/Alrescha_(star)"><i>http://wiki.travellerrpg.com/Alrescha_(star)</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> A3 V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Peixes</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 1.8<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 8.600 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 151 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  5,38</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 0,5</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 84 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><span src="assets/img/espaco/alpha_psc.jpg" type="image/jpeg" data-filename="alpha_psc.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1081"/></span>
<h1>Alrescha</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 15:43</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação de Peixes, Estrela, Grupo Local, Laniakea, Sistema Alpha Piscium, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=alf+Psc+A"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=alf+Psc+A</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> A0p</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Peixes</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 2,3<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 9.000 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 151 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,82</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 0,5</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 81 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><span src="assets/img/espaco/alphapisciumphoto.jpg" type="image/jpeg" data-filename="alphapisciumphoto.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/1000px-Pisces_constellation_map.svg.png" type="image/png" data-filename="1000px-Pisces_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1084"/></span>
<h1>Rígel</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 12:21</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Órion, Estrela, Grupo Local, Laniakea, Milhões de Anos, Sistema Beta Orionis, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=RIGEL"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=RIGEL</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B8 Ia</span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; white-space: nowrap; font-family: sans-serif;">      </span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 79 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 24<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,75 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 12.100 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 860 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 24.814 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,13</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -7,84</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 8 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 25 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPERESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/rigel-2222.jpg" type="image/jpeg" data-filename="rigel-2222.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/IC2118_Master.jpg" type="image/jpeg" data-filename="IC2118_Master.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//COMPARAÇÃO DA ESTRELA COM O SOL</span></div><div><span src="assets/img/espaco/Rigel_sun_comparison.png" type="image/png" data-filename="Rigel_sun_comparison.png" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO NO MAPA</span></div><div><span src="assets/img/espaco/Orion_constellation_map [6].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1089"/></span>
<h1>δ Orionis Aa2</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 20:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Azul, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Estrela, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Sistema Delta Orionis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+36486"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+36486</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span><span style="white-space: nowrap; font-weight: bold; font-family: sans-serif;"> </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; white-space: nowrap; font-family: sans-serif;">B1V</span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; white-space: nowrap; font-family: sans-serif;"> </span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6,5 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 8,4<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,9 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.600 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 1.200 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.303 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,9</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,9     </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 150 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Mintaka.jpg" type="image/jpeg" data-filename="Mintaka.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//REPRESENTAÇÃO GRÁFICA E LOCALIZAÇÃO DA ESTRELA</div><div><span src="assets/img/espaco/Figure-1Artists-impression-of-the-triple-system-d-Ori-A-as-viewed-from-Earth-The.png" type="image/png" data-filename="Figure-1Artists-impression-of-the-triple-system-d-Ori-A-as-viewed-from-Earth-The.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Orion_constellation_map.svg.png" type="image/png" data-filename="250px-Orion_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1093"/></span>
<h1>δ Orionis Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 09:30</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Azul, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Estrela, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Sistema Delta Orionis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astronomytrek.com/star-facts-mintaka/"><i>http://www.astronomytrek.com/star-facts-mintaka/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span><span style="white-space: nowrap; font-weight: bold; font-family: sans-serif;"> </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; white-space: nowrap; font-family: sans-serif;">B0IV</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 10,4 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 22,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,5 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 24.800 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 1.200 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.303 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,9</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -4,2 </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 220 km/s</span><span style="color: rgb(34, 34, 34);"> </span></div><div><span style="font-weight: bold;">Composição:</span> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Mintaka [1].jpg" type="image/jpeg" data-filename="Mintaka.jpg" style="cursor: default;"/></div><div><br/></div><div>//REPRESENTAÇÃO GRÁFICA E LOCALIZAÇÃO DA ESTRELA</div><div><span src="assets/img/espaco/Figure-1Artists-impression-of-the-triple-system-d-Ori-A-as-viewed-from-Earth-The [1].png" type="image/png" data-filename="Figure-1Artists-impression-of-the-triple-system-d-Ori-A-as-viewed-from-Earth-The.png" style="cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Orion_constellation_map.svg [1].png" type="image/png" data-filename="250px-Orion_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1097"/></span>
<h1>Mintaka</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 15:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Azul, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Classificação Estelar O, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Estrela, Gigante, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Sistema Delta Orionis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/mintaka"><i>https://www.universeguide.com/star/mintaka</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; white-space: nowrap; font-family: sans-serif;">O9.5II</span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; white-space: nowrap; font-family: sans-serif;">      </span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 16,5 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 24<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,37 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 29.500 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span> <span style="color: rgb(34, 34, 34);">1.200 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.303 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,5</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -6,8</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 130 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/tumblr_ozguv4aCAZ1vjhboso1_1280.jpg" type="image/jpeg" data-filename="tumblr_ozguv4aCAZ1vjhboso1_1280.jpg" style="cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Mintaka [2].jpg" type="image/jpeg" data-filename="Mintaka.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Orion_constellation_map.svg [2].png" type="image/png" data-filename="250px-Orion_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1101"/></span>
<h1>Alnilam</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 15:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:23</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Azul, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Estrela, Estrela Única, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Milhões de Anos, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, ε</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/alnilam"><i>https://www.universeguide.com/star/alnilam</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B0 Ia</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 32,4 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 40<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,0 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 27.500 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 2.000 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.303 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,69</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -6,8</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 5,7 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 55 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA DA ESTRELA</span></div><div><span src="assets/img/espaco/tumblr_ozguv4aCAZ1vjhboso1_1280 [1].jpg" type="image/jpeg" data-filename="tumblr_ozguv4aCAZ1vjhboso1_1280.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Alnilam.jpg" type="image/jpeg" data-filename="Alnilam.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/260px-Orion_constellation_map.svg [1].png" type="image/png" data-filename="260px-Orion_constellation_map.svg.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1105"/></span>
<h1>ζ Orionis B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 12:52</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Azul, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Estrela, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Milhões de Anos, Sistema Zeta Orionis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ζ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.cloudynights.com/topic/568851-alnitak-b/"><i>https://www.cloudynights.com/topic/568851-alnitak-b/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B0III</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 14<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 816 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.075 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  4,1</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -4,1</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 7 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 350 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/post-210367-0-32965400-1488775301_thumb.jpg" type="image/jpeg" data-filename="post-210367-0-32965400-1488775301_thumb.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO DA ESTRELA &quot;B&quot;</span></div><div><span src="assets/img/espaco/alnitak3.jpg" type="image/jpeg" data-filename="alnitak3.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/Orion_constellation_map [7].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1109"/></span>
<h1>ζ Orionis Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 12:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Azul, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Estrela, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Milhões de Anos, Sistema Zeta Orionis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ζ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alnitak.html"><i>http://stars.astro.illinois.edu/sow/alnitak.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">B1IV</span><span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;"> </span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 7,3 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 14<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 29.000 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 816 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.075 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  4,28</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,9</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 7,2 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/Ngc2024_2mass [1].jpg" type="image/jpeg" data-filename="Ngc2024_2mass.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO MULTIPLO SISTEMA ZETA ORIONIS</span></div><div><span src="assets/img/espaco/alnitak1.jpg" type="image/jpeg" data-filename="alnitak1.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/Orion_constellation_map [8].png" type="image/png" data-filename="Orion_constellation_map.png"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1113"/></span>
<h1>Alnitak</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 11:57</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:21</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Azul, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Classificação Estelar O, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Estrela, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Milhões de Anos, Sistema Zeta Orionis, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, ζ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alnitak.html"><i>http://stars.astro.illinois.edu/sow/alnitak.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">O9.5Iab</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Órion</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 20 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 33<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,2 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 29.500 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 816 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.075 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,08</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -6,0</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 6,4 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 110 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/c005856e7762e6be90e2380cef29f356020161afr1-489-301v2_00.jpg" type="image/jpeg" data-filename="c005856e7762e6be90e2380cef29f356020161afr1-489-301v2_00.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Ngc2024_2mass [2].jpg" type="image/jpeg" data-filename="Ngc2024_2mass.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO COMPARANDO COM O SOL</span></div><div><span src="assets/img/espaco/220px-Alnitak_sun_comparison.svg.png" type="image/png" data-filename="220px-Alnitak_sun_comparison.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/Orion_constellation_map [9].png" type="image/png" data-filename="Orion_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1118"/></span>
<h1>Kakkab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 12:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass UCL, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Lupus, Estrela, Gigante, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Alpha Lupi, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/kakkab.html"><i>http://stars.astro.illinois.edu/sow/kakkab.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B1.5III</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 10,1<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,5 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 21.820 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 460 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.780<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,29</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -4,3</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 18 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 16 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Alfa_Lupi.jpg" type="image/jpeg" data-filename="Alfa_Lupi.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Lupus_constellation_map.png" type="image/png" data-filename="250px-Lupus_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1121"/></span>
<h1>α Lupi B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 12:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass UCL, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Lupus, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Alpha Lupi, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/men"><i>https://www.universeguide.com/star/men</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> ?</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Centauro</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> ?<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 460 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.780<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  14</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição:  </span>?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Alfa_Lupi [1].jpg" type="image/jpeg" data-filename="Alfa_Lupi.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Lupus_constellation_map [1].png" type="image/png" data-filename="250px-Lupus_constellation_map.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1125"/></span>
<h1>δ Velorum C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 17:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 17:55</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Sistema Delta Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/deltavel.html"><i>http://stars.astro.illinois.edu/sow/deltavel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> (M)</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Vela</span></div><b>Raio equatorial:</b> 1,43 R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 1,35 M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> 6.600 K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 80 anos-luz</span></div><div><b>Distância do Centro Galático:</b><b>  </b>24.132<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  11</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b> <font color="#222222">?</font></div><div><b>Composição: </b>Metais neutros e dióxidos de Titânio</div><div><br/></div><div><span src="assets/img/espaco/artworks-000302059005-kzwe3x-t500x500 [1].jpg" type="image/jpeg" data-filename="artworks-000302059005-kzwe3x-t500x500.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><i>// LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [2].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1128"/></span>
<h1>Constelação do Cisne(Cygnus)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 13:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 14:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação do Cisne(Cygnus), Hemisfério Norte, NGQ1</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Cygnus, o Cisnei
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"> Cygni</font></div><div><b>Abreviação:</b> Cyg</div><div><b>Área Total:</b> 804º quadrados </div><div><b>Quadrante:</b> NGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 9</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 84</div><div style="text-align: left;"><b>Meridiano:</b> Setembro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CygnusCC.jpg" type="image/jpeg" data-filename="CygnusCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Lacerta,_Cygnus,_Lyra,_Vulpecula_and_Anser.jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Lacerta,_Cygnus,_Lyra,_Vulpecula_and_Anser.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/488px-Cygnus_IAU.svg.png" type="image/png" data-filename="488px-Cygnus_IAU.svg.png"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1132"/></span>
<h1>Constelação da Serpente</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 12:00</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 14:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação da Serpente, Equatorial, NGQ1</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Serpens, a Serpente
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"> Serpentis</font></div><div><b>Abreviação:</b> Ser</div><div><b>Área Total:</b> 637º quadrados </div><div><b>Quadrante:</b> NGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 11</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 57</div><div style="text-align: left;"><b>Meridiano:</b> Julho</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Única constelação que é divididas em duas regiões separadas e o meio da serpente fica na constelação do serpentário</div><div><br/></div><div><br/></div><span src="assets/img/espaco/SerpensCaputCC.jpg" type="image/jpeg" data-filename="SerpensCaputCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><span src="assets/img/espaco/SerpensCaudaCC.jpg" type="image/jpeg" data-filename="SerpensCaudaCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><div><br/></div><div><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Taurus_Poniatowski,_Serpentarius,_Scutum_Sobiesky,_and_Serpens.jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Taurus_Poniatowski,_Serpentarius,_Scutum_Sobiesky,_and_Serpens.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/459px-Serpens_Caput_IAU.svg.png" type="image/png" data-filename="459px-Serpens_Caput_IAU.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/> <span src="assets/img/espaco/Serpens_Cauda_IAU.svg.png" type="image/png" data-filename="Serpens_Cauda_IAU.svg.png" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1138"/></span>
<h1>Constelação do Serpentário(Ophiuchus)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 22:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 14:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação do Serpentário(Ophiuchus), Equatorial, NGQ1</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Ophiuchus, o Serpentário
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Ophiuchi</font></div><div><b>Abreviação:</b> Oph</div><div><b>Área Total:</b> 948º quadrados</div><div><b>Quadrante:</b> NGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 10</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 65</div><div style="text-align: left;"><b>Meridiano:</b> Julho</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/OphiuchusCC.jpg" type="image/jpeg" data-filename="OphiuchusCC.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania12.jpg" type="image/jpeg" data-filename="urania12.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/488px-Ophiuchus_IAU.svg.png" type="image/png" data-filename="488px-Ophiuchus_IAU.svg.png" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1142"/></span>
<h1>ρ Ophiuchi E</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 11:04</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 11:04</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Rho Oph, Constelação do Serpentário(Ophiuchus), Estrela, Grupo Local, Laniakea, Sistema Rho Ophiuchi, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ρ</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> (B)</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Serpentário(Ophiuchus)</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 360 anos-luz</span></div><div><b>Distância do Centro Galático:  </b>23.796<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> Helio e hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [37].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESLTRELA</i></div><div><span src="assets/img/espaco/Ophiuchus_constellation_map.svg.png" type="image/png" data-filename="Ophiuchus_constellation_map.svg.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1145"/></span>
<h1>ρ Ophiuchi D</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 11:02</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 11:04</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Rho Oph, Constelação do Serpentário(Ophiuchus), Estrela, Grupo Local, Laniakea, Sistema Rho Ophiuchi, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ρ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=rho+Oph+d"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=rho+Oph+d</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> (B)</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Serpentário(Ophiuchus)</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 360 anos-luz</span></div><div><b>Distância do Centro Galático:  </b>23.796<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> Helio e hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [38].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESLTRELA</i></div><div><span src="assets/img/espaco/Ophiuchus_constellation_map.svg [1].png" type="image/png" data-filename="Ophiuchus_constellation_map.svg.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1148"/></span>
<h1>ρ Ophiuchi C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 10:58</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/02/2019 11:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Rho Oph, Constelação do Serpentário(Ophiuchus), Estrela, Grupo Local, Laniakea, Sistema Rho Ophiuchi, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ρ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=rho+Oph+C"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=rho+Oph+C</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> (B)</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Serpentário(Ophiuchus)</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 360 anos-luz</span></div><div><b>Distância do Centro Galático:  </b>23.796<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> Helio e hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [39].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESLTRELA</i></div><div><span src="assets/img/espaco/Ophiuchus_constellation_map.svg [2].png" type="image/png" data-filename="Ophiuchus_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1151"/></span>
<h1>ρ Ophiuchi Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>03/02/2019 23:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>03/02/2019 23:57</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Rho Oph, Constelação do Serpentário(Ophiuchus), Estrela, Grupo Local, Laniakea, Sistema Rho Ophiuchi, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ρ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/rhooph.html"><i>http://stars.astro.illinois.edu/sow/rhooph.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> B2</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Serpentário(Ophiuchus)</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> 22.400 K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 360 anos-luz</span></div><div><b>Distância do Centro Galático:  </b>23.796<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> Helio e hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [40].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESLTRELA</i></div><div><span src="assets/img/espaco/Ophiuchus_constellation_map.svg [3].png" type="image/png" data-filename="Ophiuchus_constellation_map.svg.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1154"/></span>
<h1>ρ Ophiuchi Aa</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>03/02/2019 13:17</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>03/02/2019 23:48</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Rho Oph, Constelação do Serpentário(Ophiuchus), Estrela, Grupo Local, Laniakea, Sistema Rho Ophiuchi, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ρ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=rho+Oph"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=rho+Oph</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> B2</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Serpentário(Ophiuchus)</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> 22.400 K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 360 anos-luz</span></div><div><b>Distância do Centro Galático:  </b>23.796<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> Helio e hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [41].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESLTRELA</i></div><div><span src="assets/img/espaco/Ophiuchus_constellation_map.svg [4].png" type="image/png" data-filename="Ophiuchus_constellation_map.svg.png"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1157"/></span>
<h1>Complexo de Nuvens Moleculares Rho Oph</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>02/02/2019 00:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>03/02/2019 12:48</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Nuvens Moleculares, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Rho Oph, Constelação do Serpentário(Ophiuchus), Grupo Local, Laniakea, Nebulosa, Nebulosa Escura, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://apod.nasa.gov/apod/ap100524.html"><i>https://apod.nasa.gov/apod/ap100524.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Tipo:</b> Escura</div><div><b>Constelação:</b> Serpentário(Ophiuchus)</div><div><b>Magnitude Aparente:</b> ?</div><div><b>Dimensão Aparente:</b> 4.5x6.5 arcmins</div><div><b>Raio:</b> ? anos-luz</div><div><b>Distância:</b> 460 anoz-luz</div><div><b>Idade:</b> ? milhões de anos</div><div><b>Densidade:</b> 10[elev(17)] cm-²</div><div><b>Massa:</b> 3.000 <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Temperatura Média</b><b>:</b> 13-22 K</div><div><b>Composição:</b> Gás e poeira estelar</div><div><br/></div><div><br/></div><span src="assets/img/espaco/RhoOph-02w.jpg" type="image/jpeg" data-filename="RhoOph-02w.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1159"/></span>
<h1>Capella H</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 21:46</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 22:06</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação do Colcheiro(Auriga), Estrela, Grupo Local, Laniakea, Sistema Capella, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.constellation-guide.com/capella/"><i>https://www.constellation-guide.com/capella/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> M2.5V</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Colcheiro(Auriga)</span></div><b>Raio equatorial:</b> 0,54 R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 0,57<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> 4,7 cgs</div><div><b>Temperatura:</b> 3.700 K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 43 anos luz</span></div><div><b>Distância do Centro Galático:  </b>24.269<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  10,16</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> 9,53</span></div><div><b>Composição:</b> Metais neutros e dióxidos de Titânio</div><div><br/></div><div><i>//REPRESENTAÇÃO GRÁFICA</i></div><div><span src="assets/img/espaco/Capella-Sun_comparison-Wiki-public-domain_ST.jpg" type="image/jpeg" data-filename="Capella-Sun_comparison-Wiki-public-domain_ST.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/capella [2].jpg" type="image/jpeg" data-filename="capella.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/300px-Auriga_constellation_map.svg [2].png" type="image/png" data-filename="300px-Auriga_constellation_map.svg.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1163"/></span>
<h1>Capella L</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 21:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 22:06</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação do Colcheiro(Auriga), Estrela, Grupo Local, Laniakea, Sistema Capella, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://adsabs.harvard.edu/abs/1985ApJS...59..197B"><i>http://adsabs.harvard.edu/abs/1985ApJS...59..197B</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> M4</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Colcheiro(Auriga)</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 0,53<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 43 anos luz</span></div><div><b>Distância do Centro Galático:  </b>24.269<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  13,7</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> 13,1</span></div><div><b>Composição:</b> Metais neutros e dióxidos de Titânio</div><div><br/></div><div><i>//REPRESENTAÇÃO GRÁFICA</i></div><div><span src="assets/img/espaco/Capella-Sun_comparison-Wiki-public-domain_ST [1].jpg" type="image/jpeg" data-filename="Capella-Sun_comparison-Wiki-public-domain_ST.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/capella [3].jpg" type="image/jpeg" data-filename="capella.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/300px-Auriga_constellation_map.svg [3].png" type="image/png" data-filename="300px-Auriga_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1167"/></span>
<h1>Constelação do Cocheiro(Auriga)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 18:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 18:45</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação do Colcheiro(Auriga), Equatorial, NGQ2</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Auriga, o Cocheiro
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Aurigae</font></div><div><b>Abreviação:</b> Aur</div><div><b>Área Total:</b> 465º quadrados</div><div><b>Quadrante:</b> NGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 5(7)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 65</div><div style="text-align: left;"><b>Meridiano:</b> Fevereiro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/AurigaCC.jpg" type="image/jpeg" data-filename="AurigaCC.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania07.jpg" type="image/jpeg" data-filename="urania07.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/606px-Auriga_IAU.svg.png" type="image/png" data-filename="606px-Auriga_IAU.svg.png"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1171"/></span>
<h1>Complexo de Nuvens Moleculares Tau-Aur</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 17:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 18:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Nuvens Moleculares, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Tau-Aur, Constelação de Touro, Constelação do Colcheiro(Auriga), Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://iopscience.iop.org/article/10.1086/508937/pdf"><i>https://iopscience.iop.org/article/10.1086/508937/pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Distancia:</b> 470 anoz-luz</div><div><b>Idade:</b> 100 milhões de anos</div><div><b>Massa Total:</b> ? <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Diâmetro:</b> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/fig1-3.jpg" type="image/jpeg" data-filename="fig1-3.jpg" height="1449" style="cursor: default;cursor: default;cursor: default;" width="1280"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1173"/></span>
<h1>Grupo de Associações Estelares Sco-Cen OB 2</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 14:43</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 14:58</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Ass Sco-Cen OB 2, Grupo de Associações, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.pas.rochester.edu/~emamajek/scocen.pdf"><i>http://www.pas.rochester.edu/~emamajek/scocen.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> 380-470 anos-luz</div><div><b>Idade:</b> 17 milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/720px-Sco-CenMap.png" type="image/png" data-filename="720px-Sco-CenMap.png" style="cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [42].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1176"/></span>
<h1>Associação Upper Cen-Lup OB 2(UCL)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 17:28</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 14:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass tipo OB, Ass UCL, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Lupus, Constelação do Centauro, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.pas.rochester.edu/~emamajek/scocen.pdf"><i>http://www.pas.rochester.edu/~emamajek/scocen.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Centauro e Lupus</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> 456 anos-luz</div><div><b>Idade:</b> 17 milhões de anos</div><div><b>Estrelas:</b> pelo menos 221(massivas)</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [43].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Image [44].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1179"/></span>
<h1>Nuvem de Lupus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 12:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 12:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Lupus, Grupo Local, Laniakea, Nuvem de Lupus, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.eso.org/~fcomeron/lupus_rev.pdf"><i>https://www.eso.org/~fcomeron/lupus_rev.pdf</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Distancia:</b> 450-650 anoz-luz</div><div><b>Massa Total:</b> 36.500 <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Massa da Nuvem:</b> 18.200 <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Diâmetro:</b> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [45].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><span src="assets/img/espaco/Image [46].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1182"/></span>
<h1>Constelação de Lupus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 11:47</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/02/2019 12:07</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Lupus, Hemisfério Sul, NGQ4</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Lupus, o Lobo
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Lupi</font></div><div><b>Abreviação:</b> Lup</div><div><b>Área Total:</b> 334º quadrados</div><div><b>Quadrante:</b> NGQ4 </div><div style="text-align: left;"><b>Estrelas Principais:</b> 9</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 41</div><div style="text-align: left;"><b>Meridiano:</b> Junho</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Constellation_Lupus.jpg" type="image/jpeg" data-filename="Constellation_Lupus.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Lupuspa.jpg" type="image/jpeg" data-filename="Lupuspa.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/605px-Lupus_IAU.svg.png" type="image/png" data-filename="605px-Lupus_IAU.svg.png"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1186"/></span>
<h1>γ Crucis C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 16:33</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>31/01/2019 17:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Estrela de Bário, Grupo Local, Laniakea, Sistema Gamma Crucis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/gacrux.html"><i>http://stars.astro.illinois.edu/sow/gacrux.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <font face="sans-serif" size="2">?</font></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Crux</span></div><b>Raio equatorial:</b> ~ 0,01 R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ~ 1 M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b>Distância do Centro Galático:  </b>24.090<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 86 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> <font color="#222222" face="sans-serif"><span style="font-size: 14px;">Bário</span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><span src="assets/img/espaco/R6200191-Star_Gamma_Crucis_in_Crux.jpg" type="image/jpeg" data-filename="R6200191-Star_Gamma_Crucis_in_Crux.jpg" style="cursor: default;cursor: default;"/><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><i>// LOCALIZAÇÃO DA ESTRELA</i></span></font></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg.png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1189"/></span>
<h1>Sistema Gamma Crucis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 16:42</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>31/01/2019 16:44</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Grupo Local, Laniakea, Sistema Binário de Estrelas, Sistema Estelar, Sistema Gamma Crucis, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/gacrux.html"><i>http://stars.astro.illinois.edu/sow/gacrux.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Estrelas:</b> Gacrux e <i style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">γ </i>Crucis C
<div><b>Estrela Principal:</b> Gacrux</div><div><b>Distância do disco:</b> ?</div><div><b>Período Orbital Galático:</b> ?</div><div><b>Velocidade Orbital Galática:</b> ?</div><div><font color="#222222"><b>Distância do Centro Galático:</b></font><b> </b>24.090<font color="#222222"> anos-luz</font></div><div><font color="#222222"><br/></font></div><div><font color="#222222"><br/></font></div><div><span src="assets/img/espaco/R6200191-Star_Gamma_Crucis_in_Crux [1].jpg" type="image/jpeg" data-filename="R6200191-Star_Gamma_Crucis_in_Crux.jpg" style="cursor: default;"/><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;">// LOCALIZAÇÃO DO SISTEMA</span></font></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [1].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1192"/></span>
<h1>Gacrux</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 14:15</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>31/01/2019 16:39</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Classificação Estelar M, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Gigante, Grupo Local, Laniakea, Sistema Gamma Crucis, Subgrupo Via Láctea, Superaglomerado de Virgem, Vermelha, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/gacrux.html"><i>http://stars.astro.illinois.edu/sow/gacrux.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <font face="sans-serif" size="2">M3.5</font></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Crux</span></div><b>Raio equatorial:</b> 84 R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 3 M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> 3,6 cgs</div><div><b>Temperatura:</b> 3.400 K</div><div><b>Distância do Centro Galático:  </b>24.090<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 86 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  1,64     </font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> -0,52</span></div><div><b>Composição:</b> <font color="#222222" face="sans-serif"><span style="font-size: 14px;">Dióxido de titânio, metais neutros e hidrogênio</span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><font face="sans-serif"><span style="font-size: 14px;"><i>//REPRESENTAÇÃO GRÁFICA</i><br/></span></font><div><span src="assets/img/espaco/K0053396-Gacrux_red_giant_star,_animation.jpg" type="image/jpeg" data-filename="K0053396-Gacrux_red_giant_star,_animation.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><span src="assets/img/espaco/R6200191-Star_Gamma_Crucis_in_Crux [2].jpg" type="image/jpeg" data-filename="R6200191-Star_Gamma_Crucis_in_Crux.jpg"/><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;">// LOCALIZAÇÃO DA ESTRELA</span></font></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [2].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1197"/></span>
<h1>Zubeneschamali</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 17:05</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:19</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Libra, Estrela, Estrela Única, Grupo Local, Laniakea, Milhões de Anos, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/zubenes.html"><i>http://stars.astro.illinois.edu/sow/zubenes.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B8V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Libra</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 4,9 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 3,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 12.300 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 185 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,61</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -1,16</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 80 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 250 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/aa94b74ed2a5239b842a0b7e04a82e45.jpg" type="image/jpeg" data-filename="aa94b74ed2a5239b842a0b7e04a82e45.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/Libra-constellation-mapb.gif" type="image/gif" data-filename="Libra-constellation-mapb.gif"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1200"/></span>
<h1>α Lacertae</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 21:19</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:16</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Centena de Milhões de Anos, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação de Lacerta, Estrela, Estrela Única, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alphalac.html"><i>http://stars.astro.illinois.edu/sow/alphalac.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">A1 V</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Lacerta</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,14 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 2,19<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 9.200 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 102 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,77</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 1,27   </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 400 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 128 km/s</span><span style="color: rgb(34, 34, 34);">  </span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/gallery_261696_7517_10923.jpg" type="image/jpeg" data-filename="gallery_261696_7517_10923.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1280"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Lacerta_constellation_map.png" type="image/png" data-filename="250px-Lacerta_constellation_map.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1203"/></span>
<h1>Póllux</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 16:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:16</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Centena de Milhões de Anos, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Gêmeos, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Sistema Póllux, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+62509"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+62509</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><br/></div><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K0lll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Gêmeos</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 8,8 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,91<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,68 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.666 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 33 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,14</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 1,8</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 724 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 2,8 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Metais neutros e hidrogênio </div><div><br/></div><div><span src="assets/img/espaco/21-150H0105316.jpg" type="image/jpeg" data-filename="21-150H0105316.jpg" style="cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/Pollux.jpg" type="image/jpeg" data-filename="Pollux.jpg" style="cursor: default;"/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/605px-Gemini_IAU.svg.png" type="image/png" data-filename="605px-Gemini_IAU.svg.png" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1207"/></span>
<h1>Imai</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 16:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:14</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass LCC, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Estrela Única, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/deltacru.html"><i>http://stars.astro.illinois.edu/sow/deltacru.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="font-family: sans-serif;">B2IV</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Crux</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 8 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 9 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,88 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.570 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.976<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 345 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,79</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,2</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 18 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 210 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> H<span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">idrogênio e Hélio</span></div><div><span style="font-size: 14px;"><br/></span></div><div><span style="font-size: 14px;"><br/></span></div><div><span src="assets/img/espaco/Deep_Crux_wide_field_with_fog.jpg" type="image/jpeg" data-filename="Deep_Crux_wide_field_with_fog.jpg"/></div><div><br/></div><div><span style="font-style: italic; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [3].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1210"/></span>
<h1>β Crucis B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 13:06</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass LCC, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Beta Crucis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/mimosa"><i>https://www.universeguide.com/star/mimosa</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B2V</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Crux</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 10 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.950<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 280 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,31</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> ? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> ? km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio, <span style="font-size: 14px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif;">hélio</span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/optical-image-of-the-star-mimosa-or-beta-crucis-celestial-image-co.jpg" type="image/jpeg" data-filename="optical-image-of-the-star-mimosa-or-beta-crucis-celestial-image-co.jpg" style="cursor: default;cursor: default;" width="897"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [4].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1213"/></span>
<h1>Mimosa</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 12:20</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass LCC, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Gigante, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Beta Crucis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/mimosa"><i>https://www.universeguide.com/star/mimosa</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B0.5III</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Crux</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 8,4 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 16 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,6 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 27.000 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.950<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 280 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,23</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,42</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 8-11 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 35 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio, <span style="font-size: 14px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif;">hélio</span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/optical-image-of-the-star-mimosa-or-beta-crucis-celestial-image-co [1].jpg" type="image/jpeg" data-filename="optical-image-of-the-star-mimosa-or-beta-crucis-celestial-image-co.jpg" style="cursor: default;cursor: default;cursor: default;" width="897"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [5].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1216"/></span>
<h1>α Crucis Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 10:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Grupo Local, Laniakea, Milhões de Anos, Sistema Alpha Crucis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://wiki.travellerrpg.com/Alpha_Crucis_(star)"><i>http://wiki.travellerrpg.com/Alpha_Crucis_(star)</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="font-family: sans-serif;">?</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Crux</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 6 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.976<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 320 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">10,8 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade da Rotação:</span><span style="color: rgb(34, 34, 34);"> 200 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> <span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">?</span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/2016-04-27-acrux-1-and-acrux-21.jpg" type="image/jpeg" data-filename="2016-04-27-acrux-1-and-acrux-21.jpg" style="cursor: default;" width="2010"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [6].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1220"/></span>
<h1>Acrux</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 13:15</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 16:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass LCC, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Alpha Crucis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/acrux"><i>https://www.universeguide.com/star/acrux</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B0.5IV</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Crux</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 17 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 30.000 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.976<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 320 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,33</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> - 2,2</span></div><div><span style="color: rgb(34, 34, 34);"><b>Idade:</b> 10,8 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34);"><b>Velocidade da Rotação:</b> 120 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio, <span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">hélio</span></div><div><span style="font-size: 14px;"><br/></span></div><div><span style="font-size: 14px;"><br/></span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif;">// REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/alpha-crucis-star.jpg" type="image/jpeg" data-filename="alpha-crucis-star.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/128357158-352-k387269.jpg" type="image/jpeg" data-filename="128357158-352-k387269.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [7].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1224"/></span>
<h1>Alderamin</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 16:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação de Cepheus, Estrela, Estrela Única, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+203280"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+203280</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">A8Vn</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Cepheus</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,3 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 1,74<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,99 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 7.740 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 49 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,51</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 1,57</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 246 km/s</span></div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e metais ionizados</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/alpha_cep.jpg" type="image/jpeg" data-filename="alpha_cep.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/240px-Cepheus_constellation_map.svg.png" type="image/png" data-filename="240px-Cepheus_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA DE SUA FORMA</span></div><div><span src="assets/img/espaco/1.jpg" type="image/jpeg" data-filename="1.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1228"/></span>
<h1>Schedar</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 22:06</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:46</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bilhões de Anos, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Cassiopeia, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/shedar.html"><i>http://stars.astro.illinois.edu/sow/shedar.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K0llla</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Cassiopeia</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 42,1 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 4,5 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.530 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 228 anos-luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:</span> 24.256<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,24</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span> <span style="color: rgb(34, 34, 34);">-1,98</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 10 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 21 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Metais neutros, hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Schedar.jpg" type="image/jpeg" data-filename="Schedar.jpg"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Cassiopeia_constellation_map.png" type="image/png" data-filename="250px-Cassiopeia_constellation_map.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1231"/></span>
<h1>Deneb Algedi</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 15:11</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:44</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação de Capricórnio, Estrela, Gigante, Grupo Local, Laniakea, Sistema Delta Capricorni, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+207098"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+207098</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> A5mF2</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Capricórnio</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 1.9 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 2<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,66 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 7.301 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 38 anos luz</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,81</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 2,48</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 105 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><span src="assets/img/espaco/Deneb_Algedi.jpg" type="image/jpeg" data-filename="Deneb_Algedi.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1233"/></span>
<h1>Tarf</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 16:24</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:43</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Câncer, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Sistema Tarf, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/altarf.html"><i>http://stars.astro.illinois.edu/sow/altarf.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K4lll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Câncer</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 61 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 3<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,5 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 3.990 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 290 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,53</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -1,2</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 6,9 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Metais neutros e hidrogênio </div><div><br/></div><div><span src="assets/img/espaco/Image [47].png" type="image/png" data-filename="Image.png" style="cursor: default;" width="995"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/constellation_Cancer_large.png" type="image/png" data-filename="constellation_Cancer_large.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1236"/></span>
<h1>Hamal</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 14:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:42</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bilhões de Anos, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Áries, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Sistema Hamal, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=alf+ari"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=alf+ari</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K2 lllCa</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Áries</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 15 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 1,5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,57 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.480 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 66 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,0</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 0,4</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 3,4 Bilhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 3,4 km/s</span></div><div><span style="font-weight: bold;">Composição:</span>Metais neutros, hidrogênio e cálcio</div><div><br/></div><div><span src="assets/img/espaco/Image [48].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/Aries-constellation-mapb.gif" type="image/gif" data-filename="Aries-constellation-mapb.gif"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1239"/></span>
<h1>Sadalsuud</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 15:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:41</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Amarela, Braço de Órion, Classificação Estelar G, Complexo de Superaglomerados Pisces-Cetus, Constelação de Aquário, Estrela, Grupo Local, Laniakea, Sistema Beta Aquarii, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=ADS+15050"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=ADS+15050</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> G0 lb</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Aquário</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 50 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 6<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,05 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 5.700 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 540 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,87</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,04</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 60 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 6,3 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados e metais neutros</div><div><br/></div><div><span src="assets/img/espaco/Estrella Acuario - Beta Aquarii.jpg" type="image/jpeg" data-filename="Estrella Acuario - Beta Aquarii.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1241"/></span>
<h1>δ Velorum Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 17:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:40</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Centena de Milhões de Anos, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Sistema Delta Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/deltavel.html"><i>http://stars.astro.illinois.edu/sow/deltavel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> A2</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Vela</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,3 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 2 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,99 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 9.830 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 80 anos-luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:</span><span style="font-weight: bold;">  </span>24.132<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span> <span style="color: rgb(34, 34, 34);">0,02</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 400 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 149 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/artworks-000302059005-kzwe3x-t500x500 [2].jpg" type="image/jpeg" data-filename="artworks-000302059005-kzwe3x-t500x500.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [3].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1244"/></span>
<h1>Alsephina</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 17:28</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:39</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Sistema Delta Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/deltavel.html"><i>http://stars.astro.illinois.edu/sow/deltavel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> A1</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Vela</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 2,79 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 2,43 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,78 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 9.820 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 80 anos-luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.132<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span> <span style="color: rgb(34, 34, 34);">0,02</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span> <span style="color: rgb(34, 34, 34);">? de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span><span style="color: rgb(34, 34, 34);"> 143 km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/artworks-000302059005-kzwe3x-t500x500 [3].jpg" type="image/jpeg" data-filename="artworks-000302059005-kzwe3x-t500x500.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [4].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1247"/></span>
<h1>γ¹ Velorum Ba</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 09:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:39</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Vel OB 2, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Gigante, Grupo Local, Laniakea, Milhões de Anos, Sistema Gamma Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astronomytrek.com/star-constellation-facts-vela/"><i>http://www.astronomytrek.com/star-constellation-facts-vela/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">B2III</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Vela</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 14 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 20.000 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.322<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 840 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  4,3</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> - 3,6</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 8 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span></div><div><span style="font-weight: bold;">Composição: </span><span style="font-size: 14px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif;">hélio e hidrogênio</span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/gamma-velorum.jpg" type="image/jpeg" data-filename="gamma-velorum.jpg" style="cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/544c7da1e8c724205f95b9a1547ddc2e [1].jpg" type="image/jpeg" data-filename="544c7da1e8c724205f95b9a1547ddc2e.jpg" style="cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [5].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1251"/></span>
<h1>γ¹ Velorum Bb</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 10:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:38</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass Vel OB 2, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Milhões de Anos, Sistema Gamma Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://vizier.u-strasbg.fr/viz-bin/VizieR-5?-ref=VIZ501f06662af3&-out.add=.&-source=V/137C/XHIP&recno=39829"><i>http://vizier.u-strasbg.fr/viz-bin/VizieR-5?-ref=VIZ501f06662af3&amp;-out.add=.&amp;-source=V/137C/XHIP&amp;recno=39829</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="font-family: sans-serif;">?</span></span></div><div style="text-align: -webkit-auto;"><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Vela</span></div><div><span style="font-weight: bold; text-align: -webkit-auto;">Raio equatorial:</span><span style="text-align: -webkit-auto;"> ? R</span><sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 1,1 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: -webkit-auto;"><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div style="text-align: -webkit-auto;"><span style="font-weight: bold;">Temperatura:</span> ? K</div><div style="text-align: -webkit-auto;"><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.322<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div style="text-align: -webkit-auto;"><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 840 anos luz</span></div><div style="text-align: -webkit-auto;"><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div style="text-align: -webkit-auto;"><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> ?</span></div><div style="text-align: -webkit-auto;"><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 8 Milhões de anos</span></div><div style="text-align: -webkit-auto;"><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span><span style="color: rgb(34, 34, 34);"><br/></span></div><div style="text-align: -webkit-auto;"><span style="font-weight: bold;">Composição: </span><span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">?</span></div><div style="text-align: -webkit-auto;"><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div style="text-align: -webkit-auto;"><span style="font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div style="text-align: -webkit-auto;"><span src="assets/img/espaco/gamma-velorum [1].jpg" type="image/jpeg" data-filename="gamma-velorum.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: -webkit-auto;"><br/></div><div style="text-align: -webkit-auto;"><span src="assets/img/espaco/544c7da1e8c724205f95b9a1547ddc2e [2].jpg" type="image/jpeg" data-filename="544c7da1e8c724205f95b9a1547ddc2e.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: -webkit-auto;"><br/></div><div style="text-align: -webkit-auto;"><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div style="text-align: -webkit-auto;"><span src="assets/img/espaco/250px-Vela_constellation_map [6].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1255"/></span>
<h1>γ² Velorum WR</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 09:13</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:38</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Vel OB 2, Azul, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Milhões de Anos, Sistema Gamma Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, Wolf-Rayet(Classificação Estelar W), γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/regor.html"><i>http://stars.astro.illinois.edu/sow/regor.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">WC8</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Vela</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 9 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 57.000 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.322<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 840 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  -1,81</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> - 4,23</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Idade:</span><span style="color: rgb(34, 34, 34);"> 4,5 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Velocidade de Rotação:</span> <span style="color: rgb(34, 34, 34);">? km/s</span></div><div><span style="font-weight: bold;">Composição: </span><span style="font-size: 14px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif;">hélio, carbono e oxigênio</span></div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA<span src="assets/img/espaco/EoAdaey.jpg" type="image/jpeg" data-filename="EoAdaey.jpg" style="cursor: default;cursor: default;cursor: default;" width="1920"/></span></div><div><br/></div><div><span src="assets/img/espaco/gamma-velorum [2].jpg" type="image/jpeg" data-filename="gamma-velorum.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/544c7da1e8c724205f95b9a1547ddc2e [3].jpg" type="image/jpeg" data-filename="544c7da1e8c724205f95b9a1547ddc2e.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [7].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1261"/></span>
<h1>α Crucis Ca</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 11:13</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>31/01/2019 11:51</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Grupo Local, Laniakea, Sistema Alpha Crucis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://wiki.travellerrpg.com/Alpha_Crucis_(star)"><i>http://wiki.travellerrpg.com/Alpha_Crucis_(star)</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <font face="sans-serif" size="2">B4 IV</font></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Crux</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b>Distância do Centro Galático:  </b>23.976<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 320 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  2,09</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> 5</span></div><div><b>Composição:</b> <font color="#222222" face="sans-serif"><span style="font-size: 14px;">Hidrogênio e Hélio     </span></font></div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/2016-04-27-acrux-1-and-acrux-21 [1].jpg" type="image/jpeg" data-filename="2016-04-27-acrux-1-and-acrux-21.jpg" height="2010" style="cursor: default;" width="2010"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [8].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1264"/></span>
<h1>α Crucis Cb</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 11:24</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>31/01/2019 11:50</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Grupo Local, Laniakea, Sistema Alpha Crucis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <font face="sans-serif" size="2">?</font></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Crux</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b>Distância do Centro Galático:  </b>23.976<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 320 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> ?<font color="#222222" face="sans-serif"><span style="font-size: 14px;">   </span></font></div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/2016-04-27-acrux-1-and-acrux-21 [2].jpg" type="image/jpeg" data-filename="2016-04-27-acrux-1-and-acrux-21.jpg"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [9].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1267"/></span>
<h1>α Crucis B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 11:02</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>31/01/2019 11:49</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Azul-Branca, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Estrela, Grupo Local, Laniakea, Sistema Alpha Crucis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://earthsky.org/brightest-stars/acrux-shines-in-the-southern-cross"><i>https://earthsky.org/brightest-stars/acrux-shines-in-the-southern-cross</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <font face="sans-serif" size="2">B1V</font></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Crux</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 15 M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> 26.000 K</div><div><b>Distância do Centro Galático:  </b>23.976<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 320 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  1,75</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> 2,7</span></div><div><b>Composição:</b> <font color="#222222" face="sans-serif"><span style="font-size: 14px;">Hidrogênio e Hélio     </span></font></div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/Alpha_Crucis.png" type="image/png" data-filename="Alpha_Crucis.png"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [10].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1272"/></span>
<h1>Constelação de Crux(Cruzeiro do Sul)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 12:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>30/01/2019 13:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Crux(Cruzeiro do Sul), Hemisfério Sul, NGQ4</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Crux, a Cruz | o Cruzeiro do Sul
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Crucis</font></div><div><b>Abreviação:</b> Cru</div><div><b>Área Total:</b> 68º quadrados</div><div><b>Quadrante:</b> NGQ4 </div><div style="text-align: left;"><b>Estrelas Principais:</b> 4</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 19</div><div style="text-align: left;"><b>Meridiano:</b> Maio</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Constellation_Crux.jpg" type="image/jpeg" data-filename="Constellation_Crux.jpg" style="cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/800px-Deep_Crux_wide_field_with_fog.jpg" type="image/jpeg" data-filename="800px-Deep_Crux_wide_field_with_fog.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CruxHondius1613.jpg" type="image/jpeg" data-filename="CruxHondius1613.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CRU.gif" type="image/gif" data-filename="CRU.gif" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1277"/></span>
<h1>Constelação da Carina</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 11:26</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>30/01/2019 11:33</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação da Carina, Hemisfério Sul, SGQ4</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Carina, a Quilha
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Carinae</font></div><div><b>Abreviação:</b> Car</div><div><b>Área Total:</b> 494º quadrados</div><div><b>Quadrante:</b> SGQ4 </div><div style="text-align: left;"><b>Estrelas Principais:</b> 9(10)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 52</div><div style="text-align: left;"><b>Meridiano:</b> Março</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Constellation_Carina.jpg" type="image/jpeg" data-filename="Constellation_Carina.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania32.jpg" type="image/jpeg" data-filename="urania32.jpg" style="cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/800px-Carina_IAU.svg.png" type="image/png" data-filename="800px-Carina_IAU.svg.png"/><br/></div><div style="text-align: left;"><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1281"/></span>
<h1>γ Velorum C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 10:33</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>30/01/2019 11:17</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Vel OB 2, Braço de Órion, Branca, Cinturão de Gould, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Sistema Gamma Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/regor"><i>https://www.universeguide.com/star/regor</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <span style="font-family: 'Times New Roman'; text-align: center;">A0</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Vela</span></div><b>Raio equatorial:</b> ? R<sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b>Distância do Centro Galático:  </b>24.322<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 840 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  8,5</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição: </b>Hidrogênio e metais ionizados</div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><span src="assets/img/espaco/gammavel_airy [1].png" type="image/png" data-filename="gammavel_airy.png"/></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><span src="assets/img/espaco/544c7da1e8c724205f95b9a1547ddc2e [4].jpg" type="image/jpeg" data-filename="544c7da1e8c724205f95b9a1547ddc2e.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [8].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1286"/></span>
<h1>Io</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>07/05/2018 14:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 18:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Regular, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/jupiter-moons/io/in-depth/"><i>https://solarsystem.nasa.gov/moons/jupiter-moons/io/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 42 horas</div><div><b>Período de órbita do planeta:</b> 42 horas</div><div><b>Temperatura média:</b> -152 ºC</div><div><b>Planeta:</b> Júpiter</div><div><b>Distância do Planeta: </b>421.800 km</div><div><b>Peso:</b> 0.18 da terra</div><div><b>Gravidade:</b> 1.8 m/s²</div><div><b>Diâmetro equatorial:</b> 3.643 km</div><div><b>Atmosfera: </b>principalmente dióxido de enxofre e traços de oxigênio,sódio e outros gases</div><div><b>Vel. p/ orb. o planeta:</b> 62.423 km/h</div><div><b>Área de superfície:</b> 41.698.064 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Quase nada de oxigênio</div><div>&gt; Solo infértil</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muita lava</div><div>&gt; Ambientes muito frios e quentes</div><div>&gt; Muita radiação que emana de Júpiter</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Atividade vulcânica intensa( alguns vulcões incandesentes)</div><div>&gt; A ionosfera puxada por Júpiter causa um anel de íons iluminados perto da lua</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Júpiter sempre está a mesma face</div><div>&gt; Lugar com mais atividade vulcânica do sistema solar</div>
&gt; Forte atração gravitacional faz com que seu núcleo oscile e aqueça
</div>
<div>&gt; O Vulcão Loki libera mais calor que todos os vulcões terrestres juntos</div><div><br/></div><div><span src="assets/img/espaco/808_PIA02308.jpg" type="image/jpeg" data-filename="808_PIA02308.jpg" height="2796" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="2796"/><br/></div><div><br/></div><div><span src="assets/img/espaco/502_b_IOErupts.jpg" type="image/jpeg" data-filename="502_b_IOErupts.jpg" height="1080" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1920"/><br/></div><div><br/></div><div><br/></div></span>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1289"/></span>
<h1>Anel μ</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:59</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 86.000 km</div><div><b>Fronteira externa:</b> 103.000 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 17.000 km</div><div><b>Percentagem de poeira:</b> ? % (significativa)</div><div><b>Nível:</b> 12</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Último anel de Urano</div><div><br/></div><div><span src="assets/img/espaco/489px-Outer_Uranian_rings.jpg" type="image/jpeg" data-filename="489px-Outer_Uranian_rings.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1291"/></span>
<h1>Anel ν</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:55</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 66.100 km</div><div><b>Fronteira externa:</b> 69.900 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 3.800 km</div><div><b>Percentagem de poeira:</b> ? % (muito escassa)</div><div><b>Nível:</b> 11</div><div><br/></div><div><span src="assets/img/espaco/489px-Outer_Uranian_rings [1].jpg" type="image/jpeg" data-filename="489px-Outer_Uranian_rings.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1293"/></span>
<h1>Anel ε</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:46</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:50</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 51.059 km</div><div><b>Fronteira externa:</b> 51.149 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> 0.15 km</div><div><b>Largura:</b> 20~96 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 10</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Anel mais brilhante de Urano</div><div><br/></div><div><span src="assets/img/espaco/800px-Epsilon_ring_of_Uranus.jpg" type="image/jpeg" data-filename="800px-Epsilon_ring_of_Uranus.jpg" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1295"/></span>
<h1>Anel λ</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:45</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 50.022 km</div><div><b>Fronteira externa:</b> 50.024 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 2 km</div><div><b>Percentagem de poeira:</b> ? % (significativa)</div><div><b>Nível:</b> 9</div><div><br/></div><div><span src="assets/img/espaco/Image [49].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1297"/></span>
<h1>Anel γ</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:28</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:34</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 47.623 km</div><div><b>Fronteira externa:</b> 47.627 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> 0.15 km</div><div><b>Largura:</b> 1~4 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 7</div><div><br/></div><div><span src="assets/img/espaco/Image [50].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1299"/></span>
<h1>Anel η</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 47.136 km</div><div><b>Fronteira externa:</b> 47.176 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 40 km</div><div><b>Percentagem de poeira:</b> ? % (bastante)</div><div><b>Nível:</b> 6</div><div><br/></div><div><span src="assets/img/espaco/Image [51].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1301"/></span>
<h1>Anel β</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:18</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 45.650 km</div><div><b>Fronteira externa:</b> 45.661 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 4~11 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 5</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Dentre os anéis mais brilhantes de Urano</div><div><br/></div><div><span src="assets/img/espaco/Image [52].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1303"/></span>
<h1>Anel α</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:09</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 44.176 km</div><div><b>Fronteira externa:</b> 44.178 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 4~10 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 4</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Dentre os anéis mais brilhantes de Urano</div><div><br/></div><div><span src="assets/img/espaco/Image [53].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1305"/></span>
<h1>Anel 4</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:03</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 42.568 km</div><div><b>Fronteira externa:</b> 42.570 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 2 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 3</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Dentre os anéis mais tênues e estreitos de Urano</div><div><br/></div><div><span src="assets/img/espaco/Image [54].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1307"/></span>
<h1>Anel 6</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 16:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 41.837 km</div><div><b>Fronteira externa:</b> 41.838 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 1.5 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 1</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Dentre os anéis mais tênues e estreitos de Urano</div><div><br/></div><div><span src="assets/img/espaco/Image [55].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1309"/></span>
<h1>Anel 5</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 16:52</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 17:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 42.232 km</div><div><b>Fronteira externa:</b> 42.234 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 2 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 2</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Dentre os anéis mais tênues e estreitos de Urano</div><div><br/></div><div><span src="assets/img/espaco/Image [56].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1311"/></span>
<h1>Anel Arago</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 13:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 15:51</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Netuno, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/neptune/neptune_tables.html"><i>https://pds-rings.seti.org/neptune/neptune_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno: </b>57.100 km</div><div><b>Fronteira externa:</b> 57.200 km</div><div><b>Planeta:</b> Netuno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 100 km</div><div><b>Percentagem de poeira:</b> ? %</div><div><b>Nível:</b> 4</div><div><br/></div><div><span src="assets/img/espaco/Image [57].png" type="image/png" data-filename="Image.png" height="326" style="cursor: default;cursor: default;cursor: default;" width="928"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1313"/></span>
<h1>Anel Gossamer Amalteia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>15/05/2018 15:19</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 15:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/jupiter/jupiter_tables.html"><i>https://pds-rings.seti.org/jupiter/jupiter_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno: </b>128.940 km</div><div><b>Fronteira externa:</b> 182.000 km</div><div><b>Planeta:</b> Júpiter</div><div><b>Espessura:</b> 2.000 km</div><div><b>Largura:</b> 53.000 km</div><div><b>Percentagem de poeira:</b> 100%</div><div><b>Nível: </b>3</div><div><br/></div><div><span src="assets/img/espaco/PIA00659.jpg" type="image/jpeg" data-filename="PIA00659.jpg" style="cursor: default;"/><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1315"/></span>
<h1>Anel Adams</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 13:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 13:18</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Netuno, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/neptune/neptune_tables.html"><i>https://pds-rings.seti.org/neptune/neptune_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 62.900 km</div><div><b>Fronteira externa:</b> 62.915 km</div><div><b>Planeta:</b> Netuno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 15 km</div><div><b>Percentagem de poeira:</b> 20 ~ 40 %</div><div><b>Nível:</b> 5</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Último anel de Netuno</div><div>&gt; Possui 6 arcos(Fraternité, Egalité 1, Egalité 2, Liberté, Courage)</div><div><br/></div><div>//<i> ANEIS LE VERRIER E ADAMS</i></div><div><span src="assets/img/espaco/c1141251.gif" type="image/gif" data-filename="c1141251.gif" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1317"/></span>
<h1>Anel Le Verrier</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 12:42</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 13:11</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Netuno, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/neptune/neptune_tables.html"><i>https://pds-rings.seti.org/neptune/neptune_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno: </b>53.000 km</div><div><b>Fronteira externa:</b> 53.200 km</div><div><b>Planeta:</b> Netuno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 110 km</div><div><b>Percentagem de poeira:</b> 40 ~ 70%</div><div><b>Nível:</b> 2</div><div><br/></div><div>//<i> ANEIS LE VERRIER E ADAMS</i></div><div><span src="assets/img/espaco/c1141251 [1].gif" type="image/gif" data-filename="c1141251.gif" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1319"/></span>
<h1>Anel Lassell</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 12:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 13:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Netuno, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/neptune/neptune_tables.html"><i>https://pds-rings.seti.org/neptune/neptune_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno: </b>53.200 km</div><div><b>Fronteira externa:</b> 57.200 km</div><div><b>Planeta:</b> Netuno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 4.000 km</div><div><b>Percentagem de poeira:</b> 20 ~ 40%</div><div><b>Nível:</b> 3</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Anel mais largo de Netuno</div><div><br/></div><div><span src="assets/img/espaco/Image [58].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1321"/></span>
<h1>Anel Galle</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 12:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 13:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Netuno, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/neptune/neptune_tables.html"><i>https://pds-rings.seti.org/neptune/neptune_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 40.900 km</div><div><b>Fronteira externa:</b> 42.900 km</div><div><b>Planeta:</b> Netuno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 2.000 km</div><div><b>Percentagem de poeira:</b> 40 ~ 70%</div><div><b>Nível:</b> 1</div><div><br/></div><div><span src="assets/img/espaco/Image [59].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1323"/></span>
<h1>Anel Main</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>15/05/2018 12:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>16/05/2018 12:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/jupiter/jupiter_tables.html"><i>https://pds-rings.seti.org/jupiter/jupiter_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b>123.000 km</div><div><b>Fronteira externa:</b> 128.940 km</div><div><b>Planeta:</b> Júpiter</div><div><b>Espessura:</b> 300 km</div><div><b>Largura:</b> 6.500 km</div><div><b>Percentagem de poeira:</b> 25%</div><div><b>Nível: </b>2</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Menos espesso dos Anéis de Júpiter</div><div>&gt; Mais brilhante dos Anéis de Júpiter</div><div><br/></div><div><span src="assets/img/espaco/PIA00538.jpg" type="image/jpeg" data-filename="PIA00538.jpg" height="300" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1108"/><br/></div><div><br/></div><div><span src="assets/img/espaco/PIA00657.jpg" type="image/jpeg" data-filename="PIA00657.jpg" height="487" style="cursor: default;cursor: default;cursor: default;cursor: default;" width="1493"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1326"/></span>
<h1>Anel Gossamer Tebe</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>15/05/2018 15:43</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>15/05/2018 15:45</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/jupiter/jupiter_tables.html"><i>https://pds-rings.seti.org/jupiter/jupiter_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno: </b>128.940 km</div><div><b>Fronteira externa:</b> 226.000 km</div><div><b>Planeta:</b> Júpiter</div><div><b>Espessura:</b> 97.000 km</div><div><b>Largura:</b> 8.400 km</div><div><b>Percentagem de poeira:</b> 100%</div><div><b>Nível:</b> 4</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Anel mais tênue de Júpiter</div><div>&gt; Último anel</div><div><br/></div><div><span src="assets/img/espaco/PIA00659 [1].jpg" type="image/jpeg" data-filename="PIA00659.jpg" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1328"/></span>
<h1>Anel Halo</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>14/05/2018 22:29</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>15/05/2018 15:40</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/jupiter/jupiter_tables.html"><i>https://pds-rings.seti.org/jupiter/jupiter_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 89.400 km</div><div><b>Fronteira externa:</b> 123.000 km</div><div><b>Planeta:</b> Júpiter</div><div><b>Espessura:</b> 12.500 km</div><div><b>Largura:</b> 30.500 km</div><div><b>Percentagem de poeira:</b> 100%</div><div><b>Nível:</b>1</div><div><font color="#4B4B4B" face="Roboto, sans-serif"><span style="font-size: 15px; white-space: pre-wrap;"><br/></span></font></div><div><font color="#4B4B4B" face="Roboto, sans-serif"><span style="font-size: 15px; white-space: pre-wrap;"><i>// MAPA DE COR DOS MATERIAIS</i></span></font></div><div><span src="assets/img/espaco/PIA00658-small.jpg" type="image/jpeg" data-filename="PIA00658-small.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><i style="color: rgb(75, 75, 75); font-family: Roboto, sans-serif; font-size: 15px; white-space: pre-wrap;">// ANEL MAIN E HALO</i></div><div><span src="assets/img/espaco/PIA01622-small.jpg" type="image/jpeg" data-filename="PIA01622-small.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><font color="#4B4B4B" face="Roboto, sans-serif"><span style="font-size: 15px; white-space: pre-wrap;"><i><br/></i></span></font></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1331"/></span>
<h1>Caronte</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/05/2018 11:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 20:34</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão de Kuiper, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Plutão, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Transnetuniano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/pluto-moons/charon/in-depth/"><i>https://solarsystem.nasa.gov/moons/pluto-moons/charon/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 153 horas</div><div><b>Período de órbita do planeta:</b> 153 horas</div><div><b>Temperatura média: </b>-220 ºC</div><div><b>Planeta:</b> Plutão</div><div><b>Distância do Planeta: </b>17.536 km</div><div><b>Peso: </b>0.028 da terra</div><div><b>Gravidade: </b>0.28 m/s²</div><div><b>Diâmetro equatorial:</b> 1.207 km</div><div><b>Atmosfera:</b> sem atmosfera</div><div><b>Vel. p/ orb. o planeta:</b> 718 km/h</div><div><b>Área de superfície:</b> 4.578.343 km²</div><div><b><br/></b></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div>&gt; Sem proteção contra radiação e meteoros</div><div>&gt; Sem oxigênio</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Netuno sempre está a mesma face </div><div>&gt; Estima-se que Caronte seja outro planeta compondo um sistema binário com Plutão, em que um atrai o outro girando em sua órbita.</div><div>&gt; Estima-se que a lua capta a atmosfera de plutão em seu topo e congela aparecendo avermelhada</div><div><br/></div><div><span src="assets/img/espaco/nh-charon-neutral-bright-release_0.jpg" type="image/jpeg" data-filename="nh-charon-neutral-bright-release_0.jpg" height="985" style="cursor: default;" width="985"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1333"/></span>
<h1>Plutão</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/05/2018 12:35</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 12:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão de Kuiper, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Planeta Anão, Plutão, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Transnetuniano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/dwarf-planets/pluto/"><i>https://solarsystem.nasa.gov/planets/dwarf-planets/pluto/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Dia:</b> 153 horas</div><div><b>Ano:</b> 248 anos terrestres</div><div><b>Temperatura média:</b>  -229 ºC</div><div><b>Luas:</b> 5(Charon, Nix, Hydra, Kerberos e Styx)</div><div><b>Distância do Sol:</b> 3.360.000.000 Milhas - 39.48AU</div><div><b>Circunferência equatorial</b><b>:</b> 4.493 milhas</div><div><b>Peso:</b> 0.11 da terra</div><div><b>Gravidade:</b> 0.66 m/s²</div><div><b>Atmosfera:</b> nitrogênio, metano e monóxido de carbono</div><div><b>Vel. p/ orb. sol:</b> 16.809 km/h</div><div><b>Área de superfície:</b> 16.647.940 km²</div><div><b>Oposição:</b> </div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito Frio</div><div>&gt; Sem proteção contra radiação</div><div>&gt; Sem oxigênio</div><div>&gt; Água apenas em estado sólido</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Geadas de nitrogênio</div><div>&gt; Névoa que deixa o céu azul</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Primeiro planeta a ser classificado como planeta anão</div><div>&gt; Possui uma mancha que parece um coração</div><div>&gt; Maior astro do cinturão de Kuiper</div><div>&gt; Tão longe do sol que o sol apenas parece uma estrela brilhante em uma visão no planeta</div><div><br/></div><div><span src="assets/img/espaco/795_crop_p_color2_enhanced_release_detail.jpg" type="image/jpeg" data-filename="795_crop_p_color2_enhanced_release_detail.jpg" height="1200" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/><br/></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/blue_skies_on_pluto-final-2.png" type="image/png" data-filename="blue_skies_on_pluto-final-2.png" height="985" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="985"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1337"/></span>
<h1>Encélado</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/05/2018 13:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 11:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/saturn-moons/enceladus/in-depth/"><i>https://solarsystem.nasa.gov/moons/saturn-moons/enceladus/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 32h 54 min</div><div><b>Período de órbita do planeta:</b> 32h 54 min</div><div><b>Temperatura média:</b> -200 ºC</div><div><b>Planeta:</b> Saturno</div><div><b>Distância do Planeta:</b> 238.000 km</div><div><b>Peso:</b> 0.011 da terra</div><div><b>Gravidade:</b> 0.11 m/s²</div><div><b>Diâmetro equatorial:</b> 508 km</div><div><b>Atmosfera: </b>principalmente vapor de água, hidrogênio molecular um pouco de dióxido de carbono, monóxido de carbono e hidrogênio molecular</div><div><b>Vel. p/ orb. o planeta:</b> 45.487 km/h</div><div><b>Área de superfície:</b> 798.648 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Solo infértil</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div>&gt; Sem oxigênio</div><div><b>----- Fenômenos -----</b></div><div>&gt; Criovulcões(Vulcões gelados)</div><div>&gt; Neva</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Saturno sempre está a mesma face</div><div>&gt; Objeto mais branco e reflexivo do sistema solar</div><div>&gt; Estima-se que haja oceanos em seus polos no subterrâneo</div><div>&gt; Cria um anel de partículas de gelo em partes de sua órbita</div><div>&gt; Forte atração gravitacional faz com que seu núcleo oscile e aqueça</div><div><br/></div><div><span src="assets/img/espaco/saturn 3.jpg" type="image/jpeg" data-filename="saturn 3.jpg" height="2048" style="cursor: default;cursor: default;cursor: default;" width="1623"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1339"/></span>
<h1>Ariel</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/05/2018 16:44</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 11:46</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/uranus-moons/ariel/in-depth/"><i>https://solarsystem.nasa.gov/moons/uranus-moons/ariel/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 60 horas</div><div><b>Período de órbita do planeta:</b> 60 horas</div><div><b>Temperatura média:</b> -213 ºC</div><div><b>Planeta:</b> Urano</div><div><b>Distância do Planeta:</b> 190.900 km</div><div><b>Peso:</b> 0.025 da terra</div><div><b>Gravidade:</b> 0.25 m/s²</div><div><b>Diâmetro equatorial:</b> 1.159 km</div><div><b>Atmosfera: </b>sem atmosfera</div><div><b>Vel. p/ orb. o planeta:</b> 19.832 km/h</div><div><b>Área de superfície:</b> 4.211.307 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div>&gt; Sem oxigênio</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Urano sempre está a mesma face</div><div>&gt; A lua mais jovem de Urano</div><div>&gt; Lua mais brilhante de Urano</div><div>&gt; Forte atração gravitacional faz com que seu núcleo oscile e aqueça</div><div><br/></div><div><span src="assets/img/espaco/ariel-satelite-urano.jpg" type="image/jpeg" data-filename="ariel-satelite-urano.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1341"/></span>
<h1>Tritão</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/05/2018 17:21</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 11:46</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Netuno, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/neptune-moons/triton/in-depth/"><i>https://solarsystem.nasa.gov/moons/neptune-moons/triton/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 5 dias e 21 horas</div><div><b>Período de órbita do planeta: </b>5 dias e 21 horas</div><div><b>Temperatura média: </b>-235 ºC</div><div><b>Planeta: </b>Netuno</div><div><b>Distância do Planeta: </b>355.000 km</div><div><b>Peso: </b>0.079 da terra</div><div><b>Gravidade: </b>0.78 m/s²</div><div><b>Diâmetro equatorial:</b> 2.707 km</div><div><b>Atmosfera:</b> principalmente azoto/nitrogênio e um pouco de metano</div><div><b>Vel. p/ orb. o planeta:</b> 15.803 km/h</div><div><b>Área de superfície: </b>23.017.714 km²</div><div><b><br/></b></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div>&gt; Sem oxigênio</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Nuvens criadas pelo nitrogênio e partículas de gelo</div><div>&gt; Criovulcões(Vulcões gelados)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Netuno sempre está a mesma face</div><div>&gt; Maior lua de Netuno</div><div>&gt; Considerado objeto mais frio do sistema solar</div><div>&gt; A cada ano Tritão se aproxima de Netuno e em milhões de anos será destruído</div><div><br/></div><div><span src="assets/img/espaco/529_PIA00317_modest.jpg" type="image/jpeg" data-filename="529_PIA00317_modest.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1343"/></span>
<h1>Amalteia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/05/2018 11:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 11:46</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Regular, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/jupiter-moons/amalthea/in-depth/"><i>https://solarsystem.nasa.gov/moons/jupiter-moons/amalthea/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 11h 57 min</div><div><b>Período de órbita do planeta:</b> 11h 57 min</div><div><b>Temperatura média:</b> -153 ºC</div><div><b>Planeta:</b> Júpiter</div><div><b>Distância do Planeta: </b>181.400 km</div><div><b>Peso:</b> 0.002 da terra</div><div><b>Gravidade:</b> 0.02 m/s²</div><div><b>Diâmetro equatorial:</b> 167 km</div><div><b>Atmosfera: </b>sem atmosfera</div><div><b>Vel. p/ orb. o planeta:</b> 95.362 km/h</div><div><b>Área de superfície:</b> 87.510 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muita radiação que emana de Júpiter</div><div>&gt; Muito frio</div><div>&gt; Sem oxigênio</div><div>&gt; Exposto a ventos solares</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Júpiter sempre está a mesma face</div><div>&gt; Objeto mais avermelhado do sistema solar</div><div>&gt; Está em rota de colisão com Júpiter daqui a milhares de anos</div><div><br/></div><div><span src="assets/img/espaco/Thebe.jpg" type="image/jpeg" data-filename="Thebe.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/amalthee-lune-de-jupiter_315.png" type="image/png" data-filename="amalthee-lune-de-jupiter_315.png" style="cursor: default;cursor: default;cursor: default;"/> <span src="assets/img/espaco/275px-Jupiter's_moon_Amalthea_photographed_by_Galileo.jpg" type="image/jpeg" data-filename="275px-Jupiter's_moon_Amalthea_photographed_by_Galileo.jpg" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1347"/></span>
<h1>Proteus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/05/2018 20:44</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 11:46</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Netuno, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/neptune-moons/proteus/in-depth/"><i>https://solarsystem.nasa.gov/moons/neptune-moons/proteus/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 26 horas </div><div><b>Período de órbita do planeta: </b>26 horas </div><div><b>Temperatura média: </b>-222 ºC</div><div><b>Planeta: </b>Netuno</div><div><b>Distância do Planeta: </b>117.646 km</div><div><b>Peso: </b>0.077 da terra</div><div><b>Gravidade: </b>0.76 m/s²</div><div><b>Diâmetro equatorial:</b> 428 km</div><div><b>Atmosfera:</b> sem atmosfera</div><div><b>Vel. p/ orb. o planeta:</b> 27.450 km/h</div><div><b>Área de superfície: </b>554.176 km²</div><div><b><br/></b></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div>&gt; Sem oxigênio</div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Netuno sempre está a mesma face</div><div>&gt; Maior lua não esférica do Sistema Solar</div><div><br/></div><div><span src="assets/img/espaco/520_Proteus732X520.jpg" type="image/jpeg" data-filename="520_Proteus732X520.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1349"/></span>
<h1>Titã</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/05/2018 12:08</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/05/2018 11:46</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/saturn-moons/titan/overview/"><i>https://solarsystem.nasa.gov/moons/saturn-moons/titan/overview/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 16 dias</div><div><b>Período de órbita do planeta:</b> 16 dias</div><div><b>Temperatura média:</b> -179 ºC</div><div><b>Planeta:</b> Saturno</div><div><b>Distância do Planeta: </b>1.221.000 km</div><div><b>Peso:</b> 0.13 da terra</div><div><b>Gravidade:</b> 1.35 m/s²</div><div><b>Diâmetro equatorial:</b> 5.150 km</div><div><b>Atmosfera: </b>principalmente nitrogênio e um pouco de metano e hidrogênio</div><div><b>Vel. p/ orb. o planeta:</b> 20.051 km/h</div><div><b>Área de superfície:</b> 83.305.418 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div>&gt; Exposto ao vento solar</div><div>&gt; Sem oxigênio</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Criovulcões(Vulcões gelados)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Saturno sempre está a mesma face</div><div>&gt; Maior lua de Saturno</div><div>&gt; Lagos e chuvas de metano</div><div>&gt; Além da terra único que possui lagos na superfície</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/titan.jpg" type="image/jpeg" data-filename="titan.jpg" height="599" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="882"/></div><div><br/></div><div><span src="assets/img/espaco/300px-Titan_in_front_of_the_ring_and_Saturn.jpg" type="image/jpeg" data-filename="300px-Titan_in_front_of_the_ring_and_Saturn.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1352"/></span>
<h1>Europa</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>07/05/2018 16:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/05/2018 17:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Regular, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/jupiter-moons/europa/in-depth/"><i>https://solarsystem.nasa.gov/moons/jupiter-moons/europa/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 3 dias e 12h</div><div><b>Período de órbita do planeta:</b> 3 dias e 12h</div><div><b>Temperatura média:</b> -171 ºC</div><div><b>Planeta:</b> Júpiter</div><div><b>Distância do Planeta: </b>671.000 km</div><div><b>Peso:</b> 0.13 da terra</div><div><b>Gravidade:</b> 1.31 m/s²</div><div><b>Diâmetro equatorial:</b> 3.122 km</div><div><b>Atmosfera: </b>principalmente oxigênio</div><div><b>Vel. p/ orb. o planeta:</b> 49.476 km/h</div><div><b>Área de superfície:</b> 30.612.893 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Solo infértil</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muita radiação que emana de Júpiter</div><div>&gt; Muito frio</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Terremotos e rachaduras causados pela atração gravitacional</div><div>&gt; Jatos de água ocorrem em seu solo</div><div>&gt; Auroras</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Júpiter sempre está a mesma face</div><div>&gt; Sofre forte atração gravitaconal de Júpiter</div><div>&gt; Estima-se que no seu interior há um oceano e há uma boa possibilidade de haver vida</div><div><br/></div><span src="assets/img/espaco/8774cb2370_48971_europe-vraiescouleurs-galileo-jpl-nasa-07.jpg" type="image/jpeg" data-filename="8774cb2370_48971_europe-vraiescouleurs-galileo-jpl-nasa-07.jpg" height="1000" style="cursor: default;cursor: default;" width="1010"/><br/></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1355"/></span>
<h1>Ganimedes</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>07/05/2018 13:03</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/05/2018 17:13</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Regular, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/jupiter-moons/ganymede/in-depth/"><i>https://solarsystem.nasa.gov/moons/jupiter-moons/ganymede/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação: </b>7 dias e 3h</div><div><b>Período de órbita do planeta:</b> 7 dias e 3h</div><div><b>Temperatura média:</b> -163 ºC</div><div><b>Planeta:</b> Júpiter</div><div><b>Distância do Planeta: </b>1.070.400 km</div><div><b>Peso:</b> 0.14 da terra</div><div><b>Gravidade:</b> 1.43 m/s²</div><div><b>Diâmetro equatorial:</b> 5.262 km</div><div><b>Atmosfera:</b> traços de oxigênio</div><div><b>Vel. p/ orb. o planeta:</b> 39.165 km/h</div><div><b>Área de superfície: </b>86.999.665 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Pouco oxigênio</div><div>&gt; Solo infértil</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Auroras</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Júpiter sempre está a mesma face</div><div>&gt; A maior lua do sistema solar</div><div>&gt; Possui campo magnético e afetado pelo de júpiter</div><div>&gt; Estima-se que no seu interior há um oceano maior que o da Terra</div><div><br/></div><div><span src="assets/img/espaco/C_zDAr6W0AQ73tg.jpg" type="image/jpeg" data-filename="C_zDAr6W0AQ73tg.jpg" height="1200" style="cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1357"/></span>
<h1>Calisto</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>07/05/2018 14:20</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/05/2018 17:13</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Júpiter, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Regular, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/jupiter-moons/callisto/in-depth/"><i>https://solarsystem.nasa.gov/moons/jupiter-moons/callisto/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 16 dias e 16h</div><div><b>Período de órbita do planeta:</b> 16 dias e 16h</div><div><b>Temperatura média:</b> -172 ºC</div><div><b>Planeta:</b> Júpiter</div><div><b>Distância do Planeta: </b>1.882.700 km</div><div><b>Peso:</b> 0.12 da terra</div><div><b>Gravidade:</b> 1.24 m/s²</div><div><b>Diâmetro equatorial:</b> 5.262 km</div><div><b>Atmosfera:</b> traços de dióxido de carbono</div><div>     <b>[Oxigênio]</b> = níveis moleculares</div><div><b>Vel. p/ orb. o planeta:</b> 29.531 km/h</div><div><b>Área de superfície: </b>73.004.909 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Quase nada de oxigênio</div><div>&gt; Solo infértil</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Júpiter sempre está a mesma face</div><div>&gt; Estima-se que no seu interior há um oceano </div><div>&gt; Satélite com mais cratera do sistema solar</div><div>&gt; Possui a paisagem mais antiga do sistema solar</div><div><br/></div><div><span src="assets/img/espaco/428_PIA03456_modest.jpg" type="image/jpeg" data-filename="428_PIA03456_modest.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1359"/></span>
<h1>Fobos</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/05/2018 13:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/05/2018 13:31</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Marte, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/mars-moons/phobos/in-depth/"><i>https://solarsystem.nasa.gov/moons/mars-moons/phobos/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação: </b>7 horas 39 min</div><div><b>Período de órbita do planeta:</b> 7 horas 39 min</div><div><b>Temperatura média:</b> -40 ºC</div><div><b>Planeta:</b> Marte</div><div><b>Distância do Planeta: </b>9.380 km</div><div><b>Peso:</b> 0.0005 da terra</div><div><b>Gravidade:</b> 0.0057 m/s²</div><div><b>Diâmetro equatorial:</b> 22 km</div><div><b>Atmosfera:</b> sem atmosfera</div><div><b>Vel. p/ orb. o planeta:</b> 7.696 km/h</div><div><b>Área de superfície: </b>1.548 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Sem proteção contra radiação e meteoros</div><div>&gt; Sem oxigênio</div><div>&gt; Solo infértil</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Terremotos e rachaduras causados pela atração gravitacional</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Marte sempre está a mesma face</div><div>&gt; A lua que orbita mais próximo de seu planeta</div><div>&gt; A cada ano se aproxima 2 metros e em alguns milhões de ano será destruída pelo choque com Marte</div><div>&gt; Um dos lugares mais escuros do sistema solar</div><div><br/></div><div><span src="assets/img/espaco/436_PHOBOS_732X520.jpg" type="image/jpeg" data-filename="436_PHOBOS_732X520.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1361"/></span>
<h1>Deimos</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/05/2018 16:35</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/05/2018 13:29</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Marte, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/mars-moons/deimos/in-depth/"><i>https://solarsystem.nasa.gov/moons/mars-moons/deimos/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação: </b>30h 18 min</div><div><b>Período de órbita do planeta:</b> 30h 18 min</div><div><b>Temperatura média:</b> -40 ºC</div><div><b>Planeta:</b> Marte</div><div><b>Distância do Planeta: </b>23.460 km</div><div><b>Peso:</b> 0.0003 da terra</div><div><b>Gravidade:</b> 0.003 m/s²</div><div><b>Diâmetro equatorial:</b> 12.4km</div><div><b>Atmosfera:</b> sem atmosfera</div><div><b>Vel. p/ orb. o planeta:</b> 4.864 km/h</div><div><b>Área de superfície:</b> 483 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Sem proteção contra radiação e meteoros</div><div>&gt; Sem oxigênio</div><div>&gt; Solo infértil</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Marte sempre está a mesma face</div><div>&gt; A menor lua do sistema solar</div><div>&gt; Um dos lugares mais escuros do sistema solar</div><br/><div><span src="assets/img/espaco/435_Deimos732X520.jpg" type="image/jpeg" data-filename="435_Deimos732X520.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1363"/></span>
<h1>Lua</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/05/2018 21:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>06/05/2018 13:17</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Terra, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 27 dias</div><div><b>Período de órbita do planeta:</b> 27 dias</div><div><b>Temperatura média:</b> -53 ºC</div><div><b>Planeta:</b> Terra</div><div><b>Distância do Planeta:</b> 384.400 km</div><div><b>Peso:</b> 0.16 da terra</div><div><b>Gravidade:</b> 1.624 m/s²</div><div><b>Diâmetro equatorial:</b> 3.474 km</div><div><b>Atmosfera:</b> principalmente hélio, neônio, hidrogênio, argônio</div><div><b>     [oxigênio]:</b> Presente na água em forma de gelo</div><div><b>Vel. p/ orb. o planeta:</b> 3.680 km/h</div><div><b>Área de superfície:</b> 37.936.694 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Sem proteção contra radiação e meteoros</div><div>&gt; Quase nada de oxigênio</div><div>&gt; Solo difícil de cultivar</div><div>&gt; Pouca água</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Terremotos</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Terra sempre está a mesma face</div><div><br/></div><div><span src="assets/img/espaco/843_christmas2015fullmoon.jpg" type="image/jpeg" data-filename="843_christmas2015fullmoon.jpg" height="985" style="cursor: default;cursor: default;cursor: default;" width="985"/><br/></div><div><br/></div><div><span src="assets/img/espaco/839_as15-86-11603.jpg" type="image/jpeg" data-filename="839_as15-86-11603.jpg" style="cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/760_296636main_1241_full_full.jpg" type="image/jpeg" data-filename="760_296636main_1241_full_full.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1367"/></span>
<h1>Haumea</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/05/2018 19:20</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/05/2018 20:19</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão de Kuiper, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Haumea, Laniakea, Nuvem Interestelar Local, Planeta Anão, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Transnetuniano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/dwarf-planets/haumea"><i>https://solarsystem.nasa.gov/planets/dwarf-planets/haumea</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Dia:</b> 4 horas</div><div><b>Ano:</b> 285 anos terrestres</div><div><b>Temperatura média:</b>  -223 ºC</div><div><b>Luas:</b> 2(Namaka e Hiʻiaka)</div><div><b>Distância do Sol:</b> 4.010.000.000 Milhas - 43AU</div><div><b>Circunferência equatorial</b><b>:</b> 2740 milhas</div><div><b>Peso:</b> 0.05 da terra</div><div><b>Gravidade:</b>  0.51 m/s²</div><div><b>Atmosfera:</b> sem atmosfera</div><div><b>Vel. p/ orb. sol:</b> 16.191 km/h</div><div><b>Área de superfície:</b> ?  km²</div><div><b>Oposição:</b> </div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito Frio</div><div>&gt; Sem proteção contra radiação</div><div>&gt; Sem oxigênio</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Tão longe do sol que o sol apenas parece uma estrela brilhante em uma visão no planeta</div><div>&gt; Único entre planetas e planetas anões com formato oval do sistema solar</div><div>&gt; Possui um pequeno anel em volta dele</div><div><br/></div><div>/// APENAS REPRESENTAÇÃO CONCEITUAL ATRAVÉS DAS INFORMAÇÕES COLETADAS ///</div><div><span src="assets/img/espaco/haumea_haumea_by_snowfall_the_cat-dbrcnsy.png" type="image/png" data-filename="haumea_haumea_by_snowfall_the_cat-dbrcnsy.png"/><br/></div><div><br/></div><div><span src="assets/img/espaco/250px-2003EL61art.jpg" type="image/jpeg" data-filename="250px-2003EL61art.jpg"/><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1370"/></span>
<h1>Éris</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/05/2018 17:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/05/2018 19:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Éris, Grupo Local, Laniakea, Nuvem Interestelar Local, Planeta Anão, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/dwarf-planets/eris"><i>https://solarsystem.nasa.gov/planets/dwarf-planets/eris</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Dia:</b> 25h 54 min</div><div><b>Ano:</b> 557 anos terrestres</div><div><b>Temperatura média:</b>  -243 ºC</div><div><b>Luas:</b> 1(Dysnomia)</div><div><b>Distância do Sol:</b> 6.325.000.000 Milhas - 37.9AU</div><div><b>Circunferência equatorial</b><b>:</b> 4.448 milhas</div><div><b>Peso:</b> 0.06 da terra</div><div><b>Gravidade:</b>  0.65 m/s²</div><div><b>Atmosfera:</b> principalmente metano</div><div><b>Vel. p/ orb. sol:</b> 12.364 km/h</div><div><b>Área de superfície:</b> 21.237.166 km²</div><div><b>Oposição:</b> </div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito Frio</div><div>&gt; Sem proteção contra radiação</div><div>&gt; Sem oxigênio</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Neva em Éris</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Tão longe do sol que o sol apenas parece uma estrela brilhante em uma visão no planeta</div><div><br/></div><div>/// APENAS REPRESENTAÇÃO CONCEITUAL ATRAVÉS DAS INFORMAÇÕES COLETADAS ///</div><div><span src="assets/img/espaco/planeta-eris-na-astrologia.jpg" type="image/jpeg" data-filename="planeta-eris-na-astrologia.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/download [12].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1373"/></span>
<h1>Makemake</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/05/2018 18:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/05/2018 19:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão de Kuiper, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Makemake, Nuvem Interestelar Local, Planeta Anão, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Transnetuniano, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Dia:</b> 2 horas e 30 min</div><div><b>Ano:</b> 305 anos terrestres</div><div><b>Temperatura média:</b>  -243 ºC</div><div><b>Luas:</b> Talvez 1</div><div><b>Distância do Sol:</b> 4.253.000.000 Milhas - 45.8AU</div><div><b>Circunferência equatorial</b><b>:</b> 2740 milhas</div><div><b>Peso:</b> 0.03 da terra</div><div><b>Gravidade:</b>  0.29 m/s²</div><div><b>Atmosfera:</b> principalmente etano e metano e um pouco de nitrogênio</div><div><b>Vel. p/ orb. sol:</b> 15.816 km/h</div><div><b>Área de superfície:</b> 6.900.166 km²</div><div><b>Oposição:</b> </div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito Frio</div><div>&gt; Sem proteção contra radiação</div><div>&gt; Sem oxigênio</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Tão longe do sol que o sol apenas parece uma estrela brilhante em uma visão no planeta</div><div><br/></div><div>/// APENAS REPRESENTAÇÃO CONCEITUAL ATRAVÉS DAS INFORMAÇÕES COLETADAS ///</div><div><span src="assets/img/espaco/28433981_1825620937457848_3133061424184557568_n.jpg" type="image/jpeg" data-filename="28433981_1825620937457848_3133061424184557568_n.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/download [13].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1376"/></span>
<h1>Ceres</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/05/2018 14:39</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/05/2018 18:53</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Planeta Anão, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/planets/dwarf-planets/ceres/"><i>https://solarsystem.nasa.gov/planets/dwarf-planets/ceres/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Dia:</b> 9h 4 min</div><div><b>Ano:</b> 4 anos e 6 meses terrestres</div><div><b>Temperatura média:</b>  -106 ºC</div><div><b>Luas:</b> 0</div><div><b>Distância do Sol:</b> 257.000.000 Milhas - 2.7AU</div><div><b>Circunferência equatorial</b><b>:</b> 1.797 milhas</div><div><b>Peso:</b> 0.02 da terra</div><div><b>Gravidade:</b> 0.27 m/s²</div><div><b>Atmosfera:</b> traços de vapor de água</div><div><b>Vel. p/ orb. sol:</b> 64.360 km/h</div><div><b>Área de superfície:</b> 2.849.631 km²</div><div><b>Oposição:</b> </div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito Frio</div><div>&gt; Sem proteção contra radiação</div><div>&gt; Sem oxigênio</div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div><br/></div><div><b>----- Fenômenos -----</b></div><div>&gt; Erupções de água</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Maior objeto do cinturão de asteroides</div><div>&gt; Maior depósito de água doce do sistema solar</div><div>&gt; É possível que Ceres seja um planeta embrionário</div><div><br/></div><div><span src="assets/img/espaco/466_PIA19557_modest.jpg" type="image/jpeg" data-filename="466_PIA19557_modest.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/467_PIA21406.jpg" type="image/jpeg" data-filename="467_PIA21406.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1379"/></span>
<h1>Terra</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>07/02/2018 22:18</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>02/05/2018 11:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Planeta, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Telúrico(Rochoso), Terra, Via Láctea, Zona Habitável</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/topics/earth/"><i>https://www.nasa.gov/topics/earth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Dia:</b> 24h</div><div><b>Ano:</b> 365 dias</div><div><b>Temperatura:</b> -126 ~ 136 [ 57 ] ºC</div><div><b>Luas:</b> 1</div><div><b>Distância do Sol:</b> 93.000.000 - 1AU</div><div><b>Circunferência equatorial</b><b>:</b> 24.873 milhas</div><div><b>Gravidade:</b> 9.8 m/s²</div><div><b>Atmosfera:</b> nitrogênio, oxigênio, argônio, outros</div><div><b>Vel. p/ orb. sol:</b> 107.118 km/h</div><div><b>Área de superfície: </b>510.064.472 km²</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/581_PIA18033_1200w.jpg" type="image/jpeg" data-filename="581_PIA18033_1200w.jpg" height="900" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1200"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1384"/></span>
<h1>Jápeto</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/06/2018 13:17</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/06/2018 13:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/saturn-moons/iapetus/in-depth/"><i>https://solarsystem.nasa.gov/moons/saturn-moons/iapetus/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 79 dias</div><div><b>Período de órbita do planeta:</b> 79 dias</div><div><b>Temperatura média:</b> -163 ºC</div><div><b>Planeta:</b> Saturno</div><div><b>Distância do Planeta: </b>3.600.000 km</div><div><b>Peso:</b> 0.022 da terra</div><div><b>Gravidade:</b> 0.22 m/s²</div><div><b>Diâmetro equatorial:</b> 1.492 km</div><div><b>Atmosfera:</b> sem atmosfera</div><div><b>Vel. p/ orb. o planeta:</b> 11.772 km/h</div><div><b>Área de superfície:</b> 6.799.755 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muito frio</div><div>&gt; Exposto ao vento solar</div><div>&gt; Sem oxigênio</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Saturno sempre está a mesma face</div><div><br/></div><div><span src="assets/img/espaco/19102618_b5zYD.jpg" type="image/jpeg" data-filename="19102618_b5zYD.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/japet-lune-de-saturne.jpg" type="image/jpeg" data-filename="japet-lune-de-saturne.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1389"/></span>
<h1>C/2012 S1 (ISON)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 16:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 16:44</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cometa, Cometa Hiperbólico, Complexo de Superaglomerados Pisces-Cetus, Destruído, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/c-2012-s1-ison/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/c-2012-s1-ison/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.012AU</div><div><b>Afélio:</b> ? AU</div><div><b>Semi-eixo:</b> ? AU</div><div><b>Período de orbital:</b> ~ 1.2 milhões de anos</div><div><b>Diâmetro:</b> 1.4 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 28/11/2013</div><div><b>Próximo periélio:</b> destruído</div><div><br/></div><div><span src="assets/img/espaco/275px-Comet_ISON_(C-2012_S1)_by_TRAPPIST_on_2013-11-15.jpg" type="image/jpeg" data-filename="275px-Comet_ISON_(C-2012_S1)_by_TRAPPIST_on_2013-11-15.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1391"/></span>
<h1>C/1861 G1 (Thatcher)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 16:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 16:31</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa C/1861 G1 (Thatcher), Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/c-1861-g1-thatcher/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/c-1861-g1-thatcher/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.92AU</div><div><b>Afélio:</b> 110AU</div><div><b>Semi-eixo:</b>  8.317.000.000 km - 55.6AU</div><div><b>Período de orbital:</b> 415 anos </div><div><b>Diâmetro:</b> ? km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 03/07/1861</div><div><b>Próximo periélio:</b> ~ 2280</div><div><br/></div><div><i><a>//REPRESENTAÇÃ</a>O GRÁFICA DE SUA ÓRBITA</i></div><div><span src="assets/img/espaco/C.1086 G1.jpg" type="image/jpeg" data-filename="C.1086 G1.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1393"/></span>
<h1>C/2013 A1 Siding Spring</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 16:12</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 16:13</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cometa, Cometa Hiperbólico, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/c-2013-a1-siding-spring/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/c-2013-a1-siding-spring/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 1.3AU</div><div><b>Afélio:</b> 371AU</div><div><b>Semi-eixo:</b> 27.825.000.000 km - 186AU</div><div><b>Período de orbital:</b> milhões de anos</div><div><b>Diâmetro:</b> 0.65 km</div><div><b>Velocidade média:</b> 201.600 km/h</div><div><b>Último periélio:</b> 25/10/2014</div><div><b>Próximo periélio:</b> em 740 mil anos</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Passe muito perto de marte</div><div><br/></div><div><span src="assets/img/espaco/NASA-14090-Comet-C2013A1-SidingSpring-Hubble-20140311.jpg" type="image/jpeg" data-filename="NASA-14090-Comet-C2013A1-SidingSpring-Hubble-20140311.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1395"/></span>
<h1>C/1995 O1 (Hale-Bopp)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 15:55</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 15:59</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/c-1995-o1-hale-bopp/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/c-1995-o1-hale-bopp/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.91AU</div><div><b>Afélio:</b> 371AU</div><div><b>Semi-eixo:</b> 27.825.000.000 km - 186AU</div><div><b>Período de orbital:</b> 2.537 anos</div><div><b>Diâmetro:</b> 60 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 01/04/1997</div><div><b>Próximo periélio:</b> 4385</div><div><br/></div><div><span src="assets/img/espaco/250px-Halebopp031197.jpg" type="image/jpeg" data-filename="250px-Halebopp031197.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1397"/></span>
<h1>109P/Swift-Tuttle</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 15:26</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 15:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa 109P/Swift-Tuttle, Cometa Periódico, Cometa Tipo Halley, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Perigo para Terra, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/109p-swift-tuttle/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/109p-swift-tuttle/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.95AU</div><div><b>Afélio:</b> 51.22AU</div><div><b>Semi-eixo:</b> 3.889.000.000 km - 26AU</div><div><b>Período de orbital:</b> 133.28 anos</div><div><b>Diâmetro:</b> 26 km</div><div><b>Velocidade média:</b> 78.120 km/h</div><div><b>Último periélio:</b> 11/12/1992</div><div><b>Próximo periélio:</b> 12/07/2126</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Suas órbitas futuras apontam uma possível colisão com a terra ou a lua</div><div><br/></div><div><span src="assets/img/espaco/903_swift-tuttle_480x320_comets.jpg" type="image/jpeg" data-filename="903_swift-tuttle_480x320_comets.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1399"/></span>
<h1>81P/Wild (Wild 2)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 14:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 15:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Família Júpiter, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/81p-wild/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/81p-wild/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 1.5AU</div><div><b>Afélio:</b> 5.3AU</div><div><b>Semi-eixo:</b> 508.000.000 km - 3.4AU</div><div><b>Período de orbital:</b> 6.4 anos</div><div><b>Diâmetro:</b> 4 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 15/07/2016</div><div><b>Próximo periélio:</b> 15 de Dezembro de 2022</div><div><br/></div><div><span src="assets/img/espaco/250px-Wild2_3.jpg" type="image/jpeg" data-filename="250px-Wild2_3.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1401"/></span>
<h1>103P/Hartley (Hartley 2)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 15:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 15:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Família Júpiter, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/103p-hartley-hartley-2/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/103p-hartley-hartley-2/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 1.05AU</div><div><b>Afélio:</b> 5.87AU</div><div><b>Semi-eixo:</b> 517.000.000 km - 3.46AU</div><div><b>Período de orbital:</b> 6.4 anos</div><div><b>Diâmetro:</b> 1.6 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 20/04/2017</div><div><b>Próximo periélio:</b> 2021</div><div><br/></div><div><span src="assets/img/espaco/616_hartley2_main.jpg" type="image/jpeg" data-filename="616_hartley2_main.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1403"/></span>
<h1>67P/Churyumov-Gerasimenko</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/06/2018 14:47</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>01/06/2018 14:57</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Família Júpiter, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/67p-churyumov-gerasimenko/"><i>https://solarsystem.nasa.gov/small-bodies/comets/67p-churyumov-gerasimenko/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 1.2AU</div><div><b>Afélio:</b> 5.6AU</div><div><b>Semi-eixo:</b> 508.000.000 km - 3.4AU</div><div><b>Período de orbital:</b> 6.44 anos</div><div><b>Diâmetro:</b> 4 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 13/08/2015</div><div><b>Próximo periélio:</b> 2022</div><div><br/></div><div><span src="assets/img/espaco/1297_ESA_Rosetta_NavCam_20150131_Mosaic.jpg" type="image/jpeg" data-filename="1297_ESA_Rosetta_NavCam_20150131_Mosaic.jpg" height="1199" style="cursor: default;" width="1280"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1405"/></span>
<h1>19P/Borrelly</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/05/2018 17:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/05/2018 20:54</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/19p-borrelly/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/19p-borrelly/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 1.35AU</div><div><b>Afélio:</b> 5.83AU</div><div><b>Semi-eixo:</b> 537.000.000 km - 3.59AU</div><div><b>Período de orbital:</b> 6.86 anos</div><div><b>Diâmetro:</b> 4.8 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 28/05/2015</div><div><b>Próximo periélio:</b> 01 de fevereiro de 2022</div><div><br/></div><div><span src="assets/img/espaco/625_PIA03500.jpg" type="image/jpeg" data-filename="625_PIA03500.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1407"/></span>
<h1>9P/Tempel 1</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/05/2018 17:08</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/05/2018 17:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Família Júpiter, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/9p-tempel-1/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/9p-tempel-1/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 1.5AU</div><div><b>Afélio:</b> 4.7AU</div><div><b>Semi-eixo:</b> 471.000.000 km - 3.15AU</div><div><b>Período de orbital:</b> 5.58 anos</div><div><b>Diâmetro:</b> 6 km</div><div><b>Velocidade média:</b> ? km/h (36)</div><div><b>Último periélio:</b> 02/08/2016</div><div><b>Próximo periélio:</b> 04 de março de 2022</div><div><br/></div><div><span src="assets/img/espaco/637_PIA02137_modest.jpg" type="image/jpeg" data-filename="637_PIA02137_modest.jpg" height="900" style="cursor: default;cursor: default;cursor: default;" width="900"/><br/></div><br/></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1409"/></span>
<h1>1P/Halley</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 19:04</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/05/2018 17:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa 1P/Halley, Cometa Periódico, Cometa Tipo Halley, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/1p-halley/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/1p-halley/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.5AU</div><div><b>Afélio:</b> 35.1AU</div><div><b>Semi-eixo:</b> 2.661.000.000 km - 17.8AU</div><div><b>Período de orbital:</b> 75.3 anos</div><div><b>Diâmetro:</b> 11 km</div><div><b>Velocidade média:</b>  66.8 km/h</div><div><b>Último periélio:</b> 09/01/1986</div><div><b>Próximo periélio:</b> 28 de julho de 2061</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Único cometa que é possível ser visto a olho nu</div><div><br/></div><div><span src="assets/img/espaco/627_Giotto_halley_br.jpg" type="image/jpeg" data-filename="627_Giotto_halley_br.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/Lspn_comet_halley.jpg" type="image/jpeg" data-filename="Lspn_comet_halley.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1412"/></span>
<h1>2P/Encke</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 19:28</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/05/2018 17:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/comets/2p-encke/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/comets/2p-encke/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.33AU</div><div><b>Afélio:</b> 4.11AU</div><div><b>Semi-eixo:</b> 332.000.000 km - 2.22AU</div><div><b>Período de orbital:</b> 3.3 anos</div><div><b>Diâmetro:</b> 4.8 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 10/03/2017</div><div><b>Próximo periélio:</b> 25~27 de junho de 2020</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Cometa com a órbita mais curta encontrado</div><div><br/></div><div><span src="assets/img/espaco/631_messenger-comet-2p-encke.jpg" type="image/jpeg" data-filename="631_messenger-comet-2p-encke.jpg" height="719" style="cursor: default;cursor: default;cursor: default;cursor: default;" width="1041"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1414"/></span>
<h1>Reia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 18:05</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 18:50</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Reia, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/saturn-moons/rhea/in-depth/"><i>https://solarsystem.nasa.gov/moons/saturn-moons/rhea/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Período de rotação:</b> 4 dias e 12 horas</div><div><b>Período de órbita do planeta:</b> 4 dias e 12 horas</div><div><b>Temperatura média:</b> -197.2 ºC</div><div><b>Planeta:</b> Saturno</div><div><b>Distância do Planeta: </b>527.108 km</div><div><b>Peso:</b> 0.026 da terra</div><div><b>Gravidade:</b> 0.26 m/s²</div><div><b>Diâmetro equatorial:</b> 1.528 km</div><div><b>Atmosfera: </b>principalmente oxigênio e dióxido de carbono</div><div><b>Vel. p/ orb. o planeta:</b> 30.564 km/h</div><div><b>Área de superfície:</b> 7.337.000 km²</div><div><br/></div><div><b>/// DIFICULDADES ///</b></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Júpiter sempre está a mesma face</div><div>&gt; Possui anéis</div><div><br/></div><div><span src="assets/img/espaco/805_PIA12648.jpg" type="image/jpeg" data-filename="805_PIA12648.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1416"/></span>
<h1>Chuí</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 17:10</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 17:13</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Chariklo, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 404 km</div><div><b>Fronteira externa:</b> 407 km</div><div><b>Astro:</b> Chariklo(Asteróide)</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 3 km</div><div><b>Percentagem de poeira:</b> ? % (baixa) (6)</div><div><b>Nível:</b> 2</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Únicos aneis encontrados em um asteróide</div><div><br/></div><div><span src="assets/img/espaco/Chariklo_with_rings_eso1410b.jpg" type="image/jpeg" data-filename="Chariklo_with_rings_eso1410b.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1418"/></span>
<h1>Oiapoque</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 16:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 17:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Chariklo, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 390 km</div><div><b>Fronteira externa:</b> 397 km</div><div><b>Astro:</b> Chariklo(Asteróide)</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 7 km</div><div><b>Percentagem de poeira:</b> ? % (alta) (38)</div><div><b>Nível:</b> 1</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Únicos aneis encontrados em um asteróide</div><div><br/></div><div><span src="assets/img/espaco/Chariklo_with_rings_eso1410b [1].jpg" type="image/jpeg" data-filename="Chariklo_with_rings_eso1410b.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1420"/></span>
<h1>Chariklo</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 13:43</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 14:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Centauro, Chariklo, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/asteroids/10199-chariklo/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/asteroids/10199-chariklo/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 10199</div><div><b>Período de rotação:</b> 7h</div><div><b>Período orbital:</b> 63 anos</div><div><b>Temperatura:</b>  - ? ºC (258)</div><div><b>Distância do Sol:</b> 1.440.000.000 Milhas - 15.5AU</div><div><b>Diâmetro</b><b>:</b>  302 km</div><div><b>Gravidade:</b> ?  m/s² (fraca)</div><div><b>Composição:</b> rocha e gelo</div><div><b>Velocidade orbital:</b>  16.18 km/s</div><div><br/></div><div><div>----- <b>Curiosidades -----</b></div><div>&gt; Único asteroide conhecido que possui aneis</div>
&gt; Maior centauro conhecido</div><div><br/></div><div><span src="assets/img/espaco/Chariklo_with_rings_eso1410b [2].jpg" type="image/jpeg" data-filename="Chariklo_with_rings_eso1410b.jpg" height="785" style="cursor: default;" width="1200"/><br/></div><div><br/></div><div><span src="assets/img/espaco/eso1410a.jpg" type="image/jpeg" data-filename="eso1410a.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1423"/></span>
<h1>Vesta</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 16:58</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 13:50</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Família Vesta, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo V(Vestóide), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/asteroids/4-vesta/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/asteroids/4-vesta/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 4</div><div><b>Período de rotação:</b> 5h 18 min</div><div><b>Período orbital:</b> 3 anos e 7 meses</div><div><b>Temperatura:</b>  -103 ºC</div><div><b>Distância do Sol:</b> 219.000.000 Milhas - 2.36AU</div><div><b>Diâmetro</b><b>:</b>  530 km</div><div><b>Gravidade:</b> 0.22 m/s²</div><div><b>Composição:</b> Rocha, basalto e lava congelada</div><div><b>Velocidade orbital:</b>   19.34 km/s</div><div> </div><div>----- <b>Curiosidades -----</b></div><div>&gt; Maior asteróide do Cinturão de Asteroides</div><div>&gt; Único Asteroide ocasionalmente visto a olho nu da terra</div><div><br/></div><div><span src="assets/img/espaco/600px-Vesta_Full-Frame.jpg" type="image/jpeg" data-filename="600px-Vesta_Full-Frame.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/209_4_vesta_main.jpg" type="image/jpeg" data-filename="209_4_vesta_main.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1426"/></span>
<h1>Dembowska</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 12:43</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 13:16</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo R, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=349"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=349</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 349</div><div><b>Período de rotação:</b> 4h 42 min</div><div><b>Período orbital:</b> 5 anos</div><div><b>Temperatura:</b>  - 133 ºC</div><div><b>Distância do Sol:</b> 269.000.000 Milhas - 2.9AU</div><div><b>Diâmetro</b><b>:</b>  140 km</div><div><b>Gravidade:</b>  0.04 m/s²</div><div><b>Composição:</b> silicato</div><div><b>Velocidade orbital:</b>  17 km/s</div><div><br/></div><div>//<i>ARTE CONCEITUAL</i></div><div><span src="assets/img/espaco/265px-349Dembowska_(Lightcurve_Inversion).png" type="image/png" data-filename="265px-349Dembowska_(Lightcurve_Inversion).png" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1428"/></span>
<h1>Kassandra</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 12:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 12:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo T, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2000114#content"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2000114#content</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 114</div><div><b>Período de rotação:</b> 10h 42 min</div><div><b>Período orbital:</b> 4 anos e 4 meses</div><div><b>Temperatura:</b>  - 103 ºC</div><div><b>Distância do Sol:</b> 241.000.000 Milhas - 2.6AU</div><div><b>Diâmetro</b><b>:</b>  94 km</div><div><b>Gravidade:</b> 0.02 m/s²</div><div><b>Composição: </b>Rocha(trollita)</div><div><b>Velocidade orbital:</b>  18.2 km/s</div><div><br/></div><div>//<i>REPRESENTAÇÃO GRÁFICA EXISTENTE DO ASTEROIDE NO FILME METEORO</i></div><div><span src="assets/img/espaco/Image3.jpg" type="image/jpeg" data-filename="Image3.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div>//<i>REPRESENTAÇÃO GRÁFICA DE SUA ÓRBITA</i></div><div><span src="assets/img/espaco/download.png" type="image/png" data-filename="download.png" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1431"/></span>
<h1>Steins</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 11:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 11:35</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Família Hungária, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo E, Tipo X, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2002867#content"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2002867#content</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 2867</div><div><b>Período de rotação:</b> 6h</div><div><b>Período orbital:</b> 3 anos e 7 meses</div><div><b>Temperatura:</b>  - ? ºC (-50)</div><div><b>Distância do Sol:</b> 219.000.000 Milhas - 2.36AU</div><div><b>Diâmetro</b><b>:</b>  5.160 km</div><div><b>Gravidade:</b> ? m/s² (extremamente fraca)</div><div><b>Composição: </b>Rocha(silicatos)</div><div><b>Velocidade orbital:</b>  19.37 km/s</div><div><br/></div><div><span src="assets/img/espaco/255px-2867_Šteins_by_Rosetta_(reprocessed).png" type="image/png" data-filename="255px-2867_Šteins_by_Rosetta_(reprocessed).png" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1433"/></span>
<h1>Sylvia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/05/2018 10:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/05/2018 10:49</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo P, Tipo X, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=87"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=87</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 87</div><div><b>Período de rotação:</b> 5h 10 min</div><div><b>Período orbital:</b> 6 anos e 6 meses</div><div><b>Temperatura:</b>  - 122 ºC</div><div><b>Distância do Sol:</b> 316.000.000 Milhas - 3.4AU</div><div><b>Diâmetro</b><b>:</b>  253 km</div><div><b>Gravidade:</b> 0.06 m/s²</div><div><b>Composição:</b> Silicatos e gelo</div><div><b>Velocidade orbital:</b> 15.94 km/s</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Possui dois satélites(Rômulo e Remo)</div><div><br/></div><div>//<i>ARTE CONCEITUAL</i></div><div><span src="assets/img/espaco/2d9ebc8b-c0d1-4101-b068-0742718f07ba.png" type="image/png" data-filename="2d9ebc8b-c0d1-4101-b068-0742718f07ba.png" style="cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/download [14].jpg" type="image/jpeg" data-filename="download.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1436"/></span>
<h1>Anel δ</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>16/05/2018 17:34</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 23:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Urano, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/uranus/uranus_tables.html"><i>https://pds-rings.seti.org/uranus/uranus_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 48.223 km</div><div><b>Fronteira externa:</b> 48.230 km</div><div><b>Planeta:</b> Urano</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 3~7 km</div><div><b>Percentagem de poeira:</b> ? % (há alguma poeira mais que escassa)</div><div><b>Nível:</b> 8</div><div><br/></div><div><span src="assets/img/espaco/Image [60].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1438"/></span>
<h1>Hígia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 23:01</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 23:05</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo C, Tipo C(Carbonáceo), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=10"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=10</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 10</div><div><b>Período de rotação:</b> 1h 9 min</div><div><b>Período orbital:</b> 5 anos e 6 meses</div><div><b>Temperatura:</b>  - 112 ºC</div><div><b>Distância do Sol:</b> 241.000.000 Milhas - 2.4AU</div><div><b>Diâmetro</b><b>:</b>  407 km</div><div><b>Gravidade:</b> 0.16 m/s²</div><div><b>Composição: </b>Rocha(Minerais hidratados)</div><div><b>Velocidade orbital:</b>  16.82 km/s</div><div><br/></div>
//<i>ARTE CONCEITUAL</i><br/><div><span src="assets/img/espaco/260px-Higia.jpg" type="image/jpeg" data-filename="260px-Higia.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1440"/></span>
<h1>Fortuna</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 22:44</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 22:52</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo C(Carbonáceo), Tipo G, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=19"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=19</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 19</div><div><b>Período de rotação:</b> 18 min</div><div><b>Período orbital:</b> 3 anos e 9 meses</div><div><b>Temperatura:</b>  - 93 ºC</div><div><b>Distância do Sol:</b> 223.000.000 Milhas - 2.4AU</div><div><b>Diâmetro</b><b>:</b>  200 km</div><div><b>Gravidade:</b> 0.06 m/s²</div><div><b>Composição: </b>Rocha(Minerais hidratados)</div><div><b>Velocidade orbital:</b>  19 km/s</div><div><br/></div><div><span src="assets/img/espaco/19Fortuna-LB1-crab-mag11.jpg" type="image/jpeg" data-filename="19Fortuna-LB1-crab-mag11.jpg" height="1521" style="cursor: default;" width="1074"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1442"/></span>
<h1>Interamnia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 22:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 22:34</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo C(Carbonáceo), Tipo F, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=704"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=704</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 704</div><div><b>Período de rotação:</b> 8h 42 min</div><div><b>Período orbital:</b> 5 anos e 4 meses</div><div><b>Temperatura:</b>  - 83 ºC</div><div><b>Distância do Sol:</b> 284.000.000 Milhas - 3AU</div><div><b>Diâmetro</b><b>:</b>  306 km</div><div><b>Gravidade:</b> 0.18 m/s²</div><div><b>Composição: </b>Rocha(Minerais hidratados)</div><div><b>Velocidade orbital:</b> 17 km/s</div><div><br/></div><div><span src="assets/img/espaco/250px-Interamnia_medie.gif" type="image/gif" data-filename="250px-Interamnia_medie.gif" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1444"/></span>
<h1>Psique</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 19:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 20:13</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo M(Metálico), Tipo X, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/asteroids/16-psyche/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/asteroids/16-psyche/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 16</div><div><b>Período de rotação:</b> 4h 06 min</div><div><b>Período orbital:</b> 5 anos</div><div><b>Temperatura:</b>  -113 ºC</div><div><b>Distância do Sol:</b> 483.000.000 Milhas - 2.9AU</div><div><b>Diâmetro</b><b>:</b>  210 km</div><div><b>Gravidade:</b> 0.06 m/s²</div><div><b>Composição:</b> Ferro metálico e níquel</div><div><b>Velocidade orbital:</b> 19.9 km/s</div><div> </div><div><span src="assets/img/espaco/416_16_psyche_main.jpg" type="image/jpeg" data-filename="416_16_psyche_main.jpg" style="cursor: default;cursor: default;"/></div><br/></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1446"/></span>
<h1>Eros</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 19:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 20:07</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, NEO, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo S(Rochoso), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/asteroids/433-eros/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/asteroids/433-eros/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 433</div><div><b>Período de rotação:</b> 12 min</div><div><b>Período orbital:</b> 1 ano e 9 meses</div><div><b>Temperatura:</b>  -46 ºC</div><div><b>Distância do Sol:</b> 130.000.000 Milhas - 2.9AU</div><div><b>Diâmetro</b><b>:</b>  16 km</div><div><b>Gravidade:</b> 0.06 m/s²</div><div><b>Composição:</b> Sílica</div><div><b>Velocidade orbital:</b> 24.36 km/s</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Primeiro asteroide a ser visitado por uma sonda</div><div><br/></div><div><span src="assets/img/espaco/454_Eros_main.jpg" type="image/jpeg" data-filename="454_Eros_main.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1448"/></span>
<h1>Ida</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 18:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 20:03</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo S(Rochoso), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/small-bodies/asteroids/243-ida/in-depth/"><i>https://solarsystem.nasa.gov/small-bodies/asteroids/243-ida/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 243</div><div><b>Período de rotação:</b> 4h 36 min</div><div><b>Período orbital:</b> 4 anos e 9 meses</div><div><b>Temperatura:</b>  -73 ºC</div><div><b>Distância do Sol:</b> 260.000.000 Milhas - 5.2AU</div><div><b>Diâmetro</b><b>:</b>  32 km</div><div><b>Gravidade:</b> 0.01 m/s²</div><div><b>Composição:</b> Sílica</div><div><b>Velocidade orbital:</b> 12.4 km/s</div><div> </div><div>----- <b>Curiosidades -----</b></div><div>&gt; Possui 1 satélite(Dactyl)</div><div><br/></div><div><span src="assets/img/espaco/417_Ida_Dactyl_main.jpg" type="image/jpeg" data-filename="417_Ida_Dactyl_main.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1450"/></span>
<h1>Pallas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 17:41</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 20:00</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo B, Tipo C(Carbonáceo), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 2</div><div><b>Período de rotação: </b>18 min</div><div><b>Período orbital:</b> 4 anos e 7 meses</div><div><b>Temperatura:</b>  -109 ºC</div><div><b>Distância do Sol:</b> 269.000.000 Milhas - 2.9AU</div><div><b>Diâmetro</b><b>:</b>  545 km</div><div><b>Gravidade:</b> 0.18 m/s²</div><div><b>Composição:</b> Rocha(Minerais hidratados)</div><div><b>Velocidade orbital:</b> 17.65 km/s</div><div> </div><div>----- <b>Curiosidades -----</b></div><div>&gt; Segundo maior asteroide do cinturão de asteroides</div><div><br/></div><div>// <i>ARTE CONCEITUAL</i></div><div><span src="assets/img/espaco/asteroide-pallas.jpg" type="image/jpeg" data-filename="asteroide-pallas.jpg" style="cursor: default;cursor: default;"/><br/></div><div><br/></div><div><span src="assets/img/espaco/250px-PallasHST2007.jpg" type="image/jpeg" data-filename="250px-PallasHST2007.jpg" style="cursor: default;cursor: default;"/><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1453"/></span>
<h1>Hektor</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>24/05/2018 18:11</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>24/05/2018 18:40</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Cinturão Principal de Asteroides, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Jupiteriano, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo D(Troiano), Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=624"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=624</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Número:</b> 624</div><div><b>Período de rotação:</b> 5h 54 min</div><div><b>Período orbital:</b> 12 anos</div><div><b>Temperatura:</b>  -153 ºC</div><div><b>Distância do Sol:</b> 483.000.000 Milhas - 5.2AU</div><div><b>Diâmetro</b><b>:</b>  225 km</div><div><b>Gravidade:</b> 0.067 m/s²</div><div><b>Composição:</b> ?</div><div><b>Velocidade orbital:</b> 17.65 km/s</div><div> </div><div>----- <b>Curiosidades -----</b></div><div>&gt; Compartilha a órbita de Júpiter</div><div>&gt; Possui 1 satélite(S/2006 (624) 1)</div><div><br/></div><div><span src="assets/img/espaco/offset_453084.jpg" type="image/jpeg" data-filename="offset_453084.jpg" height="1170" style="cursor: default;cursor: default;" width="1500"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1455"/></span>
<h1>Anel Phoebe</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 20:49</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 21:39</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 7.772.240 km</div><div><b>Fronteira externa:</b> 12.488.310 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> 0.04 km</div><div><b>Largura:</b> 5.000.000 km</div><div><b>Percentagem de poeira:</b> ? % (escassa)</div><div><b>Nível:</b> 15</div><br/><div>----- <b>Curiosidades -----</b></div><div>&gt; Último e maior anel de Saturno, porém extremamente tênue </div><div><br/></div><div>// ARTE REPRESENTANDO O ANEL PHOEBE</div><div><span src="assets/img/espaco/800px-Infrared_Ring_Around_Saturn.jpg" type="image/jpeg" data-filename="800px-Infrared_Ring_Around_Saturn.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1457"/></span>
<h1>Anel Pallene</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 19:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 21:38</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Anel E, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 211.000 km</div><div><b>Fronteira externa:</b> 213.500 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 2.500 km</div><div><b>Percentagem de poeira:</b> ? %</div><div><b>Nível:</b> 14(Saturno)/3(E)</div><br/><div>----- <b>Curiosidades -----</b></div><div>&gt; Se encontra dentro do anel E</div><div><br/></div><div>// <i>ARTE CONTENDO OS ANEIS E O ANEL E ONDE SE ENCONTRA PALLENE</i></div><div><span src="assets/img/espaco/720px-Saturn's_Rings_PIA03550.jpg" type="image/jpeg" data-filename="720px-Saturn's_Rings_PIA03550.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1459"/></span>
<h1>Anel Anthe</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 20:44</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 21:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Anel E, Arco, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 197.655 km</div><div><b>Fronteira externa:</b> 212.000 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 15.600 km</div><div><b>Percentagem de poeira:</b> ? % (bastante)</div><div><b>Nível:</b> 13(Saturno)/2(E)</div><br/><div>----- <b>Curiosidades -----</b></div><div>&gt; Se encontra dentro do anel E</div><div><br/></div><div><span src="assets/img/espaco/PIA11101_Anthe_ring_arc.jpg" type="image/jpeg" data-filename="PIA11101_Anthe_ring_arc.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1461"/></span>
<h1>Anel Methone</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 20:39</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 21:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Anel E, Arco, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 194.440 km</div><div><b>Fronteira externa:</b> 197.655 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 3.215 km</div><div><b>Percentagem de poeira:</b> ? % (bastante)</div><div><b>Nível:</b> 12(Saturno)/1(E)</div><br/><div>----- <b>Curiosidades -----</b></div><div>&gt; Se encontra dentro do anel E</div><div><br/></div><div>// <i>ARTE CONTENDO OS ANEIS E O ANEL E ONDE SE ENCONTRA METHONE</i></div><div><span src="assets/img/espaco/720px-Saturn's_Rings_PIA03550 [1].jpg" type="image/jpeg" data-filename="720px-Saturn's_Rings_PIA03550.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1464"/></span>
<h1>Anel E</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 19:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 19:54</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Anel E, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 180.000 km</div><div><b>Fronteira externa:</b> 483.000 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> 2.000 km</div><div><b>Largura:</b> 302.000 km</div><div><b>Percentagem de poeira:</b> ? % (bastante)</div><div><b>Nível:</b> 12</div><br/><div>----- <b>Curiosidades -----</b></div><div>&gt; Composto de gelo dióxido de carbono e amônia</div><div><br/></div><div><span src="assets/img/espaco/440px-E_ring_with_Enceladus.jpg" type="image/jpeg" data-filename="440px-E_ring_with_Enceladus.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1466"/></span>
<h1>Anel G</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 16:22</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 16:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 166.000 km</div><div><b>Fronteira externa:</b> 173.200 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 7.200 km</div><div><b>Percentagem de poeira:</b> ? % (bastante)</div><div><b>Nível:</b> 11</div><br/><div><span src="assets/img/espaco/The_ring_arc_and_a_site_of_concentrated_ring_particles_medium.jpg" type="image/jpeg" data-filename="The_ring_arc_and_a_site_of_concentrated_ring_particles_medium.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1468"/></span>
<h1>Anel Janus/Epimetheus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 16:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 16:20</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 149.000 km</div><div><b>Fronteira externa:</b> 154.000 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 5.000 km</div><div><b>Percentagem de poeira:</b> ? % (media)</div><div><b>Nível:</b> 10</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Anel mais oculto já observado</div><div><br/></div><div><span src="assets/img/espaco/3277_PIA08322.jpg" type="image/jpeg" data-filename="3277_PIA08322.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1470"/></span>
<h1>Anel F</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 16:06</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 16:16</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 139.826 km</div><div><b>Fronteira externa:</b> 140.612 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 30~500 km</div><div><b>Percentagem de poeira:</b> ? % (10)</div><div><b>Nível:</b> 9</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Mais estreito dos anéis de Saturno</div><div><br/></div><div><span src="assets/img/espaco/PIA01387.jpg" type="image/jpeg" data-filename="PIA01387.jpg" height="1020" style="cursor: default;cursor: default;" width="1020"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1472"/></span>
<h1>Anel A</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 15:18</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 16:04</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 122.340 km</div><div><b>Fronteira externa:</b> 136.768 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> 0.02 km</div><div><b>Largura:</b> 14.600 km</div><div><b>Percentagem de poeira:</b> ? % (70)</div><div><b>Nível:</b> 8</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Mais externo dos grandes anéis de Saturno</div><div><br/></div><span src="assets/img/espaco/PIA01988.jpg" type="image/jpeg" data-filename="PIA01988.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1474"/></span>
<h1>Arco Hyugens</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 14:15</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 14:33</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Arco, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 117.825 km</div><div><b>Fronteira externa:</b> 118.185 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 285~440 km</div><div><b>Percentagem de poeira:</b> ? % (10)</div><div><b>Nível:</b> 7</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Pequeno anel dentro da Divisão de Cassini</div><div><br/></div><div>// <i>AMPLIAÇÃO DA DIVISÃO DE CASSINI COM O ARCO HYUGENS</i></div><div><span src="assets/img/espaco/c4398946.gif" type="image/gif" data-filename="c4398946.gif" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1476"/></span>
<h1>Anel B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 13:46</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 13:55</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 91.975 km</div><div><b>Fronteira externa:</b> 117.507 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> 0.015 km</div><div><b>Largura:</b> 25.500 km</div><div><b>Percentagem de poeira:</b> ? % (40)</div><div><b>Nível:</b> 6</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Mais brilhante e massivo dos anéis do sistema solar</div><div><br/></div><div><span src="assets/img/espaco/PIA01380.jpg" type="image/jpeg" data-filename="PIA01380.jpg" height="724" style="cursor: default;" width="936"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1478"/></span>
<h1>Arco 1.495</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 12:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 12:28</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel C, Arco, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 90.171 km</div><div><b>Fronteira externa:</b> 90.232 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km </div><div><b>Largura:</b> 61 km</div><div><b>Percentagem de poeira:</b> ? %</div><div><b>Nível:</b> 5(Saturno)/4(C)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Compõe o anel C</div><div><br/></div><div><i>// ARCOS QUE COMPÕE O ANEL C</i></div><div><span src="assets/img/espaco/601px-PIA06540_Outer_C_Ring.jpg" type="image/jpeg" data-filename="601px-PIA06540_Outer_C_Ring.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1480"/></span>
<h1>Arco 1.470 Rs</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 12:09</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 12:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel C, Arco, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 88.716 km</div><div><b>Fronteira externa:</b> 88.732 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km </div><div><b>Largura:</b> 16 km</div><div><b>Percentagem de poeira:</b> ? %</div><div><b>Nível:</b> 4(Saturno)/3(C)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Compõe o anel C</div><div><br/></div><div><i>// ARCOS QUE COMPÕE O ANEL C</i></div><div><span src="assets/img/espaco/601px-PIA06540_Outer_C_Ring [1].jpg" type="image/jpeg" data-filename="601px-PIA06540_Outer_C_Ring.jpg" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1482"/></span>
<h1>Arco Titan</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 11:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 12:11</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel C, Arco, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 77.871 km</div><div><b>Fronteira externa:</b> 77.896 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km </div><div><b>Largura:</b> 25 km</div><div><b>Percentagem de poeira:</b> ? %</div><div><b>Nível:</b> 2(Saturno)/1(C)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Compõe o anel C</div><div><br/></div><div><i>// ARCOS QUE COMPÕE O ANEL C</i></div><div><span src="assets/img/espaco/601px-PIA06540_Outer_C_Ring [2].jpg" type="image/jpeg" data-filename="601px-PIA06540_Outer_C_Ring.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1484"/></span>
<h1>Arco Maxwell</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 12:06</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 12:11</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel C, Arco, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 87.491 km</div><div><b>Fronteira externa:</b> 87.555 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km </div><div><b>Largura:</b> 270 km</div><div><b>Percentagem de poeira:</b> ? %</div><div><b>Nível:</b> 3(Saturno)/2(C)</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Compõe o anel C</div><div><br/></div><div><i>// ARCOS QUE COMPÕE O ANEL C</i></div><div><span src="assets/img/espaco/601px-PIA06540_Outer_C_Ring [3].jpg" type="image/jpeg" data-filename="601px-PIA06540_Outer_C_Ring.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1486"/></span>
<h1>Anel C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 11:29</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 11:49</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Anel C, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 74.658 km</div><div><b>Fronteira externa:</b> 91.975 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 17.500 km</div><div><b>Percentagem de poeira:</b> 5 %</div><div><b>Nível:</b> 2</div><div><br/></div><div>----- <b>Curiosidades -----</b></div><div>&gt; Principalmente composto de gelo</div><div><br/></div><div><span src="assets/img/espaco/Image [61].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1488"/></span>
<h1>Anel D</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>17/05/2018 11:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>17/05/2018 11:43</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anel, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Saturno, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://pds-rings.seti.org/saturn/saturn_tables.html"><i>https://pds-rings.seti.org/saturn/saturn_tables.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Limite Interno:</b> 66.900 km</div><div><b>Fronteira externa:</b> 74.568 km</div><div><b>Planeta:</b> Saturno</div><div><b>Espessura:</b> ? km</div><div><b>Largura:</b> 7.500 km</div><div><b>Percentagem de poeira:</b> ? % </div><div><b>Nível:</b> 1</div><div><br/></div><div><span src="assets/img/espaco/PIA01388.jpg" type="image/jpeg" data-filename="PIA01388.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1491"/></span>
<h1>Regor</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 22:08</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:37</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Vel OB 2, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar O, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Milhões de Anos, Sistema Gamma Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+68273"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+68273</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">O7.5III</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Vela</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 17 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 28,5 <sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 48.000 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.322<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 840 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  -1,87</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> - 5,63</span></div><div><span style="color: rgb(34, 34, 34);"><b>Idade:</b> 4,5 Milhões de anos</span></div><div><span style="color: rgb(34, 34, 34);"><b>Velocidade de Rotação:</b> ? km/s</span></div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/binary-star-system-4.png" type="image/png" data-filename="binary-star-system-4.png"/></div><div><br/></div><div><span src="assets/img/espaco/gamma-velorum [3].jpg" type="image/jpeg" data-filename="gamma-velorum.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/544c7da1e8c724205f95b9a1547ddc2e [5].jpg" type="image/jpeg" data-filename="544c7da1e8c724205f95b9a1547ddc2e.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [9].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1496"/></span>
<h1>Naos</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 17:35</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:36</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar O, Complexo de Superaglomerados Pisces-Cetus, Constelação da Puppis, Estrela, Estrela Única, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, ζ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/naos"><i>https://www.universeguide.com/star/naos</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="font-family: &quot;Times New Roman&quot;;">O5IAf</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Puppis</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 17 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 56,1<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,6 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 48.000 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ? anos</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.419<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 1.080 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,25</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> - 6,23</span></div><div><span style="font-weight: bold;">Idade:</span> ~ <span>3,2</span> Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 220 km/s</div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e Hélio</div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/220px-Zeta_Puppis.png" type="image/png" data-filename="220px-Zeta_Puppis.png"/></div><div><br/></div><div><span src="assets/img/espaco/9abe2940e97442b04dcbca12242f5d0f.png" type="image/png" data-filename="9abe2940e97442b04dcbca12242f5d0f.png" style="cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/240px-Puppis_constellation_map.svg.png" type="image/png" data-filename="240px-Puppis_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1500"/></span>
<h1>Canopus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 11:36</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:34</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca-Amarela, Classificação Estelar A, Classificação Estelar F, Complexo de Superaglomerados Pisces-Cetus, Constelação da Carina, Estrela, Estrela Única, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astronomytrek.com/the-night-skys-2nd-brightest-star-canopus/"><i>http://www.astronomytrek.com/the-night-skys-2nd-brightest-star-canopus/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="font-family: sans-serif;">A9III | F0lb</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Carina</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 71 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 8 M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,64 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 6.998 K</div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>24.181<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 310 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  -0,74</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> - 5,34</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 8 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio, <span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">hélio e metais ionizados</span></div><div><span style="font-size: 14px;"><br/></span></div><div><span style="font-size: 14px;"><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></span></div><div><span src="assets/img/espaco/v3D-AyFD9b1wGIOI-5BrOXF2kpT4hysbK8spZINqD816rG2LkCqywrHD2SHIJ_tsA-AHpjiA_jAo3Zuqa1iwV0Tad23aJVhopWCdSp8bwtH2CRml6u7-1R7OfWxyzjC95EFkqVEx3AopCDoSS19w_g.png" type="image/png" data-filename="v3D-AyFD9b1wGIOI-5BrOXF2kpT4hysbK8spZINqD816rG2LkCqywrHD2SHIJ_tsA-AHpjiA_jAo3Zuqa1iwV0Tad23aJVhopWCdSp8bwtH2CRml6u7-1R7OfWxyzjC95EFkqVEx3AopCDoSS19w_g.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/canopusChile-1-Wide-Fieldwwwsloohcom.png" type="image/png" data-filename="canopusChile-1-Wide-Fieldwwwsloohcom.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/250px-Carina_constellation_map.png" type="image/png" data-filename="250px-Carina_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1504"/></span>
<h1>Tazared</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>26/01/2019 00:54</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:33</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação da Águia, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.constellationsofwords.com/stars/Tarazed.html"><i>http://www.constellationsofwords.com/stars/Tarazed.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">3 II</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Águia</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 95 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 5,66<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,63 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.210 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 395 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,72</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,38     </span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 8 km/s</div><div><span style="font-weight: bold;">Composição:</span> H<span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">él</span><span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">io e carbono</span></div><div><span style="font-size: 14px;"><br/></span></div><div><span src="assets/img/espaco/gammaaquilae_20180515_040445_0_tnmqlc_m.png" type="image/png" data-filename="gammaaquilae_20180515_040445_0_tnmqlc_m.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1119"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/240px-Aquila_constellation_map.svg.png" type="image/png" data-filename="240px-Aquila_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1507"/></span>
<h1>Altair</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/01/2019 20:21</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:33</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Bilhões de Anos, Bolha Local, Braço de Órion, Branca, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação da Águia, Estrela, Estrela Única, Grupo Local, Laniakea, Nuvem G, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, α</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.constellation-guide.com/altair/"><i>https://www.constellation-guide.com/altair/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(248, 249, 250); font-size: 12.32px; font-family: sans-serif;">A7 V</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Águia</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 1,631 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 2<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,29 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 7.200 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 16.73 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  0,76</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 2,22</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 1,2 Bilhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 240 km/s</div><div><span style="font-weight: bold;">Composição:</span> L<span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">inhas intensas de hidrogênio e linhas de metais ionizados</span></div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Alta rotação faz com que seja achatada nos polos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/alfa-centauri.jpg" type="image/jpeg" data-filename="alfa-centauri.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/230px-Altair.jpg" type="image/jpeg" data-filename="230px-Altair.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//COMPARAÇÃO COM O SOL</span></div><div><span src="assets/img/espaco/Altair-Sun_comparison.png" type="image/png" data-filename="Altair-Sun_comparison.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO FORMA DA ESTRELA</span></div><div><span src="assets/img/espaco/230px-Altair_PR_image6_(white).jpg" type="image/jpeg" data-filename="230px-Altair_PR_image6_(white).jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/240px-Aquila_constellation_map.svg [1].png" type="image/png" data-filename="240px-Aquila_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1513"/></span>
<h1>Paikauhale</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 17:08</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:31</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Ass US, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar OB, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, τ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alniyat-t.html"><i>http://stars.astro.illinois.edu/sow/alniyat-t.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>BOV</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6,5 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 15<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,24 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 30.000 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 470 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,82</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,78</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 5,7 Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 24 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/scorpio_guisard_1328.jpg" type="image/jpeg" data-filename="scorpio_guisard_1328.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1515"/></span>
<h1>Shaula Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 12:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Branca, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Lambda Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, λ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/shaula"><i>https://www.universeguide.com/star/shaula</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> (A)</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> ?<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 365 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  12</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,70</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/descarga.jpg" type="image/jpeg" data-filename="descarga.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1517"/></span>
<h1>Shaula A</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 11:54</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Milhões de Anos, Sistema Lambda Scorpii, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, λ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/shaula.html"><i>http://stars.astro.illinois.edu/sow/shaula.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">B2IV</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6,2 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 10,6<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,9 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 25.000 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 365 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,62</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,70</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 10-13 Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 150 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hélio e Hidrogênio</div><div><br/></div><div><span src="assets/img/espaco/9487e714b91f752ca8dde23ae4d104a2_400x400.jpg" type="image/jpeg" data-filename="9487e714b91f752ca8dde23ae4d104a2_400x400.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/scorpio_guisard_1328 [1].jpg" type="image/jpeg" data-filename="scorpio_guisard_1328.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1520"/></span>
<h1>Xamidimura</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 18:36</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:29</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Braço de Órion, Classificação Estelar OB, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Mu¹ Scorpii, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, μ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+151890"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+151890</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B1.5V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 4 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 8.49<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ?</div><div><span style="font-weight: bold;">Temperatura:</span> 23.725 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 500 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,0</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,0</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/Mu-Scorpii.png" type="image/png" data-filename="Mu-Scorpii.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1522"/></span>
<h1>μ¹ Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 11:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:29</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Mu¹ Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, μ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/muscorpii"><i>https://www.universeguide.com/star/muscorpii</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B6.5 V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 4,38 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 5,33<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície: </span>? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> <span style="color: rgb(34, 34, 34);">16.850</span> K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 590 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,85</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/Mu-Scorpii [1].png" type="image/png" data-filename="Mu-Scorpii.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1524"/></span>
<h1>Girtab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 15:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul-Branca, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Kappa Scorpii, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, κ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=kap+Sco"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=kap+Sco</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B1.5ll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6.8 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 17<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4,01 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 23.400 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 480 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,39</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,46</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 25 Milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 105 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/117268382_o.png" type="image/png" data-filename="117268382_o.png" style="cursor: default;cursor: default;cursor: default;" width="1019"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1527"/></span>
<h1>γ Velorum D</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 10:44</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>30/01/2019 11:16</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Vel OB 2, Braço de Órion, Branca, Cinturão de Gould, Classificação Estelar A, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Estrela, Grupo Local, Laniakea, Sistema Gamma Velorum, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, γ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astronomytrek.com/star-constellation-facts-vela/"><i>http://www.astronomytrek.com/star-constellation-facts-vela/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> <span style="font-family: 'Times New Roman'; text-align: center;">A0</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Vela</span></div><b>Raio equatorial:</b> ? R<sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b>Distância do Centro Galático:  </b>24.322<font color="#222222"> anos-luz</font></div><div><font color="#222222"><b>Distância da Terra:</b> 840 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  9,4</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição: </b>Hidrogênio e metais ionizados</div><div><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);"><br/></span></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><span src="assets/img/espaco/gammavel_airy [2].png" type="image/png" data-filename="gammavel_airy.png" style="cursor: default;"/></div><div><font color="#222222" face="sans-serif"><span style="font-size: 14px;"><br/></span></font></div><div><span src="assets/img/espaco/544c7da1e8c724205f95b9a1547ddc2e [6].jpg" type="image/jpeg" data-filename="544c7da1e8c724205f95b9a1547ddc2e.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA ESTRELA</i></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [10].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1531"/></span>
<h1>Constelação da Vela</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 18:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 21:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação da Vela, Hemisfério Sul, NGQ4</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Vela, o Velame
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Velorum</font></div><div><b>Abreviação:</b> Vel</div><div><b>Área Total:</b> 500º quadrados</div><div><b>Quadrante:</b> NGQ4 </div><div style="text-align: left;"><b>Estrelas Principais:</b> 5</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 50</div><div style="text-align: left;"><b>Meridiano:</b> Março</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/vela_desktop_1.5x.jpg" type="image/jpeg" data-filename="vela_desktop_1.5x.jpg" height="485" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1452"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania32 [1].jpg" type="image/jpeg" data-filename="urania32.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/602px-Vela_IAU.svg.png" type="image/png" data-filename="602px-Vela_IAU.svg.png" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1535"/></span>
<h1>Associação Vel OB 2</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 20:38</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 20:57</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass tipo OB, Ass Vel OB 2, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://academic.oup.com/mnras/article/393/2/538/981832"><i>https://academic.oup.com/mnras/article/393/2/538/981832</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Vela</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> ~ 1.300 anos-luz</div><div><b>Idade:</b> 20 milhões de anos</div><div><b>Estrelas:</b> ~ 100 estrelas tipo inicial</div><div><br/>
----- <b>Curiosidades -----</b></div><div>&gt; Associação de jovem estrelas massivas mais perto</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [62].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1537"/></span>
<h1>Constelação da Puppis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 17:00</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 20:32</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação da Puppis, Hemisfério Sul, SGQ3</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Puppis, a Popa
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Puppis</font></div><div><b>Abreviação:</b> Pup</div><div><b>Área Total:</b> 673º quadrados</div><div><b>Quadrante:</b> SGQ3   </div><div style="text-align: left;"><b>Estrelas Principais:</b> 9</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 76</div><div style="text-align: left;"><b>Meridiano:</b> Fevereiro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/PuppisCC.jpg" type="image/jpeg" data-filename="PuppisCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania32 [2].jpg" type="image/jpeg" data-filename="urania32.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/476px-Puppis_IAU.svg.png" type="image/png" data-filename="476px-Puppis_IAU.svg.png" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1541"/></span>
<h1>Constelação do Cão Maior</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 21:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 17:34</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação do Cão Maior, Hemisfério Sul, SGQ3</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Canis Major, o Cão Maior
<div><b>Genitivo:</b> Canis Maioris</div><div><b>Abreviação:</b> CMa</div><div><b>Área Total:</b> 380º quadrados</div><div><b>Quadrante:</b> SGQ3</div><div style="text-align: left;"><b>Estrelas Principais:</b> 8</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 32</div><div style="text-align: left;"><b>Meridiano:</b> Fevereiro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CanisMajorCC.jpg" type="image/jpeg" data-filename="CanisMajorCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/602px-Canis_Major_IAU.svg.png" type="image/png" data-filename="602px-Canis_Major_IAU.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Canis_Major,_Lepus,_Columba_Noachi_&_Cela_Sculptoris.jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Canis_Major,_Lepus,_Columba_Noachi_&_Cela_Sculptoris.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1545"/></span>
<h1>β Orionis D</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 14:42</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 15:18</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Órion, Estrela, Grupo Local, Laniakea, Sistema Beta Orionis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/rigel.html"><i>http://stars.astro.illinois.edu/sow/rigel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> (k)<span style="background-color: rgb(248, 249, 250); font-family: sans-serif; font-size: 12.32px; white-space: nowrap;">      </span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 3,84<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><font color="#222222"><b>Distância da Terra:</b> 860 anos luz</font></div><div><font color="#222222"><b>Distância do Centro Galático:</b> 24.814 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  15</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> ?</div><div><br/></div><div><span src="assets/img/espaco/rigel-is-the-brightest-star-in-the-constellation-orion_u-l-peryw90.jpg" type="image/jpeg" data-filename="rigel-is-the-brightest-star-in-the-constellation-orion_u-l-peryw90.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/> <br/></div><div><br/></div><div><i>//LOCALIZAÇÃO NO MAPA</i></div><div><span src="assets/img/espaco/Orion_constellation_map [10].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1548"/></span>
<h1>β Orionis C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 14:02</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 15:00</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Órion, Estrela, Grupo Local, Laniakea, Sistema Beta Orionis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://jsmastronomy.30143.n7.nabble.com/Rigel-Beta-Orion-td1019.html"><i>http://jsmastronomy.30143.n7.nabble.com/Rigel-Beta-Orion-td1019.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação: </b><span style="background-color: rgb(248, 249, 250); font-family: sans-serif; font-size: 12.32px;">B9V</span><span style="background-color: rgb(248, 249, 250); font-family: sans-serif; font-size: 12.32px; white-space: nowrap;">      </span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Órion</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 2,94<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><font color="#222222"><b>Distância da Terra:</b> 860 anos luz</font></div><div><font color="#222222"><b>Distância do Centro Galático:</b> 24.814 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  7,6</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div>// REPRESENTAÇÃO GRÁFICA</div><div><span src="assets/img/espaco/Stelle e Costellazioni - Rigel.jpg" type="image/jpeg" data-filename="Stelle e Costellazioni - Rigel.jpg"/><br/></div><div><br/></div><div>// REPRESENTAÇÃO GRÁFICA</div><div><span src="assets/img/espaco/binary_starsystems_rigel.jpg" type="image/jpeg" data-filename="binary_starsystems_rigel.jpg"/></div><div><br/></div><div><span src="assets/img/espaco/Rigel_-Fresno-State_s-Campus-Observatory-400x299.jpg" type="image/jpeg" data-filename="Rigel_-Fresno-State_s-Campus-Observatory-400x299.jpg"/></div><div><br/></div><div><i>//LOCALIZAÇÃO NO MAPA</i></div><div><span src="assets/img/espaco/Orion_constellation_map [11].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1553"/></span>
<h1>β Orionis B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 13:03</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 14:57</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Órion, Estrela, Grupo Local, Laniakea, Sistema Beta Orionis, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/rigel.html"><i>http://stars.astro.illinois.edu/sow/rigel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação: </b><span style="background-color: rgb(248, 249, 250); font-family: sans-serif; font-size: 12.32px;">B9V</span><span style="background-color: rgb(248, 249, 250); font-family: sans-serif; font-size: 12.32px; white-space: nowrap;">      </span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Órion</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div style="text-align: left;"><b>Massa:</b> 3,84<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><font color="#222222"><b>Distância da Terra:</b> 860 anos luz</font></div><div><font color="#222222"><b>Distância do Centro Galático:</b> 24.814 anos luz</font></div><div><font color="#222222"><b>Magnitude Aparente:</b>  7,6</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> ?</span></div><div><b>Composição:</b> Hélio e Hidrogênio</div><div><br/></div><div><br/></div><div>// REPRESENTAÇÃO GRÁFICA</div><div><span src="assets/img/espaco/image.png" type="image/png" data-filename="image.png" height="625" style="cursor: default;cursor: default;" width="1000"/><br/></div><div><br/></div><div><span src="assets/img/espaco/Rigel_-Fresno-State_s-Campus-Observatory-400x299 [1].jpg" type="image/jpeg" data-filename="Rigel_-Fresno-State_s-Campus-Observatory-400x299.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>// REPRESENTAÇÃO GRÁFICA</div><div><span src="assets/img/espaco/binary_starsystems_rigel [1].jpg" type="image/jpeg" data-filename="binary_starsystems_rigel.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div><br/></div><div><i>//LOCALIZAÇÃO NO MAPA</i></div><div><span src="assets/img/espaco/Orion_constellation_map [12].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1558"/></span>
<h1>Cinturão de Órion(Três Marias)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 10:22</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>29/01/2019 12:08</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Asterismo, Cinturão de Órion(Três Marias), Constelação de Órion</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Nome:</b> Cinturão de Órion | As Três Marias | Os Reis Magos</div><b>Astros Relacionados:</b> Alnitak, Alnilam e Mintaka [Constelação de Órion] 
<div><b>Descrição:</b> As três estrelas alinhadas da constelação de Órion formam seu cinto, e também chamado de 3 Marias</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Orion_Belt.jpg" type="image/jpeg" data-filename="Orion_Belt.jpg" height="814" style="cursor: default;cursor: default;cursor: default;" width="1024"/></div><div><br/></div><div><span src="assets/img/espaco/Orion_constellation_map [13].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1561"/></span>
<h1>Nuvem de Perseu</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 09:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/01/2019 10:07</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Perseu, Grupo Local, Laniakea, Nuvem de Perseu, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://adsabs.harvard.edu/abs/2008hsf1.book..308B"><i>http://adsabs.harvard.edu/abs/2008hsf1.book..308B</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Distancia:</b> 600 anoz-luz</div><div><b>Massa:</b> 10.000 <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Diâmetro:</b> 50 anos-luz</div><div><br/></div><div><span src="assets/img/espaco/Image [63].png" type="image/png" data-filename="Image.png" height="773" style="cursor: default;" width="1585"/></div><div><br/></div><div><i>//LOCALIZAÇÃO DA NUVEM DE PERSEU</i></div><span src="assets/img/espaco/723px-PerseusCloudMap.png" type="image/png" data-filename="723px-PerseusCloudMap.png" style="cursor: default;"/><font color="#010101" face="Lucida, Tahoma, serif, arial" size="5"><span style="white-space: nowrap;"><b><br/></b></span></font></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1564"/></span>
<h1>Associação Per OB 2</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 21:55</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>28/01/2019 10:01</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Per OB 2, Ass tipo OB, Associação Estelar, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Perseu, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://iopscience.iop.org/article/10.1088/0004-6256/150/3/95"><i>https://iopscience.iop.org/article/10.1088/0004-6256/150/3/95</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Constelações:</b> Perseu</div><div><b>Classificação Estelar:</b> O e B</div><div><b>Distância do Sol:</b> 978 anos-luz</div><div><b>Idade:</b> 6 milhões de anos</div><div><b>Estrelas:</b> Mais de Mil(massas menores) e ~12(OB)</div><div><br/></div><div><span src="assets/img/espaco/Image [64].png" type="image/png" data-filename="Image.png" style="cursor: default;cursor: default;"/></div><div><br/></div><span src="assets/img/espaco/aj518026f4_lr.jpg" type="image/jpeg" data-filename="aj518026f4_lr.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/><div><br/></div><div><i>//DISTRIBUIÇÃO ESPACIAL</i></div><div><span src="assets/img/espaco/aj518026f9_lr.jpg" type="image/jpeg" data-filename="aj518026f9_lr.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1568"/></span>
<h1>Constelação de Lacerta</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 17:24</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>27/01/2019 17:32</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Lacerta, Hemisfério Norte, NGQ4</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Lacerta, o Lagarto
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Lacertae</font></div><div><b>Abreviação:</b> Lac</div><div><b>Área Total:</b> 201º quadrados</div><div><b>Quadrante:</b> NGQ4</div><div style="text-align: left;"><b>Estrelas Principais:</b> 5</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 17</div><div style="text-align: left;"><b>Meridiano:</b> Outubro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/LacertaCC.jpg" type="image/jpeg" data-filename="LacertaCC.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Lacerta,_Cygnus,_Lyra,_Vulpecula_and_Anser [1].jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Lacerta,_Cygnus,_Lyra,_Vulpecula_and_Anser.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/800px-Lacerta_IAU.svg.png" type="image/png" data-filename="800px-Lacerta_IAU.svg.png"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1572"/></span>
<h1>Constelação de Cepheus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 15:57</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>27/01/2019 16:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Cepheus, Hemisfério Norte, NGQ2</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Cepheus, O Rei
<div><b>Genitivo:</b><font color="#222222" face="sans-serif" size="2"><b> </b>Cephei</font></div><div><b>Abreviação:</b> Cep</div><div><b>Área Total:</b> 588º quadrados</div><div><b>Quadrante:</b> NGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 7</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 43</div><div style="text-align: left;"><b>Meridiano:</b> Outubro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CepheusCC.jpg" type="image/jpeg" data-filename="CepheusCC.jpg"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania04.jpg" type="image/jpeg" data-filename="urania04.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CEP.gif" type="image/gif" data-filename="CEP.gif"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1577"/></span>
<h1>κ Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 10:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Milhões de Anos, Sistema Kappa Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, κ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/girtab"><i>https://www.universeguide.com/star/girtab</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>?</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 5,8 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 12<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 4 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 18.000 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 480 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,39</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,46</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/download [15].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1579"/></span>
<h1>θ Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 10:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Theta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, θ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://wiki.travellerrpg.com/Sargas_(star)"><i>http://wiki.travellerrpg.com/Sargas_(star)</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> ?</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> ?<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 270 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  5,36</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,71</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/download [16].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1581"/></span>
<h1>Sargas</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 12:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca-Amarela, Classificação Estelar F, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Gigante, Grupo Local, Laniakea, Sistema Theta Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, θ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/girtab.html"><i>http://stars.astro.illinois.edu/sow/girtab.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">F1II</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 26 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 5<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,4 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 7.268 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 270 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  1,84</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,71</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 125 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e metais ionizados</div><div><br/></div><div><span src="assets/img/espaco/ngc6388.jpg" type="image/jpeg" data-filename="ngc6388.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1583"/></span>
<h1>η Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/07/2018 23:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:22</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bilhões de Anos, Braço de Órion, Branca-Amarela, Classificação Estelar F, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Grupo Local, Laniakea, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, η</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=eta+Sco"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=eta+Sco</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> F5 IV</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 3.76 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 1.75<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,1 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 6.519 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 73.5 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,33</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 1,58</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 1,1 milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 150 km/s</div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e metais ionizados</div><div><br/></div><div><span src="assets/img/espaco/Eta_Scorpii.jpg" type="image/jpeg" data-filename="Eta_Scorpii.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [13].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1586"/></span>
<h1>ζ² Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/07/2018 22:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:19</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bilhões de Anos, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ζ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=zet02+Sco"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=zet02+Sco</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> K4 lll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 21 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 1.19<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 1,84 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.169 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 132 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,59</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> 0,3</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 5,8 Bilhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 2,3 km/s</div><div><span style="font-weight: bold;">Composição:</span>Metais neutros e hidrogênio </div><div><br/></div><div><span src="assets/img/espaco/Zeta-Scorpii.jpg" type="image/jpeg" data-filename="Zeta-Scorpii.jpg" style="cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [14].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1589"/></span>
<h1>ι1 Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 18:15</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Branca-Amarela, Classificação Estelar F, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Grupo Local, Laniakea, Milhões de Anos, Subgrupo Via Láctea, Superaglomerado de Virgem, Supergigante, Via Láctea, ι</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/iota1sco.html"><i>http://stars.astro.illinois.edu/sow/iota1sco.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> F2lae</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 212 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 12.1<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 0,75 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 7.000 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 1.900 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,03</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -5,85</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 17 milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e metais ionizados</div><div><br/></div><div><span src="assets/img/espaco/Iota1-2_Scorpii.jpg" type="image/jpeg" data-filename="Iota1-2_Scorpii.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [15].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1592"/></span>
<h1>σ Scorpii Aa2</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/07/2018 14:08</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Gigante, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Sigma Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, σ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alniyat-s.html"><i>http://stars.astro.illinois.edu/sow/alniyat-s.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B1V</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 11 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 11,9<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 470 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  8,7</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,78</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/Sh2-9.jpg" type="image/jpeg" data-filename="Sh2-9.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1594"/></span>
<h1>σ Scorpii Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/07/2018 14:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:09</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Sigma Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, σ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alniyat-s.html"><i>http://stars.astro.illinois.edu/sow/alniyat-s.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> ? M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> ? K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 470 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  ?</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,78</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/Sh2-9 [1].jpg" type="image/jpeg" data-filename="Sh2-9.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1596"/></span>
<h1>Al Niyat(σ)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 17:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:08</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Gigante, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Sigma Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, σ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alniyat-s.html"><i>http://stars.astro.illinois.edu/sow/alniyat-s.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B1lll</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 12 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 18<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,85 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 26.150 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 568 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,88</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -4,5</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 8-10 milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 25 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/alniyat.jpg" type="image/jpeg" data-filename="alniyat.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [16].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1599"/></span>
<h1>Lesath</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 16:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:06</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Grupo Local, Laniakea, Milhões de Anos, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, υ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=ups+Sco"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=ups+Sco</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B2lV</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6,1 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 11<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ?</div><div><span style="font-weight: bold;">Temperatura:</span> 22.831 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 570 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,7</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,53</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 20 milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/117281517_o.png" type="image/png" data-filename="117281517_o.png" style="cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [17].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1602"/></span>
<h1>Pipirima</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 13:30</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:04</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass LCC, Azul-Branca, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, μ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/mu2scorpii"><i>https://www.universeguide.com/star/mu2scorpii</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div style="text-align: left;"><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> B2 IV</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 7 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;">☉</sub></div><div style="text-align: left;"><span style="font-weight: bold;">Massa:</span> 8,7<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; font-size: 11px; color: rgb(58, 58, 58); line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249);">☉</sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 23.133 K</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 474 anos luz</span></div><div><span style="font-weight: bold;">Distância do Centro Galático:  </span>23.676<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  3,56</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -2,25</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 18 milões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 58 km/s</div><div><span style="font-weight: bold;">Composição: </span>Hidrogênio e hélio</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/113125745.eIprV0xH.jpg" type="image/jpeg" data-filename="113125745.eIprV0xH.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/Scorpius_constellation_map [10].png" type="image/png" data-filename="Scorpius_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1605"/></span>
<h1>δ Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 10:42</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:03</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Milhões de Anos, Sistema Delta Scorpii, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://stardate.org/radio/program/delta-scorpii"><i>https://stardate.org/radio/program/delta-scorpii</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> ?</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> ? R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 8,2 M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> ? cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 22.000 K</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 270 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,3 </span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,8</span></div><div><span style="font-weight: bold;">Idade:</span> ~ 9-10 milhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> ? km/s</div><div><span style="font-weight: bold;">Composição:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/Sh2-7.jpg" type="image/jpeg" data-filename="Sh2-7.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1607"/></span>
<h1>Dschubba</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 15:17</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 12:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Delta Scorpii, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Delta+Scorpii"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=Delta+Scorpii</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação: </span>B0.3IV</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 6.7 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 13<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 3,92 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 27.400 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 400 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,29</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -3,8</span></div><div><span style="font-weight: bold;">Idade:</span> ~ ? de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 180 km/s</div><div><span style="font-weight: bold;">Composição:</span> Hidrogênio, Hélio e Cálcio</div><div><br/></div><div><span src="assets/img/espaco/SS2142767.jpg" type="image/jpeg" data-filename="SS2142767.jpg" style="cursor: default;cursor: default;cursor: default;" width="1200"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [18].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1610"/></span>
<h1>Wei</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>05/07/2018 14:36</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/02/2019 11:58</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bilhões de Anos, Braço de Órion, Classificação Estelar K, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Estrela Única, Gigante, Grupo Local, Laniakea, Laranja, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, ε</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+151680"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD+151680</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Classificação:</span> <span style="background-color: rgb(249, 249, 249); font-size: 12.32px; text-align: left; font-family: sans-serif;">K2IIIb</span></span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><span style="background-color: rgb(249, 249, 249); font-weight: bold;">Constelação:</span> Escorpião</span></div><div><span style="font-weight: bold;">Raio equatorial:</span> 12.6 R<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Massa:</span> 1,24<sup style="box-sizing: border-box; position: relative; vertical-align: baseline; top: -0.5em; text-align: center; font-size: 9px;"><span style="vertical-align: super; color: rgb(58, 58, 58); font-size: 11px; line-height: 0;"> </span></sup>M<sub style="background-image: none; background-color: rgb(249, 249, 249); text-align: left;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="background-image: none; vertical-align: sub; background-color: rgb(249, 249, 249); line-height: 1; color: rgb(11, 0, 128); text-decoration: none;" title="Massa solar">☉</a></sub></div><div><span style="font-weight: bold;">Gravidade da Superfície:</span> 2,49 cgs</div><div><span style="font-weight: bold;">Temperatura:</span> 4.560 K</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">26.000 anos-luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância da Terra:</span><span style="color: rgb(34, 34, 34);"> 65 anos luz</span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Magnitude Aparente:</span><span style="color: rgb(34, 34, 34);">  2,3</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Magnitude Absoluta:</span><span style="color: rgb(34, 34, 34);"> -0,8</span></div><div><span style="font-weight: bold;">Idade:</span> ~4 bilhões de anos</div><div><span style="font-weight: bold;">Velocidade de Rotação:</span> 2.6 km/s</div><div><span style="font-weight: bold;">Composição:</span> Metais neutros e hidrogênio    </div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/117240945_o.png" type="image/png" data-filename="117240945_o.png" style="cursor: default;cursor: default;cursor: default;cursor: default;" width="935"/> </div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map.jpg" type="image/jpeg" data-filename="sco_map.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1614"/></span>
<h1>Nuvem Molecular Órion 1(OMC-1)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 12:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 20:05</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Local, Laniakea, Nebulosa de Órion, NGM Ori A, Nuvem Interestelar, OMC-1, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.sci-news.com/astronomy/alma-star-birth-orion-molecular-cloud-04767.html"><i>http://www.sci-news.com/astronomy/alma-star-birth-orion-molecular-cloud-04767.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Distancia:</span> 1.400 anoz-luz</span></div><div><span style="font-weight: bold;">Massa Total:</span> ? <span style="text-align: left;">M</span><sub style="text-align: left; background-image: none; background-color: rgb(249, 249, 249);">☉</sub></div><div><span style="font-weight: bold;">Massa da Nuvem:</span> ? <span style="text-align: left;">M</span><sub style="text-align: left; background-image: none; background-color: rgb(249, 249, 249);">☉</sub></div><div><span style="font-weight: bold;">Diâmetro:</span> 0,4 anos-luz</div><div><span style="font-weight: bold;">Idade:</span> ? milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [65].png" type="image/png" data-filename="Image.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1616"/></span>
<h1>Nuvem da Serpente</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 12:23</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 19:56</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação do Serpentário(Ophiuchus), Grupo Local, Laniakea, Nuvem da Serpente, Nuvem Interestelar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402656280&Name=NAME%20Serpens%20Cloud&submit=submit"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=%402656280&amp;Name=NAME%20Serpens%20Cloud&amp;submit=submit</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Distancia:</b> 1.370 anoz-luz</div><div><b>Massa Total:</b> ? <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Massa da Nuvem:</b> ? <span style="text-align: left;">M</span><sub style="text-align: left; color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Diâmetro:</b> 65 anos-luz</div><div><b>Idade:</b> 8 milhões de anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [66].png" type="image/png" data-filename="Image.png" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1618"/></span>
<h1>Espada Inferior</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>07/02/2019 17:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 19:08</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>maicksantos05@hotmail.com</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado Aberto, Aglomerado de Estrelas, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Local, Laniakea, NGM Ori A, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NGC+1980&jsessionid=F7C399270C70455F0DA1D93FC8B8C9AB"><i>http://simbad.u-strasbg.fr/simbad/sim-id?Ident=NGC+1980&amp;jsessionid=F7C399270C70455F0DA1D93FC8B8C9AB</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">ID:</span> NGC 1980</div><div><span style="font-weight: bold;">Constelações:</span> Órion</div><div><span style="font-weight: bold;">Distância:</span> 1.795 anos-luz</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> 2,5</div><div><span style="font-weight: bold;">Idade:</span> 4,7 milhões de anos</div><div><span style="font-weight: bold;">Número estimado de estrelas:</span> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Image [67].png" type="image/png" data-filename="Image.png"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DO AGLOMERADO</span></div><div><span src="assets/img/espaco/Orion_constellation_map [14].png" type="image/png" data-filename="Orion_constellation_map.png"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1621"/></span>
<h1>Espada Superior</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/02/2019 23:10</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 19:08</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>maicksantos05@hotmail.com</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Aglomerado Aberto, Aglomerado de Estrelas, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Òrion, Grupo Local, Laniakea, NGM Ori A, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=NGC+1981&submit=SIMBAD+search"><i>http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=NGC+1981&amp;submit=SIMBAD+search</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">ID:</span> NGC 1981</div><div><span style="font-weight: bold;">Constelações:</span> Órion</div><div><span style="font-weight: bold;">Distância:</span> 1.300 anos-luz</div><div><span style="font-weight: bold;">Magnitude Aparente:</span> 4.2</div><div><span style="font-weight: bold;">Idade:</span> 2-6 milhões de anos</div><div><span style="font-weight: bold;">Número estimado de estrelas:</span> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/NGC_1981.jpg" type="image/jpeg" data-filename="NGC_1981.jpg"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DO AGLOMERADO</span></div><div><span src="assets/img/espaco/Orion_constellation_map [15].png" type="image/png" data-filename="Orion_constellation_map.png"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1624"/></span>
<h1>Sistema Delta Velorum</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 17:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:34</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Grupo Local, Laniakea, Sistema Delta Velorum, Sistema Estelar, Sistema Múltiplo, Sistema Quádruplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/deltavel.html"><i>http://stars.astro.illinois.edu/sow/deltavel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Alsephina, δ Velorum Ab, δ Velorum C e δ Velorum D</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Alsephina</div><div><span style="font-weight: bold;">Distância do disco:</span> ~ 1.850 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="font-weight: bold;"> </span>24.123<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância do Sol:</b> 80,6 anos-luz</span></div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">δ Velorum Ab</span><span style="font-size: 14px; font-weight: bold; color: rgb(34, 34, 34); font-family: sans-serif;">:</span> 45,2 dias [90.61 UA]</div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">δ Velorum C/</span><span style="font-weight: bold;">δ Velorum D -&gt; Alsephina</span><span style="color: rgb(34, 34, 34); font-weight: bold;">:</span><span style="color: rgb(34, 34, 34);"><span style="color: rgb(34, 34, 34); font-weight: bold;"> </span>28.000 anos </span></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="font-weight: bold;">δ Velorum D -&gt; </span><span style="font-weight: bold;">δ Velorum C:</span> 2.000 anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/300px-Del_Vel_Pos.svg.png" type="image/png" data-filename="300px-Del_Vel_Pos.svg.png" style="cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">// LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [11].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1627"/></span>
<h1>Sistema Rho Ophiuchi</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/02/2019 11:05</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:32</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Rho Oph, Constelação do Serpentário(Ophiuchus), Grupo Local, Laniakea, Sistema Estelar, Sistema Múltiplo, Sistema Quíntuplo de Estrelas, Sistema Rho Ophiuchi, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://inspirehep.net/record/1216440/plots"><i>http://inspirehep.net/record/1216440/plots</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> ρ Ophiuchi Aa, ρ Ophiuchi Ab, ρ Ophiuchi C, ρ Ophiuchi D e ρ Ophiuchi E</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> ρ Ophiuchi Aa</div><div><span style="font-weight: bold;">Distância do disco:</span> ~ 19.000 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="font-weight: bold;"> </span>23.796<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><b>Distância do Sol:</b> 360 anos-luz</div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">ρ Ophiuchi Ab</span><span style="font-size: 14px; font-weight: bold; color: rgb(34, 34, 34); font-family: sans-serif;">:</span> <span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">2.400 anos [334 UA]</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">ρ Ophiuchi D -&gt; ρ Ophiuchi E</span><span style="font-weight: bold; color: rgb(34, 34, 34);">:</span><span style="color: rgb(34, 34, 34);">  680 anos</span></div><div><br/></div><div><br/></div><div><span style="color: rgb(34, 34, 34);"><span src="assets/img/espaco/r_Oph_system_c.png" type="image/png" data-filename="r_Oph_system_c.png" style="cursor: default;cursor: default;cursor: default;"/></span></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DO SISTEMA</span></div><div><span src="assets/img/espaco/Ophiuchus_constellation_map.svg [5].png" type="image/png" data-filename="Ophiuchus_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1630"/></span>
<h1>Sistema Capella</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 21:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação do Colcheiro(Auriga), Grupo Local, Laniakea, Sistema Capella, Sistema Estelar, Sistema Múltiplo, Sistema Quádruplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Capella Aa, Capella Ab, Capella H e Capella L</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Capella A</div><div><span style="font-weight: bold;">Distância do disco:</span> ~ 10.000 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="font-weight: bold;"> </span>24.269<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><b>Distância do Sol:</b> 42 anos-luz</div><div><br/></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita Capella Ab</span><span style="font-weight: bold; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;">:</span> <span style="font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">104 dias</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita</span> <span style="font-size: 14px; font-weight: bold; color: rgb(34, 34, 34); font-family: sans-serif;">Capella H/Capella L -&gt; Capella A</span><span style="font-weight: bold; color: rgb(34, 34, 34);">:</span><span style="color: rgb(34, 34, 34);">  400 anos</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Capella-Sun_comparison-Wiki-public-domain_ST [2].jpg" type="image/jpeg" data-filename="Capella-Sun_comparison-Wiki-public-domain_ST.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DO SISTEMA</span></div><div><span src="assets/img/espaco/300px-Auriga_constellation_map.svg [4].png" type="image/png" data-filename="300px-Auriga_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1633"/></span>
<h1>Sistema Alpha Lupi</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 12:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:29</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass UCL, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Lupus, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Alpha Lupi, Sistema Binário de Estrelas, Sistema Estelar, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/kakkab.html"><i>http://stars.astro.illinois.edu/sow/kakkab.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Krakkab e <span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;">α </span>Lupi B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Krakkab</div><div><span style="font-weight: bold;">Distância do disco:</span> ~ 2.800 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="font-weight: bold;"> </span>23.780<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><b>Distância do Sol:</b> 460 anos-luz</div><div><br/></div><div><b>Órbita </b><span style="color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><b>α Lupi B:</b> 7h 6 min</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Alfa_Lupi [2].jpg" type="image/jpeg" data-filename="Alfa_Lupi.jpg" style="cursor: default;"/></div><div><br/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO  DO SISTEMA</span></div><div><span src="assets/img/espaco/250px-Lupus_constellation_map [2].png" type="image/png" data-filename="250px-Lupus_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1636"/></span>
<h1>Sistema Beta Centauri</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>01/02/2019 11:36</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:26</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Grupo Local, Laniakea, Sistema Beta Centauri, Sistema Estelar, Sistema Múltiplo, Sistema Ternário de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/hadar.html"><i>http://stars.astro.illinois.edu/sow/hadar.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Hadar, <span style="font-size: 14px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif;">β Centauri Ab e Beta Centauri B</span></span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Hadar</div><div><span style="font-weight: bold;">Distância do disco:</span> ~ 120 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="font-weight: bold;"> </span>23.878<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância do Sol:</b> 390 anos-luz</span></div><div><br/></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita </span><span style="font-weight: bold; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;">β Centauri Ab: </span><span style="color: rgb(34, 34, 34);">357 dias [5.5 UA]</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="background-color: rgb(255, 255, 255); font-size: 14px;"><span style="background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; font-weight: bold;">β</span></span><span style="font-weight: bold;"> Centauri B</span><span style="font-weight: bold; color: rgb(34, 34, 34);">:</span><span style="color: rgb(34, 34, 34);">  225 anos</span></div><div><br/></div><div><span src="assets/img/espaco/BETACENTAURISISTEMI12 [3].jpg" type="image/jpeg" data-filename="BETACENTAURISISTEMI12.jpg" style="cursor: default;cursor: default;" width="1100"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DO SISTEMA</span></div><div><span src="assets/img/espaco/250px-Centaurus_constellation_map [3].png" type="image/png" data-filename="250px-Centaurus_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1639"/></span>
<h1>Sistema Alpha Centauri</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 09:54</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:24</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação do Centauro, Grupo Local, Laniakea, Nuvem G, Sistema Alpha Centauri, Sistema Estelar, Sistema Múltiplo, Sistema Ternário de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/rigil-kent.html"><i>http://stars.astro.illinois.edu/sow/rigil-kent.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Rigil Kentaurus, Toliman e Próxima Centauri</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Rigil Kentaurus</div><div><span style="font-weight: bold;">Distância do disco:</span> 8.732 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.132 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Sol:</span> 4,37 anos-luz</span></div><div><br/></div><div><span style="color: rgb(34, 34, 34);"><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita Toliman:</span> 79,9 anos [11,2 UA]</span></div><div><span style="color: rgb(34, 34, 34);"><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita Próxima Centauri:</span> 547 anos [8700 UA]</span></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO DE SUA ÓRBITA</span></div><div><span src="assets/img/espaco/Alpha_Centauri_B_orbit.jpg" type="image/jpeg" data-filename="Alpha_Centauri_B_orbit.jpg"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DO SISTEMA</span></div><div><span src="assets/img/espaco/600px-Centaurus_constellation_map.png" type="image/png" data-filename="600px-Centaurus_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1642"/></span>
<h1>Sistema Beta Crucis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 13:12</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:19</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass LCC, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Beta Crucis, Sistema Binário de Estrelas, Sistema Estelar, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/mimosa.html"><i>http://stars.astro.illinois.edu/sow/mimosa.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Mimosa e <span style="font-size: 14px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif;">β </span> Crucis B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Mimosa</div><div><span style="font-weight: bold;">Distância do disco:</span> ~ 9,6 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="font-weight: bold;"> </span>23.950<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><b>Distância do Sol:</b> 280 anos-luz</div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="background-color: rgb(255, 255, 255); font-size: 14px;"><span style="background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; font-weight: bold;">β</span></span><span style="font-weight: bold;"> Crucis B</span><span style="font-weight: bold; color: rgb(34, 34, 34);">:</span><span style="color: rgb(34, 34, 34);"> 5 anos [7 UA]</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/optical-image-of-the-star-mimosa-or-beta-crucis-celestial-image-co [2].jpg" type="image/jpeg" data-filename="optical-image-of-the-star-mimosa-or-beta-crucis-celestial-image-co.jpg"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [11].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1645"/></span>
<h1>Sistema Alpha Crucis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>31/01/2019 11:29</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:17</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Crux(Cruzeiro do Sul), Grupo Local, Laniakea, Sistema Alpha Crucis, Sistema Estelar, Sistema Múltiplo, Sistema Quíntuplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/acrux.html"><i>http://stars.astro.illinois.edu/sow/acrux.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Acrux, α Crucis Ab, α Crucis B, α Crucis Ca e α Crucis Cb</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Acrux</div><div><span style="font-weight: bold;">Distância do disco:</span> pelo menos ~9.610 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> </span>23.976<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><b>Distância do Sol:</b> 320 anos-luz</div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">α Crucis Ab</span><span style="font-weight: bold; color: rgb(34, 34, 34);">:</span><span style="color: rgb(34, 34, 34);"> 75.78 dias [1 UA]</span></div><div><span style="font-weight: bold;">Órbita α Crucis B:</span> 1.300 anos [430]</div><div><span style="font-weight: bold;">Órbita α Crucis Cb -&gt; </span><span style="font-weight: bold;">α Crucis Ca:</span> 29 horas </div><div><span style="font-weight: bold;">Órbita α Crucis Cb/</span><span style="font-weight: bold;">α Crucis Ca -&gt; Acrux:</span> ?</div><div><br/></div><div><span src="assets/img/espaco/Alpha_Crucis [1].png" type="image/png" data-filename="Alpha_Crucis.png" style="cursor: default;cursor: default;" width="1278"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DO SISTEMA</span></div><div><span src="assets/img/espaco/250px-Crux_constellation_map.svg [12].png" type="image/png" data-filename="250px-Crux_constellation_map.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1649"/></span>
<h1>Constelação da Águia</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>25/01/2019 21:19</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>27/01/2019 16:10</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação da Águia, Equatorial, SGQ1</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Aquila, a Águia
<div><b>Genitivo: </b><span style="background-color: rgb(248, 250, 253); color: rgb(34, 34, 34); font-family: sans-serif; font-size: 11.9px;">Aquilae</span></div><div><b>Abreviação:</b> Aql</div><div><b>Área Total:</b> 652º quadrados</div><div><b>Quadrante:</b> SGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 8(10)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 65</div><div style="text-align: left;"><b>Meridiano:</b> Agosto</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/aquilacc-resp320-2.jpg" type="image/jpeg" data-filename="aquilacc-resp320-2.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/800px-Aquila_IAU.svg.png" type="image/png" data-filename="800px-Aquila_IAU.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania13.jpg" type="image/jpeg" data-filename="urania13.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/eagleform.jpg" type="image/jpeg" data-filename="eagleform.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/block.jpg" type="image/jpeg" data-filename="block.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1655"/></span>
<h1>Esporão Polar Norte</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 15:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>27/01/2019 15:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Loop I, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Esporão, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://heasarc.gsfc.nasa.gov/docs/rosat/gallery/misc_nps.html"><i>https://heasarc.gsfc.nasa.gov/docs/rosat/gallery/misc_nps.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><font face="Arial, Sans Serif" size="2"><b>Idade:</b> 13 milhões de anos</font></div><div><font face="Arial, Sans Serif" size="2"><b>Sigla:</b> NPS(North Polar Spur)</font></div><div><br/></div><span src="assets/img/espaco/misc_nps.jpg" type="image/jpeg" data-filename="misc_nps.jpg" style="cursor: default;cursor: default;cursor: default;"/></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1657"/></span>
<h1>Bolha Loop I</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>27/01/2019 02:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>27/01/2019 02:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha, Bolha Loop I, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="file:///"><i>file:///</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Extensão:</b> &gt; 300 anos-luz</div><div><b>Raio:</b> ? anos-luz</div><div><b>Densidade:</b> ? atoms/cm3</div><div><b>Prevalência:</b> Hidrogênio ionizado e gás neutro</div><div><b>Distancia:</b> 330 anos-luz</div><div><br/></div><div><br/></div><div><i>//REPRESENTAÇÃO EM DESENHO</i></div><div><i><span src="assets/img/espaco/Local_bubble.jpg" type="image/jpeg" data-filename="Local_bubble.jpg" style="cursor: default;"/></i></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1659"/></span>
<h1>Bolha Local</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>26/01/2019 19:13</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>27/01/2019 02:15</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha, Bolha Local, Complexo de Superaglomerados Pisces-Cetus, Laniakea, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.solstation.com/x-objects/chimney.htm"><i>http://www.solstation.com/x-objects/chimney.htm</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Extensão:</b> 300 anos-luz</div><div><b>Raio:</b> 150 anos-luz</div><div><b>Densidade:</b> 0.05 atoms/cm3</div><div><b>Prevalência:</b> Hidrogênio ionizado e gás neutro</div><div><br/></div><div><br/></div><div><i>//REPRESENTAÇÃO EM DESENHO</i></div><div><span src="assets/img/espaco/loc1bubb.jpg" type="image/jpeg" data-filename="loc1bubb.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1661"/></span>
<h1>Constelação de Órion</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 00:25</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>14/07/2019 11:58</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Órion, Equatorial, SGQ3</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Nome:</span> Órion, o Caçador</span></div><div><span style="font-weight: bold;">Genitivo:</span> Orionis</div><div><span style="font-weight: bold;">Abreviação:</span> Ori</div><div><span style="font-weight: bold;">Área Total:</span> 594º quadrados</div><div><span style="font-weight: bold;">Quadrante:</span> SGQ3</div><div style="text-align: left;"><span style="font-weight: bold;">Estrelas Principais:</span> 7(10)</div><div style="text-align: left;"><span style="font-weight: bold;">Estrelas Secundárias:</span> 81</div><div style="text-align: left;"><span style="font-weight: bold;">Meridiano:</span> Janeiro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/OrionCC.jpg" type="image/jpeg" data-filename="OrionCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/400px-Orion_IAU.svg.png" type="image/png" data-filename="400px-Orion_IAU.svg.png" style="cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/916e6aab3cce1c7f18c457d94fa7209a--constellations-hall.jpg" type="image/jpeg" data-filename="916e6aab3cce1c7f18c457d94fa7209a--constellations-hall.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1665"/></span>
<h1>δ Capricorni C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/07/2018 18:53</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>25/01/2019 21:00</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Capricórnio, Estrela, Grupo Local, Laniakea, Sistema Delta Capricorni, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.solstation.com/stars2/del-cap.htm"><i>http://www.solstation.com/stars2/del-cap.htm</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> ?</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Capricórnio</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div><b>Massa:</b> ?<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 38 anos luz</span></div><div><b style="color: rgb(34, 34, 34);">Magnitude Aparente:</b><span style="color: rgb(34, 34, 34);">  12,7</span></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b> <span style="color: rgb(34, 34, 34);">?</span></div><div><b>Composição: </b>?</div><div><b><br/></b></div><div><span src="assets/img/espaco/download [17].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1669"/></span>
<h1>δ Capricorni B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/07/2018 18:49</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/07/2018 18:53</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Capricórnio, Estrela, Grupo Local, Laniakea, Sistema Delta Capricorni, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.solstation.com/stars2/del-cap.htm"><i>http://www.solstation.com/stars2/del-cap.htm</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> ?</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Capricórnio</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div><b>Massa:</b> ?<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 38 anos luz</span></div><div><b style="color: rgb(34, 34, 34);">Magnitude Aparente:</b><span style="color: rgb(34, 34, 34);">  15,8</span></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> 2,48</span></div><div><b>Composição:</b> ?</div><div><b><br/></b></div><div><span src="assets/img/espaco/download [18].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1671"/></span>
<h1>δ Capricorni Ab</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/07/2018 14:38</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/07/2018 18:46</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Anã, Braço de Órion, Classificação Estelar G, Complexo de Superaglomerados Pisces-Cetus, Estrela, Grupo Local, Laniakea, Laranja, Sistema Delta Capricorni, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, δ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.solstation.com/stars2/del-cap.htm"><i>http://www.solstation.com/stars2/del-cap.htm</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> G</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Capricórnio</span></div><b>Raio equatorial:</b> 0.9 R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div><b>Massa:</b> 0,73<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> 4.500 K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 38 anos luz</span></div><div><b style="color: rgb(34, 34, 34);">Magnitude Aparente:</b><span style="color: rgb(34, 34, 34);">  ?</span></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> 2,48</span></div><div><b>Composição: </b>Hidrogênio e metais ionizados e metais neutros</div>
 
  </div>
<div><span src="assets/img/espaco/download [19].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;"/></div></span>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1673"/></span>
<h1>σ Scorpii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>13/07/2018 14:17</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>13/07/2018 14:17</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Azul, Braço de Órion, Cinturão de Gould, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Sigma Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, σ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alniyat-s.html"><i>http://stars.astro.illinois.edu/sow/alniyat-s.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Classificação: </b>B9</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Escorpião</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div><b>Massa:</b> ? M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 470 anos luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  ?(9)</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> -2,78</span></div><div><b>Composição:</b> Hidrogênio e Hélio</div><div><br/></div><div><span src="assets/img/espaco/Sh2-9 [2].jpg" type="image/jpeg" data-filename="Sh2-9.jpg" style="cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1675"/></span>
<h1>β Aquarii C</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 13:09</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>12/07/2018 14:19</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Aquário, Estrela, Grupo Local, Laniakea, Sistema Beta Aquarii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> ?</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Aquário</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div><b>Massa:</b> ?<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 540 anos luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  11,6</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> -3,04</span></div><div><b>Composição:</b> ?</div><div><br/></div><div><span src="assets/img/espaco/Estrella Acuario - Beta Aquarii [1].jpg" type="image/jpeg" data-filename="Estrella Acuario - Beta Aquarii.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1677"/></span>
<h1>β Aquarii B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 13:08</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>12/07/2018 14:18</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Aquário, Estrela, Grupo Local, Laniakea, Sistema Beta Aquarii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, β</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/sadalsuud"><i>https://www.universeguide.com/star/sadalsuud</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div style="text-align: left;"><b>Classificação:</b> ?</div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Aquário</span></div><b>Raio equatorial:</b> ? R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div><b>Massa:</b> ?<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> ? cgs</div><div><b>Temperatura:</b> ? K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 540 anos luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  11</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> -3,04</span></div><div><b>Composição:</b> ?</div><div><br/></div><div><span src="assets/img/espaco/Estrella Acuario - Beta Aquarii [2].jpg" type="image/jpeg" data-filename="Estrella Acuario - Beta Aquarii.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1679"/></span>
<h1>Shaula B</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>12/07/2018 12:58</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>12/07/2018 13:02</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Azul, Braço de Órion, Classificação Estelar B, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Estrela, Grupo Local, Laniakea, Sistema Lambda Scorpii, Subgigante, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, λ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.universeguide.com/star/shaula"><i>https://www.universeguide.com/star/shaula</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Classificação:</b> <span style="background-color: rgb(255, 255, 255); color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px;">B</span></div><div><span style="background-color: rgb(249, 249, 249); text-align: left;"><b>Constelação:</b> Escorpião</span></div><b>Raio equatorial:</b> 4,7 R<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub><div><b>Massa:</b> 9,6<sup style="box-sizing: border-box; line-height: 0; position: relative; vertical-align: baseline; top: -0.5em; color: rgb(58, 58, 58); text-align: center; font-size: 9px;"><font size="3" style="font-size: 11px;"> </font></sup>M<sub style="color: rgb(11, 0, 128); background-image: none; background-color: rgb(249, 249, 249); text-align: left; line-height: 1;"><a href="https://pt.wikipedia.org/wiki/Massa_solar" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none;" title="Massa solar">☉</a></sub></div><div><b>Gravidade da Superfície:</b> 4 cgs</div><div><b>Temperatura:</b> 25.000 K</div><div><b style="color: rgb(34, 34, 34);">Distância da Terra:</b><span style="color: rgb(34, 34, 34);"> 365 anos luz</span></div><div><font color="#222222"><b>Magnitude Aparente:</b>  12</font></div><div><b style="color: rgb(34, 34, 34);">Magnitude Absoluta:</b><span style="color: rgb(34, 34, 34);"> -3,70</span></div><div><b>Composição:</b> Hélio e Hidrogênio</div><div><br/></div><div><span src="assets/img/espaco/descarga [1].jpg" type="image/jpeg" data-filename="descarga.jpg" style="cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1681"/></span>
<h1>Sistema Próxima Centauri</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>10/07/2018 11:39</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>10/07/2018 11:49</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem G, Sistema Alpha Centauri, Sistema Planetário, Sistema Próxima Centauri, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Estrela:</b> Próxima Centauri
<div><b>Planetas Conhecidos:</b> 1</div><div><b>Distância do disco:</b> ~30 AU</div><div><br/></div><div><span src="assets/img/espaco/Proxima-Centauri.jpg" type="image/jpeg" data-filename="Proxima-Centauri.jpg"/></div><div><i><br/></i></div><div><i>//DISTÂNCIAS</i></div><div><span src="assets/img/espaco/screen-shot-2017-11-13-at-21-27-12-1024x737.png" type="image/png" data-filename="screen-shot-2017-11-13-at-21-27-12-1024x737.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><i><br/></i></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1684"/></span>
<h1>Constelação de Pégasus</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>10/07/2018 10:39</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>10/07/2018 10:51</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Pégasus, Hemisfério Norte, SGQ2</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Pégasus, o Cavalo Alado
<div><b>Genitivo:</b> Pegasi</div><div><b>Abreviação:</b> Peg</div><div><b>Área Total:</b> 1121º quadrados</div><div><b>Quadrante:</b> SGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 9(12)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 88</div><div style="text-align: left;"><b>Meridiano:</b> Outubro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/PegasusCC.jpg" type="image/jpeg" data-filename="PegasusCC.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/597px-Pegasus_IAU.svg.png" type="image/png" data-filename="597px-Pegasus_IAU.svg.png" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Pegasus_and_Equuleus_(best_currently_available_version_-_2014).jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Pegasus_and_Equuleus_(best_currently_available_version_-_2014).jpg"/><br/></div>
</div>
 
</span>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1688"/></span>
<h1>Constelação de Lyra</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 22:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>10/07/2018 10:44</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação da Lyra, Hemisfério Norte, NGQ1</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Lyra, a Lira
<div><b>Genitivo:</b> Lyrae</div><div><b>Abreviação:</b> Lyr</div><div><b>Área Total:</b> 286º quadrados</div><div><b>Quadrante:</b> NGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 5(6)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 25</div><div style="text-align: left;"><b>Meridiano:</b> Agosto</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/LyraCC.jpg" type="image/jpeg" data-filename="LyraCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/611px-Lyra_IAU.svg.png" type="image/png" data-filename="611px-Lyra_IAU.svg.png" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Lyra_Urania´s_Mirror.jpg" type="image/jpeg" data-filename="Lyra_Urania´s_Mirror.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1692"/></span>
<h1>Constelação do Boieiro</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 22:21</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>10/07/2018 10:43</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação do Boieiro, Hemisfério Norte, NGQ1</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Boötes, o Boieiro
<div><b>Genitivo:</b> Bootis</div><div><b>Abreviação:</b> Boo</div><div><b>Área Total:</b> 907º quadrados</div><div><b>Quadrante:</b> NGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 7(10)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 59</div><div style="text-align: left;"><b>Meridiano:</b> Junho</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/BootesCC.jpg" type="image/jpeg" data-filename="BootesCC.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/607px-Boötes_IAU.svg.png" type="image/png" data-filename="607px-Boötes_IAU.svg.png" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/470_quadrantids_main.jpg" type="image/jpeg" data-filename="470_quadrantids_main.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1696"/></span>
<h1>Constelação de Perseu</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 22:13</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>10/07/2018 10:43</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Perseu, Hemisfério Norte, SGQ2</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Perseus, o herói Perseu
<div><b>Genitivo:</b> Persei</div><div><b>Abreviação:</b> Per</div><div><b>Área Total:</b> 615º quadrados</div><div><b>Quadrante:</b> SGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 19</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 65</div><div style="text-align: left;"><b>Meridiano:</b> Outubro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/PerseusCC.jpg" type="image/jpeg" data-filename="PerseusCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/606px-Perseus_IAU.svg.png" type="image/png" data-filename="606px-Perseus_IAU.svg.png" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania06.jpg" type="image/jpeg" data-filename="urania06.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1700"/></span>
<h1>Constelação do Centauro</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>09/07/2018 21:40</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>09/07/2018 22:04</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação do Centauro, Hemisfério Sul, NGQ4</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Centaurus, o Centauro
<div><b>Genitivo:</b> Centauri</div><div><b>Abreviação:</b> Cen</div><div><b>Área Total:</b> 1060º quadrados</div><div><b>Quadrante:</b> NGQ4</div><div style="text-align: left;"><b>Estrelas Principais:</b> 11(12)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 69</div><div style="text-align: left;"><b>Meridiano:</b> Maio</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Constellation_Centaurus.jpg" type="image/jpeg" data-filename="Constellation_Centaurus.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/400px-Centaurus_IAU.svg.png" type="image/png" data-filename="400px-Centaurus_IAU.svg.png" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Centauro 2.jpg" type="image/jpeg" data-filename="Centauro 2.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1704"/></span>
<h1>Constelação de Escorpião</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>06/07/2018 23:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:48</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Escorpião, NGQ4, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Scorpius, o Escorpião
<div><b>Genitivo:</b> Scorpii</div><div><b>Abreviação:</b> Sco</div><div><b>Área Total:</b> 497º quadrados</div><div style="text-align: left;"><b>Quadrante:</b> <font face="sans-serif" size="2">NGQ4</font></div><div style="text-align: left;"><b>Estrelas Principais:</b> 18(14)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 47</div><div style="text-align: left;"><b>Meridiano:</b> Julho</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/scorpio_guisard_constellation_960.jpg" type="image/jpeg" data-filename="scorpio_guisard_constellation_960.jpg" height="638" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="960"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/sco_map [1].jpg" type="image/jpeg" data-filename="sco_map.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania23.jpg" type="image/jpeg" data-filename="urania23.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/527px-Antares_overlooking_an_Auxiliary_Telescope.jpg" type="image/jpeg" data-filename="527px-Antares_overlooking_an_Auxiliary_Telescope.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1709"/></span>
<h1>Constelação de Áries</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 20:58</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:48</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Áries, SGQ2, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Áries, o Carneiro
<div><b>Genitivo:</b> Arietis</div><div><b>Abreviação:</b> Ari</div><div><b>Área Total:</b> 441º quadrados</div><div><b>Quadrante:</b> SGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 5(4)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 67</div><div style="text-align: left;"><b>Meridiano: </b>Dezembro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/understanding-an-aries-woman-7.jpg" type="image/jpeg" data-filename="understanding-an-aries-woman-7.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Aries-constellation-mapb [1].gif" type="image/gif" data-filename="Aries-constellation-mapb.gif" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/aries2.jpg" type="image/jpeg" data-filename="aries2.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/vdb13stardust_falesiedi.jpg" type="image/jpeg" data-filename="vdb13stardust_falesiedi.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1714"/></span>
<h1>Constelação de Peixes</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 22:02</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:48</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Peixes, SGQ2, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Pisces, os Peixes
<div><b>Genitivo:</b> Piscium</div><div><b>Abreviação:</b> Psc</div><div><b>Área Total:</b> 506º quadrados</div><div><b>Quadrante:</b> SGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 18</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 86</div><div style="text-align: left;"><b>Meridiano: </b>Novembro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/th.jpg" type="image/jpeg" data-filename="th.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/1000px-Pisces_constellation_map.svg [1].png" type="image/png" data-filename="1000px-Pisces_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania27 (1).jpg" type="image/jpeg" data-filename="urania27 (1).jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1718"/></span>
<h1>Constelação de Leão</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 22:17</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:48</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Leão, NGQ3, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Leo, o Leão
<div><b>Genitivo:</b> Leonis</div><div><b>Abreviação:</b> Leo</div><div><b>Área Total:</b> 947º quadrados</div><div><b>Quadrante:</b> NGQ3</div><div style="text-align: left;"><b>Estrelas Principais:</b> 9(10)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 92</div><div style="text-align: left;"><b>Meridiano: </b>Abril</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/LeoCC.jpg" type="image/jpeg" data-filename="LeoCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/1024px-Leo_constellation_map [5].png" type="image/png" data-filename="1024px-Leo_constellation_map.png" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/leão urania.jpg" type="image/jpeg" data-filename="leão urania.jpg" style="cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/leo.jpg" type="image/jpeg" data-filename="leo.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1723"/></span>
<h1>Constelação de Sagitário</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 22:39</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:48</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, SGQ1, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Sagittarius, o Arqueiro
<div><b>Genitivo:</b> Sagittarii</div><div><b>Abreviação:</b> Sgr</div><div><b>Área Total:</b> 867º quadrados</div><div><b>Quadrante:</b> SGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 12</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 68</div><div style="text-align: left;"><b>Meridiano: </b>Agosto</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/476278cd4e402a84979a823157ce3411.jpg" type="image/jpeg" data-filename="476278cd4e402a84979a823157ce3411.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/1000px-Sagittarius_constellation_map.svg [2].png" type="image/png" data-filename="1000px-Sagittarius_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania24.jpg" type="image/jpeg" data-filename="urania24.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/estrelas-sagitario.jpg" type="image/jpeg" data-filename="estrelas-sagitario.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1728"/></span>
<h1>Constelação de Aquário</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 22:47</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Aquário, SGQ1, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Aquarius, o Aguareiro
<div><b>Genitivo:</b> Aquarii</div><div><b>Abreviação:</b> Aqr</div><div><b>Área Total:</b> 980º quadrados</div><div><b>Quadrante:</b> SGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 10(13)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 97</div><div style="text-align: left;"><b>Meridiano:</b> Outubro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/aquario_foto.jpg" type="image/jpeg" data-filename="aquario_foto.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/1000px-Aquarius_IAU.svg.png" type="image/png" data-filename="1000px-Aquarius_IAU.svg.png" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania26.jpg" type="image/jpeg" data-filename="urania26.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1732"/></span>
<h1>Constelação de Câncer</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 22:56</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Câncer, NGQ3, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Câncer, o Carangueijo
<div><b>Genitivo:</b> Cancri</div><div><b>Abreviação:</b> Cnc</div><div><b>Área Total:</b> 506º quadrados     </div><div><b>Quadrante:</b> NGQ3</div><div style="text-align: left;"><b>Estrelas Principais:</b> 5(6)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 76</div><div style="text-align: left;"><b>Meridiano:</b> Fevereiro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/a412ccdb8a4269f1613abf95aaa08854--cancer-constellation-zodiac.jpg" type="image/jpeg" data-filename="a412ccdb8a4269f1613abf95aaa08854--cancer-constellation-zodiac.jpg" style="cursor: default;cursor: default;"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/constellation_Cancer_large [1].png" type="image/png" data-filename="constellation_Cancer_large.png" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania19.jpg" type="image/jpeg" data-filename="urania19.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1736"/></span>
<h1>Constelação de Gêmeos</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 23:10</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Gêmeos, NGQ3, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Gemini, os Gêmeos
<div><b>Genitivo:</b> Geminorum</div><div><b>Abreviação:</b> Gem</div><div><b>Área Total:</b> 514º quadrados</div><div><b>Quadrante:</b> NGQ3</div><div style="text-align: left;"><b>Estrelas Principais:</b> 8(14)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 97</div><div style="text-align: left;"><b>Meridiano:</b> Fevereiro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/gemeos.jpg" type="image/jpeg" data-filename="gemeos.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/605px-Gemini_IAU.svg [1].png" type="image/png" data-filename="605px-Gemini_IAU.svg.png" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania18.jpg" type="image/jpeg" data-filename="urania18.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/2017121401283315132653132947818.jpg" type="image/jpeg" data-filename="2017121401283315132653132947818.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1741"/></span>
<h1>Constelação de Libra</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 23:20</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Libra, NGQ4, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Libra, a Balança
<div><b>Genitivo:</b> Librae</div><div><b>Abreviação:</b> Lib</div><div><b>Área Total:</b> 538º quadrados</div><div><b>Quadrante:</b> NGQ4</div><div style="text-align: left;"><b>Estrelas Principais:</b> 6</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 46</div><div style="text-align: left;"><b>Meridiano:</b> Junho</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/LibraCC.jpg" type="image/jpeg" data-filename="LibraCC.jpg" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Libra-constellation-mapb [1].gif" type="image/gif" data-filename="Libra-constellation-mapb.gif" style="cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Libra.jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Libra.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Alpha Librae.jpg" type="image/jpeg" data-filename="Alpha Librae.jpg" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1747"/></span>
<h1>Sistema Gamma Valerum</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>30/01/2019 10:58</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:15</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Vel OB 2, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação da Vela, Grupo Local, Laniakea, Sistema Estelar, Sistema Gamma Velorum, Sistema Múltiplo, Sistema Sétuplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.daviddarling.info/encyclopedia/G/Gamma_Velorum.html"><i>http://www.daviddarling.info/encyclopedia/G/Gamma_Velorum.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Regor, γ² Velorum WR, γ¹ Velorum Ba, γ¹ Velorum Bb, γ Velorum C, γ Velorum D e γ Velorum E</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Regor</div><div><span style="font-weight: bold;">Distância do disco:</span> pelo menos 15.000 UA(mas tende a ser muito mais)</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> </span>24.322<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância do Sol:</b> 336 anos-luz</span></div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">γ² Velorum WR</span><span style="font-weight: bold; color: rgb(34, 34, 34);">:</span><span style="color: rgb(34, 34, 34);"> 78.5 dias [1.6 UA]</span></div><div><span style="font-weight: bold;">Órbita γ¹ Velorum Ba/γ¹ Velorum Bb -&gt; Regor: </span><span style="background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(34, 34, 34); font-family: sans-serif;">1,483 dias</span></div><div><span style="background-color: rgb(255, 255, 255); font-size: 14px;"><br/></span></div><div><span style="background-color: rgb(255, 255, 255); font-size: 14px;"><br/></span></div><div><span src="assets/img/espaco/gammavel_airy [3].png" type="image/png" data-filename="gammavel_airy.png" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO DA ESTRELA</span></div><div><span src="assets/img/espaco/250px-Vela_constellation_map [12].png" type="image/png" data-filename="250px-Vela_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1750"/></span>
<h1>Sistema Sirius</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 16:27</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação do Cão Maior, Grupo Local, Laniakea, Sistema Binário de Estrelas, Sistema Estelar, Sistema Múltiplo, Sistema Sirius, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://universavvy.com/interesting-facts-about-the-sirius-star-you-may-not-know"><i>https://universavvy.com/interesting-facts-about-the-sirius-star-you-may-not-know</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Sirius A e Sirius B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Sirius A</div><div><span style="font-weight: bold;">Distância do disco:</span> 20 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> </span>24.142<span style="color: rgb(34, 34, 34);"> anos-luz</span></div><div><b>Distância do Sol:</b> 8,6 anos-luz</div><div><br/></div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Órbita Sirius B:</span> <span style="color: rgb(34, 34, 34);">50.1 anos</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/sirius-1.jpg" type="image/jpeg" data-filename="sirius-1.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//REPRESENTAÇÃO GRÁFICA</span></div><div><span src="assets/img/espaco/Sirius A en B.jpg" type="image/jpeg" data-filename="Sirius A en B.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DA ESTRELA NO MAPA</span></div><div><span src="assets/img/espaco/249px-Canis_Major_constellation_map.svg [2].png" type="image/png" data-filename="249px-Canis_Major_constellation_map.svg.png" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1754"/></span>
<h1>Sistema Beta Orionis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 14:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:12</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Órion, Grupo Local, Laniakea, Sistema Beta Orionis, Sistema Estelar, Sistema Múltiplo, Sistema Quádruplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/rigel.html"><i>http://stars.astro.illinois.edu/sow/rigel.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Rígel, β Orionis Ba, β Orionis Bb e β Orionis C</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Rígel</div><div><span style="font-weight: bold;">Distância do disco:</span> 2.200 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> </span><span style="color: rgb(34, 34, 34);">24.814 anos luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância do Sol:</b> 860 anos-luz</span></div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">β Orionis Ba/</span><span style="font-weight: bold; color: rgb(34, 34, 34);"> </span><span style="font-weight: bold;">β Orionis Bb -&gt; Rígel: </span> 18.000 anos [2.200 UA]</div><div><span style="font-weight: bold;">Órbita </span><span style="font-weight: bold;">β Orionis Ba/</span><span style="font-weight: bold; color: rgb(34, 34, 34);"> </span><span style="font-weight: bold;">β Orionis Bb:</span> 400 anos </div><div><span style="font-weight: bold;">Órbita β Orionis</span> <span style="color: rgb(34, 34, 34); font-weight: bold;">C:</span> <span style="color: rgb(34, 34, 34);">250.000 anos</span></div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/binary_starsystems_rigel [2].jpg" type="image/jpeg" data-filename="binary_starsystems_rigel.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span style="font-style: italic;">//LOCALIZAÇÃO NO MAPA</span></div><div><span src="assets/img/espaco/Orion_constellation_map [16].png" type="image/png" data-filename="Orion_constellation_map.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1757"/></span>
<h1>Sistema Delta Orionis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>29/01/2019 10:07</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:07</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Sistema Delta Orionis, Sistema Estelar, Sistema Múltiplo, Sistema Ternário de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.astronomytrek.com/star-facts-mintaka/"><i>http://www.astronomytrek.com/star-facts-mintaka/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Mintaka, δ Orionis Aa2, δ Orionis Ab</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Mintaka</div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> </span><span style="color: rgb(34, 34, 34);">25.303</span><span style="color: rgb(34, 34, 34);"> anos luz</span></div><div><b>Distância do Sol:</b> 1.200 anos-luz</div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">δ Orionis Aa2:</span> 5.7<span style="background-color: rgb(248, 249, 250); font-size: 12.32px; text-align: left; font-family: sans-serif;"> dias</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">δ</span><span style="font-weight: bold;"> Orionis Ab:</span> 350<span style="background-color: rgb(248, 249, 250); font-size: 12.32px; text-align: left; font-family: sans-serif;"> anos </span></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span style="font-style: italic; font-family: sans-serif; font-size: small;">//REPRESENTAÇÃO DO SISTEMA COM AS TRÊS ESTRELAS</span></div><div style="text-align: left;"><span src="assets/img/espaco/dori_ill.jpg" type="image/jpeg" data-filename="dori_ill.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div style="text-align: left;"><div style="text-align: -webkit-auto;"><br/></div><div style="text-align: -webkit-auto;"><div style="text-align: left;"><span style="font-family: sans-serif; font-style: italic;">//REPRESENTAÇÃO DO SISTEMA COM AS TRÊS ESTRELAS</span></div><div style="text-align: left;"><span style="font-style: italic; font-family: sans-serif; font-size: small;"><span src="assets/img/espaco/Figure-1Artists-impression-of-the-triple-system-d-Ori-A-as-viewed-from-Earth-The [2].png" type="image/png" data-filename="Figure-1Artists-impression-of-the-triple-system-d-Ori-A-as-viewed-from-Earth-The.png" style="cursor: default;cursor: default;"/></span></div></div><div style="text-align: -webkit-auto;"><br/></div><div style="text-align: -webkit-auto;"><span style="font-style: italic;">//LOCALIZAÇÃO DO SISTEMA</span></div><div style="text-align: -webkit-auto;"><span src="assets/img/espaco/250px-Orion_constellation_map.svg [3].png" type="image/png" data-filename="250px-Orion_constellation_map.svg.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1761"/></span>
<h1>Sistema Zeta Orionis</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>28/01/2019 14:14</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:04</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass Ori OB 1b, Braço de Órion, Cinturão de Gould, Cinturão de Órion(Três Marias), Complexo de Superaglomerados Pisces-Cetus, Complexo Nuvem Mol Ori, Constelação de Órion, Constelação de Òrion, Grupo Ass Ori OB 1, Grupo Local, Laniakea, Sistema Estelar, Sistema Múltiplo, Sistema Ternário de Estrelas, Sistema Zeta Orionis, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://www.solstation.com/x-objects/alnitak3.htm"><i>http://www.solstation.com/x-objects/alnitak3.htm</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Alnitak, ζ Orionis Ab e ζ Orionis B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Alnitak</div><div><span style="font-weight: bold;">Distância do disco:</span> 300 UA(Maybe)</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="color: rgb(34, 34, 34); font-weight: bold;">Distância do Centro Galático:</span><span style="color: rgb(34, 34, 34);"> 25.075 anos luz</span></div><div><b>Distância do Sol:</b> 1.260 anos-luz</div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">ζ Orionis Ab: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; text-align: left; font-family: sans-serif;">2687.3 dias</span></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">ζ Orionis B: </span><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; text-align: left; font-family: sans-serif;">1508.6 anos</span></div><div><span style="background-color: rgb(248, 249, 250); font-size: 12.32px; text-align: left;"><br/></span></div><div style="text-align: left;"><span style="font-family: sans-serif; font-style: italic;">//REPRESENTAÇÃO DO SISTEMA COM AS TRÊS ESTRELAS</span></div><div style="text-align: left;"><span src="assets/img/espaco/alnitak1 [1].jpg" type="image/jpeg" data-filename="alnitak1.jpg"/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><div style="text-align: -webkit-auto;"><span style="font-style: italic;">//LOCALIZAÇÃO DO SISTEMA</span></div><div style="text-align: -webkit-auto;"><span src="assets/img/espaco/Orion_constellation_map [17].png" type="image/png" data-filename="Orion_constellation_map.png"/></div></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1764"/></span>
<h1>Sistema Sigma Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 14:24</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 17:01</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Estelar, Sistema Múltiplo, Sistema Quádruplo de Estrelas, Sistema Sigma Scorpii, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alniyat-s.html"><i>http://stars.astro.illinois.edu/sow/alniyat-s.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Al Niyat(σ), σ Scorpii Aa2, σ Scorpii Ab, σ Scorpii B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Al Niyat(σ)</div><div><span style="font-weight: bold;">Distância do disco:</span> 4620 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.490 anos-luz</span></div><div><b>Distância do Sol:</b> 568 anos-luz</div><div><b><br/></b></div><div><b>Órbita </b><b>σ Scorpii Aa2:</b> 33 dias </div><div><b>Órbita </b><b>σ Scorpii Ab:</b> 100 anos [120 UA]</div><div><b>Órbita </b><b>σ Scorpii B:</b> ?</div><div><br/></div><div><b><br/></b></div><div><b><br/></b></div><div><span src="assets/img/espaco/Sh2-9 [3].jpg" type="image/jpeg" data-filename="Sh2-9.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [19].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1767"/></span>
<h1>Sistema Delta Capricorni</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 15:50</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:55</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Capricórnio, Grupo Local, Laniakea, Sistema Delta Capricorni, Sistema Estelar, Sistema Múltiplo, Sistema Quádruplo de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/denebalgedi.html"><i>http://stars.astro.illinois.edu/sow/denebalgedi.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Deneb Algedi, δ Capricorni Ab, δ Capricorni B e δ Capricorni C</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Deneb ALlgedi</div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> 10.227.683 dias</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> 75 km/s</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.113 anos-luz</span></div><div><b>Distância do Sol:</b> 38 anos-luz</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/download [20].jpg" type="image/jpeg" data-filename="download.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/CAP.gif" type="image/gif" data-filename="CAP.gif" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1770"/></span>
<h1>Sistema Beta Aquarii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 15:58</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:53</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Aquário, Grupo Local, Laniakea, Sistema Beta Aquarii, Sistema Estelar, Sistema Múltiplo, Sistema Ternário de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/sadalsuud.html"><i>http://stars.astro.illinois.edu/sow/sadalsuud.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Sadalsuud, β Aquarii B e β Aquarii C</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Sadalsuud</div><div><span style="font-weight: bold;">Distância do disco:</span> ?</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.858 anos-luz</span></div><div><b>Distância do Sol:</b> 540 anos-luz</div><div><br/></div><div><b>Órbita </b><b>β Aquarii C -&gt; </b><b>β Aquarii B:</b> ?</div><div><b>Órbita </b><b>β Aquarii B/</b><b>β Aquarii C -&gt; Sadalsuud:</b> ?</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Estrella Acuario - Beta Aquarii [3].jpg" type="image/jpeg" data-filename="Estrella Acuario - Beta Aquarii.jpg" style="cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/1000px-Aquarius_IAU.svg [1].png" type="image/png" data-filename="1000px-Aquarius_IAU.svg.png" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1773"/></span>
<h1>Sistema Lambda Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 13:16</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:50</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema Estelar, Sistema Lambda Scorpii, Sistema Múltiplo, Sistema Ternário de Estrelas, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea, λ</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/shaula.html"><i>http://stars.astro.illinois.edu/sow/shaula.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Shaula A, Shaula Ab e Shaula B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Shaula A</div><div><span style="font-weight: bold;">Distância do disco:</span> 5,9 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.581 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância do Sol: </b></span> 470 anos-luz</div><div><br/></div><div><b>Órbita Shaula Ab:</b> 5,95 dias [0,2 UA]</div><div><b>Órbita Shaula B:</b> 2,96 anos [5,7 UA]</div><div><span style="color: rgb(34, 34, 34);"><br/></span></div><div><br/></div><div><span src="assets/img/espaco/descarga [2].jpg" type="image/jpeg" data-filename="descarga.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map [2].jpg" type="image/jpeg" data-filename="sco_map.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1776"/></span>
<h1>Sistema Kappa Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 14:09</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:44</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema Binário de Estrelas, Sistema Estelar, Sistema Kappa Scorpii, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/kappasco.html"><i>http://stars.astro.illinois.edu/sow/kappasco.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Girtab e κ Scorpii B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Girtab</div><div><span style="font-weight: bold;">Distância do disco:</span> 1,7 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.659 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância da Terra:</b> 480 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><br/></span></div><div><span style="color: rgb(34, 34, 34);"><b>Órbita</b></span> <span style="color: rgb(34, 34, 34);"><b>κ </b></span><span style="color: rgb(34, 34, 34);"><b>Scorpii B:</b> 195 dias</span></div><div><br/></div><div><span src="assets/img/espaco/download [21].jpg" type="image/jpeg" data-filename="download.jpg"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO Do SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [20].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1779"/></span>
<h1>Sistema Mu¹ Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 14:51</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:39</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Local, Laniakea, Sistema Binário de Estrelas, Sistema Estelar, Sistema Mu¹ Scorpii, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/mu1sco.html"><i>http://stars.astro.illinois.edu/sow/mu1sco.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Xamidimura e μ¹ Scorpii B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Xamidimura</div><div><span style="font-weight: bold;">Distância do disco:</span> 0,007 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.663 anos-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b>Distância da Terra:</b> 500 anozs-luz</span></div><div><span style="color: rgb(34, 34, 34);"><b><br/></b></span></div><div><span style="color: rgb(34, 34, 34);"><b>Órbita </b></span><span style="color: rgb(34, 34, 34);"><b>μ¹ Scorpii B:</b> 34h</span></div><div><br/></div><div><span src="assets/img/espaco/Mu-Scorpii [2].png" type="image/png" data-filename="Mu-Scorpii.png" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div>//<span style="font-style: italic;">LOCALIZAÇÃO DO SISTEMA NO MAPA</span></div><div><span src="assets/img/espaco/sco_map - Copia [21].jpg" type="image/jpeg" data-filename="sco_map - Copia.jpg" style="cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1782"/></span>
<h1>Sistema Alpha Piscium</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 18:43</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:36</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Braço de Órion, Complexo de Superaglomerados Pisces-Cetus, Constelação de Peixes, Grupo Local, Laniakea, Sistema Alpha Piscium, Sistema Binário de Estrelas, Sistema Estelar, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/alrescha.html"><i>http://stars.astro.illinois.edu/sow/alrescha.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Alrescha e α Piscium B</span></div><div><span style="font-weight: bold;">Estrela Principal: </span>Alrescha</div><div><span style="font-weight: bold;">Distância do disco:</span> 190 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> 3.267 anos</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">24.214 anos-luz</span></div><div><b>Distância da Terra:</b> 311 anos-luz</div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="color: rgb(34, 34, 34);"><b>α</b></span><span style="font-weight: bold;"> Piscium B</span><span style="font-size: 14px; font-weight: bold; color: rgb(34, 34, 34); font-family: sans-serif;">:</span> 700 anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/alpha_psc [1].jpg" type="image/jpeg" data-filename="alpha_psc.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1784"/></span>
<h1>Sistema Delta Scorpii</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>11/07/2018 13:49</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>07/02/2019 16:30</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Ass US, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Constelação de Escorpião, Grupo Ass Sco-Cen OB 2, Grupo Local, Laniakea, Sistema Binário de Estrelas, Sistema Delta Scorpii, Sistema Estelar, Sistema Múltiplo, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="http://stars.astro.illinois.edu/sow/dschubba.html"><i>http://stars.astro.illinois.edu/sow/dschubba.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><span style="font-weight: bold;">Estrelas:</span> Dschubba e δ Scorpii B</span></div><div><span style="font-weight: bold;">Estrela Principal:</span> Dschubba</div><div><span style="font-weight: bold;">Distância do disco:</span> 0,4 UA</div><div><span style="font-weight: bold;">Período Orbital Galático:</span> ?</div><div><span style="font-weight: bold;">Velocidade Orbital Galática:</span> ?</div><div><span style="font-weight: bold;">Distância do Centro Galático: </span>~<span style="color: rgb(34, 34, 34); font-weight: bold;"> </span><span style="color: rgb(34, 34, 34);">23.777 anos-luz</span></div><div><b>Distância da Terra:</b> 400 anos-luz</div><div><br/></div><div><span style="font-weight: bold; color: rgb(34, 34, 34);">Órbita </span><span style="font-weight: bold;">δ Scorpii B</span><span style="font-size: 14px; font-weight: bold; color: rgb(34, 34, 34); font-family: sans-serif;">:</span> 10.5 anos</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Sh2-7 [1].jpg" type="image/jpeg" data-filename="Sh2-7.jpg" style="cursor: default;cursor: default;"/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1787"/></span>
<h1>Constelação de Touro</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 23:28</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Touro, SGQ2, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Tauros, o Touro
<div><b>Genitivo:</b> Tauri</div><div><b>Abreviação:</b> Tau</div><div><b>Área Total:</b> 797º quadrados</div><div><b>Quadrante:</b> SGQ2</div><div style="text-align: left;"><b>Estrelas Principais:</b> 19</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 132</div><div style="text-align: left;"><b>Meridiano:</b> Janeiro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/TaurusCC.jpg" type="image/jpeg" data-filename="TaurusCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/TAU [1].gif" type="image/gif" data-filename="TAU.gif" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/constellation_Taurus.jpg" type="image/jpeg" data-filename="constellation_Taurus.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Central_area_of_constellation_Taurus.jpg" type="image/jpeg" data-filename="Central_area_of_constellation_Taurus.jpg"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1792"/></span>
<h1>Constelação de Virgem</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 23:37</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 23:47</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Virgem, NGQ4, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Virgo, a Virgem
<div><b>Genitivo:</b> Virginis</div><div><b>Abreviação:</b> Vir</div><div><b>Área Total: </b>1924º quadrados</div><div><b>Quadrante:</b> NGQ4</div><div style="text-align: left;"><b>Estrelas Principais:</b> 9(11)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 96</div><div style="text-align: left;"><b>Meridiano:</b> Maio</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/VirgoCC.jpg" type="image/jpeg" data-filename="VirgoCC.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/VIR [3].gif" type="image/gif" data-filename="VIR.gif" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/Sidney_Hall_-_Urania's_Mirror_-_Virgo.jpg" type="image/jpeg" data-filename="Sidney_Hall_-_Urania's_Mirror_-_Virgo.jpg" style="cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1796"/></span>
<h1>Constelação de Capricórnio</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>08/07/2018 21:31</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>08/07/2018 22:25</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Constelação, Constelação de Capricórnio, SGQ1, Zodíaco</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html"><i>https://www.nasa.gov/audience/forstudents/k-4/dictionary/Constellation.html</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Nome:</b> Capricornus, o Capricórnio
<div><b>Genitivo:</b> Capricorni</div><div><b>Abreviação:</b> Cap</div><div><b>Área Total:</b> 867º quadrados</div><div><b>Quadrante:</b> SGQ1</div><div style="text-align: left;"><b>Estrelas Principais:</b> 23(9)</div><div style="text-align: left;"><b>Estrelas Secundárias:</b> 49</div><div style="text-align: left;"><b>Meridiano: </b>Setembro</div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/775104c092943edc60b78b925ff5a183.jpg" type="image/jpeg" data-filename="775104c092943edc60b78b925ff5a183.jpg" style="cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/CAP [1].gif" type="image/gif" data-filename="CAP.gif" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/urania25.jpg" type="image/jpeg" data-filename="urania25.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div><div style="text-align: left;"><br/></div><div style="text-align: left;"><span src="assets/img/espaco/M30-NRGBhi.jpg" type="image/jpeg" data-filename="M30-NRGBhi.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1801"/></span>
<h1>Sistema Solar</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 18:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>05/07/2018 10:40</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Estrela Única, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Estelar, Sistema Planetário, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/solar-system/our-solar-system/in-depth/"><i>https://solarsystem.nasa.gov/solar-system/our-solar-system/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Estrelas:</b> Sol
<div><b>Planetas Conhecidos:</b> 8</div><div><b>Distância do disco:</b> ~200.000 AU</div><div><br/></div><div><i>//REPRESENTAÇÃO ARTÍSTICAS E DISTÂNCIAS IRREAIS</i></div><div><span src="assets/img/espaco/545_solarsystem_0.jpg" type="image/jpeg" data-filename="545_solarsystem_0.jpg" height="1717" style="cursor: default;cursor: default;" width="3000"/></div><div><font color="#000000"><i><br/></i></font></div><div><font color="#000000"><i>//REPRESENTAÇÃO ARTÍSTICAS E DISTÂNCIAS IRREAIS</i></font></div><div><span src="assets/img/espaco/1366x768_d5e28cf08e48c21.jpg" type="image/jpeg" data-filename="1366x768_d5e28cf08e48c21.jpg" height="768" style="cursor: default;cursor: default;cursor: default;cursor: default;cursor: default;" width="1366"/><br/></div><div><br/></div><div><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1804"/></span>
<h1>Nuvem de Oort</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 16:06</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/07/2018 17:25</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Disco Circunstelar, Grupo Local, Laniakea, Nuvem Cometária, Nuvem de Oort, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/solar-system/oort-cloud/in-depth/"><i>https://solarsystem.nasa.gov/solar-system/oort-cloud/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><b>Ínicio:</b> ~ 1.000AU
<div><b>Fim:</b> ~ 100.000AU</div><div><b>Largura:</b> ~100.000AU</div><div><b>Composição:</b> Cometas, asteroides e possivelmente outros astros</div><div><b>Massa:</b> ?</div><div><br/></div><div><span src="assets/img/espaco/202.jpg" type="image/jpeg" data-filename="202.jpg" height="586" style="cursor: default;" width="1041"/><br/></div><div><br/></div><div>//<i>REPRESENTAÇÃO GRÁFICA DA NASA</i></div><div><span src="assets/img/espaco/491_OortCloud1000w.jpg" type="image/jpeg" data-filename="491_OortCloud1000w.jpg" height="1000" style="cursor: default;cursor: default;" width="1000"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1808"/></span>
<h1>Phaethon</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 14:17</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/07/2018 14:27</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Apollo(Tipo de Asteroide), Asteroide, Bolha Local, Braço de Órion, Cinturão de Gould, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Perigo para Terra, Phaethon, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Tipo B, Via Láctea</i></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><strong>Número:</strong> 3200</div><div><strong>Período de rotação:</strong> 3 horas e 36 min</div><div><strong>Período orbital:</strong> 1 ano e 5 meses</div><div><strong>Temperatura:</strong>  - ? ºC (-50)</div><div><strong>Distância do Sol:</strong> 118.000.000 Milhas - 1.27AU</div><div><strong>Diâmetro</strong><strong>:</strong>  5.8 km</div><div><strong>Gravidade:</strong> ? m/s² (extremamente fraca)</div><div><strong>Composição:</strong> Rocha(Minerais hidratados)</div><div><strong>Velocidade orbital:</strong>  19.9 km/s</div><div><br/></div><div><span src="assets/img/espaco/PIA22185.gif" type="image/gif" data-filename="PIA22185.gif" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div><div><br/></div><div><span src="assets/img/espaco/504px-Asteroid_3200_Phaethon_(1983_TB).gif" type="image/gif" data-filename="504px-Asteroid_3200_Phaethon_(1983_TB).gif" style="cursor: default;cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1812"/></span>
<h1>55P/Tempel-Tuttle</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 13:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/07/2018 14:08</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa 55P/Tempel-Tuttle, Cometa Periódico, Cometa Tipo Halley, Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=55p"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?sstr=55p</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.9 AU</div><div><b>Afélio:</b> 19.69 AU</div><div><b>Semi-eixo:</b> 960.000.000 - 10.33AU</div><div><b>Período de orbital:</b> ~ 33 anos e 2 meses</div><div><b>Diâmetro:</b> 3.6 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 28/02/1998</div><div><b>Próximo periélio:</b> 20 de Maio de 2031</div><div><br/></div><div><span src="assets/img/espaco/055p980217nao.jpg" type="image/jpeg" data-filename="055p980217nao.jpg" style="cursor: default;cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1814"/></span>
<h1>96P/Machholz 1</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 12:45</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/07/2018 12:51</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa Periódico, Complexo de Superaglomerados Pisces-Cetus, Família Júpiter, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?ID=c00096_0"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?ID=c00096_0</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 0.12 AU</div><div><b>Afélio:</b> 5.94 AU</div><div><b>Semi-eixo:</b> 278.000.000 - 3AU</div><div><b>Período de orbital:</b> ~ 5 anos e 3 meses</div><div><b>Diâmetro:</b> 6.4 km</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 27/10/2017</div><div><b>Próximo periélio:</b> 31 de Janeiro de 2023</div><div><br/></div><div><span src="assets/img/espaco/275px-96P_20070403_000500_HI1A.png" type="image/png" data-filename="275px-96P_20070403_000500_HI1A.png" style="cursor: default;"/><br/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1816"/></span>
<h1>C/2004 Q2 (Machholz)</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>04/07/2018 11:02</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>04/07/2018 11:16</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Braço de Órion, Cinturão de Gould, Cometa, Cometa C/2004(Machholz), Complexo de Superaglomerados Pisces-Cetus, Grupo Local, Laniakea, Nuvem Interestelar Local, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://ssd.jpl.nasa.gov/sbdb.cgi?ID=dK04Q020"><i>https://ssd.jpl.nasa.gov/sbdb.cgi?ID=dK04Q020</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;"><div><b>Periélio:</b> 1.205 AU</div><div><b>Afélio:</b> 1.070 AU</div><div><b>Semi-eixo:</b> 537 AU</div><div><b>Período de orbital:</b> ~ 12.400 anos</div><div><b>Diâmetro:</b> ?</div><div><b>Velocidade média:</b> ? km/h</div><div><b>Último periélio:</b> 24/01/2005</div><div><b>Próximo periélio:</b> 14.418</div><br/><div><span src="assets/img/espaco/250px-Machholz-cutout.jpg" type="image/jpeg" data-filename="250px-Machholz-cutout.jpg" style="cursor: default;cursor: default;cursor: default;cursor: default;"/></div></span>
</div>
</div>
<hr><div class="div-item">
<span class="titulo-item" name="1830"/></span>
<h1>Thebe</h1>
<div>
<table bgcolor="#D4DDE5" border="0">
<tr><td><b>Criada em:</b></td><td><i>26/07/2019 20:59</i></td></tr>
<tr><td><b>Atualizada em:</b></td><td><i>26/07/2019 21:25</i></td></tr>
<tr><td><b>Autor:</b></td><td><i>Maick</i></td></tr>
<tr><td><b>Etiquetas:</b></td><td><i>Bolha Local, Cinturão de Gould, Grupo Local, Júpiter, Laniakea, Lua(Satélite), Nuvem Interestelar Local, Regular, Sistema Solar, Subgrupo Via Láctea, Superaglomerado de Virgem, Via Láctea</i></td></tr>
<tr><td><b>Origem:</b></td><td><a href="https://solarsystem.nasa.gov/moons/jupiter-moons/thebe/in-depth/"><i>https://solarsystem.nasa.gov/moons/jupiter-moons/thebe/in-depth/</i></a></td></tr>
</table>
</div>
<br/>

<div class="prop-item">
<span><div><span style="font-weight: bold;">Período de rotação:</span> 16h 11 min</div><div><span style="font-weight: bold;">Período de órbita do planeta:</span> 16h 11 min</div><div><span style="font-weight: bold;">Temperatura média:</span> -149 ºC</div><div><span style="font-weight: bold;">Planeta:</span> Júpiter</div><div><span style="font-weight: bold;">Distância do Planeta:</span> 221.900 km</div><div><span style="font-weight: bold;">Peso:</span> 0,4 da terra</div><div><span style="font-weight: bold;">Gravidade:</span> 0,041 m/s²</div><div><span style="font-weight: bold;">Diâmetro equatorial:</span> 309,8 km</div><div><span style="font-weight: bold;">Atmosfera: </span>sem atmosfera</div><div><span style="font-weight: bold;">Vel. p/ orb. o planeta:</span> 86.057 km/h</div><div><span style="font-weight: bold;">Área de superfície:</span> 30.542 km²</div><div><br/></div><div><span style="font-weight: bold;">/// DIFICULDADES ///</span></div><div>&gt; Muito pouca gravidade(Atrofiação dos músculos)</div><div>&gt; Muita radiação que emana de Júpiter</div><div>&gt; Muito frio</div><div>&gt; Sem oxigênio</div><div>&gt; Exposto a ventos solares</div><div><br/></div><div>----- <span style="font-weight: bold;">Curiosidades -----</span></div><div>&gt; Período de rotação é o mesmo de órbita o que significa que para a Júpiter sempre está a mesma face</div><div>&gt; Possui um anel composto por seu nome, pois é considerada uma lua pastora que ejeta detritos e poeira que alimentam o anel próximo a ela</div><div><br/></div><div><br/></div><div><span src="assets/img/espaco/Thebe [1].jpg" type="image/jpeg" data-filename="Thebe.jpg"/></div><div><br/></div></span>
</div></body></html>
</div>