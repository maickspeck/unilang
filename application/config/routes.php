<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['save/(:any)'] = 'dados/pagesave/$1';
$route['save/(:any)/(:num)'] = 'dados/pagesave/$1/$2';

$route['db/save'] = 'dados/save/';

$route['novotipo'] = 'tipos/novotipo';
$route['editatipo'] = 'tipos/editatipo';

$route['restringir'] = 'dados/getValoresRestringidos';
$route['campo'] = 'dados/campo';

$route['robo'] = 'robo/api';

